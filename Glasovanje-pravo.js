//var naslov = "A8-0245/2015 - Ska Keller - Am 44/1";
var url = "/podatki/";


$(document).ready(function(){
  $("#izbiraAkta").change(function() {
    var podIzbira = document.querySelector("#podizbiraDiv");
    						
    						//Če nalaga vsa iskanja
						    if (document.getElementById("naloziVse").checked) {
						    	podIzbira.innerHTML = '<br><i class="fa fa-spinner fa-spin"></i> Nalaganje podatkov ...';
						    	if ($("#izbiraAkta").val() == 100) {
						    		podIzbira.innerHTML = '';
						    		return;
						    	}
						    	$.getJSON(url + $('#izbiraAkta').val() + ".json", function(jsonSample) {
						    		for (var j = 0; j < jsonSample.RollCallVoteResult.length; j++) {
						    			for (var l = 0; l < seznam.length; l++) {
						    				if (seznam[l].id == $('#izbiraAkta').val()) {
						    					if (seznam[l].glasovanja.indexOf(jsonSample.RollCallVoteResult[j].RollCallVoteDescriptionText) === -1) {
						    						seznam[l].glasovanja.push(jsonSample.RollCallVoteResult[j].RollCallVoteDescriptionText);
						    						seznam[l].imena_glasovanj.push(jsonSample.RollCallVoteResult[j].RollCallVoteDescriptionText);
						    					}
						    					//console.log(seznam[l].glasovanja);
						    					//console.log(seznam[l].imena_glasovanj);
						    				}
						    			}
						    		}
								    var podizbiraString = "";
		                for (var k = 0; k < seznam.length; k++) {
		                  if (seznam[k].id == $('#izbiraAkta').val()) {
		                    for (var n = 0; n < seznam[k].glasovanja.length; n++) {
		                      podizbiraString = podizbiraString + "<option value=" + n + ">" + seznam[k].imena_glasovanj[n] + "</option>";
		                    }
		                  }
		                }
			            podIzbira.innerHTML = '<br/><select id="podizbira" class="form-control">' + podizbiraString + '</select>';
	                if ($('#izbiraAkta').val() == 100) {
	                podIzbira.innerHTML = '';
	                }
						    		
						    	});
						    }
						    else {
	                var podizbiraString = "";
	                for (var k = 0; k < seznam.length; k++) {
	                  if (seznam[k].id == $('#izbiraAkta').val()) {
	                    for (var n = 0; n < seznam[k].glasovanja.length; n++) {
	                      podizbiraString = podizbiraString + "<option value=" + n + ">" + seznam[k].imena_glasovanj[n] + "</option>";
	                    }
	                  }
	                }
	                
	                
	                podIzbira.innerHTML = '<br/><select id="podizbira" class="form-control">' + podizbiraString + '</select>';
	                if ($('#izbiraAkta').val() == 100) {
	                	podIzbira.innerHTML = '';
	                }
						    }
  });
  
  $("#prikaziPoslance").click(function() {
  	var prikaz = document.querySelector("#izracunSporocilo");
  	prikaz.innerHTML = '';
  	vnos.innerHTML = '';
  	vnosStranke.innerHTML = '';
    vnosDrzave.innerHTML = '';
    vnosSkupine.innerHTML = '';
  	var poslanciTabString = "<br><table class='table table-bordered'><thead><tr><th>Država</th><th>Skupina</th><th>Stranka</th><th>Priimek</th><th>ID</th></tr></thead><tbody>";
  	for (var c = 0; c < stranke.length; c++) {
  		for (var d = 0; d < poslanci.length; d++) {
  			if (stranke[c].id == poslanci[d].StrankaId) {
  				poslanciTabString = poslanciTabString + "<tr><td>" + stranke[c].država + "</td><td>" + stranke[c].skupina + "</td><td>" + stranke[c].ime + "</td><td>" + poslanci[d].Priimek + "</td><td>" + poslanci[d].Id + "</td></tr>";
  			}
  		}
  	}
  	prikaz.innerHTML = poslanciTabString;
  	
  });
  
  $("#pomoc").click(function() {
  	var prikaz = document.querySelector("#izracunSporocilo");
  	prikaz.innerHTML = '';
  	vnos.innerHTML = '';
  	vnosStranke.innerHTML = '';
    vnosDrzave.innerHTML = '';
    vnosSkupine.innerHTML = '';
    var infoString = "<br><br><strong>Aplikacija izračuna izbrano glasovanje za stranke/koalicije, kakor so nastopile na Evropskih volitvah leta 2014. </strong>"
    + "<br>Seznam vseh aktov in imena glasovanj v aktih - kakor jih lahko izbirate z menija - ter njihove podrobnosti se nahajajo v datoteki <strong>'Akti.docx'</strong>. " 
    + "Z menija lahko izbirate bodisi glasovanja, uporabljena v raziskavi, ki so poimenovana s šiframi, bodisi vsa glasovanja v eni datoteki (izberete možnost <strong>'naloži vsa glasovanja'</strong>). "
    + "Z gumbom <strong>'Zamenjaj manjkajoče'</strong> zamenjate glasovanja manjkajočih strank z enim izmed ponujenih algoritmov. "
    + "<br><br>Glasovanja so shranjena v datotekah tipa <strong>.JSON</strong> v mapi 'podatki'. Če želite dodati nova glasovanja, je potrebno datoteke z glasovanji Roll-Call votes v obliki (.xml) pridobiti z uradne spletne strani Evropskega parlamenta. "
    + "Nato te datoteke <strong>s pomočjo konverterja (v mapi 'konverter')</strong> pretvorite v obliko .JSON, jih shranite v mapo 'podatki', ter v datoteki <strong>'Seznam.js'</strong> in v meniju na 'index.html' dodate njihov naslov in imena. " 
    + "V datoteki <strong>'Stranke.js'</strong> se nahaja seznam strank in skupin, v datoteki <strong>'Poslanci.js'</strong> pa seznam poslancev v povezavi z njihovimi pripadajočimi Id-ji in strankami. "
    + "<br><br><strong>Avtor aplikacije: Miha Nahtigal. Cerknica, 2017. Copyright&#169; SVS 2017. <br></strong>Dovoljena uporaba, kopiranje in redistribucija ob navedbi avtorja aplikacije.</storng>";
    
    prikaz.innerHTML = infoString;
  });
  
  //Naloži vse, če se je izbira že izvršila resetira na prazno
  $("#naloziVse").change(function() {
  	var podIzbira = document.querySelector("#podizbiraDiv");
  	podIzbira.innerHTML = '';
  	$('#izbiraAkta').val("100");
  });
  
  //Če še ni "checked"
  $("#zamenjaj").prop("checked", false).change(function() {
  	    var izbiraMenjaveDiv = document.querySelector("#izbiraMenjaveDiv");
        izbiraMenjaveDiv.innerHTML = '<select id="izbiraMenjave" class="form-control"><option value="0">Mediana</option><option value="1">Mediana (>33%)</option><option value="2">Povprečje</option></select>';
        if (document.getElementById("zamenjaj").checked == false) {
        	izbiraMenjaveDiv.innerHTML = '';
        }
  });
  
  $("#izracunaj").click(function() {
  	if ($('#izbiraAkta').val() == 100) {
  		alert("Prazen vnos!\nProsim izberite glasovanje z menija ali ga vnesite ročno.");
  		//stavek return prekine funkcijo
  		return;
  	}
  	
  	//loading
  	var $this = $(this);
  	$this.button('loading');
  	
  	//vnos
    var vnos = document.getElementById("vnos");
    var vnosStranke = document.getElementById("vnosStranke");
    var vnosDrzave = document.getElementById("vnosDrzave");
    var vnosSkupine = document.getElementById("vnosSkupine");
    var vnosSkup = document.getElementById("izracunSporocilo");
      vnos.innerHTML = '';
      vnosStranke.innerHTML = '';
      vnosDrzave.innerHTML = '';
      vnosSkupine.innerHTML = '';
      vnosSkup.innerHTML = '';
      
      //pridobi JASON
      $.getJSON(url + $('#izbiraAkta').val() + ".json", function(jsonSample) {
        var aktIndeks = $('#izbiraAkta').val();
        var naslov = $('#dodajAkt').val();
        var glasovanjeIndeks = $('#podizbira').val();
        if (naslov == '') {
        	//console.log(aktIndeks);
        	//console.log(glasovanjeIndeks);
        	//console.log(naslov);
        	var aktIndeksTab = 0;
        	for (var q = 0; q < seznam.length; q++) {
        		if (seznam[q].id == aktIndeks) {
        			aktIndeksTab = q;
        		}
        	}
        	naslov = seznam[aktIndeksTab].glasovanja[glasovanjeIndeks];
        }
        
        //Obrni
        var obrni = false;
        if (document.getElementById("obrni").checked) {
          obrni = true;
        }
        //console.log(obrni);
        	console.log(naslov);
        	
        	//Pridobitev glasovanj in njihov prepis k poslancem (po metodi povprečja)
            for (i = 0; i < jsonSample.RollCallVoteResult.length; i++) {
        	   //console.log(jsonSample.RollCallVoteResult[i].RollCallVoteDescriptionText);
        		if (jsonSample.RollCallVoteResult[i].RollCallVoteDescriptionText == naslov) {
        		   //console.log("ja");
        		   if (jsonSample.RollCallVoteResult[i].ResultFor != null) {
	        		  //za
	        			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList.length; j++)  {
	        				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
	        				   //console.log(jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId);
	        				   
	        					for (m = 0; m < poslanci.length; m++) {
	        					   //console.log("poslanci" + poslanci[m].Id);
	        						if (jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId == poslanci[m].Id) {
	        						   //console.log("JA");
	        							poslanci[m].glas = 1;
	        						}
	        					}
	        				}
	        			}
        		   }

        			
        			if (jsonSample.RollCallVoteResult[i].ResultAgainst != null) {
        				//proti
	        			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList.length; j++)  {
	        				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
	        					for (m = 0; m < poslanci.length; m++) {
	        						if (jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId == poslanci[m].Id) {
	        						   //console.log("NE");
	        							poslanci[m].glas = -1;
	        						}
	        					}
	        				}
	        			}
        			}

        			if (jsonSample.RollCallVoteResult[i].ResultAbstention != null) {
	        			//neodločeno
	        			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList.length; j++)  {
	        				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
	        					for (m = 0; m < poslanci.length; m++) {
	        						if (jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId == poslanci[m].Id) {
	        						   //console.log("Neodločeno");
	        							poslanci[m].glas = 0;
	        						}
	        					}
	        				}
	        			}
        			}

        		}
        	}
        	
        	//Izračun glasovanja strank
        	for (s = 0; s < stranke.length; s++) {
        		for (p = 0; p < poslanci.length; p++) {
        		  //console.log(poslanci[p].glas);
        			if (stranke[s].id == poslanci[p].StrankaId) {
        			   //console.log("Stranke");
        				if ((stranke[s].glasovanje == null) && (poslanci[p].glas != null)) {
        					stranke[s].glasovanje = 0;
        				}
        				if (stranke[s].glasovanje != null) {
        				  stranke[s].glasovanje = stranke[s].glasovanje + poslanci[p].glas;
        				}
        				//Zapis glasovanja skupin
        				for (n = 0; n < skupine.length; n++) {
        					if (skupine[n].Skupina == stranke[s].skupina) {
        						if (poslanci[p].glas == 1) {
        							skupine[n].ZA = skupine[n].ZA + 1;
        							skupine[n].Vsi = skupine[n].Vsi + 1;
        							//console.log("Zala" + skupine[n].ZA);
        						}
        						else if (poslanci[p].glas == -1) {
        							skupine[n].PROTI = skupine[n].PROTI + 1;
        							skupine[n].Vsi = skupine[n].Vsi + 1;
        						}
        						else if (poslanci[p].glas == 0) {
        							skupine[n].Neodloceno = skupine[n].Neodloceno + 1;
        							skupine[n].Vsi = skupine[n].Vsi + 1;
        						}
        						else if (poslanci[p].glas == null) {
        							skupine[n].Manjkajoci = skupine[n].Manjkajoci + 1;
        						}
        					}
        				}
        			}
        		}
        	}
        	//console.log("poslanci" + poslanci.length);
        	//------------------------------------------Tabela skupin
        	var TabelaString = "";
        	var max = 0;
        	TabelaString = "<br><table class='table table-bordered'><thead><tr><th>Skupina</th><th>ZA</th><th>PROTI</th><th>Neodločeni</th><th>Manjkajoči</th><th>Vsi</th><th>Kohezija<sup>1</sup></th><th>Kohezija VW<sup>2</sup></th><th>Kohezija Rice<sup>3</sup></th><th>Glasovanje (mediana)<sup>4</sup></th><th>Glasovanje (>33%)<sup>5</sup></th><th>Glasovanje (povpr.)<sup>6</sup></th></tr></thead><tbody>";
        	
        	for (y = 0; y < skupine.length; y++) {
        		
        		max = Math.max(skupine[y].ZA, skupine[y].PROTI, skupine[y].Neodloceno);
        		
        		var kohezijaSkupine = 0;
        		var kohezijaSkupineVW = 0;
        		var kohezijaRice = 0;
        		var glasSkupine = "0";
        		var glasSkupinePovpr = "0";
        		
        		//Kohezije in glasovi - za, proti, neodločeno
        		if (max == skupine[y].ZA) {
        			kohezijaSkupine = (skupine[y].ZA - skupine[y].PROTI)/(skupine[y].ZA + skupine[y].PROTI) * 100;
        			kohezijaSkupineVW = (skupine[y].ZA - (skupine[y].PROTI/2) - (skupine[y].Neodloceno/2))/skupine[y].Vsi * 100;
        			kohezijaRice = (Math.abs(skupine[y].ZA - skupine[y].PROTI - (skupine[y].Neodloceno/2)))/(skupine[y].ZA + skupine[y].PROTI + (skupine[y].Neodloceno/2)) * 100;
        			glasSkupine = "1";
        		}
        		else if (max == skupine[y].PROTI) {
        			kohezijaSkupine = (skupine[y].PROTI - skupine[y].ZA)/(skupine[y].ZA + skupine[y].PROTI) * 100;
        			kohezijaSkupineVW = (skupine[y].PROTI - (skupine[y].ZA/2) - (skupine[y].Neodloceno/2))/skupine[y].Vsi * 100;
        			kohezijaRice = (Math.abs(skupine[y].PROTI - skupine[y].ZA - (skupine[y].Neodloceno/2)))/(skupine[y].ZA + skupine[y].PROTI + (skupine[y].Neodloceno/2)) * 100;
        			glasSkupine = "0";
        		}
        		else if (max == skupine[y].Neodloceno) {
        			kohezijaSkupine = (skupine[y].Neodloceno - (skupine[y].ZA/2) - (skupine[y].PROTI/2))/skupine[y].Vsi * 100;
        			kohezijaSkupineVW = (skupine[y].Neodloceno - (skupine[y].PROTI/2) - (skupine[y].ZA/2))/skupine[y].Vsi * 100;
        			kohezijaRice = (Math.abs(skupine[y].Neodloceno - (skupine[y].ZA/2) - (skupine[y].PROTI/2)))/(skupine[y].Neodloceno + (skupine[y].ZA/2) + (skupine[y].PROTI/2)) * 100;
        			glasSkupine = "0,5";
        		}
        		
        		//Glasovi povprečje
        		if (skupine[y].ZA == skupine[y].PROTI) {
        			glasSkupine = "0,5";
        			glasSkupinePovpr = "0,5";
        		}
        		else if (skupine[y].ZA > skupine[y].PROTI) {
        			glasSkupinePovpr = "1";
        		}
        		else if (skupine[y].PROTI > skupine[y].ZA) {
        			glasSkupinePovpr = "0";
        		}
        		
        		if (obrni == true) {
        			if (glasSkupine == "1") glasSkupine = "0";
        			else if (glasSkupine == "0") glasSkupine = "1";
        			if (glasSkupinePovpr == "1") glasSkupinePovpr = "0";
        			else if (glasSkupinePovpr == "0") glasSkupinePovpr = "1";
        		}
        		
        		if (kohezijaSkupine < 33.333) {
        			skupine[y].Glasovanje10pp = "0,5";
        			//console.log("KOHEZIJA" + skupine[y].Glasovanje10pp);
        		}
        		else if (kohezijaSkupine >= 33.333) {
        			skupine[y].Glasovanje10pp = glasSkupine;
        		}
        		
        		skupine[y].Kohezija = Math.round(kohezijaSkupine) + "%";
        		skupine[y].KohezijaVW = Math.round(kohezijaSkupineVW) + "%";
        		skupine[y].KohezijaRice = Math.round(kohezijaRice) + "%";
        		skupine[y].Glasovanje = glasSkupine;
        		skupine[y].GlasovanjePovpr = glasSkupinePovpr;
        		//console.log("GLAS" + skupine[y].Glasovanje10pp)
        		
        		TabelaString = TabelaString + "<tr><td>" + skupine[y].Skupina + "</td><td>" + skupine[y].ZA + "</td><td>" + skupine[y].PROTI + "</td><td>" + skupine[y].Neodloceno + "</td><td>" + skupine[y].Manjkajoci + "</td><td>" + skupine[y].Vsi + "</td><td>" + skupine[y].Kohezija + "</td><td>" + skupine[y].KohezijaVW + "</td><td>" + skupine[y].KohezijaRice + "</td><td>" + skupine[y].Glasovanje + "</td><td>" + skupine[y].Glasovanje10pp + "</td><td>" + skupine[y].GlasovanjePovpr + "</td></tr>";
        		//console.log(max);
        	}
        	var str1 = "<sup>1</sup><strong><i>Kohezija(%) = (X  - Y) / N * 100</i></strong>,  X = najbolj pogosto glasovanje, Y = nasprotno glasovanje. V primeru X = neodločeno, velja formula KohezijaVW.";
        	var str2 = "<br><sup>2</sup><strong><i>Kohezija-VoteWatch(%) = (X – ½ Y<sub>1</sub> – ½ Y<sub>2</sub>) / N * 100</i></strong>,  X = najbolj pogosto glasovanje, Y<sub>1</sub>, Y<sub>2</sub> = druga glasovanja.";
        	var str3 = "<br><sup>3</sup><strong><i>Kohezija-Rice(%) = (|X – Y - ½ Z|) / (X + Y + ½ Z)</i></strong>,  X = najbolj pogosto glasovanje, Y = nasprotno glasovanje, Z = preostalo glasovanje. V primeru X = neodločeno je tudi Y prepolovljen.";
        	var str4 = "<br><sup>4</sup>Za privzeto glasovanje skupine se upošteva mediana glasovanja poslancev";
        	var str5 = "<br><sup>5</sup>Za privzeto glasovanje skupine se upošteva mediana, v primeru Kohezija <= 33% (razmerje 33:66 ali manj) pa je glasovanje neodločeno.";
        	var str6 = "<br><sup>4</sup>Za privzeto glasovanje skupine se upošteva povprečje glasovanja poslancev";
        	
        	vnosSkup.innerHTML = TabelaString + "</tbody></table><div align='left'><small>" + str1 + str2 + str3 + str4 + str5 + str6 + "</small></div>";
        	//-----------------------------Zaključek tabele
        	
        	//Zapisovanje glasovanja strankam
        	for (v = 0; v < stranke.length; v++) {
        		if (stranke[v].glasovanje > 0) {
        		  if (obrni == false) {
        		    stranke[v].glasovanje = "1";
        		  }
        		  else if (obrni == true) {
        		    stranke[v].glasovanje = "0";
        		  }
        			
        		}
        		else if (stranke[v].glasovanje < 0) {
        		  if (obrni == false) {
        		    stranke[v].glasovanje = "0";
        		  }
        		  else if (obrni == true) {
        		    stranke[v].glasovanje = "1";
        		  }
        			
        		}
        		else if (stranke[v].glasovanje == 0) {
        			stranke[v].glasovanje = "0,5";
        		}
        		else if (stranke[v].glasovanje == null) {
        			if (document.getElementById("zamenjaj").checked) {
        					for (var w = 0; w < skupine.length; w++) {
        						//Menjaj po mediani
        						var menjava = $('#izbiraMenjave').val();
        						if ((skupine[w].Skupina == stranke[v].skupina) && (menjava == 0)) {
        							stranke[v].glasovanje = skupine[w].Glasovanje;
        						}
        						//Menjaj po mediani >10%
        						else if ((skupine[w].Skupina == stranke[v].skupina) && (menjava == 1)) {
        							stranke[v].glasovanje = skupine[w].Glasovanje10pp;
        						}
        						//Menjaj po povprečju
        						else if ((skupine[w].Skupina == stranke[v].skupina) && (menjava == 2)) {
        							stranke[v].glasovanje = skupine[w].GlasovanjePovpr;
        						}
        					}
        			}
        			else if (document.getElementById("zamenjaj").checked == false) {
        				stranke[v].glasovanje = "-1";
        			}
        		}
        		
        		//Izpis glasovanja po strankah
        		vnos.innerHTML = vnos.innerHTML + "<br>" + stranke[v].glasovanje;
        		vnosStranke.innerHTML = vnosStranke.innerHTML + "<br>" + stranke[v].ime;
        		vnosSkupine.innerHTML = vnosSkupine.innerHTML + "<br>" + stranke[v].skupina;
        		vnosDrzave.innerHTML = vnosDrzave.innerHTML + "<br>" + stranke[v].država;
        		//console.log(stranke[v].glasovanje);
        	}
        	
        	//Resetiranje vrednosti za stranke, poslance in skupine
        	for (s = 0; s < stranke.length; s++) {
        		stranke[s].glasovanje = null;
        	}
        	for (p = 0; p < poslanci.length; p++) {
        	  poslanci[p].glas = null;
        	}
        	
        	for (o = 0; o < skupine.length; o++) {
        		skupine[o].Vsi = 0;
        		skupine[o].ZA = 0;
        		skupine[o].PROTI = 0;
        		skupine[o].Neodloceno = 0;
        		skupine[o].Manjkajoci = 0;
        	}
        	$this.button('reset');
      });
  });
});

/*
function printajPodatke() {
   	for (i = 0; i < jsonSample.RollCallVoteResult.length; i++) {
	   console.log(jsonSample.RollCallVoteResult[i].RollCallVoteDescriptionText);
		if (jsonSample.RollCallVoteResult[i].RollCallVoteDescriptionText == naslov) {
		   console.log("ja");
			//za
			var vnos = document.getElementById("vnos");
			
			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList.length; j++)  {
				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
				   console.log(jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId);
				   console.log(jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].text);
				   vnos.innerHTML = vnos.innerHTML + jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].text + " " + jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId + "<br>";
				}
			}
			
			//proti
			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList.length; j++)  {
				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
				   vnos.innerHTML = vnos.innerHTML + jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].text + " " + jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId + "<br>";
				}
			}
			
			//neodločeno
			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList.length; j++)  {
				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
				   vnos.innerHTML = vnos.innerHTML + jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].text + " " + jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId + "<br>";
				}
			}
		}
	}

}


function IzracunajGlasovanje(obrni) {
   
	for (i = 0; i < jsonSample.RollCallVoteResult.length; i++) {
	   //console.log(jsonSample.RollCallVoteResult[i].RollCallVoteDescriptionText);
		if (jsonSample.RollCallVoteResult[i].RollCallVoteDescriptionText == naslov) {
		   //console.log("ja");
			//za
			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList.length; j++)  {
				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
				   //console.log(jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId);
				   
					for (m = 0; m < poslanci.length; m++) {
					   //console.log("poslanci" + poslanci[m].Id);
						if (jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId == poslanci[m].Id) {
						   console.log("JA");
							poslanci[m].glas = 1;
						}
					}
				}
			}
			
			//proti
			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList.length; j++)  {
				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
					for (m = 0; m < poslanci.length; m++) {
						if (jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId == poslanci[m].Id) {
						   console.log("NE");
							poslanci[m].glas = -1;
						}
					}
				}
			}
			
			//neodločeno
			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList.length; j++)  {
				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
					for (m = 0; m < poslanci.length; m++) {
						if (jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId == poslanci[m].Id) {
						   console.log("Neodločeno");
							poslanci[m].glas = 0;
						}
					}
				}
			}
		}
	}


	for (s = 0; s < stranke.length; s++) {
		for (p = 0; p < poslanci.length; p++) {
		  //console.log(poslanci[p].glas);
			if (stranke[s].id == poslanci[p].StrankaId) {
			   //console.log("Stranke");
				if ((stranke[s].glasovanje == null) && (poslanci[p].glas != null)) {
					stranke[s].glasovanje = 0;
				}
				if (stranke[s].glasovanje != null) {
				  stranke[s].glasovanje = stranke[s].glasovanje + poslanci[p].glas;
				}
			}
		}
	}
	
	for (v = 0; v < stranke.length; v++) {
		if (stranke[v].glasovanje > 0) {
		  if (obrni == false) {
		    stranke[v].glasovanje = "1";
		  }
		  else if (obrni == true) {
		    stranke[v].glasovanje = "0";
		  }
			
		}
		else if (stranke[v].glasovanje < 0) {
		  if (obrni == false) {
		    stranke[v].glasovanje = "0";
		  }
		  else if (obrni == true) {
		    stranke[v].glasovanje = "1";
		  }
			
		}
		else if (stranke[v].glasovanje == 0) {
			stranke[v].glasovanje = "0,5";
		}
		var vnos = document.getElementById("vnos");
		vnos.innerHTML = vnos.innerHTML + "<br>" + stranke[v].glasovanje;
		console.log(stranke[v].glasovanje);
	}
}
*/
