var seznam = [
    {
        "id": 1,
        "datum": "17/07/2014",
        "glasovanja": [
            "RC-B8-0027/2014 - Am 14",
            "RC-B8-0027/2014 - Am 12",
            "RC-B8-0027/2014 - Am 11",
            "RC-B8-0027/2014 - Am 7S",
            "RC-B8-0059/2014 - Am 2",
            "RC-B8-0027/2014 - Am 13",
            "RC-B8-0027/2014 - Am 10",
            "RC-B8-0027/2014 - Am 4S",
            "RC-B8-0027/2014 - § 44",
            "RC-B8-0027/2014 - § 42/2"
            
        ],
        "imena_glasovanj": [
            "ES_EL_1_javni_invest_za_dela",
            "ES_EL_2_critisizes_liberal_struct_reforms",
            "ES_EL_3_vec_zascite_za_vajence_pripravnike",
            "ES_EL_4_zascita_kvaliteta_delo_mladi_BRISI",
            "SLS_SCG_7_2_amandma_Irak",
            "ES_EL_13_sankcioniranje_selfEmployed",
            "ES_EL_14_publicSector",
            "ESG_ELS_15_tam_kjer_ne_spostujejo_BRISI",
            "ES_EL_15_studij_za_potrebe_trga",
            "ES_EL_16_proti_prevec_regulacijam"
        ]
    },
    {
        "id": 2,
        "datum": "27/11/2014",
        "glasovanja": [
            "B8-0285/2014 - Am 2",
            "B8-0285/2014 - Am 5",
            "RC-B8-0286/2014 - Am 17",
            "RC-B8-0286/2014 - Am 22",
            "B8-0285/2014 - Am 11"
        ],
        "imena_glasovanj": [
            "ES_EL_5_obsodba_varcevalne_politike",
            "ESSC_EL_12_vecja_podpora_materinstvu_ocetovstvu",
            "SC_SL_1_IT_ponudniki_oblasti_kriminal",
            "SC_SL_5_internet_cenzura_za_otroke_neprimerno",
            "SC_SL_15_spolna_vzgoja_diskr"
        ]
    },
    {
        "id": 3,
        "datum": "03/02/2016",
        "glasovanja": [
            "A8-0009/2016 -  Viviane Reding - Am 36",
            "A8-0009/2016 -  Viviane Reding - Am 3=25",
            "A8-0009/2016 -  Viviane Reding - Am 20rev",
            "A8-0009/2016 -  Viviane Reding - Am 53",
            "A8-0009/2016 -  Viviane Reding - Am 46",
            "A8-0009/2016 -  Viviane Reding - Am 50",
            "A8-0009/2016 -  Viviane Reding - Am 41",
            "A8-0009/2016 -  Viviane Reding - Am 31"
        ],
        "imena_glasovanj": [
            "ES_EL_6_zahteva_po_ratificiranju_ILO_standardov_TISA",
            "SES_ELG_4_odstop_od_sporazuma_tisa_del_pravice",
            "SES_ELG_14_izstop_TISA",
            "SE_EG_17_Tiso_potrditi_parl_vseh_drz",
            "ES_EL_16_pra vice_nad_pogodbami",
            "SES_ELG_15_protekcionizem_zemlja",
            "SES_ELG_80_domaci_sporazumi_prednost",
            "SES_ELG_81_lokalna_populacija_prednosti"
        ]
    },
        {
        "id": 4,
        "datum": "27/10/2015",
        "glasovanja": [
            "A8-0300/2015 -  Pilar del Castillo Vera - Am 8=19="
        ],
        "imena_glasovanj": [
            "ES_EL_7_net_neutrality"
        ]
    },
        {
        "id": 5,
        "datum": "17/12/2015",
        "glasovanja": [
            "B8-1347/2015 - Am 26",
            "B8-1347/2015 - Am 27",
            "B8-1347/2015 - Am 34",
            "B8-1347/2015 - Am 22",
            "B8-1347/2015 - Am 5",
            "B8-1347/2015 - Am 36",
            "B8-1347/2015 - Am 3",
            "B8-1347/2015 - Am 28",
            "B8-1347/2015 - Am 30",
            "RC-B8-1394/2015 - Am 1",
            "RC-B8-1394/2015 - § 4/3"
        ],
        "imena_glasovanj": [
            "ES_EL_8_podreditev_ekonomske_moci_dem_politicni_moci_j_sektor",
            "ES_EL_9_nacionalizacija_javni_sektor",
            "ES_EL_10_promo_j_dela_kooperative_mala_podjetja_lokalna_vlada",
            "SES_ELG_2_proti_vsaki_omejitvi_n_drzav_do_lastne_ekon_polit_gr",
            "SE_EG_8_kritika_skupne_valute_in_monetarne_unije",
            "SE_EG_10_pravica_ljudi_do_ref_odločanja_o_ekonom_vladanju_sprem",
            "SE_EG_17_niso_upostevali_alternativ",
            "ES_EL_21_pravica_drz_za_regulacijo_cen",
            "ES_EL_22_boj_proti_davcnim_oazam",
            "ES_EL_50_zascita_kmetov_patenti",
            "ES_EL_51_ne_vkljucevat_v_pravice_krizarjev"
        ]
    },
        {
        "id": 6,
        "datum": "26/10/2016",
        "glasovanja": [
            "A8-0309/2016 -  Alfred Sant - Am 21"
            
        ],
        "imena_glasovanj": [
            "SES_ELG_8_proti_kazni_za_soc_politiko"
            ]
    },
        {
        "id": 7,
        "datum": "09/03/2016",
        "glasovanja": [
            "RC-B8-0312/2016 - § 11"
        ],
        "imena_glasovanj": [
            "ES_EL_11_konec_deala_tob_ind_proti_svercu"
        ]
    },
        {
        "id": 8,
        "datum": "16/09/2014",
        "glasovanja": [
            "A8-0002/2014 -  Jacek Saryusz-Wolski - approbation"
        ],
        "imena_glasovanj": [
            "SES_ELG_1_free_trade_Ukraine"
        ]
    },
        {
        "id": 9,
        "datum": "08/09/2015",
        "glasovanja": [
            "A8-0228/2015 -  Lynn Boylan - § 22/3",
            "A8-0230/2015 -  Laura Ferrara - § 89",
            "A8-0230/2015 -  Laura Ferrara - § 84",
            "A8-0230/2015 -  Laura Ferrara - § 167",
            "A8-0230/2015 -  Laura Ferrara - § 117",
            "A8-0228/2015 -  Lynn Boylan - Am 4",
            "A8-0230/2015 -  Laura Ferrara - Considérant BD",
            "A8-0230/2015 -  Laura Ferrara - Considérant BG",
            "A8-0230/2015 -  Laura Ferrara - § 43",
            "A8-0230/2015 -  Laura Ferrara - § 87",
            "A8-0230/2015 -  Laura Ferrara - § 118",
            "A8-0230/2015 -  Laura Ferrara - § 134"
        ],
        "imena_glasovanj": [
            "SES_ELG_3_vodni_viri_ne_v_trg_sporazume",
            "SC_SL_9_obsoja_spremeni_def_transgender_mentalno_bolan",
            "SC_SL_10_poziv_za_zakonski_boj_proti_homofobiji",
            "SS_SLG_5_stalni_tuji_drzavljani_volilna_pravica",
            "SCS_SLG_5_obtozba_varovanja_meja_v_Evropi_imigr_gredo_tihotapcem",
            "ES_EL_38_privatizacija_vode",
            "SC_SL_26_diskriminacija_lgbt_ce_ni_porok",
            "SC_SL_27_sekularizem_najbolj_garantira",
            "SC_SL_28_AntiIslamske_demonstr",
            "SC_SL_29_diversity_zaposlovanje_lgbt",
            "SCS_SLG_8_rights_sensitive_zascita",
            "SS_SG_11_proti_zaporam_schengna"
        ]
    },
        {
        "id": 10,
        "datum": "25/02/2016",
        "glasovanja": [
            "B8-0250/2016 - Am 4",
            "B8-0255/2016 - Am 3",
            "A8-0224/2015 -  Heinz K. Becker - Am 181=183=",
            "A8-0031/2016 -  Sofia Ribeiro - § 3",
            "A8-0031/2016 -  Sofia Ribeiro - § 19",
            "A8-0031/2016 -  Sofia Ribeiro - § 62",
            "A8-0030/2016 -  Maria João Rodrigues - Am 10",
            "A8-0030/2016 -  Maria João Rodrigues - Am 19",
            "A8-0030/2016 -  Maria João Rodrigues - Am 22",
            "A8-0030/2016 -  Maria João Rodrigues - Am 15",
            "A8-0030/2016 -  Maria João Rodrigues - Am 38",
            "A8-0030/2016 -  Maria João Rodrigues - Am 16",
            "A8-0030/2016 -  Maria João Rodrigues - Am 12",
            "B8-0250/2016 - Am 7",
            "B8-0250/2016 - Am 10",
            "B8-0250/2016 - Am 11"
        ],
        "imena_glasovanj": [
            "SES_ELG_6_proti_kakrsenkoli_pr_trg_AU_NZ",
            "SES_ELG_7_protekcionizem_free_trade_olive_oil",
            "SE_EG_11_izstop_mobilnost",
            "SE_EG_12_EMU",
            "ES_EL_14_razlike_v_placah",
            "ES_EL_15_social_dumping",
            "ES_EL_17_posebej_biznis_okolje",
            "SE_EG_14_ena_valuta_slabo",
            "SE_EG_15_demokratična_odločitev_EMU",
            "ES_EL_18_javno_produktive_investicije",
            "SE_EG_16_specificni_predlogi_zalitev",
            "ES_EL_17_boj_revscina_izklj",
            "SES_ELG_16_odstop_TTIP_TISA",
            "SE_EG_21_demokr_free_tr_AU",
            "ES_EL_36_zivalski_standardi",
            "ES_EL_37_abour_human_rights"
        ]
    },
        {
        "id": 12,
        "datum": "16/07/2014",
        "glasovanja": [
            "A8-0001/2014 -  Werner Langen - Am 4",
            "A8-0001/2014 -  Werner Langen - Résolution législative"
        ],
        "imena_glasovanj": [
            "SE_EG_9_refendum_za_euro",
            "S_G_1_siritev_unije"
        ]
    },
        {
        "id": 14,
        "datum": "09/06/2015",
        "glasovanja": [
            "A8-0163/2015 -  Maria Noichl - proposition de résolution de la commission FEMM",
            "A8-0163/2015 -  Maria Noichl - § 24"
        ],
        "imena_glasovanj": [
            "SC_SL_2_gender_equality",
            "SC_SL_4_legalizacija_izbire_spola_po_zelji",
            ""
        ]
    },
        {
        "id": 15,
        "datum": "09/09/2015",
        "glasovanja": [
            "A8-0235/2015 -  Elissavet Vozemberg - § 42",
            "A8-0245/2015 -  Ska Keller - Am 44/1",
            "A8-0235/2015 -  Elissavet Vozemberg - § 22",
            "A8-0235/2015 -  Elissavet Vozemberg - § 25",
            "A8-0206/2015 -  Liliana Rodrigues - § 30",
            "A8-0206/2015 -  Liliana Rodrigues - § 28",
            "A8-0245/2015 -  Ska Keller - Am 9"
            
        ],
        "imena_glasovanj": [
            "SC_SL_3_financiranje_promotion_gender_equality",
            "SS_SLG_4_locevanje_ekon_migr_begunc_vracanje_financira",
            "SC_SL_18_ocetovska_porodniska",
            "SCS_SLG_7_enakost_spolov_po_nac_drzavah",
            "SC_SL_30_sex_ucenje",
            "SC_SL_31_stereotipi_ucenje",
            "SS_SG_16_skupni_azilni_sistem"
            
        ]
    },
        {
        "id": 16,
        "datum": "10/03/2015",
        "glasovanja": [
            "A8-0015/2015 -  Marc Tarabella - § 32",
            "A8-0015/2015 -  Marc Tarabella - Am 10",
            "A8-0015/2015 -  Marc Tarabella - Am 11",
            "A8-0019/2015 -  Morten Messerschmidt - Am 3",
            "A8-0019/2015 -  Morten Messerschmidt - Am 6",
            "A8-0019/2015 -  Morten Messerschmidt - Am 7",
            "A8-0019/2015 -  Morten Messerschmidt - § 85",
            "A8-0015/2015 -  Marc Tarabella - § 9",
            "A8-0015/2015 -  Marc Tarabella - § 41",
            "A8-0015/2015 -  Marc Tarabella - § 44",
            "A8-0015/2015 -  Marc Tarabella - Am 16"
        ],
        "imena_glasovanj": [
            "SC_SL_6_predlog_drzave_ne_dokumentirajo_migr_zensk_delavk",
            "SS_SLG_9_zdravstvo_sex_politika_stvar_nac_drzav",
            "SCS_SLG_1_dostop_kontracepcija_splav_NE",
            "ES_EL_22_tekmovanje_podrejeno_trajn_rasti",
            "ES_EL_23_obtozba_privat_bank",
            "SES_ELG_15_proti_ISDS",
            "ES_EL_24_zagotoviti_tekmovanje",
            "SC_SL_19_1_stars_druzine_ugodnosti",
            "SC_SL_20_solstvo_boj_proti_stereotipom",
            "SC_SL_21_splav_legalen_ga_je_manj",
            "SC_SL_22_zascita_zensk_na_tv"
        ]
    },
        {
        "id": 17,
        "datum": "08/10/2015",
        "glasovanja": [
            "A8-0213/2015 -  Anna Záborská - Considérant Q",
            "B8-0988/2015 - Am 2/1",
            "A8-0213/2015 -  Anna Záborská - § 13/1",
            "A8-0213/2015 -  Anna Záborská - § 17/1",
            "A8-0213/2015 -  Anna Záborská - § 17/2",
            "A8-0213/2015 -  Anna Záborská - Am 1",
            "RC-B8-0998/2015 - § 7"
        ],
        "imena_glasovanj": [
            "SC_SL_7_posebna_diskriminacija_lezb_starih_trans_invalid",
            "SC_SL_8_free_abortion_services",
            "ES_EL_39_zascita_zensk_po_porodniski",
            "SC_SL_23_proti_spol_nadl_na_del_mestu",
            "SC_SL_24_trans_diskriminacija",
            "SC_SL_25_gender_identity_diskriminacija",
            "SC_SL_32_smrtna_kazen_nekombatibilna"
        ]
    },
        {
        "id": 20,
        "datum": "25/11/2015",
        "glasovanja": [
            "A8-0316/2015 -  Rachida Dati - § 72",
            "A8-0316/2015 -  Rachida Dati - Considérant AA/2",
            "A8-0316/2015 -  Rachida Dati - § 16/2",
            "A8-0316/2015 -  Rachida Dati - Am 36",
            "A8-0316/2015 -  Rachida Dati - Am 50/2",
            "A8-0316/2015 -  Rachida Dati - Am 4",
            "A8-0316/2015 -  Rachida Dati - Am 6",
            "A8-0316/2015 -  Rachida Dati - Am 7",
            "A8-0316/2015 -  Rachida Dati - Am 10",
            "A8-0316/2015 -  Rachida Dati - Am 13",
            "A8-0316/2015 -  Rachida Dati - Am 21",
            "A8-0316/2015 -  Rachida Dati - Am 30",
            "A8-0316/2015 -  Rachida Dati - Am 39",
            "A8-0316/2015 -  Rachida Dati - Am 40",
            "A8-0316/2015 -  Rachida Dati - § 52"
        ],
        "imena_glasovanj": [
            "SC_SL_11_kriminalna_obtozba_hatespeecha",
            "SC_SL_12_ce_nekdo_clan_teror_org_se_ne_pomeni_razloga_za_sum",
            "SC_SL_13_sodel_drz_podjetja_proti_net_hatespeech_terorism",
            "SC_SL_14_it_soc_media_kazen_ce_ne_ukrep_proti_ekstremist_objavam",
            "SS_SLG_10_meje_za_nadzor_imigr_rase_etn_kontra_eu_vrednotam",
            "SCS_SLG_2_obmejne_kontrole",
            "SCS_SLG_3_multikult_open_borders_groznja",
            "SCS_SLG_16_meje_nazaj_EU_drz",
            "SS_SG_17_vrniti_migrante_nazaj",
            "SC_SL_34_manj_strogo_za_izbris_hatespeecha",
            "SC_SL_35_za_teror_kriv_antiislam",
            "SC_SL_36_izkoristek_terorizma_za_nadzor",
            "SC_SL_38_sifrirani_podatki_tezek_nadzor",
            "SC_SL_39_tuji_borci_zapor",
            "SLS_SCG_10_obtozba_zalivskih_drz"
        ]
    },
        {
        "id": 21,
        "datum": "10/09/2015",
        "glasovanja": [
            "RC-B8-0832/2015 - Considérant J/2",
            "RC-B8-0832/2015 - § 9",
            "RC-B8-0832/2015 - § 5/1",
            "RC-B8-0832/2015 - Am 4/rev",
            "A8-0247/2015 -  Verónica Lope Fontagné - § 50",
            "A8-0222/2015 -  Martina Dlabajová - § 38",
            "A8-0222/2015 -  Martina Dlabajová - § 71",
            "RC-B8-0832/2015 - § 5/2",
            "RC-B8-0832/2015 - § 15/1",
            "RC-B8-0832/2015 - Am 11",
            "RC-B8-0832/2015 - Am 1"
        ],
        "imena_glasovanj": [
            "SS_SLG_1_problematizira_razlicne_azilne_procedure_standarde",
            "SS_SLG_2_ustanovitev_trajnega_sistema_kvot",
            "SS_SLG_3_poudarjanje_odprtih_mej",
            "SLS_SCG_6_kritika_Z_intervencij",
            "SE_EG_18_brez_omejitev_za_prosto_del_silo",
            "ES_EL_16_Probiznis_solstvo",
            "SE_EG_20_erazmus_za_mobilnost",
            "SS_SG_13_schengen_najvecji_dosezek",
            "SS_SG_14_obtozba_antiimigrantov",
            "SCS_SLG_14_konec_deportacij_migrantov",
            "SS_SG_15_koridorje_za_migrante"
        ]
    },
        {
        "id": 23,
        "datum": "16/12/2015",
        "glasovanja": [
            "RC-B8-1351/2015 - § 5",
            "A8-0349/2015 -  Anneliese Dodds et  Luděk Niedermayer - Am 6",
            "A8-0349/2015 -  Anneliese Dodds et  Luděk Niedermayer - Am 11",
            "RC-B8-1351/2015 - § 7"
        ],
        "imena_glasovanj": [
            "SS_SLG_6_Protimadžarska_resolucija_kršitev_migranti",
            "ESG_ELS_4_obtozba_drzav_pomagali_multinac_skrivat_davke",
            "SC_SL_15_place_za_zvizgace",
            "SS_SG_56_proti_Madzarski_zaradi_azila"
        ]
    },
        {
        "id": 24,
        "datum": "19/02/2016",
        "glasovanja": [
            "A8-0373/2015 -  Julie Ward - Résolution de la commission CULT"
        ],
        "imena_glasovanj": [
            "SS_SLG_8_kulturna_diversity_promocija_predvsem_znotraj_Evrope"
        ]
    },
        {
        "id": 25,
        "datum": "25/11/2014",
        "glasovanja": [
            "B8-0265/2014 - Résolution"
        ],
        "imena_glasovanj": [
            "SLS_SCG_1_PNR_data_civil_rights_vs_terrorism_sodisce"
        ]
    },
        {
        "id": 26,
        "datum": "21/01/2016",
        "glasovanja": [
            "RC-B8-0043/2016 - Am 1",
            "RC-B8-0043/2016 - § 18",
            "RC-B8-0043/2016 - Considérant H/1",
            "RC-B8-0050/2016 - § 46",
            "RC-B8-0050/2016 - § 45/2",
            "RC-B8-0050/2016 - § 34",
            "RC-B8-0068/2016 - Am 5",
            "RC-B8-0068/2016 - Am 14",
            "RC-B8-0068/2016 - Am 17"
        ],
        "imena_glasovanj": [
            "SLS_SCG_3_zaveznistvo_fight_terrorism",
            "SLS_SCG_5_EU_defence_union",
            "SCG_SLS_4_večja_aktivnost_EU_defence",
            "SC_SL_66_podpora_LGBT",
            "SCS_SLG_55_opazovanje_situacije_kjer_antilgbt_zakoni",
            "SCS_SLG_56_klimatski_begunec",
            "SS_SG_91_Ukrajina_sporazum_ni_povezan_s_siritvijo_EU",
            "ES_EL_47_ustavit_uvoz_child_labour",
            "SE_EG_48_spostovanje_referenduma"
        ]
    },
        {
        "id": 28,
        "datum": "12/03/2015",
        "glasovanja": [
            "RC-B8-0240/2015 - § 4"
        ],
        "imena_glasovanj": [
            "SLS_SCG_8_podpora_antiISIL_koaliciji"
        ]
    },
        {
        "id": 29,
        "datum": "23/10/2014",
        "glasovanja": [
            "RC-B8-0166/2014 - Am 2"
        ],
        "imena_glasovanj": [
            "SEG_ELS_1_spremembe_v_pogodbi_le_ce_upostevajo"
        ]
    },
        {
        "id": 30,
        "datum": "08/07/2015",
        "glasovanja": [
            "A8-0175/2015 -  Bernd Lange - Résolution",
            "A8-0205/2015 -  Laura Agea - Am 10",
            "A8-0205/2015 -  Laura Agea - Am 49",
            "A8-0175/2015 -  Bernd Lange - Am 19",
            "A8-0175/2015 -  Bernd Lange - Am 30",
            "A8-0175/2015 -  Bernd Lange - Am 41",
            "A8-0175/2015 -  Bernd Lange - Am 101",
            "A8-0175/2015 -  Bernd Lange - Am 96",
            "A8-0175/2015 -  Bernd Lange - Am 92",
            "A8-0175/2015 -  Bernd Lange - Am 85S",
            "A8-0175/2015 -  Bernd Lange - Am 67",
            "A8-0175/2015 -  Bernd Lange - Am 72",
            "A8-0175/2015 -  Bernd Lange - Am 75",
            "A8-0175/2015 -  Bernd Lange - Am 94",
            "A8-0175/2015 -  Bernd Lange - Am 110",
            "A8-0175/2015 -  Bernd Lange - Am 113",
            "A8-0175/2015 -  Bernd Lange - Am 60"
        ],
        "imena_glasovanj": [
            "SES_ELG_5_pogajanjaTTIP_resolucija",
            "SE_EG_19_brezkompetenc_za_min_wage",
            "ESG_ELS_18_min_placa",
            "SES_ELG_16_pri_razlikah_ni_sporazuma",
            "SES_ELG_17_STOP_TTIP_iniciativa",
            "SES_ELG_18_TTIP_v_javnost",
            "ES_EL_25_TTIP_delavci",
            "ES_EL_26_velika_vecina_proti_GMO",
            "SES_ELG_19_protekcionizem_hrana",
            "SES_ELG_20_proti_migr_delavcem_US",
            "SES_ELG_21_energija_TTIP",
            "SE_EG_78_TTIPproti_suverenosti",
            "SES_ELG_77_proti_hrani_inht_trade",
            "SES_ELG_78_drz_intervencija",
            "SES_ELG_79_povecati_st_zascitenih_geo_porekla",
            "SE_EG_79_moznost_izhoda_iz_TTIP",
            "SES_ELG_79_odstranitev_tarif"
        ]
    },
        {
        "id": 32,
        "datum": "14/04/2016",
        "glasovanja": [
            "A8-0040/2016 -  Tamás Meszerics - § 8"
        ],
        "imena_glasovanj": [
            "ESG_ELS_2_proti_zakonom_revscina"
        ]
    },
        {
        "id": 34,
        "datum": "12/05/2016",
        "glasovanja": [
            "A8-0076/2016 -  Maria Arena - § 7",
            "A8-0076/2016 -  Maria Arena - Considérant S",
            "RC-B8-0607/2016 - Am 7"
        ],
        "imena_glasovanj": [
            "SE_EG_13_sociala_nac_drzave",
            "SL_SC_11_homoseks_otroci",
            "SE_EG_22_Kitajska_proti_market_status"
        ]
    },
        {
        "id": 35,
        "datum": "15/09/2016",
        "glasovanja": [
            "A8-0248/2016 -  Zdzisław Krasnodębski - § 17/2",
            "A8-0225/2016 -  Renate Weber - § 18/1",
            "A8-0225/2016 -  Renate Weber - § 18/2",
            "A8-0225/2016 -  Renate Weber - Considérant U"
        ],
        "imena_glasovanj": [
            "ES_EL_15_selfEmployment_problemi",
            "SC_SL_15_svoboda_vesti",
            "SC_SL_16_splav_ugovor_vesti",
            "SC_SL_17_enakost_med_spoloma_direktorji"
        ]
    },
        {
        "id": 37,
        "datum": "26/05/2016",
        "glasovanja": [
            "A8-0171/2016 -  Lara Comi - Am 2",
            "A8-0171/2016 -  Lara Comi - Am 7",
            "A8-0171/2016 -  Lara Comi - § 4",
            "A8-0171/2016 -  Lara Comi - § 71/2"
        ],
        "imena_glasovanj": [
            "SES_ELG_9_protekcionizem",
            "SES_ELG_10_protekcionizem_services",
            "SES_ELG_11_odstranjevanje_barier",
            "SES_ELG_12_proti_fragm_sk_trga"
        ]
    },
        {
        "id": 38,
        "datum": "05/07/2016",
        "glasovanja": [
            "A8-0217/2016 -  Eleonora Forenza - Considérant F",
            "A8-0220/2016 -  Tiziana Beghin - § 50",
            "A8-0220/2016 -  Tiziana Beghin - § 48/2",
            "A8-0220/2016 -  Tiziana Beghin - § 5/2"
        ],
        "imena_glasovanj": [
            "SES_ELG_13_tuje_investicije_clov_pravice",
            "SE_EG_49_spodbuja_nacionalne_org_podjetja_k_pogajanjem",
            "SE_EG_50_takoj_zacet_investic_sporazum_Tajvan",
            "SE_EG_51_globalni_prosti_trg_korist"
        ]
    },
        {
        "id": 39,
        "datum": "29/10/2015",
        "glasovanja": [
            "B8-1093/2015 - Am 10"
        ],
        "imena_glasovanj": [
            "SES_ELG_17_nizjecenovne_drz_soc_dump"
        ]
    },
        {
        "id": 43,
        "datum": "11/03/2015",
        "glasovanja": [
            "A8-0043/2015 -  Sergio Gutiérrez Prieto - Am 1",
            "A8-0043/2015 -  Sergio Gutiérrez Prieto - Am 2",
            "A8-0043/2015 -  Sergio Gutiérrez Prieto - Am 3",
            "A8-0043/2015 -  Sergio Gutiérrez Prieto - Considérant N"
        ],
        "imena_glasovanj": [
            "ES_EL_17_proti_neoliberalizmu",
            "ES_EL_18_proti_prekarnemu_delu",
            "ES_EL_19_promovirat_public_services",
            "ES_EL_20_evropski_soc_model_ogrozen"
        ]
    },
        {
        "id": 48,
        "datum": "08/03/2016",
        "glasovanja": [
            "A8-0024/2016 -  Mary Honeyball - § 60"
        ],
        "imena_glasovanj": [
            "SS_SG_12_azil_v_vseh_clanicah"
        ]
    },
        {
        "id": 50,
        "datum": "14/09/2016",
        "glasovanja": [
            "B8-0977/2016 - Am 5"
        ],
        "imena_glasovanj": [
            "SCS_SLG_13_proti_Polski_antimultikulturi"
        ]
    },
        {
        "id": 52,
        "datum": "15/01/2015",
        "glasovanja": [
            "RC-B8-0011/2015 - Am 6"
        ],
        "imena_glasovanj": [
            "SCS_SLG_15_obtozba_interv_Libija"
        ]
    },
        {
        "id": 53,
        "datum": "04/02/2016",
        "glasovanja": [
            "B8-0167/2016 - Am 2"
        ],
        "imena_glasovanj": [
            "SC_SL_33_kristijani_zascita"
        ]
    },
        {
        "id": 54,
        "datum": "23/11/2016",
        "glasovanja": [
            "A8-0290/2016 -  Anna Elżbieta Fotyga - Résolution"
        ],
        "imena_glasovanj": [
            "SC_SL_40_boj_proti_antiEU_propagandi"
        ]
    },
        {
        "id": 55,
        "datum": "22/11/2016",
        "glasovanja": [
            "A8-0316/2016 -  Urmas Paet - § 11"
        ],
        "imena_glasovanj": [
            "SS_SG_ 19_EU_vojska_povezave"
        ]
    },
        {
        "id": 56,
        "datum": "24/11/2015",
        "glasovanja": [
            "A8-0308/2015 -  Paavo Väyrynen - § 16/2",
            "A8-0308/2015 -  Paavo Väyrynen - Am 3"
        ],
        "imena_glasovanj": [
            "SS_SG_20_EU_enGlas_na_UN",
            "SCS_SLG_16_klimatski_begunec"
        ]
    },
        {
        "id": 57,
        "datum": "11/06/2015",
        "glasovanja": [
            "A8-0171/2015 -  Ioan Mircea Paşcu - § 30",
            "A8-0171/2015 -  Ioan Mircea Paşcu - § 32/1"
        ],
        "imena_glasovanj": [
            "SC_SL_41_povecat_voj_proracun",
            "SLS_SCG_11_nato_naj_ostane_v_c_morju"
        ]
    },
        {
        "id": 58,
        "datum": "30/04/2015",
        "glasovanja": [
            "RC-B8-0382/2015 - Am 6",
            "RC-B8-0382/2015 - Am 16"
        ],
        "imena_glasovanj": [
            "SC_SL_42_Kristjani_najbolj_pregnjani",
            "SC_SL_43_Poziv_muslimanom_da_obtozijo_nasilje"
        ]
    },
        {
        "id": 59,
        "datum": "17/09/2015",
        "glasovanja": [
            "C8-0271/2015 - Résolution législative",
            "C8-0271/2015 - Proposition de la Commission"
        ],
        "imena_glasovanj": [
            "SS_SG_21_kvote_Italija_Grc_Madz",
            "SS_SG_21_kvote_Italija_Grc_Madz_predlog_komisije"
        ]
    },
        {
        "id": 60,
        "datum": "25/10/2016",
        "glasovanja": [
            "A8-0283/2016 -  Sophia in 't Veld - Am 1",
            "A8-0283/2016 -  Sophia in 't Veld - § 20, tiret 6-7"
        ],
        "imena_glasovanj": [
            "SS_SG_22_proti_skupnemu_mehanizmu_demokracije",
            "SCS_SLG_17_proti_soglasju_pri_fund_pravicah"
        ]
    },
        {
        "id": 61,
        "datum": "01/06/2017",
        "glasovanja": [
            "B8-0383/2017 - Am 1"
        ],
        "imena_glasovanj": [
            "SC_SL_40_kritikaIzraela_antisemitism"
        ]
    },
        {
        "id": 62,
        "datum": "17/05/2017",
        "glasovanja": [
            "B8-0295/2017 - Am 1",
            "B8-0295/2017 - Am 5",
            "B8-0295/2017 - Am 6",
            "B8-0295/2017 - § 7",
            "B8-0298/2017 - § 26/1",
            "B8-0298/2017 - Am 2",
            "B8-0298/2017 - Am 12"
        ],
        "imena_glasovanj": [
            "SS_SG_36_EU_skupnost_suvern_drzav",
            "SCS_SLG_45_podpora_Madzarski",
            "SCS_SLG_46_spostovanje_Madz_zakonov",
            "SS_SG_49_teu",
            "SCS_SLG_40_vkljucitev_migrantov_v_trg_druzbo",
            "SE_EG_52_prost_pretok_migr_del_sile_slab",
            "SE_EG_53_nac_drzave_ne_smejo_izgubiti_kvalitete"
            
        ]
    },
        {
        "id": 63,
        "datum": "06/04/2017",
        "glasovanja": [
            "A8-0274/2016 -  Mariya Gabriel - Am 2",
            "B8-0235/2017 - § 16",
            "B8-0235/2017 - § 17",
            "RC-B8-0252/2017 - Am 1"
        ],
        "imena_glasovanj": [
            "SS_SG_37_proti_liberalizaciji_viz_Ukrajina",
            "SC_SL_41_masovni_nadzor_proti_vrednotam_EU",
            "SC_SL_42_ZDA_manj_stroge_kriterije_os_podatki",
            "SC_SL_46_splav_v_Bangladesu"
        ]
    },
        {
        "id": 65,
        "datum": "05/04/2017",
        "glasovanja": [
            "A8-0092/2017 -  Tomáš Zdechovský - Projet du Conseil",
            "A8-0091/2017 -  Judith Sargentini - Projet du Conseil",
            "A8-0095/2017 -  Filiz Hyusmenova - Projet du Conseil",
            "RC-B8-0237/2017 - Considérant I/1",
            "RC-B8-0237/2017 - Considérant I/2",
            "RC-B8-0237/2017 - Am 19",
            "RC-B8-0237/2017 - Am 9"
        ],
        "imena_glasovanj": [
            "SLS_SCG_10_avtomatska_izmenjava_prstnih_odtisov",
            "SLS_SCG_11_avtomatska_izmenjava_DNA",
            "SLS_SCG_12_avtomatska_izmenjava_reg_tablic",
            "SE_EG_41_UK_ostane_v_single_market",
            "SE_EG_42_UK_ne_dovolimo_svoje_trade_policy",
            "SS_SG_41_konec_jursidikcije_CJEU_v_UK",
            "SS_SG_42_brez_prostega_pretoka"
        ]
    },
        {
        "id": 68,
        "datum": "02/02/2017",
        "glasovanja": [
            "A8-0260/2016 -  Mariya Gabriel - Am 1"
        ],
        "imena_glasovanj": [
            "SS_SG_38_visa_Liberalizacija_Gruzija"
        ]
    },
        {
        "id": 69,
        "datum": "16/02/2017",
        "glasovanja": [
            "A8-0386/2016 -  Mercedes Bresso et  Elmar Brok - § 134",
            "A8-0386/2016 -  Mercedes Bresso et  Elmar Brok - § 121"
        ],
        "imena_glasovanj": [
            "SS_SG_39_vracanje_migr_zmanj_migracij",
            "SS_SG_40_mocnejse_povezovanje_na_vojaskem_podrocju"
        ]
    },
        {
        "id": 71,
        "datum": "14/09/2017",
        "glasovanja": [
            "A8-0133/2017 -  Sven Giegold - Am 24S",
            "A8-0133/2017 -  Sven Giegold - Am 25"
        ],
        "imena_glasovanj": [
            "SC_SL_43_zvizgaci_prevec_brez_zascite",
            "ES_EL_40_podjetja_ki_krsijo_javni_seznam" 
        ]
    },
        {
        "id": 72,
        "datum": "05/07/2017",
        "glasovanja": [
            "RC-B8-0434/2017 - Am 6",
            "RC-B8-0434/2017 - Am 14",
            "RC-B8-0434/2017 - Am 21",
            "RC-B8-0434/2017 - Am 25",
            "RC-B8-0434/2017 - Am 27",
            "RC-B8-0434/2017 - Am 28",
            "RC-B8-0434/2017 - Am 33",
            "RC-B8-0434/2017 - Am 77",
            "RC-B8-0434/2017 - Am 91",
            "RC-B8-0434/2017 - Am 94",
            "RC-B8-0434/2017 - Am 99",
            "RC-B8-0434/2017 - § 7/1",
            "RC-B8-0434/2017 - § 9"
        ],
        "imena_glasovanj": [
            "SES_SLG_45_prodelavska_zakonodaja_nac_drzave_same_odlocajo",
            "SS_SG_46_proti_temu_da_drz_odlocajo",
            "SE_EG_43_nac_drz_odlocajo_o_ekon_politiki",
            "ES_EL_41_demokr_javni_nadzor_nad_bankami",
            "ES_EL_42_protiliberalizaciji_del_zakonodaje_varcevanju",
            "SS_SG_47_proti_EU_vojski_tujim_voj_bazam_NATU",
            "ES_EL_43_ugodnosti_za_studente_ranljive_skupine",
            "SCS_SLG_43_legalne_poti_begunci_izvor_problemov",
            "SS_SG_49_proti_temu_da_UK_brexit_bill",
            "SE_EG_44_proti_sankcijam_ce_UK_sklepa_trgovske_sp",
            "SEL_ESG_15_min_place_po_vseh_drz",
            "SS_SG_50_za_razporeditev_migrantov_po_drz",
            "SES_ELG_16_za_prosti_trg_naprej"
        ]
    },
        {
        "id": 73,
        "datum": "14/06/2017",
        "glasovanja": [
            "A8-0197/2017 -  Constance Le Grip - Am 16",
            "A8-0197/2017 -  Constance Le Grip - Am 22"
        ],
        "imena_glasovanj": [
            "ES_EL_45_osebne_penzije_ne_vkljucujejo_skrb_otroci",
            "SC_SL_45_kazni_ce_ne_upostevajo_gend_eq"
        ]
    },
        {
        "id": 74,
        "datum": "14/03/2017",
        "glasovanja": [
            "A8-0046/2017 -  Ernest Urtasun - Am 2",
            "A8-0046/2017 -  Ernest Urtasun - Considérant AH",
            "A8-0033/2017 -  Clare Moody - Am 2",
            "A8-0033/2017 -  Clare Moody - § 79",
            "A8-0043/2017 -  Agnieszka Kozłowska-Rajewicz - § 10"
        ],
        "imena_glasovanj": [
            "SC_SL_46_ugovor_vesti",
            "SCS_SLG_44_seks_reprodukt_pravice_v_EU_strategijo",
            "SC_SL_51_pomoc_org_namesto_Trumpa",
            "SCS_SLG_47_nadzor_nad_Poljsko_glede_sp_enakosti",
            "SC_SL_52_diskriminacija_trans"
        ]
    },
        {
        "id": 75,
        "datum": "14/02/2017",
        "glasovanja": [
            "A8-0380/2016 -  Beatriz Becerra Basterrechea - § 17/1",
            "A8-0380/2016 -  Beatriz Becerra Basterrechea - § 44",
            "A8-0380/2016 -  Beatriz Becerra Basterrechea - Considérant AR",
            "A8-0018/2017 -  Constance Le Grip et  Maria Arena - Am 3",
            "A8-0018/2017 -  Constance Le Grip et  Maria Arena - Am 4",
            "A8-0018/2017 -  Constance Le Grip et  Maria Arena - Am 7/1",
            "A8-0001/2017 -  Tibor Szanyi - § 11/1"
        ],
        "imena_glasovanj": [
            "SC_SL_47_skrb_mentalno_zdravje_begunke",
            "SC_SL_48_sp_pravice_tudi_splav",
            "SC_SL_49_sp_ucenje_kontracepcija_pozitivno",
            "SCS_SLG_45_suverena_pravica_z_ozirom_na_religijsko_pravico",
            "SCS_SLG_46_glede_splava_le_na_nac_ravni_ne_sme_se_ga_propagirat",
            "SC_SL_50_obtozba_Trumpovega_global_gag_rule",
            "SES_ELG_45_prosti_trg_povecati"
        ]
    },
        {
        "id": 79,
        "datum": "04/04/2017",
        "glasovanja": [
            "A8-0058/2017 -  Marijana Petir et  Maria Lidia Senra Rodríguez - Am 8",
            "A8-0058/2017 -  Marijana Petir et  Maria Lidia Senra Rodríguez - § 12/2",
            "B8-0177/2017 - Am 30",
            "B8-0177/2017 - Am 29"
        ],
        "imena_glasovanj": [
            "SC_SL_53_sp_enakost_vkljucena_v_vse_EU_programe",
            "SC_SL_54_ranljive_manjsine",
            "ES_EL_47_odskodnina_kjer_vozilo_slabse_dela",
            "ES_EL_48_poplacilo_ce_vozilo_ni_sprejeto"
            
        ]
    },
        {
        "id": 80,
        "datum": "04/07/2017",
        "glasovanja": [
            "A8-0227/2017 -  Hugues Bayet et  Evelyn Regner - Am 65",
            "A8-0227/2017 -  Hugues Bayet et  Evelyn Regner - Am 61rev",
            "A8-0224/2017 -  Neoklis Sylikiotis - § 31/2",
            "A8-0224/2017 -  Neoklis Sylikiotis - § 45/2"
        ],
        "imena_glasovanj": [
            "ES_EL_40_objava_donacij_polit",
            "ES_EL_41_nadzor_korporacij",
            "ES_EL_50_enako_placilo_za_enako_delo",
            "ES_EL_51_proti_0_ur_pogodbam"
        ]
    },
        {
        "id": 81,
        "datum": "27/04/2017",
        "glasovanja": [
            "A8-0121/2017 -  Georgios Kyrtsos - Am 8",
            "A8-0121/2017 -  Georgios Kyrtsos - Am 5",
            "A8-0121/2017 -  Georgios Kyrtsos - Am 10",
            "A8-0121/2017 -  Georgios Kyrtsos - Am 12",
            "A8-0138/2017 -  Ulrike Rodust - Am 1",
            "A8-0138/2017 -  Ulrike Rodust - Am 1"
        ],
        "imena_glasovanj": [
            "ES_EL_42_max_financna_transparentnost_EIB",
            "ES_EL_43_transadriatski_cevovod_proti",
            "ES_EL_44_ocena_projektov_izven_EU",
            "ES_EL_45_proti_velikim_projektom_ki_ne_koristijo_preb",
            "ES_EL_49_financno_podpreti_tradic_ribolov",
            "SC_SL_49_podpreti_tradicionalni_ribolov"
        ]
    },
        {
        "id": 82,
        "datum": "15/02/2017",
        "glasovanja": [
            "A8-0039/2017 -  Gunnar Hökmark - Am 7",
            "A8-0039/2017 -  Gunnar Hökmark - Am 6",
            "A8-0039/2017 -  Gunnar Hökmark - Am 8",
            "A8-0039/2017 -  Gunnar Hökmark - § 33",
            "A8-0039/2017 -  Gunnar Hökmark - Am 1",
            "A8-0039/2017 -  Gunnar Hökmark - Am 5",
            "A8-0019/2017 -  Danuta Maria Hübner - Am 4",
            "A8-0019/2017 -  Danuta Maria Hübner - Am 6",
            "A8-0037/2017 -  Yana Toom - Am 3",
            "A8-0037/2017 -  Yana Toom - Am 4",
            "A8-0037/2017 -  Yana Toom - Am 2",
            "A8-0037/2017 -  Yana Toom - § 28/2",
            "A8-0009/2017 -  Artis Pabriks - approbation",
            "A8-0023/2017 -  Knut Fleckenstein - Am 1",
            "A8-0023/2017 -  Knut Fleckenstein - § 40"
        ],
        "imena_glasovanj": [
            "SES_ELG_40_proti_sankcijam_ce_drz_prevec_trosi",
            "SES_ELG_41_proti_izsiljevanju_neolib",
            "SES_ELG_42_ocene_fisk_vzdrzljivosti",
            "SE_EG_43_vse_drz_morajo_upostevati_sgp",
            "SES_ELG_43_varcevanje_unicujoce_za_perif_drzave",
            "SE_EG_44_demokr_izhod_iz_enotne_valute",
            "SE_EG_45_komisija_na_nacionalni_ravni",
            "ES_EL_51_omejitev _bank_ glass_steagall_act",
            "ES_EL_56_seznam_plac_naj_sefov",
            "ES_EL_57_univerzalne_penzije_solidarnost",
            "ES_EL_58_okrepit_place_sindikate",
            "ES_EL_59_enako_obravnavanje_zaposlitev",
            "SES_ELG_44_CETA_resolucija_glasovanje",
            "SCS_SLG_46_obsodba_krvnih_fevdov_Alb",
            "SS_SG_47_sankcije_proti_Rusiji"
        ]
    },
        {
        "id": 88,
        "datum": "18/05/2017",
        "glasovanja": [
            "A8-0123/2017 -  Adam Szejnfeld - Am 8",
            "A8-0123/2017 -  Adam Szejnfeld - Am 10",
            "A8-0123/2017 -  Adam Szejnfeld - Am 16",
            "A8-0123/2017 -  Adam Szejnfeld - Am 14",
            "B8-0340/2017 - Résolution"
        ],
        "imena_glasovanj": [
            "ESG_SEL_30_obtozba_korejskega_obravnavanja_sindikatov",
            "ES_EL_54_bolj_podrobno_efekte_prost_sporazuma",
            "SES_SLG_43_ProtiISDS_metodam",
            "ES_EL_46_pustiti_regulacije_pomembne_za_zdravje_okolje",
            "SS_SG_46_prisiliti_uveljavljanje_kvot"
        ]
    },
        {
        "id": 90,
        "datum": "15/06/2017",
        "glasovanja": [
            "RC-B8-0407/2017 - Am 1",
            "RC-B8-0407/2017 - Am 5",
            "RC-B8-0407/2017 - Am 6",
            "RC-B8-0407/2017 - Am 3"
        ],
        "imena_glasovanj": [
            "SC_SL_46_proti_prodaji_orozja_SA",
            "SC_SL_47_embargo_orozje",
            "SC_SL_48_vojni_zlocini_Jemen",
            "SCS_SLG_45_obsodba_savd_arabije"
        ]
    },
        {
        "id": 93,
        "datum": "19/01/2017",
        "glasovanja": [
            "RC-B8-0072/2017 - Am 1"
        ],
        "imena_glasovanj": [
            "SES_ELG_44_free_tr_z_indonezijo_ojacal_multinacionalke"
        ]
    },
        {
        "id": 98,
        "datum": "29/04/2015",
        "glasovanja": [
            "RC-B8-0367/2015 - Am 8",
            "RC-B8-0367/2015 - Am 6",
            "RC-B8-0367/2015 - Am 5",
            "RC-B8-0367/2015 - Am 4",
            "RC-B8-0367/2015 - Am 3",
            "RC-B8-0367/2015 - § 8"
        ],
        "imena_glasovanj": [
            "SCS_SLG_47_izbris_vracanja_mige",
            "SCS_SLG_49_samo_re4sevanje_zivlj",
            "SCS_SLG_50_ni_legalnih_prehodov",
            "SCS_SLG_51_omogocit_vecji_legalni_pritok_migr_v_EU",
            "SCS_SLG_52_brez_viz_za_sirske_begunce",
            "SS_SG_56_kvote_migranti"
        ]
    },
        {
        "id": 99,
        "datum": "28/10/2015",
        "glasovanja": [
            "A8-0305/2015 -  Giovanni La Via - Am 3"
        ],
        "imena_glasovanj": [
            "ES_EL_57_GMO"
        ]
    },
        {
        "id": 101,
        "datum": "09/06/2016",
        "glasovanja": [
            "A8-0156/2016 -  Mady Delvaux - Am 1"
        ],
        "imena_glasovanj": [
            "SS_SG_54_proti_promociji_prostega_pretoka"
        ]
    },
        {
        "id": 102,
        "datum": "13/12/2016",
        "glasovanja": [
            "A8-0345/2016 -  József Nagy - Am 5",
            "A8-0345/2016 -  József Nagy - Am 6",
            "A8-0345/2016 -  József Nagy - Am 7",
            "A8-0345/2016 -  József Nagy - Am 11"
        ],
        "imena_glasovanj": [
            "SCS_SLG_56_nasilje_nad_z_zaradi_imigracije",
            "SCS_SLG_57_podeljevanje_azila_v_regijah_kjer_prihajajo",
            "SCS_SLG_58_skrb_zaradi_mas_imigracije_legitimna",
            "ES_EL_57_pravica_do_zascite_pred_revscino"
        ]
    },
        {
        "id": 103,
        "datum": "28/04/2016",
        "glasovanja": [
            "A8-0141/2016 -  Laura Ferrara - Am 1"
        ],
        "imena_glasovanj": [
            "SC_SL_58_vec_dostopa_javnosti"
        ]
    },
        {
        "id": 104,
        "datum": "12/04/2016",
        "glasovanja": [
            "A8-0021/2016 -  Damian Drăghici - § 4",
            "A8-0021/2016 -  Damian Drăghici - § 22",
            "A8-0021/2016 -  Damian Drăghici - § 13",
            "A8-0066/2016 -  Roberta Metsola et  Kashetu Kyenge - Am 8",
            "A8-0066/2016 -  Roberta Metsola et  Kashetu Kyenge - Am 4",
            "A8-0066/2016 -  Roberta Metsola et  Kashetu Kyenge - Am 23",
            "A8-0066/2016 -  Roberta Metsola et  Kashetu Kyenge - Am 27",
            "A8-0066/2016 -  Roberta Metsola et  Kashetu Kyenge - Am 38",
            "A8-0066/2016 -  Roberta Metsola et  Kashetu Kyenge - § 80",
            "A8-0066/2016 -  Roberta Metsola et  Kashetu Kyenge - § 46",
            "A8-0066/2016 -  Roberta Metsola et  Kashetu Kyenge - § 23"
        ],
        "imena_glasovanj": [
            "SS_SG_67_EU_bolj_integrirana",
            "SS_SG_68_spodbujanjeEUmobilnosti_EU_dimenzije",
            "SS_SG_69_EU_ucenje_zgodovine",
            "SS_SG_70_masovna_migr_nikdar_resitev_dem_vprasanja",
            "SS_SG_71_spet_nac_drzave_meje",
            "SS_SG_72_ne_sem_diskriminirati",
            "SC_SL_73_posebno_za_zenske_migr",
            "SC_SL_74_migranti_pravice",
            "SS_SG_79_schengen_meje",
            "SE_EG_89_prosti_trg_migr_zaposlitev",
            "SS_SG_90_kvote_po_celiEU"
        ]
    },
        {
        "id": 108,
        "datum": "26/11/2014",
        "glasovanja": [
            "A8-0028/2014 -  Kay Swinburne - Résolution législative"
        ],
        "imena_glasovanj": [
            "SE_EG_52_moc_ECB_da_sankcionira"
        ]
    }
];