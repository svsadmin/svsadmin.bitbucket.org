var poslanci = [
  {
    "StrankaId": 1,
    "Priimek": "Mlinar",
    "Id": 6631,
    "glas": null
  },
  {
    "StrankaId": 2,
    "Priimek": "Deprez",
    "Id": 1060,
    "glas": null
  },
  {
    "StrankaId": 2,
    "Priimek": "Ries",
    "Id": 4676,
    "glas": null
  },
  {
    "StrankaId": 2,
    "Priimek": "Michel",
    "Id": 5857,
    "glas": null
  },
  {
    "StrankaId": 3,
    "Priimek": "De Backer",
    "Id": 6226,
    "glas": null
  },
  {
    "StrankaId": 3,
    "Priimek": "Neyts-Uyttebroeck",
    "Id": 3899,
    "glas": null
  },
  {
    "StrankaId": 3,
    "Priimek": "Verhofstadt",
    "Id": 6159,
    "glas": null
  },
  {
    "StrankaId": 3,
    "Priimek": "Vautmans",
    "Id": 6710,
    "glas": null
  },
  {
    "StrankaId": 3,
    "Priimek": "Wierinck",
    "Id": 6729,
    "glas": null
  },
  {
    "StrankaId": 4,
    "Priimek": "Ali",
    "Id": 5682,
    "glas": null
  },
  {
    "StrankaId": 4,
    "Priimek": "Hyusmenova",
    "Id": 5681,
    "glas": null
  },
  {
    "StrankaId": 4,
    "Priimek": "Kyuchyuk",
    "Id": 6504,
    "glas": null
  },
  {
    "StrankaId": 4,
    "Priimek": "Mihaylova",
    "Id": 6690,
    "glas": null
  },
  {
    "StrankaId": 13,
    "Priimek": "Jakovčić",
    "Id": 6388,
    "glas": null
  },
  {
    "StrankaId": 13,
    "Priimek": "Radoš",
    "Id": 6266,
    "glas": null
  },
  {
    "StrankaId": 5,
    "Priimek": "Charanzová",
    "Id": 6339,
    "glas": null
  },
  {
    "StrankaId": 5,
    "Priimek": "Dlabajová",
    "Id": 6340,
    "glas": null
  },
  {
    "StrankaId": 5,
    "Priimek": "Ježek",
    "Id": 6338,
    "glas": null
  },
  {
    "StrankaId": 5,
    "Priimek": "Telička",
    "Id": 6337,
    "glas": null
  },
  {
    "StrankaId": 6,
    "Priimek": "Petersen",
    "Id": 6510,
    "glas": null
  },
  {
    "StrankaId": 6,
    "Priimek": "Rohde",
    "Id": 5890,
    "glas": null
  },
  {
    "StrankaId": 7,
    "Priimek": "Tørnæs",
    "Id": 6518,
    "glas": null
  },
  {
    "StrankaId": 7,
    "Priimek": "Løkkegaard",
    "Id": 5889,
    "glas": null
  },
  {
    "StrankaId": 8,
    "Priimek": "Ansip",
    "Id": 6327,
    "glas": null
  },
  {
    "StrankaId": 8,
    "Priimek": "Kallas",
    "Id": 6328,
    "glas": null
  },
  {
    "StrankaId": 8,
    "Priimek": "Paet",
    "Id": 6705,
    "glas": null
  },
  {
    "StrankaId": 9,
    "Priimek": "Toom",
    "Id": 6331,
    "glas": null
  },
  {
    "StrankaId": 10,
    "Priimek": "Jäätteenmäki",
    "Id": 5433,
    "glas": null
  },
  {
    "StrankaId": 10,
    "Priimek": "Rehn",
    "Id": 4544,
    "glas": null
  },
  {
    "StrankaId": 10,
    "Priimek": "Väyrynen",
    "Id": 4550,
    "glas": null
  },
  {
    "StrankaId": 10,
    "Priimek": "Takkula",
    "Id": 5435,
    "glas": null
  },
  {
    "StrankaId": 11,
    "Priimek": "Torvalds",
    "Id": 6281,
    "glas": null
  },
  {
    "StrankaId": 12,
    "Priimek": "Arthuis",
    "Id": 6407,
    "glas": null
  },
  {
    "StrankaId": 12,
    "Priimek": "Cavada",
    "Id": 5328,
    "glas": null
  },
  {
    "StrankaId": 12,
    "Priimek": "de Sarnez",
    "Id": 4758,
    "glas": null
  },
  {
    "StrankaId": 12,
    "Priimek": "Goulard",
    "Id": 6169,
    "glas": null
  },
  {
    "StrankaId": 12,
    "Priimek": "Griesbeck",
    "Id": 5330,
    "glas": null
  },
  {
    "StrankaId": 12,
    "Priimek": "Riquet",
    "Id": 6056,
    "glas": null
  },
  {
    "StrankaId": 12,
    "Priimek": "Rochefort",
    "Id": 6154,
    "glas": null
  },
  {
    "StrankaId": 14,
    "Priimek": "Harkin",
    "Id": 5246,
    "glas": null
  },
  {
    "StrankaId": 15,
    "Priimek": "Mazuronis",
    "Id": 6402,
    "glas": null
  },
  {
    "StrankaId": 15,
    "Priimek": "Uspaskich",
    "Id": 5879,
    "glas": null
  },
  {
    "StrankaId": 16,
    "Priimek": "Auštrevičius",
    "Id": 6400,
    "glas": null
  },
  {
    "StrankaId": 16,
    "Priimek": "Guoga",
    "Id": 6397,
    "glas": null
  },
  {
    "StrankaId": 17,
    "Priimek": "Goerens",
    "Id": 1793,
    "glas": null
  },
  {
    "StrankaId": 18,
    "Priimek": "Lambsdorff",
    "Id": 5362,
    "glas": null
  },
  {
    "StrankaId": 18,
    "Priimek": "Meissner",
    "Id": 6041,
    "glas": null
  },
  {
    "StrankaId": 18,
    "Priimek": "Theurer",
    "Id": 6042,
    "glas": null
  },
  {
    "StrankaId": 19,
    "Priimek": "Müller",
    "Id": 6500,
    "glas": null
  },
  {
    "StrankaId": 20,
    "Priimek": "Gerbrandy",
    "Id": 6098,
    "glas": null
  },
  {
    "StrankaId": 20,
    "Priimek": "in 't Veld",
    "Id": 5385,
    "glas": null
  },
  {
    "StrankaId": 20,
    "Priimek": "van Miltenburg",
    "Id": 6626,
    "glas": null
  },
  {
    "StrankaId": 20,
    "Priimek": "Schaake",
    "Id": 6100,
    "glas": null
  },
  {
    "StrankaId": 21,
    "Priimek": "van Baalen",
    "Id": 6097,
    "glas": null
  },
  {
    "StrankaId": 21,
    "Priimek": "Huitema",
    "Id": 6622,
    "glas": null
  },
  {
    "StrankaId": 21,
    "Priimek": "van Nieuwenhuizen",
    "Id": 6621,
    "glas": null
  },
  {
    "StrankaId": 22,
    "Priimek": "Marinho e Pinto",
    "Id": 6376,
    "glas": null
  },
  {
    "StrankaId": 23,
    "Priimek": "Diaconu",
    "Id": 6439,
    "glas": null
  },
  {
    "StrankaId": 24,
    "Priimek": "Sulík",
    "Id": 6546,
    "glas": null
  },
  {
    "StrankaId": 25,
    "Priimek": "Vajgl",
    "Id": 6151,
    "glas": null
  },
  {
    "StrankaId": 26,
    "Priimek": "Girauta Vidal",
    "Id": 6658,
    "glas": null
  },
  {
    "StrankaId": 26,
    "Priimek": "Nart",
    "Id": 6614,
    "glas": null
  },
  {
    "StrankaId": 26,
    "Priimek": "Punset",
    "Id": 6728,
    "glas": null
  },
  {
    "StrankaId": 27,
    "Priimek": "Tremosa i Balcells",
    "Id": 6181,
    "glas": null
  },
  {
    "StrankaId": 27,
    "Priimek": "Bilbao Barandica",
    "Id": 6089,
    "glas": null
  },
  {
    "StrankaId": 28,
    "Priimek": "Becerra Basterrechea",
    "Id": 6646,
    "glas": null
  },
  {
    "StrankaId": 28,
    "Priimek": "Calvet Chambon",
    "Id": 6708,
    "glas": null
  },
  {
    "StrankaId": 28,
    "Priimek": "Pagazaurtundúa Ruiz",
    "Id": 6644,
    "glas": null
  },
  {
    "StrankaId": 28,
    "Priimek": "Sosa Wagner",
    "Id": 6174,
    "glas": null
  },
  {
    "StrankaId": 28,
    "Priimek": "Maura Barandiarán",
    "Id": 6645,
    "glas": null
  },
  {
    "StrankaId": 29,
    "Priimek": "Federley",
    "Id": 6601,
    "glas": null
  },
  {
    "StrankaId": 30,
    "Priimek": "Paulsen",
    "Id": 4693,
    "glas": null
  },
  {
    "StrankaId": 30,
    "Priimek": "Wikström",
    "Id": 5864,
    "glas": null
  },
  {
    "StrankaId": 30,
    "Priimek": "Selimovic",
    "Id": 6722,
    "glas": null
  },
  {
    "StrankaId": 31,
    "Priimek": "Bearder",
    "Id": 6110,
    "glas": null
  },
  {
    "StrankaId": 32,
    "Priimek": "Demesmaeker",
    "Id": 6291,
    "glas": null
  },
  {
    "StrankaId": 32,
    "Priimek": "Ide",
    "Id": 6685,
    "glas": null
  },
  {
    "StrankaId": 32,
    "Priimek": "Stevens",
    "Id": 6682,
    "glas": null
  },
  {
    "StrankaId": 32,
    "Priimek": "Van Overtveldt",
    "Id": 6683,
    "glas": null
  },
  {
    "StrankaId": 32,
    "Priimek": "Loones",
    "Id": 6704,
    "glas": null
  },
  {
    "StrankaId": 32,
    "Priimek": "Van Bossuyt",
    "Id": 6711,
    "glas": null
  },
  {
    "StrankaId": 33,
    "Priimek": "Barekov",
    "Id": 6509,
    "glas": null
  },
  {
    "StrankaId": 33,
    "Priimek": "Dzhambazki",
    "Id": 6511,
    "glas": null
  },
  {
    "StrankaId": 34,
    "Priimek": "Tošenovský",
    "Id": 5892,
    "glas": null
  },
  {
    "StrankaId": 34,
    "Priimek": "Zahradil",
    "Id": 5052,
    "glas": null
  },
  {
    "StrankaId": 35,
    "Priimek": "Messerschmidt",
    "Id": 5851,
    "glas": null
  },
  {
    "StrankaId": 35,
    "Priimek": "Karlsson",
    "Id": 6514,
    "glas": null
  },
  {
    "StrankaId": 35,
    "Priimek": "Vistisen",
    "Id": 6513,
    "glas": null
  },
  {
    "StrankaId": 35,
    "Priimek": "Dohrmann",
    "Id": 6516,
    "glas": null
  },
  {
    "StrankaId": 36,
    "Priimek": "Halla-aho",
    "Id": 6358,
    "glas": null
  },
  {
    "StrankaId": 36,
    "Priimek": "Terho",
    "Id": 6220,
    "glas": null
  },
  {
    "StrankaId": 36,
    "Priimek": "Ruohonen-Lerner",
    "Id": 6718,
    "glas": null
  },
  {
    "StrankaId": 37,
    "Priimek": "Marias",
    "Id": 6667,
    "glas": null
  },
  {
    "StrankaId": 38,
    "Priimek": "Tomašić",
    "Id": 6300,
    "glas": null
  },
  {
    "StrankaId": 39,
    "Priimek": "Crowley",
    "Id": "",
    "glas": null
  },
  {
    "StrankaId": 40,
    "Priimek": "Zīle",
    "Id": 5598,
    "glas": null
  },
  {
    "StrankaId": 41,
    "Priimek": "Tomaševski",
    "Id": 5878,
    "glas": null
  },
  {
    "StrankaId": 42,
    "Priimek": "Henkel",
    "Id": 6460,
    "glas": null
  },
  {
    "StrankaId": 42,
    "Priimek": "Kölmel",
    "Id": 6461,
    "glas": null
  },
  {
    "StrankaId": 42,
    "Priimek": "Lucke",
    "Id": 6457,
    "glas": null
  },
  {
    "StrankaId": 42,
    "Priimek": "Starbatty",
    "Id": 6464,
    "glas": null
  },
  {
    "StrankaId": 42,
    "Priimek": "Trebesius",
    "Id": 6466,
    "glas": null
  },
  {
    "StrankaId": 42,
    "Priimek": "Pretzell",
    "Id": 6467,
    "glas": null
  },
  {
    "StrankaId": 42,
    "Priimek": "von Storch",
    "Id": 6462,
    "glas": null
  },
  {
    "StrankaId": 43,
    "Priimek": "Gericke",
    "Id": 6452,
    "glas": null
  },
  {
    "StrankaId": 44,
    "Priimek": "Belder",
    "Id": 4911,
    "glas": null
  },
  {
    "StrankaId": 44,
    "Priimek": "van Dalen",
    "Id": 5981,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Czarnecki",
    "Id": 5490,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Duda",
    "Id": 6538,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Fotyga",
    "Id": 5472,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Gosiewska",
    "Id": 6539,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Gróbarczyk",
    "Id": 5963,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Jackiewicz",
    "Id": 6520,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Jurek",
    "Id": 6531,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Karski",
    "Id": 6525,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Krasnodębski",
    "Id": 6530,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Kuźmiuk",
    "Id": 5507,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Legutko",
    "Id": 5969,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Ożóg",
    "Id": 6536,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Piecha",
    "Id": 6512,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Piotrowski",
    "Id": 5460,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Poręba",
    "Id": 5974,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Ujazdowski",
    "Id": 5113,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Wiśniewska",
    "Id": 6515,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Wojciechowski",
    "Id": 5108,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Złotowski",
    "Id": 6522,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Czesak",
    "Id": 6719,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Hoc",
    "Id": 6725,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Krupa",
    "Id": 5453,
    "glas": null
  },
  {
    "StrankaId": 45,
    "Priimek": "Kłosowski",
    "Id": 6726,
    "glas": null
  },
  {
    "StrankaId": 46,
    "Priimek": "Žitňanská",
    "Id": 6540,
    "glas": null
  },
  {
    "StrankaId": 47,
    "Priimek": "Škripek",
    "Id": 6545,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Ashworth",
    "Id": 5262,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Bashir",
    "Id": 6574,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Bradbourn",
    "Id": 4944,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Campbell Bannerman",
    "Id": 6081,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Deva",
    "Id": 4960,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Duncan",
    "Id": 6579,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Ford",
    "Id": 6104,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Foster",
    "Id": 4957,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Fox",
    "Id": 6112,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Girling",
    "Id": 6111,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Hannan",
    "Id": 4959,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Kamall",
    "Id": 5630,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Karim",
    "Id": 5563,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Kirkhope",
    "Id": 4946,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Lewer",
    "Id": 6561,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "McClarkin",
    "Id": 6087,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "McIntyre",
    "Id": 6234,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Swinburne",
    "Id": 6088,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Tannock",
    "Id": 4925,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Van Orden",
    "Id": 4917,
    "glas": null
  },
  {
    "StrankaId": 48,
    "Priimek": "Procter",
    "Id": 6736,
    "glas": null
  },
  {
    "StrankaId": 49,
    "Priimek": "Nicholson",
    "Id": 1230,
    "glas": null
  },
  {
    "StrankaId": 50,
    "Priimek": "Mach",
    "Id": 6448,
    "glas": null
  },
  {
    "StrankaId": 51,
    "Priimek": "Bergeron",
    "Id": 6374,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Adinolfi",
    "Id": 6468,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Agea",
    "Id": 6445,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Aiuto",
    "Id": 6480,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Beghin",
    "Id": 6411,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Borrelli",
    "Id": 6430,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Castaldo",
    "Id": 6446,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Corrao",
    "Id": 6494,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "D'Amato",
    "Id": 6472,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Evi",
    "Id": 6413,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Ferrara",
    "Id": 6470,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Moi",
    "Id": 6497,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Pedicini",
    "Id": 6482,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Tamburrano",
    "Id": 6447,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Valli",
    "Id": 6412,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Zullo",
    "Id": 6695,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Affronte",
    "Id": 6431,
    "glas": null
  },
  {
    "StrankaId": 52,
    "Priimek": "Zanni",
    "Id": 6414,
    "glas": null
  },
  {
    "StrankaId": 53,
    "Priimek": "Grigule",
    "Id": 6379,
    "glas": null
  },
  {
    "StrankaId": 54,
    "Priimek": "Paksas",
    "Id": 5875,
    "glas": null
  },
  {
    "StrankaId": 55,
    "Priimek": "Lundgren",
    "Id": 6608,
    "glas": null
  },
  {
    "StrankaId": 55,
    "Priimek": "Winberg",
    "Id": 6607,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Agnew",
    "Id": 6066,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Aker",
    "Id": 6559,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Arnott",
    "Id": 6577,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Batten",
    "Id": 5567,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Bours",
    "Id": 6583,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Coburn",
    "Id": 6586,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Carver",
    "Id": 6590,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Collins",
    "Id": 6573,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "(The Earl of) Dartmouth",
    "Id": 6113,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Farage",
    "Id": 4929,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Finch",
    "Id": 6558,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Gill Nathan",
    "Id": 6584,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Helmer",
    "Id": 4920,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Hookem",
    "Id": 6575,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Nuttall",
    "Id": 5977,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "O'Flynn",
    "Id": 6557,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Parker",
    "Id": 6563,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Reid",
    "Id": 6564,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Seymour",
    "Id": 6568,
    "glas": null
  },
  {
    "StrankaId": 57,
    "Priimek": "Becker",
    "Id": 6216,
    "glas": null
  },
  {
    "StrankaId": 57,
    "Priimek": "Karas",
    "Id": 4669,
    "glas": null
  },
  {
    "StrankaId": 57,
    "Priimek": "Köstinger",
    "Id": 6053,
    "glas": null
  },
  {
    "StrankaId": 57,
    "Priimek": "Rübig",
    "Id": 4596,
    "glas": null
  },
  {
    "StrankaId": 57,
    "Priimek": "Schmidt",
    "Id": 6619,
    "glas": null
  },
  {
    "StrankaId": 58,
    "Priimek": "Belet",
    "Id": 5377,
    "glas": null
  },
  {
    "StrankaId": 58,
    "Priimek": "Thyssen",
    "Id": 3738,
    "glas": null
  },
  {
    "StrankaId": 58,
    "Priimek": "Vandenkendelaere",
    "Id": 6706,
    "glas": null
  },
  {
    "StrankaId": 59,
    "Priimek": "Rolin",
    "Id": 6684,
    "glas": null
  },
  {
    "StrankaId": 60,
    "Priimek": "Arimont",
    "Id": 6594,
    "glas": null
  },
  {
    "StrankaId": 61,
    "Priimek": "Donchev",
    "Id": 6481,
    "glas": null
  },
  {
    "StrankaId": 61,
    "Priimek": "Gabriel",
    "Id": 6020,
    "glas": null
  },
  {
    "StrankaId": 61,
    "Priimek": "Kovatchev",
    "Id": 6196,
    "glas": null
  },
  {
    "StrankaId": 61,
    "Priimek": "Maydell",
    "Id": 6483,
    "glas": null
  },
  {
    "StrankaId": 61,
    "Priimek": "Radev",
    "Id": 6488,
    "glas": null
  },
  {
    "StrankaId": 61,
    "Priimek": "Urutchev",
    "Id": 5734,
    "glas": null
  },
  {
    "StrankaId": 61,
    "Priimek": "Novakov",
    "Id": 6709,
    "glas": null
  },
  {
    "StrankaId": 62,
    "Priimek": "Malinov",
    "Id": 6245,
    "glas": null
  },
  {
    "StrankaId": 63,
    "Priimek": "Stylianides",
    "Id": 6320,
    "glas": null
  },
  {
    "StrankaId": 63,
    "Priimek": "Christoforou",
    "Id": 5236,
    "glas": null
  },
  {
    "StrankaId": 63,
    "Priimek": "Theocharous",
    "Id": 5207,
    "glas": null
  },
  {
    "StrankaId": 64,
    "Priimek": "Niedermayer",
    "Id": 6332,
    "glas": null
  },
  {
    "StrankaId": 64,
    "Priimek": "Polčák",
    "Id": 6335,
    "glas": null
  },
  {
    "StrankaId": 64,
    "Priimek": "Pospíšil",
    "Id": 6700,
    "glas": null
  },
  {
    "StrankaId": 64,
    "Priimek": "Štětina",
    "Id": 6333,
    "glas": null
  },
  {
    "StrankaId": 65,
    "Priimek": "Šojdrová",
    "Id": 6341,
    "glas": null
  },
  {
    "StrankaId": 65,
    "Priimek": "Svoboda",
    "Id": 6672,
    "glas": null
  },
  {
    "StrankaId": 65,
    "Priimek": "Zdechovský",
    "Id": 6344,
    "glas": null
  },
  {
    "StrankaId": 66,
    "Priimek": "Bendtsen",
    "Id": 5885,
    "glas": null
  },
  {
    "StrankaId": 67,
    "Priimek": "Kelam",
    "Id": 5542,
    "glas": null
  },
  {
    "StrankaId": 68,
    "Priimek": "Pietikäinen",
    "Id": 5788,
    "glas": null
  },
  {
    "StrankaId": 68,
    "Priimek": "Sarvamaa",
    "Id": 6259,
    "glas": null
  },
  {
    "StrankaId": 68,
    "Priimek": "Virkkunen",
    "Id": 6356,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Alliot-Marie",
    "Id": 1340,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Cadec",
    "Id": 6021,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Danjean",
    "Id": 5921,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Dantin",
    "Id": 6194,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Dati",
    "Id": 6172,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Delahaye",
    "Id": 6364,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Grossetête",
    "Id": 4032,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Hortefeux",
    "Id": 4987,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Joulaud",
    "Id": 6360,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Juvin",
    "Id": 6055,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Lamassoure",
    "Id": 1350,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Lavrilleux",
    "Id": 6363,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Le Grip",
    "Id": 6200,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Morano",
    "Id": 6368,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Morin-Chartier",
    "Id": 5732,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Muselier",
    "Id": 6361,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Ponga",
    "Id": 6092,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Proust",
    "Id": 6224,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Saïfi",
    "Id": 4768,
    "glas": null
  },
  {
    "StrankaId": 69,
    "Priimek": "Sander",
    "Id": 6370,
    "glas": null
  },
  {
    "StrankaId": 70,
    "Priimek": "Kefalogiannis",
    "Id": 6666,
    "glas": null
  },
  {
    "StrankaId": 70,
    "Priimek": "Kyrtsos",
    "Id": 6662,
    "glas": null
  },
  {
    "StrankaId": 70,
    "Priimek": "Spyraki",
    "Id": 6663,
    "glas": null
  },
  {
    "StrankaId": 70,
    "Priimek": "Vozemberg",
    "Id": 6664,
    "glas": null
  },
  {
    "StrankaId": 70,
    "Priimek": "Zagorakis",
    "Id": 6665,
    "glas": null
  },
  {
    "StrankaId": 71,
    "Priimek": "Maletić",
    "Id": 6303,
    "glas": null
  },
  {
    "StrankaId": 71,
    "Priimek": "Petir",
    "Id": 6383,
    "glas": null
  },
  {
    "StrankaId": 71,
    "Priimek": "Plenković",
    "Id": 6268,
    "glas": null
  },
  {
    "StrankaId": 71,
    "Priimek": "Stier",
    "Id": 6302,
    "glas": null
  },
  {
    "StrankaId": 71,
    "Priimek": "Šuica",
    "Id": 6301,
    "glas": null
  },
  {
    "StrankaId": 71,
    "Priimek": "Tolić",
    "Id": 6733,
    "glas": null
  },
  {
    "StrankaId": 71,
    "Priimek": "Zovko",
    "Id": 6734,
    "glas": null
  },
  {
    "StrankaId": 72,
    "Priimek": "Clune",
    "Id": 6600,
    "glas": null
  },
  {
    "StrankaId": 72,
    "Priimek": "Hayes",
    "Id": 6596,
    "glas": null
  },
  {
    "StrankaId": 72,
    "Priimek": "Kelly",
    "Id": 5855,
    "glas": null
  },
  {
    "StrankaId": 72,
    "Priimek": "McGuinness",
    "Id": 5245,
    "glas": null
  },
  {
    "StrankaId": 73,
    "Priimek": "Cesa",
    "Id": 5554,
    "glas": null
  },
  {
    "StrankaId": 73,
    "Priimek": "La Via",
    "Id": 5988,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Cicu",
    "Id": 6492,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Cirio",
    "Id": 6406,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Comi",
    "Id": 5948,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Gardini",
    "Id": 5803,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Martusciello",
    "Id": 6465,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Matera",
    "Id": 5985,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Mussolini",
    "Id": 5547,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Patriciello",
    "Id": 5702,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Pogliese",
    "Id": 6491,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Salini",
    "Id": 6698,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Tajani",
    "Id": 4482,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Toti",
    "Id": 6398,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Maullu",
    "Id": 6721,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Fitto",
    "Id": 4880,
    "glas": null
  },
  {
    "StrankaId": 74,
    "Priimek": "Sernagiotto",
    "Id": 6428,
    "glas": null
  },
  {
    "StrankaId": 75,
    "Priimek": "Dorfmann",
    "Id": 5960,
    "glas": null
  },
  {
    "StrankaId": 76,
    "Priimek": "Dombrovskis",
    "Id": 5601,
    "glas": null
  },
  {
    "StrankaId": 76,
    "Priimek": "Kalniete",
    "Id": 6095,
    "glas": null
  },
  {
    "StrankaId": 76,
    "Priimek": "Kariņš",
    "Id": 6070,
    "glas": null
  },
  {
    "StrankaId": 76,
    "Priimek": "Pabriks",
    "Id": 6377,
    "glas": null
  },
  {
    "StrankaId": 76,
    "Priimek": "Vaidere",
    "Id": 5600,
    "glas": null
  },
  {
    "StrankaId": 77,
    "Priimek": "Landsbergis",
    "Id": 6393,
    "glas": null
  },
  {
    "StrankaId": 77,
    "Priimek": "Saudargas",
    "Id": 5877,
    "glas": null
  },
  {
    "StrankaId": 77,
    "Priimek": "Andrikienė",
    "Id": 5395,
    "glas": null
  },
  {
    "StrankaId": 78,
    "Priimek": "Bach",
    "Id": 6191,
    "glas": null
  },
  {
    "StrankaId": 78,
    "Priimek": "Engel",
    "Id": 6047,
    "glas": null
  },
  {
    "StrankaId": 78,
    "Priimek": "Reding",
    "Id": 1335,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Bocskor",
    "Id": 6343,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Deli",
    "Id": 6345,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Deutsch",
    "Id": 5998,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Erdős",
    "Id": 6342,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Gál",
    "Id": 5280,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Gáll-Pelcz",
    "Id": 6202,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Gyürk",
    "Id": 5138,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Hölvényi",
    "Id": 6346,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Kósa",
    "Id": 6001,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Schöpflin",
    "Id": 5265,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Szájer",
    "Id": 5143,
    "glas": null
  },
  {
    "StrankaId": 79,
    "Priimek": "Tőkés",
    "Id": 5783,
    "glas": null
  },
  {
    "StrankaId": 80,
    "Priimek": "Casa",
    "Id": 5252,
    "glas": null
  },
  {
    "StrankaId": 80,
    "Priimek": "Comodini Cachia",
    "Id": 6587,
    "glas": null
  },
  {
    "StrankaId": 80,
    "Priimek": "Metsola",
    "Id": 6297,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Balz",
    "Id": 6135,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Böge",
    "Id": 1507,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Brok",
    "Id": 1278,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Caspary",
    "Id": 5341,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Collin-Langen",
    "Id": 6273,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Deß",
    "Id": 5350,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Ehler",
    "Id": 5348,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Ferber",
    "Id": 3833,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Florenz",
    "Id": 1508,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Gahler",
    "Id": 4659,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Gieseke",
    "Id": 6441,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Gräßle",
    "Id": 5342,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Hohlmeier",
    "Id": 5953,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Jahr",
    "Id": 5945,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Koch",
    "Id": 3759,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Kuhn",
    "Id": 5940,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Langen",
    "Id": 3856,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Liese",
    "Id": 3855,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Lins",
    "Id": 6442,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "McAllister",
    "Id": 6440,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Mann",
    "Id": 3845,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Niebler",
    "Id": 4712,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Pieper",
    "Id": 5346,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Quisthoudt-Rowohl",
    "Id": 1487,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Reul",
    "Id": 5347,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Schulze",
    "Id": 6443,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Schwab",
    "Id": 5345,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Sommer",
    "Id": 4705,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Verheyen",
    "Id": 5930,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Voss",
    "Id": 5934,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Weber Manfred",
    "Id": 5351,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Wieland",
    "Id": 4643,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Winkler Hermann",
    "Id": 5937,
    "glas": null
  },
  {
    "StrankaId": 81,
    "Priimek": "Zeller",
    "Id": 5935,
    "glas": null
  },
  {
    "StrankaId": 82,
    "Priimek": "Lenaers",
    "Id": 6592,
    "glas": null
  },
  {
    "StrankaId": 82,
    "Priimek": "Schreijer-Pierik",
    "Id": 6636,
    "glas": null
  },
  {
    "StrankaId": 82,
    "Priimek": "de Lange",
    "Id": 5727,
    "glas": null
  },
  {
    "StrankaId": 82,
    "Priimek": "van Nistelrooij",
    "Id": 5295,
    "glas": null
  },
  {
    "StrankaId": 82,
    "Priimek": "van de Camp",
    "Id": 5928,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Boni",
    "Id": 6535,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Buzek",
    "Id": 5388,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Hübner",
    "Id": 5952,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Jazłowiecka",
    "Id": 5954,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Kozłowska-Rajewicz",
    "Id": 6527,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Kudrycka",
    "Id": 5403,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Lewandowski",
    "Id": 5105,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Łukacijewska",
    "Id": 5964,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Olbrycht",
    "Id": 5407,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Pitera",
    "Id": 6537,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Plura",
    "Id": 6519,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Rosati",
    "Id": 5512,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Saryusz-Wolski",
    "Id": 5416,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Szejnfeld",
    "Id": 6528,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Thun und Hohenstein",
    "Id": 5949,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Wałęsa",
    "Id": 5947,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Wenta",
    "Id": 6542,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Zdrojewski",
    "Id": 6532,
    "glas": null
  },
  {
    "StrankaId": 83,
    "Priimek": "Zwiefka",
    "Id": 5420,
    "glas": null
  },
  {
    "StrankaId": 84,
    "Priimek": "Grzyb",
    "Id": 5109,
    "glas": null
  },
  {
    "StrankaId": 84,
    "Priimek": "Hetman",
    "Id": 6534,
    "glas": null
  },
  {
    "StrankaId": 84,
    "Priimek": "Kalinowski",
    "Id": 5944,
    "glas": null
  },
  {
    "StrankaId": 84,
    "Priimek": "Siekierski",
    "Id": 5111,
    "glas": null
  },
  {
    "StrankaId": 85,
    "Priimek": "Coelho",
    "Id": 3800,
    "glas": null
  },
  {
    "StrankaId": 85,
    "Priimek": "Fernandes",
    "Id": 6068,
    "glas": null
  },
  {
    "StrankaId": 85,
    "Priimek": "Monteiro de Aguiar",
    "Id": 6366,
    "glas": null
  },
  {
    "StrankaId": 85,
    "Priimek": "Rangel",
    "Id": 6072,
    "glas": null
  },
  {
    "StrankaId": 85,
    "Priimek": "Ruas",
    "Id": 6362,
    "glas": null
  },
  {
    "StrankaId": 85,
    "Priimek": "Faria",
    "Id": 6679,
    "glas": null
  },
  {
    "StrankaId": 85,
    "Priimek": "Melo",
    "Id": 6121,
    "glas": null
  },
  {
    "StrankaId": 85,
    "Priimek": "Ribeiro",
    "Id": 6365,
    "glas": null
  },
  {
    "StrankaId": 86,
    "Priimek": "Buda",
    "Id": 6617,
    "glas": null
  },
  {
    "StrankaId": 86,
    "Priimek": "Stolojan",
    "Id": 5778,
    "glas": null
  },
  {
    "StrankaId": 86,
    "Priimek": "Ungureanu",
    "Id": 6007,
    "glas": null
  },
  {
    "StrankaId": 86,
    "Priimek": "Marinescu",
    "Id": 5646,
    "glas": null
  },
  {
    "StrankaId": 86,
    "Priimek": "Macovei",
    "Id": 5996,
    "glas": null
  },
  {
    "StrankaId": 87,
    "Priimek": "Mureşan",
    "Id": 6436,
    "glas": null
  },
  {
    "StrankaId": 87,
    "Priimek": "Preda",
    "Id": 6010,
    "glas": null
  },
  {
    "StrankaId": 88,
    "Priimek": "Buşoi",
    "Id": 5729,
    "glas": null
  },
  {
    "StrankaId": 88,
    "Priimek": "Hellvig",
    "Id": 5715,
    "glas": null
  },
  {
    "StrankaId": 88,
    "Priimek": "Vălean",
    "Id": 5714,
    "glas": null
  },
  {
    "StrankaId": 88,
    "Priimek": "Ţurcanu",
    "Id": 6717,
    "glas": null
  },
  {
    "StrankaId": 88,
    "Priimek": "Mănescu",
    "Id": 5774,
    "glas": null
  },
  {
    "StrankaId": 88,
    "Priimek": "Weber Renate",
    "Id": 5770,
    "glas": null
  },
  {
    "StrankaId": 88,
    "Priimek": "Nicolai",
    "Id": 5997,
    "glas": null
  },
  {
    "StrankaId": 89,
    "Priimek": "Winkler Iuliu",
    "Id": 5782,
    "glas": null
  },
  {
    "StrankaId": 89,
    "Priimek": "Sógor",
    "Id": 5781,
    "glas": null
  },
  {
    "StrankaId": 90,
    "Priimek": "Mikolášik",
    "Id": 5308,
    "glas": null
  },
  {
    "StrankaId": 90,
    "Priimek": "Záborská",
    "Id": 5189,
    "glas": null
  },
  {
    "StrankaId": 91,
    "Priimek": "Nagy",
    "Id": 6544,
    "glas": null
  },
  {
    "StrankaId": 92,
    "Priimek": "Štefanec",
    "Id": 6547,
    "glas": null
  },
  {
    "StrankaId": 92,
    "Priimek": "Kukan",
    "Id": 5840,
    "glas": null
  },
  {
    "StrankaId": 93,
    "Priimek": "Csáky",
    "Id": 6548,
    "glas": null
  },
  {
    "StrankaId": 94,
    "Priimek": "Bogovič",
    "Id": 6613,
    "glas": null
  },
  {
    "StrankaId": 94,
    "Priimek": "Peterle",
    "Id": 5033,
    "glas": null
  },
  {
    "StrankaId": 95,
    "Priimek": "Tomc",
    "Id": 6681,
    "glas": null
  },
  {
    "StrankaId": 95,
    "Priimek": "Zver",
    "Id": 6094,
    "glas": null
  },
  {
    "StrankaId": 95,
    "Priimek": "Šulin",
    "Id": 6680,
    "glas": null
  },
  {
    "StrankaId": 96,
    "Priimek": "Gambús",
    "Id": 6615,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Arias Cañete",
    "Id": 1178,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Ayuso",
    "Id": 4742,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Díaz de Mera García Consuegra",
    "Id": 5515,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Estaràs Ferragut",
    "Id": 5983,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Fisas Ayxelà",
    "Id": 5903,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "González Pons",
    "Id": 6632,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "del Castillo Vera",
    "Id": 5508,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "de Grandes Pascual",
    "Id": 5511,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Herranz García",
    "Id": 5018,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Jiménez-Becerril Barrio",
    "Id": 5978,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Lope Fontagné",
    "Id": 5938,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Mato",
    "Id": 6096,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Millán Mon",
    "Id": 5518,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Zalba Bidegain",
    "Id": 5936,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "López-Istúriz White",
    "Id": 5517,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Valcárcel Siso",
    "Id": 6638,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Zalba Bidegain",
    "Id": 5936,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Iturgaiz",
    "Id": 5516,
    "glas": null
  },
  {
    "StrankaId": 97,
    "Priimek": "Salafranca Sánchez-Neyra",
    "Id": 3991,
    "glas": null
  },
  {
    "StrankaId": 98,
    "Priimek": "Adaktusson",
    "Id": 6602,
    "glas": null
  },
  {
    "StrankaId": 99,
    "Priimek": "Hökmark",
    "Id": 5254,
    "glas": null
  },
  {
    "StrankaId": 99,
    "Priimek": "Corazza Bildt",
    "Id": 5861,
    "glas": null
  },
  {
    "StrankaId": 99,
    "Priimek": "Fjellner",
    "Id": 5256,
    "glas": null
  },
  {
    "StrankaId": 100,
    "Priimek": "Lunacek",
    "Id": 6149,
    "glas": null
  },
  {
    "StrankaId": 100,
    "Priimek": "Vana",
    "Id": 6551,
    "glas": null
  },
  {
    "StrankaId": 100,
    "Priimek": "Reimon",
    "Id": 6552,
    "glas": null
  },
  {
    "StrankaId": 101,
    "Priimek": "Lamberts",
    "Id": 5838,
    "glas": null
  },
  {
    "StrankaId": 102,
    "Priimek": "Staes",
    "Id": 4983,
    "glas": null
  },
  {
    "StrankaId": 103,
    "Priimek": "Auken",
    "Id": 5291,
    "glas": null
  },
  {
    "StrankaId": 104,
    "Priimek": "Tarand",
    "Id": 6168,
    "glas": null
  },
  {
    "StrankaId": 105,
    "Priimek": "Hautala",
    "Id": 4535,
    "glas": null
  },
  {
    "StrankaId": 106,
    "Priimek": "Bové",
    "Id": 5918,
    "glas": null
  },
  {
    "StrankaId": 106,
    "Priimek": "Delli",
    "Id": 6040,
    "glas": null
  },
  {
    "StrankaId": 106,
    "Priimek": "Durand",
    "Id": 6324,
    "glas": null
  },
  {
    "StrankaId": 106,
    "Priimek": "Jadot",
    "Id": 5914,
    "glas": null
  },
  {
    "StrankaId": 106,
    "Priimek": "Joly",
    "Id": 6054,
    "glas": null
  },
  {
    "StrankaId": 106,
    "Priimek": "Rivasi",
    "Id": 5917,
    "glas": null
  },
  {
    "StrankaId": 107,
    "Priimek": "Škrlec",
    "Id": 6390,
    "glas": null
  },
  {
    "StrankaId": 108,
    "Priimek": "Ždanoka",
    "Id": 5602,
    "glas": null
  },
  {
    "StrankaId": 109,
    "Priimek": "Ropé",
    "Id": 6694,
    "glas": null
  },
  {
    "StrankaId": 110,
    "Priimek": "Turmes",
    "Id": 4847,
    "glas": null
  },
  {
    "StrankaId": 111,
    "Priimek": "Jávor",
    "Id": 6350,
    "glas": null
  },
  {
    "StrankaId": 112,
    "Priimek": "Meszerics",
    "Id": 6349,
    "glas": null
  },
  {
    "StrankaId": 113,
    "Priimek": "Bütikofer",
    "Id": 5913,
    "glas": null
  },
  {
    "StrankaId": 113,
    "Priimek": "Cramer",
    "Id": 5358,
    "glas": null
  },
  {
    "StrankaId": 113,
    "Priimek": "Giegold",
    "Id": 5904,
    "glas": null
  },
  {
    "StrankaId": 113,
    "Priimek": "Harms",
    "Id": 5353,
    "glas": null
  },
  {
    "StrankaId": 113,
    "Priimek": "Keller Ska",
    "Id": 5908,
    "glas": null
  },
  {
    "StrankaId": 113,
    "Priimek": "Lochbihler",
    "Id": 5902,
    "glas": null
  },
  {
    "StrankaId": 113,
    "Priimek": "Albrecht",
    "Id": 5910,
    "glas": null
  },
  {
    "StrankaId": 113,
    "Priimek": "Heubuch",
    "Id": 6486,
    "glas": null
  },
  {
    "StrankaId": 113,
    "Priimek": "Häusling",
    "Id": 5926,
    "glas": null
  },
  {
    "StrankaId": 113,
    "Priimek": "Reintke",
    "Id": 6529,
    "glas": null
  },
  {
    "StrankaId": 113,
    "Priimek": "Trüpel",
    "Id": 5360,
    "glas": null
  },
  {
    "StrankaId": 114,
    "Priimek": "Buchner",
    "Id": 6455,
    "glas": null
  },
  {
    "StrankaId": 115,
    "Priimek": "Reda",
    "Id": 6453,
    "glas": null
  },
  {
    "StrankaId": 116,
    "Priimek": "Eickhout",
    "Id": 5899,
    "glas": null
  },
  {
    "StrankaId": 116,
    "Priimek": "Sargentini",
    "Id": 5987,
    "glas": null
  },
  {
    "StrankaId": 117,
    "Priimek": "Šoltes",
    "Id": 6612,
    "glas": null
  },
  {
    "StrankaId": 118,
    "Priimek": "Terricabras",
    "Id": 6549,
    "glas": null
  },
  {
    "StrankaId": 118,
    "Priimek": "Solé",
    "Id": 6738,
    "glas": null
  },
  {
    "StrankaId": 118,
    "Priimek": "Maragall",
    "Id": 6550,
    "glas": null
  },
  {
    "StrankaId": 119,
    "Priimek": "Urtasun",
    "Id": 6591,
    "glas": null
  },
  {
    "StrankaId": 120,
    "Priimek": "Sebastià",
    "Id": 6659,
    "glas": null
  },
  {
    "StrankaId": 120,
    "Priimek": "Marcellesi",
    "Id": 6732,
    "glas": null
  },
  {
    "StrankaId": 121,
    "Priimek": "Andersson",
    "Id": 6606,
    "glas": null
  },
  {
    "StrankaId": 121,
    "Priimek": "Ceballos",
    "Id": 6605,
    "glas": null
  },
  {
    "StrankaId": 121,
    "Priimek": "Eriksson",
    "Id": 6554,
    "glas": null
  },
  {
    "StrankaId": 121,
    "Priimek": "Lövin",
    "Id": 5862,
    "glas": null
  },
  {
    "StrankaId": 121,
    "Priimek": "Engström",
    "Id": 6703,
    "glas": null
  },
  {
    "StrankaId": 121,
    "Priimek": "Dalunde",
    "Id": 6730,
    "glas": null
  },
  {
    "StrankaId": 122,
    "Priimek": "Lambert",
    "Id": 4935,
    "glas": null
  },
  {
    "StrankaId": 122,
    "Priimek": "Scott Cato",
    "Id": 6560,
    "glas": null
  },
  {
    "StrankaId": 122,
    "Priimek": "Taylor",
    "Id": 6204,
    "glas": null
  },
  {
    "StrankaId": 123,
    "Priimek": "Evans",
    "Id": 4954,
    "glas": null
  },
  {
    "StrankaId": 124,
    "Priimek": "Hudghton",
    "Id": 4655,
    "glas": null
  },
  {
    "StrankaId": 124,
    "Priimek": "Smith",
    "Id": 5571,
    "glas": null
  },
  {
    "StrankaId": 125,
    "Priimek": "Hadjigeorgiou",
    "Id": 6076,
    "glas": null
  },
  {
    "StrankaId": 125,
    "Priimek": "Sylikiotis",
    "Id": 6321,
    "glas": null
  },
  {
    "StrankaId": 126,
    "Priimek": "Konečná",
    "Id": 5039,
    "glas": null
  },
  {
    "StrankaId": 126,
    "Priimek": "Maštálka",
    "Id": 5044,
    "glas": null
  },
  {
    "StrankaId": 126,
    "Priimek": "Ransdorf",
    "Id": 5046,
    "glas": null
  },
  {
    "StrankaId": 126,
    "Priimek": "Kohlíček",
    "Id": 5450,
    "glas": null
  },
  {
    "StrankaId": 127,
    "Priimek": "Kari",
    "Id": 6313,
    "glas": null
  },
  {
    "StrankaId": 128,
    "Priimek": "Kyllönen",
    "Id": 6369,
    "glas": null
  },
  {
    "StrankaId": 129,
    "Priimek": "Le Hyaric",
    "Id": 5909,
    "glas": null
  },
  {
    "StrankaId": 129,
    "Priimek": "Mélenchon",
    "Id": 5916,
    "glas": null
  },
  {
    "StrankaId": 129,
    "Priimek": "Omarjee",
    "Id": 6250,
    "glas": null
  },
  {
    "StrankaId": 130,
    "Priimek": "Vergiat",
    "Id": 6030,
    "glas": null
  },
  {
    "StrankaId": 131,
    "Priimek": "Chrysogonos",
    "Id": 6660,
    "glas": null
  },
  {
    "StrankaId": 131,
    "Priimek": "Glezos",
    "Id": 472,
    "glas": null
  },
  {
    "StrankaId": 131,
    "Priimek": "Katrougkalos",
    "Id": 6661,
    "glas": null
  },
  {
    "StrankaId": 131,
    "Priimek": "Kuneva",
    "Id": 6675,
    "glas": null
  },
  {
    "StrankaId": 131,
    "Priimek": "Papadimoulis",
    "Id": 5596,
    "glas": null
  },
  {
    "StrankaId": 131,
    "Priimek": "Sakorafa",
    "Id": 6674,
    "glas": null
  },
  {
    "StrankaId": 131,
    "Priimek": "Kouloglou",
    "Id": 6713,
    "glas": null
  },
  {
    "StrankaId": 131,
    "Priimek": "Chountis",
    "Id": 5237,
    "glas": null
  },
  {
    "StrankaId": 132,
    "Priimek": "Flanagan",
    "Id": 6597,
    "glas": null
  },
  {
    "StrankaId": 133,
    "Priimek": "Ní Riada",
    "Id": 6599,
    "glas": null
  },
  {
    "StrankaId": 133,
    "Priimek": "Carthy",
    "Id": 6598,
    "glas": null
  },
  {
    "StrankaId": 133,
    "Priimek": "Boylan",
    "Id": 6595,
    "glas": null
  },
  {
    "StrankaId": 134,
    "Priimek": "Forenza",
    "Id": 6693,
    "glas": null
  },
  {
    "StrankaId": 134,
    "Priimek": "Maltese",
    "Id": 6692,
    "glas": null
  },
  {
    "StrankaId": 134,
    "Priimek": "Spinelli",
    "Id": 6444,
    "glas": null
  },
  {
    "StrankaId": 135,
    "Priimek": "De Masi",
    "Id": 6496,
    "glas": null
  },
  {
    "StrankaId": 135,
    "Priimek": "Ernst",
    "Id": 6024,
    "glas": null
  },
  {
    "StrankaId": 135,
    "Priimek": "Händel",
    "Id": 6023,
    "glas": null
  },
  {
    "StrankaId": 135,
    "Priimek": "Lösing",
    "Id": 6026,
    "glas": null
  },
  {
    "StrankaId": 135,
    "Priimek": "Michels",
    "Id": 6310,
    "glas": null
  },
  {
    "StrankaId": 135,
    "Priimek": "Scholz",
    "Id": 5837,
    "glas": null
  },
  {
    "StrankaId": 135,
    "Priimek": "Zimmer",
    "Id": 5368,
    "glas": null
  },
  {
    "StrankaId": 136,
    "Priimek": "Eck",
    "Id": 6501,
    "glas": null
  },
  {
    "StrankaId": 137,
    "Priimek": "Hazekamp",
    "Id": 6628,
    "glas": null
  },
  {
    "StrankaId": 138,
    "Priimek": "de Jong",
    "Id": 5922,
    "glas": null
  },
  {
    "StrankaId": 138,
    "Priimek": "Mineur",
    "Id": 6643,
    "glas": null
  },
  {
    "StrankaId": 139,
    "Priimek": "Matias",
    "Id": 5992,
    "glas": null
  },
  {
    "StrankaId": 140,
    "Priimek": "Ferreira João",
    "Id": 5886,
    "glas": null
  },
  {
    "StrankaId": 140,
    "Priimek": "Viegas",
    "Id": 6678,
    "glas": null
  },
  {
    "StrankaId": 140,
    "Priimek": "Zuber",
    "Id": 6253,
    "glas": null
  },
  {
    "StrankaId": 140,
    "Priimek": "Pimenta Lopes",
    "Id": 6727,
    "glas": null
  },
  {
    "StrankaId": 141,
    "Priimek": "Albiol Guzmán",
    "Id": 6654,
    "glas": null
  },
  {
    "StrankaId": 141,
    "Priimek": "Couso Permuy",
    "Id": 6701,
    "glas": null
  },
  {
    "StrankaId": 141,
    "Priimek": "López Paloma",
    "Id": 6653,
    "glas": null
  },
  {
    "StrankaId": 141,
    "Priimek": "Senra Rodríguez",
    "Id": 6655,
    "glas": null
  },
  {
    "StrankaId": 141,
    "Priimek": "Vallina",
    "Id": 6656,
    "glas": null
  },
  {
    "StrankaId": 141,
    "Priimek": "Meyer",
    "Id": "",
    "glas": null
  },
  {
    "StrankaId": 142,
    "Priimek": "Juaristi Abaunz",
    "Id": 6657,
    "glas": null
  },
  {
    "StrankaId": 143,
    "Priimek": "Echenique",
    "Id": 6642,
    "glas": null
  },
  {
    "StrankaId": 143,
    "Priimek": "Iglesias",
    "Id": 6637,
    "glas": null
  },
  {
    "StrankaId": 143,
    "Priimek": "Jiménez Villarejo",
    "Id": 6640,
    "glas": null
  },
  {
    "StrankaId": 143,
    "Priimek": "Rodriguez-Rubio Vázquez",
    "Id": 6639,
    "glas": null
  },
  {
    "StrankaId": 143,
    "Priimek": "Sánchez Caldentey",
    "Id": 6641,
    "glas": null
  },
  {
    "StrankaId": 143,
    "Priimek": "González Peñas",
    "Id": 6702,
    "glas": null
  },
  {
    "StrankaId": 143,
    "Priimek": "Urbán Crespo",
    "Id": 6715,
    "glas": null
  },
  {
    "StrankaId": 143,
    "Priimek": "Torres Martínez",
    "Id": 6716,
    "glas": null
  },
  {
    "StrankaId": 143,
    "Priimek": "Benito Ziluaga",
    "Id": 6723,
    "glas": null
  },
  {
    "StrankaId": 144,
    "Priimek": "Björk",
    "Id": 6604,
    "glas": null
  },
  {
    "StrankaId": 145,
    "Priimek": "Anderson Martina",
    "Id": 6280,
    "glas": null
  },
  {
    "StrankaId": 146,
    "Priimek": "Kappel",
    "Id": 6629,
    "glas": null
  },
  {
    "StrankaId": 146,
    "Priimek": "Mayer Georg",
    "Id": 6627,
    "glas": null
  },
  {
    "StrankaId": 146,
    "Priimek": "Obermayr",
    "Id": 6123,
    "glas": null
  },
  {
    "StrankaId": 146,
    "Priimek": "Vilimsky",
    "Id": 6610,
    "glas": null
  },
  {
    "StrankaId": 147,
    "Priimek": "Annemans",
    "Id": 6593,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Aliot",
    "Id": 6451,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Arnautu",
    "Id": 6382,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Bay",
    "Id": 6394,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Bilde",
    "Id": 6405,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Boutonnet",
    "Id": 6387,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Briois",
    "Id": 6391,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "D'Ornano",
    "Id": 6384,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Ferrand",
    "Id": 6401,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Goddyn",
    "Id": 6450,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Jalkh",
    "Id": 6404,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Lebreton",
    "Id": 6372,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Loiseau",
    "Id": 6699,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Martin Dominique",
    "Id": 6385,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Mélin",
    "Id": 6399,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Le Pen Marine",
    "Id": 5332,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Monot",
    "Id": 6395,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Montel",
    "Id": 6403,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Philippot",
    "Id": 6474,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Schaffhauser",
    "Id": 6389,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Troszczynski",
    "Id": 6392,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Le Pen Jean-Marie",
    "Id": 1529,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Gollnisch",
    "Id": 1380,
    "glas": null
  },
  {
    "StrankaId": 148,
    "Priimek": "Chauprade",
    "Id": 6386,
    "glas": null
  },
  {
    "StrankaId": 149,
    "Priimek": "Papadakis Konstantinos",
    "Id": 6676,
    "glas": null
  },
  {
    "StrankaId": 149,
    "Priimek": "Zarianopoulos",
    "Id": 6673,
    "glas": null
  },
  {
    "StrankaId": 150,
    "Priimek": "Epitideios",
    "Id": 6669,
    "glas": null
  },
  {
    "StrankaId": 150,
    "Priimek": "Synadinos",
    "Id": 6670,
    "glas": null
  },
  {
    "StrankaId": 150,
    "Priimek": "Fountoulis",
    "Id": 6668,
    "glas": null
  },
  {
    "StrankaId": 151,
    "Priimek": "Bizzotto",
    "Id": 6179,
    "glas": null
  },
  {
    "StrankaId": 151,
    "Priimek": "Borghezio",
    "Id": 5010,
    "glas": null
  },
  {
    "StrankaId": 151,
    "Priimek": "Buonanno",
    "Id": 6507,
    "glas": null
  },
  {
    "StrankaId": 151,
    "Priimek": "Fontana",
    "Id": 6132,
    "glas": null
  },
  {
    "StrankaId": 151,
    "Priimek": "Salvini",
    "Id": 5522,
    "glas": null
  },
  {
    "StrankaId": 151,
    "Priimek": "Zanni",
    "Id": 6414,
    "glas": null
  },
  {
    "StrankaId": 151,
    "Priimek": "Ciocca",
    "Id": 6731,
    "glas": null
  },
  {
    "StrankaId": 151,
    "Priimek": "Tosi",
    "Id": "",
    "glas": null
  },
  {
    "StrankaId": 152,
    "Priimek": "Balczó",
    "Id": 6048,
    "glas": null
  },
  {
    "StrankaId": 152,
    "Priimek": "Kovács",
    "Id": 6203,
    "glas": null
  },
  {
    "StrankaId": 152,
    "Priimek": "Morvai",
    "Id": 5849,
    "glas": null
  },
  {
    "StrankaId": 153,
    "Priimek": "Sonneborn",
    "Id": 6471,
    "glas": null
  },
  {
    "StrankaId": 154,
    "Priimek": "Voigt",
    "Id": 6469,
    "glas": null
  },
  {
    "StrankaId": 155,
    "Priimek": "Jansen",
    "Id": 6635,
    "glas": null
  },
  {
    "StrankaId": 155,
    "Priimek": "de Graaff",
    "Id": 6630,
    "glas": null
  },
  {
    "StrankaId": 155,
    "Priimek": "Maeijer",
    "Id": 6633,
    "glas": null
  },
  {
    "StrankaId": 155,
    "Priimek": "Stuger",
    "Id": 6634,
    "glas": null
  },
  {
    "StrankaId": 155,
    "Priimek": "Zijlstra",
    "Id": 6228,
    "glas": null
  },
  {
    "StrankaId": 156,
    "Priimek": "Marusik",
    "Id": 6533,
    "glas": null
  },
  {
    "StrankaId": 156,
    "Priimek": "Żółtek",
    "Id": 6541,
    "glas": null
  },
  {
    "StrankaId": 156,
    "Priimek": "Korwin-Mikke",
    "Id": 6517,
    "glas": null
  },
  {
    "StrankaId": 156,
    "Priimek": "Iwaszkiewicz",
    "Id": 6524,
    "glas": null
  },
  {
    "StrankaId": 157,
    "Priimek": "Dodds Diane",
    "Id": 6106,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "James",
    "Id": 6556,
    "glas": null
  },
  {
    "StrankaId": 56,
    "Priimek": "Woolfe",
    "Id": 6585,
    "glas": null
  },
  {
    "StrankaId": 158,
    "Priimek": "Freund",
    "Id": 6620,
    "glas": null
  },
  {
    "StrankaId": 158,
    "Priimek": "Kadenbach",
    "Id": 6146,
    "glas": null
  },
  {
    "StrankaId": 158,
    "Priimek": "Leichtfried",
    "Id": 5371,
    "glas": null
  },
  {
    "StrankaId": 158,
    "Priimek": "Regner",
    "Id": 6136,
    "glas": null
  },
  {
    "StrankaId": 158,
    "Priimek": "Weidenholzer",
    "Id": 6235,
    "glas": null
  },
  {
    "StrankaId": 158,
    "Priimek": "Graswander-Hainz",
    "Id": 6720,
    "glas": null
  },
  {
    "StrankaId": 159,
    "Priimek": "Arena",
    "Id": 6553,
    "glas": null
  },
  {
    "StrankaId": 159,
    "Priimek": "Bayet",
    "Id": 6611,
    "glas": null
  },
  {
    "StrankaId": 159,
    "Priimek": "Tarabella",
    "Id": 5624,
    "glas": null
  },
  {
    "StrankaId": 160,
    "Priimek": "Van Brempt",
    "Id": 4990,
    "glas": null
  },
  {
    "StrankaId": 161,
    "Priimek": "Iotova",
    "Id": 5738,
    "glas": null
  },
  {
    "StrankaId": 161,
    "Priimek": "Pirinski",
    "Id": 6493,
    "glas": null
  },
  {
    "StrankaId": 161,
    "Priimek": "Nekov",
    "Id": 6618,
    "glas": null
  },
  {
    "StrankaId": 161,
    "Priimek": "Stanishev",
    "Id": 6490,
    "glas": null
  },
  {
    "StrankaId": 161,
    "Priimek": "Kouroumbashev",
    "Id": 6495,
    "glas": null
  },
  {
    "StrankaId": 162,
    "Priimek": "Mavrides",
    "Id": 6322,
    "glas": null
  },
  {
    "StrankaId": 163,
    "Priimek": "Papadakis Demetris",
    "Id": 6323,
    "glas": null
  },
  {
    "StrankaId": 164,
    "Priimek": "Keller Jan",
    "Id": 6326,
    "glas": null
  },
  {
    "StrankaId": 164,
    "Priimek": "Poc",
    "Id": 5894,
    "glas": null
  },
  {
    "StrankaId": 164,
    "Priimek": "Poche",
    "Id": 6330,
    "glas": null
  },
  {
    "StrankaId": 164,
    "Priimek": "Sehnalová",
    "Id": 5897,
    "glas": null
  },
  {
    "StrankaId": 165,
    "Priimek": "Christensen",
    "Id": 5284,
    "glas": null
  },
  {
    "StrankaId": 165,
    "Priimek": "Kofod",
    "Id": 6508,
    "glas": null
  },
  {
    "StrankaId": 165,
    "Priimek": "Schaldemose",
    "Id": 5713,
    "glas": null
  },
  {
    "StrankaId": 166,
    "Priimek": "Lauristin",
    "Id": 6329,
    "glas": null
  },
  {
    "StrankaId": 167,
    "Priimek": "Jaakonsaari",
    "Id": 5870,
    "glas": null
  },
  {
    "StrankaId": 167,
    "Priimek": "Kumpula-Natri",
    "Id": 6367,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Andrieu",
    "Id": 6279,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Balas",
    "Id": 6378,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Denanot",
    "Id": 5816,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Guillaume",
    "Id": 6107,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Berés",
    "Id": 3972,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Manscour",
    "Id": 6355,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Martin Edouard",
    "Id": 6565,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Maurel",
    "Id": 6354,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Pargneaux",
    "Id": 6103,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Peillon",
    "Id": 5296,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Revault D'Allonnes Bonnefoy",
    "Id": 6316,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Rozière",
    "Id": 6357,
    "glas": null
  },
  {
    "StrankaId": 168,
    "Priimek": "Thomas",
    "Id": 6282,
    "glas": null
  },
  {
    "StrankaId": 169,
    "Priimek": "Grammatikakis",
    "Id": 6688,
    "glas": null
  },
  {
    "StrankaId": 169,
    "Priimek": "Kyrkos",
    "Id": 6689,
    "glas": null
  },
  {
    "StrankaId": 170,
    "Priimek": "Androulakis",
    "Id": 6687,
    "glas": null
  },
  {
    "StrankaId": 170,
    "Priimek": "Kaili",
    "Id": 6686,
    "glas": null
  },
  {
    "StrankaId": 171,
    "Priimek": "Borzan",
    "Id": 6264,
    "glas": null
  },
  {
    "StrankaId": 171,
    "Priimek": "Picula",
    "Id": 6261,
    "glas": null
  },
  {
    "StrankaId": 172,
    "Priimek": "Childers",
    "Id": 5835,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Benifei",
    "Id": 6505,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Bettini",
    "Id": 6456,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Bonafè",
    "Id": 6449,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Bresso",
    "Id": 5465,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Briano",
    "Id": 6421,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Caputo",
    "Id": 6489,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Chinnici",
    "Id": 6499,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Cofferati",
    "Id": 6083,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Costa",
    "Id": 6085,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Cozzolino",
    "Id": 6051,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Danti",
    "Id": 6458,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "De Castro",
    "Id": 6062,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "De Monte",
    "Id": 6437,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Gasbarra",
    "Id": 6454,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Gentile",
    "Id": 6485,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Giuffrida",
    "Id": 6502,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Gualtieri",
    "Id": 6063,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Kyenge",
    "Id": 6435,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Moretti",
    "Id": 6433,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Morgano",
    "Id": 6422,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Mosca",
    "Id": 6506,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Panzeri",
    "Id": 5483,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Paolucci",
    "Id": 6487,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Picierno",
    "Id": 6484,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Pittella",
    "Id": 4851,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Sassoli",
    "Id": 6036,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Schlein",
    "Id": 6438,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Toia",
    "Id": 5459,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Zanonato",
    "Id": 6434,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Zoffoli",
    "Id": 6714,
    "glas": null
  },
  {
    "StrankaId": 173,
    "Priimek": "Soru",
    "Id": 6498,
    "glas": null
  },
  {
    "StrankaId": 174,
    "Priimek": "Mamikins",
    "Id": 6380,
    "glas": null
  },
  {
    "StrankaId": 175,
    "Priimek": "Balčytis",
    "Id": 5866,
    "glas": null
  },
  {
    "StrankaId": 175,
    "Priimek": "Blinkevičiūtė",
    "Id": 5867,
    "glas": null
  },
  {
    "StrankaId": 176,
    "Priimek": "Delvaux-Stehres",
    "Id": 6410,
    "glas": null
  },
  {
    "StrankaId": 177,
    "Priimek": "Molnár",
    "Id": 6351,
    "glas": null
  },
  {
    "StrankaId": 177,
    "Priimek": "Niedermüller",
    "Id": 6352,
    "glas": null
  },
  {
    "StrankaId": 178,
    "Priimek": "Szanyi",
    "Id": 6334,
    "glas": null
  },
  {
    "StrankaId": 178,
    "Priimek": "Ujhelyi",
    "Id": 6336,
    "glas": null
  },
  {
    "StrankaId": 179,
    "Priimek": "Dalli",
    "Id": 6589,
    "glas": null
  },
  {
    "StrankaId": 179,
    "Priimek": "Mizzi",
    "Id": 6296,
    "glas": null
  },
  {
    "StrankaId": 179,
    "Priimek": "Sant",
    "Id": 6415,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Bullmann",
    "Id": 4690,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Ertug",
    "Id": 6014,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Fleckenstein",
    "Id": 6012,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Gebhardt",
    "Id": 3827,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Geier",
    "Id": 6005,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Groote",
    "Id": 5692,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Hoffmann",
    "Id": 6479,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Kammerevert",
    "Id": 6009,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Kaufmann",
    "Id": 3756,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Köster",
    "Id": 6459,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Krehl",
    "Id": 3761,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Lange",
    "Id": 3823,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Leinen",
    "Id": 4685,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Lietz",
    "Id": 6477,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Melior",
    "Id": 6476,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Neuser",
    "Id": 6016,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Noichl",
    "Id": 6473,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Preuß",
    "Id": 6526,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Rodust",
    "Id": 5813,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Schulz",
    "Id": 3825,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Schuster",
    "Id": 6475,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Simon Peter",
    "Id": 6008,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Sippel",
    "Id": 6093,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Steinruck",
    "Id": 6003,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "von Weizsäcker",
    "Id": 6478,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Werner",
    "Id": 6463,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Westphal",
    "Id": 6011,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Wölken",
    "Id": 6735,
    "glas": null
  },
  {
    "StrankaId": 180,
    "Priimek": "Kohn",
    "Id": 6739,
    "glas": null
  },
  {
    "StrankaId": 181,
    "Priimek": "Jongerius",
    "Id": 6624,
    "glas": null
  },
  {
    "StrankaId": 181,
    "Priimek": "Piri",
    "Id": 6625,
    "glas": null
  },
  {
    "StrankaId": 181,
    "Priimek": "Tang",
    "Id": 6623,
    "glas": null
  },
  {
    "StrankaId": 182,
    "Priimek": "Geringer de Oedenberg",
    "Id": 5495,
    "glas": null
  },
  {
    "StrankaId": 182,
    "Priimek": "Gierek",
    "Id": 5497,
    "glas": null
  },
  {
    "StrankaId": 182,
    "Priimek": "Liberadzki",
    "Id": 5094,
    "glas": null
  },
  {
    "StrankaId": 182,
    "Priimek": "Łybacka",
    "Id": 6521,
    "glas": null
  },
  {
    "StrankaId": 182,
    "Priimek": "Zemke",
    "Id": 5957,
    "glas": null
  },
  {
    "StrankaId": 183,
    "Priimek": "Assis",
    "Id": 5426,
    "glas": null
  },
  {
    "StrankaId": 183,
    "Priimek": "Ferreira Elisa",
    "Id": 5427,
    "glas": null
  },
  {
    "StrankaId": 183,
    "Priimek": "Gomes",
    "Id": 5425,
    "glas": null
  },
  {
    "StrankaId": 183,
    "Priimek": "Rodrigues Maria João",
    "Id": 6371,
    "glas": null
  },
  {
    "StrankaId": 183,
    "Priimek": "Serrão Santos",
    "Id": 6375,
    "glas": null
  },
  {
    "StrankaId": 183,
    "Priimek": "Silva Pereira",
    "Id": 6381,
    "glas": null
  },
  {
    "StrankaId": 183,
    "Priimek": "Zorrinho",
    "Id": 6373,
    "glas": null
  },
  {
    "StrankaId": 183,
    "Priimek": "dos Santos",
    "Id": 5013,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Boştinaru",
    "Id": 5768,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Creţu",
    "Id": 5661,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Cristea",
    "Id": 6616,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Dăncilă",
    "Id": 5829,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Drăghici",
    "Id": 6420,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Frunzulică",
    "Id": 6424,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Grapini",
    "Id": 6419,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Ivan",
    "Id": 6029,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Moisă",
    "Id": 6423,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Negrescu",
    "Id": 6691,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Nica",
    "Id": 6418,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Paşcu",
    "Id": 5648,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Sârbu",
    "Id": 5653,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Tănăsescu",
    "Id": 6019,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Tapardel",
    "Id": 6427,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Pavel",
    "Id": 6707,
    "glas": null
  },
  {
    "StrankaId": 184,
    "Priimek": "Rebega",
    "Id": 6426,
    "glas": null
  },
  {
    "StrankaId": 185,
    "Priimek": "Flašíková Beňová",
    "Id": 5173,
    "glas": null
  },
  {
    "StrankaId": 185,
    "Priimek": "Maňka",
    "Id": 5314,
    "glas": null
  },
  {
    "StrankaId": 185,
    "Priimek": "Smolková",
    "Id": 5844,
    "glas": null
  },
  {
    "StrankaId": 185,
    "Priimek": "Zala",
    "Id": 5845,
    "glas": null
  },
  {
    "StrankaId": 186,
    "Priimek": "Fajon",
    "Id": 6080,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "Aguilera García",
    "Id": 6651,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "Ayala Sender",
    "Id": 5411,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "Blanco López",
    "Id": 6650,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "Cabezón Ruiz",
    "Id": 6647,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "García Pérez",
    "Id": 5417,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "Gardiazabal Rubial",
    "Id": 6130,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "Guerrero Salom",
    "Id": 5941,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "Gutiérrez Prieto",
    "Id": 6212,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "Jáuregui Atondo",
    "Id": 6161,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "López Aguilar",
    "Id": 5984,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "López",
    "Id": 6648,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "Rodríguez-Piñero Fernández",
    "Id": 6649,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "Valenciano",
    "Id": 4757,
    "glas": null
  },
  {
    "StrankaId": 187,
    "Priimek": "Rodrigues Liliana",
    "Id": 6677,
    "glas": null
  },
  {
    "StrankaId": 188,
    "Priimek": "Post",
    "Id": 6609,
    "glas": null
  },
  {
    "StrankaId": 189,
    "Priimek": "Guteland",
    "Id": 6603,
    "glas": null
  },
  {
    "StrankaId": 189,
    "Priimek": "Hedh",
    "Id": 5261,
    "glas": null
  },
  {
    "StrankaId": 189,
    "Priimek": "Ludvigsson",
    "Id": 5860,
    "glas": null
  },
  {
    "StrankaId": 189,
    "Priimek": "Nilsson",
    "Id": 6242,
    "glas": null
  },
  {
    "StrankaId": 189,
    "Priimek": "Ulvskog",
    "Id": 5859,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Anderson Lucy",
    "Id": 6567,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Brannen",
    "Id": 6572,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Corbett",
    "Id": 4628,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Dance",
    "Id": 6570,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Dodds Anneliese",
    "Id": 6588,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Gill Neena",
    "Id": 4937,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Griffin",
    "Id": 6580,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Honeyball",
    "Id": 4994,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Howitt",
    "Id": 4103,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Khan",
    "Id": 6581,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Kirton-Darling",
    "Id": 6571,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "McAvan",
    "Id": 4646,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Martin David",
    "Id": 1133,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Moody",
    "Id": 6562,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Moraes",
    "Id": 4923,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Simon Siôn",
    "Id": 6566,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Stihler",
    "Id": 4949,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Vaughan",
    "Id": 6086,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Ward",
    "Id": 6582,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Willmott",
    "Id": 5698,
    "glas": null
  },
  {
    "StrankaId": 190,
    "Priimek": "Mayer Alex",
    "Id": 6737,
    "glas": null
  }
];