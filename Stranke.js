var skupine =
[
	{
		"Skupina": "ALDE",
		"ZA": 0,
		"PROTI": 0,
		"Neodloceno": 0,
		"Manjkajoci": 0,
		"Kohezija": 0,
		"KohezijaVW": 0,
		"KohezijaRice": 0,
		"Glasovanje": null,
		"Glasovanje10pp": null,
		"GlasovanjePovpr": null,
		"Vsi": 0
	},
	{
		"Skupina": "ECR",
		"ZA": 0,
		"PROTI": 0,
		"Neodloceno": 0,
		"Manjkajoci": 0,
		"Kohezija": 0,
		"KohezijaVW": 0,
		"KohezijaRice": 0,
		"Glasovanje": null,
		"Glasovanje10pp": null,
		"GlasovanjePovpr": null,
		"Vsi": 0
	},
	{
		"Skupina": "EFDD",
		"ZA": 0,
		"PROTI": 0,
		"Neodloceno": 0,
		"Manjkajoci": 0,
		"Kohezija": 0,
		"KohezijaVW": 0,
		"KohezijaRice": 0,
		"Glasovanje": null,
		"Glasovanje10pp": null,
		"GlasovanjePovpr": null,
		"Vsi": 0
	},
	{
		"Skupina": "EPP",
		"ZA": 0,
		"PROTI": 0,
		"Neodloceno": 0,
		"Manjkajoci": 0,
		"Kohezija": 0,
		"KohezijaVW": 0,
		"KohezijaRice": 0,
		"Glasovanje": null,
		"Glasovanje10pp": null,
		"GlasovanjePovpr": null,
		"Vsi": 0
	},
	{
		"Skupina": "Greens",
		"ZA": 0,
		"PROTI": 0,
		"Neodloceno": 0,
		"Manjkajoci": 0,
		"Kohezija": 0,
		"KohezijaVW": 0,
		"KohezijaRice": 0,
		"Glasovanje": null,
		"Glasovanje10pp": null,
		"GlasovanjePovpr": null,
		"Vsi": 0
	},
	{
		"Skupina": "GUE",
		"ZA": 0,
		"PROTI": 0,
		"Neodloceno": 0,
		"Manjkajoci": 0,
		"Kohezija": 0,
		"KohezijaVW": 0,
		"KohezijaRice": 0,
		"Glasovanje": null,
		"Glasovanje10pp": null,
		"GlasovanjePovpr": null,
		"Vsi": 0
	},
	{
		"Skupina": "NI",
		"ZA": 0,
		"PROTI": 0,
		"Neodloceno": 0,
		"Manjkajoci": 0,
		"Kohezija": 0,
		"KohezijaVW": 0,
		"KohezijaRice": 0,
		"Glasovanje": null,
		"Glasovanje10pp": null,
		"GlasovanjePovpr": null,
		"Vsi": 0
	},
	{
		"Skupina": "S&D",
		"ZA": 0,
		"PROTI": 0,
		"Neodloceno": 0,
		"Manjkajoci": 0,
		"Kohezija": 0,
		"KohezijaVW": 0,
		"KohezijaRice": 0,
		"Glasovanje": null,
		"Glasovanje10pp": null,
		"GlasovanjePovpr": null,
		"Vsi": 0
	}
];

var stranke =
[
  {
    "id": 1,
    "skupina": "ALDE",
    "ime": "NEOS",
    "država": "Austria",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 2,
    "skupina": "ALDE",
    "ime": "MR",
    "država": "Belgium",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 3,
    "skupina": "ALDE",
    "ime": "Open VLD",
    "država": "Belgium",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 4,
    "skupina": "ALDE",
    "ime": "DPS/ДПС",
    "država": "Bulgaria",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 5,
    "skupina": "ALDE",
    "ime": "ANO 2011",
    "država": "Češka",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 6,
    "skupina": "ALDE",
    "ime": "B. (RV)",
    "država": "Danska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 7,
    "skupina": "ALDE",
    "ime": "V. (V)",
    "država": "Danska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 8,
    "skupina": "ALDE",
    "ime": "ER",
    "država": "Estonija",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 9,
    "skupina": "ALDE",
    "ime": "KE",
    "država": "Estonija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 10,
    "skupina": "ALDE",
    "ime": "KESK",
    "država": "Finska",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 11,
    "skupina": "ALDE",
    "ime": "SFP (RKP)",
    "država": "Finska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 12,
    "skupina": "ALDE",
    "ime": "Alternative (UDI+MoDem)",
    "država": "Francija",
    "StPoslancev": 7,
    "glasovanje": null
  },
  {
    "id": 13,
    "skupina": "ALDE",
    "ime": "SDP+HNS+HSU+SDSS+IDS",
    "država": "Hrvatska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 14,
    "skupina": "ALDE",
    "ime": "Ind. + other parties",
    "država": "Irska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 15,
    "skupina": "ALDE",
    "ime": "DP",
    "država": "Litva",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 16,
    "skupina": "ALDE",
    "ime": "LRLS",
    "država": "Litva",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 17,
    "skupina": "ALDE",
    "ime": "DP/PD",
    "država": "Luxemburg",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 18,
    "skupina": "ALDE",
    "ime": "FDP",
    "država": "Nemčija",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 19,
    "skupina": "ALDE",
    "ime": "FREIE WÄHLER",
    "država": "Nemčija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 20,
    "skupina": "ALDE",
    "ime": "D66",
    "država": "Nizozemska",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 21,
    "skupina": "ALDE",
    "ime": "VVD",
    "država": "Nizozemska",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 22,
    "skupina": "ALDE",
    "ime": "MPT",
    "država": "Portugalska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 23,
    "skupina": "ALDE",
    "ime": "Ind. M. Diaconu",
    "država": "Romunija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 24,
    "skupina": "ALDE",
    "ime": "SaS",
    "država": "Slovaška",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 25,
    "skupina": "ALDE",
    "ime": "DeSUS",
    "država": "Slovenija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 26,
    "skupina": "ALDE",
    "ime": "C'S",
    "država": "Španija",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 27,
    "skupina": "ALDE",
    "ime": "CEU (CDC+UDC+EAJ-PNV+ CCA-PNC+",
    "država": "Španija",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 28,
    "skupina": "ALDE",
    "ime": "UPyD",
    "država": "Španija",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 29,
    "skupina": "ALDE",
    "ime": "C",
    "država": "Švedska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 30,
    "skupina": "ALDE",
    "ime": "FP",
    "država": "Švedska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 31,
    "skupina": "ALDE",
    "ime": "LDP",
    "država": "Združeno Kraljest",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 32,
    "skupina": "ECR",
    "ime": "N-VA",
    "država": "Belgium",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 33,
    "skupina": "ECR",
    "ime": "ББЦ+ВМРО-БНД et al.",
    "država": "Bulgaria",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 34,
    "skupina": "ECR",
    "ime": "ODS",
    "država": "Češka",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 35,
    "skupina": "ECR",
    "ime": "O. (DF)",
    "država": "Danska",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 36,
    "skupina": "ECR",
    "ime": "PS",
    "država": "Finska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 37,
    "skupina": "ECR",
    "ime": "ΑΝ.ΕΛ/ANEL",
    "država": "Grčija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 38,
    "skupina": "ECR",
    "ime": "HDZ+HSP AS",
    "država": "Hrvatska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 39,
    "skupina": "ECR",
    "ime": "FF",
    "država": "Irska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 40,
    "skupina": "ECR",
    "ime": "Coal. NA (TB/LNNK+VL!)",
    "država": "Latvija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 41,
    "skupina": "ECR",
    "ime": "LLRA (AWPL)",
    "država": "Litva",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 42,
    "skupina": "ECR",
    "ime": "AfD",
    "država": "Nemčija",
    "StPoslancev": 7,
    "glasovanje": null
  },
  {
    "id": 43,
    "skupina": "ECR",
    "ime": "FAMILIE",
    "država": "Nemčija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 44,
    "skupina": "ECR",
    "ime": "Coalition CU - SGP",
    "država": "Nizozemska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 45,
    "skupina": "ECR",
    "ime": "PiS",
    "država": "Poljska",
    "StPoslancev": 19,
    "glasovanje": null
  },
  {
    "id": 46,
    "skupina": "ECR",
    "ime": "NOVA",
    "država": "Slovaška",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 47,
    "skupina": "ECR",
    "ime": "OL'aNO",
    "država": "Slovaška",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 48,
    "skupina": "ECR",
    "ime": "Cons.",
    "država": "Združeno Kraljest",
    "StPoslancev": 19,
    "glasovanje": null
  },
  {
    "id": 49,
    "skupina": "ECR",
    "ime": "UUP",
    "država": "Združeno Kraljest",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 50,
    "skupina": "EFDD",
    "ime": "Svobodní",
    "država": "Češka",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 51,
    "skupina": "EFDD",
    "ime": "Independent",
    "država": "Francija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 52,
    "skupina": "EFDD",
    "ime": "M5S",
    "država": "Italija",
    "StPoslancev": 17,
    "glasovanje": null
  },
  {
    "id": 53,
    "skupina": "EFDD",
    "ime": "Coal. ZZS (LZS+LZP)",
    "država": "Latvija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 54,
    "skupina": "EFDD",
    "ime": "TT",
    "država": "Litva",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 55,
    "skupina": "EFDD",
    "ime": "SD",
    "država": "Švedska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 56,
    "skupina": "EFDD",
    "ime": "UKIP",
    "država": "Združeno Kraljest",
    "StPoslancev": 24,
    "glasovanje": null
  },
  {
    "id": 57,
    "skupina": "EPP",
    "ime": "ÖVP",
    "država": "Austria",
    "StPoslancev": 5,
    "glasovanje": null
  },
  {
    "id": 58,
    "skupina": "EPP",
    "ime": "CD&V",
    "država": "Belgium",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 59,
    "skupina": "EPP",
    "ime": "cdH",
    "država": "Belgium",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 60,
    "skupina": "EPP",
    "ime": "CSP",
    "država": "Belgium",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 61,
    "skupina": "EPP",
    "ime": "GERB/ГЕРБ",
    "država": "Bulgaria",
    "StPoslancev": 6,
    "glasovanje": null
  },
  {
    "id": 62,
    "skupina": "EPP",
    "ime": "RB/Реформаторски",
    "država": "Bulgaria",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 63,
    "skupina": "EPP",
    "ime": "ΔΗΣΥ/DISY",
    "država": "Ciper",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 64,
    "skupina": "EPP",
    "ime": "Coal. (TOP 09 + STAN)",
    "država": "Češka",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 65,
    "skupina": "EPP",
    "ime": "KDU-ČSL",
    "država": "Češka",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 66,
    "skupina": "EPP",
    "ime": "C. (KF)",
    "država": "Danska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 67,
    "skupina": "EPP",
    "ime": "IRL",
    "država": "Estonija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 68,
    "skupina": "EPP",
    "ime": "KOK",
    "država": "Finska",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 69,
    "skupina": "EPP",
    "ime": "UMP",
    "država": "Francija",
    "StPoslancev": 20,
    "glasovanje": null
  },
  {
    "id": 70,
    "skupina": "EPP",
    "ime": "Ν.Δ/N.D.",
    "država": "Grčija",
    "StPoslancev": 5,
    "glasovanje": null
  },
  {
    "id": 71,
    "skupina": "EPP",
    "ime": "HDZ+HSP AS",
    "država": "Hrvatska",
    "StPoslancev": 5,
    "glasovanje": null
  },
  {
    "id": 72,
    "skupina": "EPP",
    "ime": "FG",
    "država": "Irska",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 73,
    "skupina": "EPP",
    "ime": "Coal. (NCD+UDC+PPI)",
    "država": "Italija",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 74,
    "skupina": "EPP",
    "ime": "FI",
    "država": "Italija",
    "StPoslancev": 13,
    "glasovanje": null
  },
  {
    "id": 75,
    "skupina": "EPP",
    "ime": "SVP",
    "država": "Italija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 76,
    "skupina": "EPP",
    "ime": "V.",
    "država": "Latvija",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 77,
    "skupina": "EPP",
    "ime": "TS-LKD",
    "država": "Litva",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 78,
    "skupina": "EPP",
    "ime": "CSV/PCS",
    "država": "Luxemburg",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 79,
    "skupina": "EPP",
    "ime": "FIDESZ - KDNP",
    "država": "Madžarska",
    "StPoslancev": 12,
    "glasovanje": null
  },
  {
    "id": 80,
    "skupina": "EPP",
    "ime": "PN/NP",
    "država": "Malta",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 81,
    "skupina": "EPP",
    "ime": "CDU / CSU",
    "država": "Nemčija",
    "StPoslancev": 34,
    "glasovanje": null
  },
  {
    "id": 82,
    "skupina": "EPP",
    "ime": "CDA",
    "država": "Nizozemska",
    "StPoslancev": 5,
    "glasovanje": null
  },
  {
    "id": 83,
    "skupina": "EPP",
    "ime": "PO",
    "država": "Poljska",
    "StPoslancev": 19,
    "glasovanje": null
  },
  {
    "id": 84,
    "skupina": "EPP",
    "ime": "PSL",
    "država": "Poljska",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 85,
    "skupina": "EPP",
    "ime": "Coal. (PSD + CDS-PP)",
    "država": "Portugalska",
    "StPoslancev": 7,
    "glasovanje": null
  },
  {
    "id": 86,
    "skupina": "EPP",
    "ime": "PDL",
    "država": "Romunija",
    "StPoslancev": 5,
    "glasovanje": null
  },
  {
    "id": 87,
    "skupina": "EPP",
    "ime": "PMP",
    "država": "Romunija",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 88,
    "skupina": "EPP",
    "ime": "PNL",
    "država": "Romunija",
    "StPoslancev": 6,
    "glasovanje": null
  },
  {
    "id": 89,
    "skupina": "EPP",
    "ime": "UDMR",
    "država": "Romunija",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 90,
    "skupina": "EPP",
    "ime": "KDH",
    "država": "Slovaška",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 91,
    "skupina": "EPP",
    "ime": "MOST-HID",
    "država": "Slovaška",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 92,
    "skupina": "EPP",
    "ime": "SDKÚ - DS",
    "država": "Slovaška",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 93,
    "skupina": "EPP",
    "ime": "SMK - MPK",
    "država": "Slovaška",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 94,
    "skupina": "EPP",
    "ime": "Coalition (NSi + SLS)",
    "država": "Slovenija",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 95,
    "skupina": "EPP",
    "ime": "SDS",
    "država": "Slovenija",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 96,
    "skupina": "EPP",
    "ime": "CEU (CDC+UDC+EAJ-PNV+ CCA-PNC+",
    "država": "Španija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 97,
    "skupina": "EPP",
    "ime": "PP",
    "država": "Španija",
    "StPoslancev": 16,
    "glasovanje": null
  },
  {
    "id": 98,
    "skupina": "EPP",
    "ime": "KD",
    "država": "Švedska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 99,
    "skupina": "EPP",
    "ime": "M",
    "država": "Švedska",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 100,
    "skupina": "Greens",
    "ime": "GRÜNE",
    "država": "Austria",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 101,
    "skupina": "Greens",
    "ime": "ECOLO",
    "država": "Belgium",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 102,
    "skupina": "Greens",
    "ime": "GROEN",
    "država": "Belgium",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 103,
    "skupina": "Greens",
    "ime": "F. (SF)",
    "država": "Danska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 104,
    "skupina": "Greens",
    "ime": "I. Tarand (Ind.)",
    "država": "Estonija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 105,
    "skupina": "Greens",
    "ime": "VIHR",
    "država": "Finska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 106,
    "skupina": "Greens",
    "ime": "Europe écologie",
    "država": "Francija",
    "StPoslancev": 6,
    "glasovanje": null
  },
  {
    "id": 107,
    "skupina": "Greens",
    "ime": "ORaH",
    "država": "Hrvatska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 108,
    "skupina": "Greens",
    "ime": "LKS",
    "država": "Latvija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 109,
    "skupina": "Greens",
    "ime": "LVZS",
    "država": "Litva",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 110,
    "skupina": "Greens",
    "ime": "Déi Gréng/Les Verts",
    "država": "Luxemburg",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 111,
    "skupina": "Greens",
    "ime": "Együtt-PM",
    "država": "Madžarska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 112,
    "skupina": "Greens",
    "ime": "LMP",
    "država": "Madžarska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 113,
    "skupina": "Greens",
    "ime": "GRÜNE",
    "država": "Nemčija",
    "StPoslancev": 11,
    "glasovanje": null
  },
  {
    "id": 114,
    "skupina": "Greens",
    "ime": "ÖDP",
    "država": "Nemčija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 115,
    "skupina": "Greens",
    "ime": "PIRATEN",
    "država": "Nemčija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 116,
    "skupina": "Greens",
    "ime": "GroenLinks",
    "država": "Nizozemska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 117,
    "skupina": "Greens",
    "ime": "Verjamem",
    "država": "Slovenija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 118,
    "skupina": "Greens",
    "ime": "EPDD (ERC+NECat+Ind.+et al.)",
    "država": "Španija",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 119,
    "skupina": "Greens",
    "ime": "IP (IU+ICV+Anova+et al.)",
    "država": "Španija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 120,
    "skupina": "Greens",
    "ime": "Primavera Europea",
    "država": "Španija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 121,
    "skupina": "Greens",
    "ime": "MP",
    "država": "Švedska",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 122,
    "skupina": "Greens",
    "ime": "GP",
    "država": "Združeno Kraljest",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 123,
    "skupina": "Greens",
    "ime": "PL-PW",
    "država": "Združeno Kraljest",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 124,
    "skupina": "Greens",
    "ime": "SNP",
    "država": "Združeno Kraljest",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 125,
    "skupina": "GUE",
    "ime": "ΑΚΕΛ/ΑΚΕL",
    "država": "Ciper",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 126,
    "skupina": "GUE",
    "ime": "KSČM",
    "država": "Češka",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 127,
    "skupina": "GUE",
    "ime": "N.",
    "država": "Danska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 128,
    "skupina": "GUE",
    "ime": "VAS",
    "država": "Finska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 129,
    "skupina": "GUE",
    "ime": "FG (PCF+PG+Ens.+et al.)",
    "država": "Francija",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 130,
    "skupina": "GUE",
    "ime": "UOM",
    "država": "Francija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 131,
    "skupina": "GUE",
    "ime": "ΣΥ.ΡΙ.ΖΑ.",
    "država": "Grčija",
    "StPoslancev": 6,
    "glasovanje": null
  },
  {
    "id": 132,
    "skupina": "GUE",
    "ime": "Ind. + other parties",
    "država": "Irska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 133,
    "skupina": "GUE",
    "ime": "SF",
    "država": "Irska",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 134,
    "skupina": "GUE",
    "ime": "L’Altra Europa – Con Tsipr",
    "država": "Italija",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 135,
    "skupina": "GUE",
    "ime": "DIE LINKE",
    "država": "Nemčija",
    "StPoslancev": 7,
    "glasovanje": null
  },
  {
    "id": 136,
    "skupina": "GUE",
    "ime": "Tierschutzpartei",
    "država": "Nemčija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 137,
    "skupina": "GUE",
    "ime": "PvdD",
    "država": "Nizozemska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 138,
    "skupina": "GUE",
    "ime": "SP",
    "država": "Nizozemska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 139,
    "skupina": "GUE",
    "ime": "B.E.",
    "država": "Portugalska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 140,
    "skupina": "GUE",
    "ime": "CDU (PCP + PEV)",
    "država": "Portugalska",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 141,
    "skupina": "GUE",
    "ime": "IP (IU+ICV+Anova+et al.)",
    "država": "Španija",
    "StPoslancev": 5,
    "glasovanje": null
  },
  {
    "id": 142,
    "skupina": "GUE",
    "ime": "LPD (BNG+EH Bildu)",
    "država": "Španija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 143,
    "skupina": "GUE",
    "ime": "Podemos",
    "država": "Španija",
    "StPoslancev": 5,
    "glasovanje": null
  },
  {
    "id": 144,
    "skupina": "GUE",
    "ime": "V",
    "država": "Švedska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 145,
    "skupina": "GUE",
    "ime": "SF",
    "država": "Združeno Kraljest",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 146,
    "skupina": "NI",
    "ime": "FPÖ",
    "država": "Austria",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 147,
    "skupina": "NI",
    "ime": "Vlaams Belang",
    "država": "Belgium",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 148,
    "skupina": "NI",
    "ime": "FN",
    "država": "Francija",
    "StPoslancev": 23,
    "glasovanje": null
  },
  {
    "id": 149,
    "skupina": "NI",
    "ime": "KKE",
    "država": "Grčija",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 150,
    "skupina": "NI",
    "ime": "X.A.",
    "država": "Grčija",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 151,
    "skupina": "NI",
    "ime": "LN",
    "država": "Italija",
    "StPoslancev": 5,
    "glasovanje": null
  },
  {
    "id": 152,
    "skupina": "NI",
    "ime": "JOBBIK",
    "država": "Madžarska",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 153,
    "skupina": "NI",
    "ime": "Die Partei",
    "država": "Nemčija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 154,
    "skupina": "NI",
    "ime": "NPD",
    "država": "Nemčija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 155,
    "skupina": "NI",
    "ime": "PVV",
    "država": "Nizozemska",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 156,
    "skupina": "NI",
    "ime": "KNP",
    "država": "Poljska",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 157,
    "skupina": "NI",
    "ime": "DUP",
    "država": "Združeno Kraljest",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 158,
    "skupina": "S&D",
    "ime": "SPÖ",
    "država": "Austria",
    "StPoslancev": 5,
    "glasovanje": null
  },
  {
    "id": 159,
    "skupina": "S&D",
    "ime": "PS",
    "država": "Belgium",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 160,
    "skupina": "S&D",
    "ime": "SP.A",
    "država": "Belgium",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 161,
    "skupina": "S&D",
    "ime": "BSP/БСП",
    "država": "Bulgaria",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 162,
    "skupina": "S&D",
    "ime": "ΔΗΚΟ/DIKO",
    "država": "Ciper",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 163,
    "skupina": "S&D",
    "ime": "ΚΣ ΕΔΕΚ/KS EDEK",
    "država": "Ciper",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 164,
    "skupina": "S&D",
    "ime": "ČSSD",
    "država": "Češka",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 165,
    "skupina": "S&D",
    "ime": "A. (S)",
    "država": "Danska",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 166,
    "skupina": "S&D",
    "ime": "SDE",
    "država": "Estonija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 167,
    "skupina": "S&D",
    "ime": "SDP",
    "država": "Finska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 168,
    "skupina": "S&D",
    "ime": "PS - PRG",
    "država": "Francija",
    "StPoslancev": 13,
    "glasovanje": null
  },
  {
    "id": 169,
    "skupina": "S&D",
    "ime": "To Potami",
    "država": "Grčija",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 170,
    "skupina": "S&D",
    "ime": "Ελιά ΔΠ / ELIA DA",
    "država": "Grčija",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 171,
    "skupina": "S&D",
    "ime": "SDP+HNS+HSU+SDSS+IDS",
    "država": "Hrvatska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 172,
    "skupina": "S&D",
    "ime": "Ind. + other parties",
    "država": "Irska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 173,
    "skupina": "S&D",
    "ime": "PD",
    "država": "Italija",
    "StPoslancev": 31,
    "glasovanje": null
  },
  {
    "id": 174,
    "skupina": "S&D",
    "ime": "Saskaņa SDP",
    "država": "Latvija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 175,
    "skupina": "S&D",
    "ime": "LSDP",
    "država": "Litva",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 176,
    "skupina": "S&D",
    "ime": "LSAP/POSL",
    "država": "Luxemburg",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 177,
    "skupina": "S&D",
    "ime": "DK",
    "država": "Madžarska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 178,
    "skupina": "S&D",
    "ime": "MSZP",
    "država": "Madžarska",
    "StPoslancev": 2,
    "glasovanje": null
  },
  {
    "id": 179,
    "skupina": "S&D",
    "ime": "PL/MLP",
    "država": "Malta",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 180,
    "skupina": "S&D",
    "ime": "SPD",
    "država": "Nemčija",
    "StPoslancev": 27,
    "glasovanje": null
  },
  {
    "id": 181,
    "skupina": "S&D",
    "ime": "PvdA",
    "država": "Nizozemska",
    "StPoslancev": 3,
    "glasovanje": null
  },
  {
    "id": 182,
    "skupina": "S&D",
    "ime": "SLD",
    "država": "Poljska",
    "StPoslancev": 5,
    "glasovanje": null
  },
  {
    "id": 183,
    "skupina": "S&D",
    "ime": "PS",
    "država": "Portugalska",
    "StPoslancev": 8,
    "glasovanje": null
  },
  {
    "id": 184,
    "skupina": "S&D",
    "ime": "PSD+PC+UNPR",
    "država": "Romunija",
    "StPoslancev": 16,
    "glasovanje": null
  },
  {
    "id": 185,
    "skupina": "S&D",
    "ime": "SMER - SD",
    "država": "Slovaška",
    "StPoslancev": 4,
    "glasovanje": null
  },
  {
    "id": 186,
    "skupina": "S&D",
    "ime": "SD",
    "država": "Slovenija",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 187,
    "skupina": "S&D",
    "ime": "PSOE/PSC",
    "država": "Španija",
    "StPoslancev": 14,
    "glasovanje": null
  },
  {
    "id": 188,
    "skupina": "S&D",
    "ime": "FI",
    "država": "Švedska",
    "StPoslancev": 1,
    "glasovanje": null
  },
  {
    "id": 189,
    "skupina": "S&D",
    "ime": "S",
    "država": "Švedska",
    "StPoslancev": 5,
    "glasovanje": null
  },
  {
    "id": 190,
    "skupina": "S&D",
    "ime": "Lab.",
    "država": "Združeno Kraljest",
    "StPoslancev": 20,
    "glasovanje": null
  }
];