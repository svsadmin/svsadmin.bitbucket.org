
var stranke = [
{
	"id": 1,
	"skupina": "Zeleni",
	"država": "SLO",
	"glasovanje" : null
},
{
	"id": 2,
	"skupina": "GUE/NGL",
	"država": "Bosna",
	"glasovanje" : null
},
{
	"id": 3,
	"skupina": "ZdruženiFašisti",
	"država": "Italija",
	"glasovanje" : null
}
]

var poslanci = [
{
	"glas": null,
	"Id": 6151,
	"Priimek": "Vajgl",
	"Stranka": "Verjamem",
	"StrankaId": 1
	
},
{
	"glas": null,
	"Id": 6693,
	"Priimek": "Forenza",
	"Stranka": "Komunisti",
	"StrankaId": 2
},
{
	"glas": null,
	"Id": 6628,
	"Priimek": "Hazekamp",
	"Stranka": "Komunisti",
	"StrankaId": 2
},
{
	"glas": null,
	"Id": 6533,
	"Priimek": "Marusik",
	"Stranka": "Komunisti",
	"StrankaId": 2
},
{
	"glas": null,
	"Id": 6376,
	"Priimek": "Marinho e Pinto",
	"Stranka": "Komunisti",
	"StrankaId": 2
},
{
	"glas": null,
	"Id": 6327,
	"Priimek": "Ansip",
	"Stranka": "Fašo",
	"StrankaId": 3
},
{
	"glas": null,
	"Id": 6646,
	"Priimek": "Becerra Basterrechea",
	"Stranka": "Fašo",
	"StrankaId": 3
}
];

var naslov = "Nomination de 4 membres de la Commission européenne - Jyrki Katainen, Jacek Dominik, Ferdinando Nelli Feroci et Martine Reicherts";


var jsonSample = 
{
   "MDocument.Language": "XL",
   "MEP.Number": "PE 536.445",
   "MEP.Reference": "P8_PV(2014)07-16",
   "MSitting.Date": "2014-07-16",
   "MSitting.Identifier": "1598473",
   "RollCallVoteResults.Titles": [
      {
         "MLanguage": "BG",
         "text": [
            "ПРОТОКОЛ",
            "Резултат от поименни гласувания - Приложение 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "CS",
         "text": [
            "ZÁPIS",
            "Výsledek jmenovitého hlasování - Příloha 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "DA",
         "text": [
            "PROTOKOL",
            "Resultat af afstemningerne ved navneopråb - Bilag 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "DE",
         "text": [
            "PROTOKOLL",
            "Ergebnis der namentlichen Abstimmungen - Anlage 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "EL",
         "text": [
            "ΣΥΝΟΠΤIΚΑ ΠΡΑΚΤIΚΑ",
            "Αποτέλεσμα των ψηφοφοριών με ονομαστική κλήση - Παράρτηµα 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "EN",
         "text": [
            "MINUTES",
            "Result of roll-call votes - Annex 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "ES",
         "text": [
            "ACTA",
            "Resultados de las votaciones nominales - Anexo 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "ET",
         "text": [
            "PROTOKOLL",
            "Nimelise hääletuse tulemused - lisa 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "FI",
         "text": [
            "PÖYTÄKIRJA",
            "Nimenhuutoäänestysten tulokset - Liite 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "FR",
         "text": [
            "PROCÈS-VERBAL",
            "Résultat des votes par appel nominal - Annexe 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "HR",
         "text": [
            "ZAPISNIK",
            "Rezultat poimeničnog glasovanja - Prilog 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "HU",
         "text": [
            "JEGYZŐKÖNYV",
            "A név szerinti szavazások eredménye - melléklet 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "IT",
         "text": [
            "PROCESSO VERBALE",
            "Risultato delle votazioni per appello nominale - Allegato 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "LT",
         "text": [
            "PROTOKOLAS",
            "Vardinio balsavimo rezultatai - priedas 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "LV",
         "text": [
            "PROTOKOLS",
            "Rezultāti balsošanai pēc saraksta - pielikums 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "MT",
         "text": [
            "MINUTI",
            "Riżultat tal-votazzjoni bis-sejħa tal-ismijiet - Anness 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "NL",
         "text": [
            "NOTULEN",
            "Uitslag van de hoofdelijke stemmingen - Bijlage 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "PL",
         "text": [
            "PROTOKÓŁ",
            "Wyniki głosowań imiennych - Załącznik 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "PT",
         "text": [
            "ATA",
            "Resultados das votações nominais - Anexo 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "RO",
         "text": [
            "PROCES-VERBAL",
            "Rezultatul voturilor prin apel nominal - Anexa 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "SK",
         "text": [
            "ZÁPISNICA",
            "Výsledok hlasovania podľa mien - Príloha 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "SL",
         "text": [
            "ZAPISNIK",
            "Izid poimenskega glasovanja - Priloga 2"
         ],
         "line-break": null
      },
      {
         "MLanguage": "SV",
         "text": [
            "PROTOKOLL",
            "Resultat av omröstningarna med namnupprop - Bilaga 2"
         ],
         "line-break": null
      }
   ],
   "RollCallVoteResult": [
      {
         "MDate": "2014-07-16 09:19:33",
         "MIdentifier": "47643",
         "RollCallVoteDescriptionText": "Nomination de 4 membres de la Commission européenne - Jyrki Katainen, Jacek Dominik, Ferdinando Nelli Feroci et Martine Reicherts",
         "ResultFor": {
            "MNumber": "421",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ALDE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5682",
                        "text": "Ali"
                     },
                     {
                        "MMepId": "6327",
                        "text": "Ansip"
                     },
                     {
                        "MMepId": "6407",
                        "text": "Arthuis"
                     },
                     {
                        "MMepId": "6400",
                        "text": "Auštrevičius"
                     },
                     {
                        "MMepId": "6110",
                        "text": "Bearder"
                     },
                     {
                        "MMepId": "6646",
                        "text": "Becerra Basterrechea"
                     },
                     {
                        "MMepId": "6089",
                        "text": "Bilbao Barandica"
                     },
                     {
                        "MMepId": "5328",
                        "text": "Cavada"
                     },
                     {
                        "MMepId": "6226",
                        "text": "De Backer"
                     },
                     {
                        "MMepId": "1060",
                        "text": "Deprez"
                     },
                     {
                        "MMepId": "6439",
                        "text": "Diaconu"
                     },
                     {
                        "MMepId": "6340",
                        "text": "Dlabajová"
                     },
                     {
                        "MMepId": "6679",
                        "text": "Faria"
                     },
                     {
                        "MMepId": "6601",
                        "text": "Federley"
                     },
                     {
                        "MMepId": "6098",
                        "text": "Gerbrandy"
                     },
                     {
                        "MMepId": "6658",
                        "text": "Girauta Vidal"
                     },
                     {
                        "MMepId": "1793",
                        "text": "Goerens"
                     },
                     {
                        "MMepId": "6169",
                        "text": "Goulard"
                     },
                     {
                        "MMepId": "5330",
                        "text": "Griesbeck"
                     },
                     {
                        "MMepId": "6397",
                        "text": "Guoga"
                     },
                     {
                        "MMepId": "5246",
                        "text": "Harkin"
                     },
                     {
                        "MMepId": "6622",
                        "text": "Huitema"
                     },
                     {
                        "MMepId": "5681",
                        "text": "Hyusmenova"
                     },
                     {
                        "MMepId": "6388",
                        "text": "Jakovčić"
                     },
                     {
                        "MMepId": "6338",
                        "text": "Ježek"
                     },
                     {
                        "MMepId": "5433",
                        "text": "Jäätteenmäki"
                     },
                     {
                        "MMepId": "6328",
                        "text": "Kallas"
                     },
                     {
                        "MMepId": "6504",
                        "text": "Kyuchyuk"
                     },
                     {
                        "MMepId": "5362",
                        "text": "Lambsdorff"
                     },
                     {
                        "MMepId": "6645",
                        "text": "Maura Barandiarán"
                     },
                     {
                        "MMepId": "6041",
                        "text": "Meissner"
                     },
                     {
                        "MMepId": "5857",
                        "text": "Michel"
                     },
                     {
                        "MMepId": "6690",
                        "text": "Mihaylova"
                     },
                     {
                        "MMepId": "6631",
                        "text": "Mlinar"
                     },
                     {
                        "MMepId": "6500",
                        "text": "Müller"
                     },
                     {
                        "MMepId": "3899",
                        "text": "Neyts-Uyttebroeck"
                     },
                     {
                        "MMepId": "5997",
                        "text": "Nicolai"
                     },
                     {
                        "MMepId": "6644",
                        "text": "Pagazaurtundúa Ruiz"
                     },
                     {
                        "MMepId": "6510",
                        "text": "Petersen"
                     },
                     {
                        "MMepId": "6266",
                        "text": "Radoš"
                     },
                     {
                        "MMepId": "4544",
                        "text": "Rehn"
                     },
                     {
                        "MMepId": "6056",
                        "text": "Riquet"
                     },
                     {
                        "MMepId": "5890",
                        "text": "Rohde"
                     },
                     {
                        "MMepId": "6100",
                        "text": "Schaake"
                     },
                     {
                        "MMepId": "6174",
                        "text": "Sosa Wagner"
                     },
                     {
                        "MMepId": "6042",
                        "text": "Theurer"
                     },
                     {
                        "MMepId": "6281",
                        "text": "Torvalds"
                     },
                     {
                        "MMepId": "6181",
                        "text": "Tremosa i Balcells"
                     },
                     {
                        "MMepId": "6518",
                        "text": "Tørnæs"
                     },
                     {
                        "MMepId": "6159",
                        "text": "Verhofstadt"
                     },
                     {
                        "MMepId": "4550",
                        "text": "Väyrynen"
                     },
                     {
                        "MMepId": "5864",
                        "text": "Wikström"
                     },
                     {
                        "MMepId": "4758",
                        "text": "de Sarnez"
                     },
                     {
                        "MMepId": "5385",
                        "text": "in 't Veld"
                     },
                     {
                        "MMepId": "6097",
                        "text": "van Baalen"
                     },
                     {
                        "MMepId": "6626",
                        "text": "van Miltenburg"
                     },
                     {
                        "MMepId": "6621",
                        "text": "van Nieuwenhuizen"
                     }
                  ]
               },
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5490",
                        "text": "Czarnecki"
                     },
                     {
                        "MMepId": "6291",
                        "text": "Demesmaeker"
                     },
                     {
                        "MMepId": "6538",
                        "text": "Duda"
                     },
                     {
                        "MMepId": "5472",
                        "text": "Fotyga"
                     },
                     {
                        "MMepId": "6539",
                        "text": "Gosiewska"
                     },
                     {
                        "MMepId": "5963",
                        "text": "Gróbarczyk"
                     },
                     {
                        "MMepId": "6525",
                        "text": "Karski"
                     },
                     {
                        "MMepId": "6530",
                        "text": "Krasnodębski"
                     },
                     {
                        "MMepId": "5507",
                        "text": "Kuźmiuk"
                     },
                     {
                        "MMepId": "6536",
                        "text": "Ożóg"
                     },
                     {
                        "MMepId": "6512",
                        "text": "Piecha"
                     },
                     {
                        "MMepId": "5974",
                        "text": "Poręba"
                     },
                     {
                        "MMepId": "6682",
                        "text": "Stevens"
                     },
                     {
                        "MMepId": "5113",
                        "text": "Ujazdowski"
                     },
                     {
                        "MMepId": "6515",
                        "text": "Wiśniewska"
                     },
                     {
                        "MMepId": "5108",
                        "text": "Wojciechowski"
                     },
                     {
                        "MMepId": "6522",
                        "text": "Złotowski"
                     }
                  ]
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6590",
                        "text": "Carver"
                     },
                     {
                        "MMepId": "6472",
                        "text": "D'Amato"
                     },
                     {
                        "MMepId": "6413",
                        "text": "Evi"
                     }
                  ]
               },
               {
                  "MIdentifier": "PPE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "1340",
                        "text": "Alliot-Marie"
                     },
                     {
                        "MMepId": "1178",
                        "text": "Arias Cañete"
                     },
                     {
                        "MMepId": "6594",
                        "text": "Arimont"
                     },
                     {
                        "MMepId": "4742",
                        "text": "Ayuso"
                     },
                     {
                        "MMepId": "6191",
                        "text": "Bach"
                     },
                     {
                        "MMepId": "6135",
                        "text": "Balz"
                     },
                     {
                        "MMepId": "6216",
                        "text": "Becker"
                     },
                     {
                        "MMepId": "5377",
                        "text": "Belet"
                     },
                     {
                        "MMepId": "5885",
                        "text": "Bendtsen"
                     },
                     {
                        "MMepId": "6343",
                        "text": "Bocskor"
                     },
                     {
                        "MMepId": "6613",
                        "text": "Bogovič"
                     },
                     {
                        "MMepId": "6535",
                        "text": "Boni"
                     },
                     {
                        "MMepId": "1278",
                        "text": "Brok"
                     },
                     {
                        "MMepId": "6617",
                        "text": "Buda"
                     },
                     {
                        "MMepId": "5388",
                        "text": "Buzek"
                     },
                     {
                        "MMepId": "5729",
                        "text": "Buşoi"
                     },
                     {
                        "MMepId": "1507",
                        "text": "Böge"
                     },
                     {
                        "MMepId": "5252",
                        "text": "Casa"
                     },
                     {
                        "MMepId": "5341",
                        "text": "Caspary"
                     },
                     {
                        "MMepId": "5554",
                        "text": "Cesa"
                     },
                     {
                        "MMepId": "6492",
                        "text": "Cicu"
                     },
                     {
                        "MMepId": "6406",
                        "text": "Cirio"
                     },
                     {
                        "MMepId": "6600",
                        "text": "Clune"
                     },
                     {
                        "MMepId": "3800",
                        "text": "Coelho"
                     },
                     {
                        "MMepId": "6273",
                        "text": "Collin-Langen"
                     },
                     {
                        "MMepId": "5948",
                        "text": "Comi"
                     },
                     {
                        "MMepId": "6587",
                        "text": "Comodini Cachia"
                     },
                     {
                        "MMepId": "6548",
                        "text": "Csáky"
                     },
                     {
                        "MMepId": "5921",
                        "text": "Danjean"
                     },
                     {
                        "MMepId": "6194",
                        "text": "Dantin"
                     },
                     {
                        "MMepId": "6364",
                        "text": "Delahaye"
                     },
                     {
                        "MMepId": "6345",
                        "text": "Deli"
                     },
                     {
                        "MMepId": "5998",
                        "text": "Deutsch"
                     },
                     {
                        "MMepId": "5350",
                        "text": "Deß"
                     },
                     {
                        "MMepId": "5601",
                        "text": "Dombrovskis"
                     },
                     {
                        "MMepId": "6481",
                        "text": "Donchev"
                     },
                     {
                        "MMepId": "5960",
                        "text": "Dorfmann"
                     },
                     {
                        "MMepId": "5515",
                        "text": "Díaz de Mera García Consuegra"
                     },
                     {
                        "MMepId": "5348",
                        "text": "Ehler"
                     },
                     {
                        "MMepId": "6047",
                        "text": "Engel"
                     },
                     {
                        "MMepId": "6342",
                        "text": "Erdős"
                     },
                     {
                        "MMepId": "5983",
                        "text": "Estaràs Ferragut"
                     },
                     {
                        "MMepId": "3833",
                        "text": "Ferber"
                     },
                     {
                        "MMepId": "6068",
                        "text": "Fernandes"
                     },
                     {
                        "MMepId": "5903",
                        "text": "Fisas Ayxelà"
                     },
                     {
                        "MMepId": "4880",
                        "text": "Fitto"
                     },
                     {
                        "MMepId": "1508",
                        "text": "Florenz"
                     },
                     {
                        "MMepId": "6020",
                        "text": "Gabriel"
                     },
                     {
                        "MMepId": "4659",
                        "text": "Gahler"
                     },
                     {
                        "MMepId": "6615",
                        "text": "Gambús"
                     },
                     {
                        "MMepId": "5803",
                        "text": "Gardini"
                     },
                     {
                        "MMepId": "6441",
                        "text": "Gieseke"
                     },
                     {
                        "MMepId": "6632",
                        "text": "González Pons"
                     },
                     {
                        "MMepId": "4032",
                        "text": "Grossetête"
                     },
                     {
                        "MMepId": "5109",
                        "text": "Grzyb"
                     },
                     {
                        "MMepId": "5342",
                        "text": "Gräßle"
                     },
                     {
                        "MMepId": "5138",
                        "text": "Gyürk"
                     },
                     {
                        "MMepId": "5280",
                        "text": "Gál"
                     },
                     {
                        "MMepId": "6202",
                        "text": "Gáll-Pelcz"
                     },
                     {
                        "MMepId": "6596",
                        "text": "Hayes"
                     },
                     {
                        "MMepId": "5715",
                        "text": "Hellvig"
                     },
                     {
                        "MMepId": "5018",
                        "text": "Herranz García"
                     },
                     {
                        "MMepId": "6534",
                        "text": "Hetman"
                     },
                     {
                        "MMepId": "5953",
                        "text": "Hohlmeier"
                     },
                     {
                        "MMepId": "4987",
                        "text": "Hortefeux"
                     },
                     {
                        "MMepId": "5254",
                        "text": "Hökmark"
                     },
                     {
                        "MMepId": "6346",
                        "text": "Hölvényi"
                     },
                     {
                        "MMepId": "5952",
                        "text": "Hübner"
                     },
                     {
                        "MMepId": "5945",
                        "text": "Jahr"
                     },
                     {
                        "MMepId": "5954",
                        "text": "Jazłowiecka"
                     },
                     {
                        "MMepId": "5978",
                        "text": "Jiménez-Becerril Barrio"
                     },
                     {
                        "MMepId": "6360",
                        "text": "Joulaud"
                     },
                     {
                        "MMepId": "6055",
                        "text": "Juvin"
                     },
                     {
                        "MMepId": "5944",
                        "text": "Kalinowski"
                     },
                     {
                        "MMepId": "6095",
                        "text": "Kalniete"
                     },
                     {
                        "MMepId": "4669",
                        "text": "Karas"
                     },
                     {
                        "MMepId": "6070",
                        "text": "Kariņš"
                     },
                     {
                        "MMepId": "6666",
                        "text": "Kefalogiannis"
                     },
                     {
                        "MMepId": "5542",
                        "text": "Kelam"
                     },
                     {
                        "MMepId": "5855",
                        "text": "Kelly"
                     },
                     {
                        "MMepId": "3759",
                        "text": "Koch"
                     },
                     {
                        "MMepId": "6196",
                        "text": "Kovatchev"
                     },
                     {
                        "MMepId": "6527",
                        "text": "Kozłowska-Rajewicz"
                     },
                     {
                        "MMepId": "5403",
                        "text": "Kudrycka"
                     },
                     {
                        "MMepId": "5940",
                        "text": "Kuhn"
                     },
                     {
                        "MMepId": "5840",
                        "text": "Kukan"
                     },
                     {
                        "MMepId": "6662",
                        "text": "Kyrtsos"
                     },
                     {
                        "MMepId": "6001",
                        "text": "Kósa"
                     },
                     {
                        "MMepId": "6053",
                        "text": "Köstinger"
                     },
                     {
                        "MMepId": "6393",
                        "text": "Landsbergis"
                     },
                     {
                        "MMepId": "3856",
                        "text": "Langen"
                     },
                     {
                        "MMepId": "6363",
                        "text": "Lavrilleux"
                     },
                     {
                        "MMepId": "6200",
                        "text": "Le Grip"
                     },
                     {
                        "MMepId": "6592",
                        "text": "Lenaers"
                     },
                     {
                        "MMepId": "5105",
                        "text": "Lewandowski"
                     },
                     {
                        "MMepId": "6442",
                        "text": "Lins"
                     },
                     {
                        "MMepId": "5938",
                        "text": "Lope Fontagné"
                     },
                     {
                        "MMepId": "6303",
                        "text": "Maletić"
                     },
                     {
                        "MMepId": "6245",
                        "text": "Malinov"
                     },
                     {
                        "MMepId": "6096",
                        "text": "Mato"
                     },
                     {
                        "MMepId": "6440",
                        "text": "McAllister"
                     },
                     {
                        "MMepId": "5245",
                        "text": "McGuinness"
                     },
                     {
                        "MMepId": "6297",
                        "text": "Metsola"
                     },
                     {
                        "MMepId": "5308",
                        "text": "Mikolášik"
                     },
                     {
                        "MMepId": "5518",
                        "text": "Millán Mon"
                     },
                     {
                        "MMepId": "6366",
                        "text": "Monteiro de Aguiar"
                     },
                     {
                        "MMepId": "5732",
                        "text": "Morin-Chartier"
                     },
                     {
                        "MMepId": "6436",
                        "text": "Mureşan"
                     },
                     {
                        "MMepId": "6361",
                        "text": "Muselier"
                     },
                     {
                        "MMepId": "5547",
                        "text": "Mussolini"
                     },
                     {
                        "MMepId": "6544",
                        "text": "Nagy"
                     },
                     {
                        "MMepId": "4712",
                        "text": "Niebler"
                     },
                     {
                        "MMepId": "6332",
                        "text": "Niedermayer"
                     },
                     {
                        "MMepId": "5407",
                        "text": "Olbrycht"
                     },
                     {
                        "MMepId": "6377",
                        "text": "Pabriks"
                     },
                     {
                        "MMepId": "5702",
                        "text": "Patriciello"
                     },
                     {
                        "MMepId": "5033",
                        "text": "Peterle"
                     },
                     {
                        "MMepId": "6383",
                        "text": "Petir"
                     },
                     {
                        "MMepId": "5346",
                        "text": "Pieper"
                     },
                     {
                        "MMepId": "5788",
                        "text": "Pietikäinen"
                     },
                     {
                        "MMepId": "6537",
                        "text": "Pitera"
                     },
                     {
                        "MMepId": "6268",
                        "text": "Plenković"
                     },
                     {
                        "MMepId": "6519",
                        "text": "Plura"
                     },
                     {
                        "MMepId": "6491",
                        "text": "Pogliese"
                     },
                     {
                        "MMepId": "6335",
                        "text": "Polčák"
                     },
                     {
                        "MMepId": "6092",
                        "text": "Ponga"
                     },
                     {
                        "MMepId": "6700",
                        "text": "Pospíšil"
                     },
                     {
                        "MMepId": "6010",
                        "text": "Preda"
                     },
                     {
                        "MMepId": "6224",
                        "text": "Proust"
                     },
                     {
                        "MMepId": "1487",
                        "text": "Quisthoudt-Rowohl"
                     },
                     {
                        "MMepId": "6488",
                        "text": "Radev"
                     },
                     {
                        "MMepId": "6072",
                        "text": "Rangel"
                     },
                     {
                        "MMepId": "1335",
                        "text": "Reding"
                     },
                     {
                        "MMepId": "5347",
                        "text": "Reul"
                     },
                     {
                        "MMepId": "6684",
                        "text": "Rolin"
                     },
                     {
                        "MMepId": "5512",
                        "text": "Rosati"
                     },
                     {
                        "MMepId": "6362",
                        "text": "Ruas"
                     },
                     {
                        "MMepId": "4596",
                        "text": "Rübig"
                     },
                     {
                        "MMepId": "6698",
                        "text": "Salini"
                     },
                     {
                        "MMepId": "6370",
                        "text": "Sander"
                     },
                     {
                        "MMepId": "6259",
                        "text": "Sarvamaa"
                     },
                     {
                        "MMepId": "5416",
                        "text": "Saryusz-Wolski"
                     },
                     {
                        "MMepId": "4768",
                        "text": "Saïfi"
                     },
                     {
                        "MMepId": "6619",
                        "text": "Schmidt"
                     },
                     {
                        "MMepId": "6636",
                        "text": "Schreijer-Pierik"
                     },
                     {
                        "MMepId": "6443",
                        "text": "Schulze"
                     },
                     {
                        "MMepId": "5345",
                        "text": "Schwab"
                     },
                     {
                        "MMepId": "5265",
                        "text": "Schöpflin"
                     },
                     {
                        "MMepId": "6428",
                        "text": "Sernagiotto"
                     },
                     {
                        "MMepId": "5778",
                        "text": "Stolojan"
                     },
                     {
                        "MMepId": "6320",
                        "text": "Stylianides"
                     },
                     {
                        "MMepId": "6672",
                        "text": "Svoboda"
                     },
                     {
                        "MMepId": "6528",
                        "text": "Szejnfeld"
                     },
                     {
                        "MMepId": "5143",
                        "text": "Szájer"
                     },
                     {
                        "MMepId": "5781",
                        "text": "Sógor"
                     },
                     {
                        "MMepId": "4482",
                        "text": "Tajani"
                     },
                     {
                        "MMepId": "5207",
                        "text": "Theocharous"
                     },
                     {
                        "MMepId": "5949",
                        "text": "Thun und Hohenstein"
                     },
                     {
                        "MMepId": "3738",
                        "text": "Thyssen"
                     },
                     {
                        "MMepId": "6681",
                        "text": "Tomc"
                     },
                     {
                        "MMepId": "6398",
                        "text": "Toti"
                     },
                     {
                        "MMepId": "5783",
                        "text": "Tőkés"
                     },
                     {
                        "MMepId": "6007",
                        "text": "Ungureanu"
                     },
                     {
                        "MMepId": "5734",
                        "text": "Urutchev"
                     },
                     {
                        "MMepId": "5930",
                        "text": "Verheyen"
                     },
                     {
                        "MMepId": "6356",
                        "text": "Virkkunen"
                     },
                     {
                        "MMepId": "5934",
                        "text": "Voss"
                     },
                     {
                        "MMepId": "6664",
                        "text": "Vozemberg"
                     },
                     {
                        "MMepId": "5714",
                        "text": "Vălean"
                     },
                     {
                        "MMepId": "5947",
                        "text": "Wałęsa"
                     },
                     {
                        "MMepId": "5351",
                        "text": "Weber Manfred"
                     },
                     {
                        "MMepId": "5770",
                        "text": "Weber Renate"
                     },
                     {
                        "MMepId": "6542",
                        "text": "Wenta"
                     },
                     {
                        "MMepId": "4643",
                        "text": "Wieland"
                     },
                     {
                        "MMepId": "5782",
                        "text": "Winkler Iuliu"
                     },
                     {
                        "MMepId": "6665",
                        "text": "Zagorakis"
                     },
                     {
                        "MMepId": "5936",
                        "text": "Zalba Bidegain"
                     },
                     {
                        "MMepId": "6344",
                        "text": "Zdechovský"
                     },
                     {
                        "MMepId": "6532",
                        "text": "Zdrojewski"
                     },
                     {
                        "MMepId": "6094",
                        "text": "Zver"
                     },
                     {
                        "MMepId": "5420",
                        "text": "Zwiefka"
                     },
                     {
                        "MMepId": "5189",
                        "text": "Záborská"
                     },
                     {
                        "MMepId": "5727",
                        "text": "de Lange"
                     },
                     {
                        "MMepId": "5508",
                        "text": "del Castillo Vera"
                     },
                     {
                        "MMepId": "5295",
                        "text": "van Nistelrooij"
                     },
                     {
                        "MMepId": "5928",
                        "text": "van de Camp"
                     },
                     {
                        "MMepId": "5964",
                        "text": "Łukacijewska"
                     },
                     {
                        "MMepId": "6333",
                        "text": "Štětina"
                     },
                     {
                        "MMepId": "6301",
                        "text": "Šuica"
                     },
                     {
                        "MMepId": "6680",
                        "text": "Šulin"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6651",
                        "text": "Aguilera García"
                     },
                     {
                        "MMepId": "6567",
                        "text": "Anderson Lucy"
                     },
                     {
                        "MMepId": "6279",
                        "text": "Andrieu"
                     },
                     {
                        "MMepId": "6687",
                        "text": "Androulakis"
                     },
                     {
                        "MMepId": "6553",
                        "text": "Arena"
                     },
                     {
                        "MMepId": "5426",
                        "text": "Assis"
                     },
                     {
                        "MMepId": "5411",
                        "text": "Ayala Sender"
                     },
                     {
                        "MMepId": "6378",
                        "text": "Balas"
                     },
                     {
                        "MMepId": "5866",
                        "text": "Balčytis"
                     },
                     {
                        "MMepId": "6505",
                        "text": "Benifei"
                     },
                     {
                        "MMepId": "6456",
                        "text": "Bettini"
                     },
                     {
                        "MMepId": "5867",
                        "text": "Blinkevičiūtė"
                     },
                     {
                        "MMepId": "6449",
                        "text": "Bonafè"
                     },
                     {
                        "MMepId": "6264",
                        "text": "Borzan"
                     },
                     {
                        "MMepId": "5768",
                        "text": "Boştinaru"
                     },
                     {
                        "MMepId": "5465",
                        "text": "Bresso"
                     },
                     {
                        "MMepId": "6421",
                        "text": "Briano"
                     },
                     {
                        "MMepId": "6647",
                        "text": "Cabezón Ruiz"
                     },
                     {
                        "MMepId": "6489",
                        "text": "Caputo"
                     },
                     {
                        "MMepId": "5835",
                        "text": "Childers"
                     },
                     {
                        "MMepId": "5284",
                        "text": "Christensen"
                     },
                     {
                        "MMepId": "6083",
                        "text": "Cofferati"
                     },
                     {
                        "MMepId": "4628",
                        "text": "Corbett"
                     },
                     {
                        "MMepId": "6085",
                        "text": "Costa"
                     },
                     {
                        "MMepId": "6051",
                        "text": "Cozzolino"
                     },
                     {
                        "MMepId": "5661",
                        "text": "Creţu"
                     },
                     {
                        "MMepId": "6589",
                        "text": "Dalli"
                     },
                     {
                        "MMepId": "6570",
                        "text": "Dance"
                     },
                     {
                        "MMepId": "6062",
                        "text": "De Castro"
                     },
                     {
                        "MMepId": "6437",
                        "text": "De Monte"
                     },
                     {
                        "MMepId": "6410",
                        "text": "Delvaux-Stehres"
                     },
                     {
                        "MMepId": "5816",
                        "text": "Denanot"
                     },
                     {
                        "MMepId": "6588",
                        "text": "Dodds Anneliese"
                     },
                     {
                        "MMepId": "6420",
                        "text": "Drăghici"
                     },
                     {
                        "MMepId": "6080",
                        "text": "Fajon"
                     },
                     {
                        "MMepId": "6652",
                        "text": "Fernández"
                     },
                     {
                        "MMepId": "5427",
                        "text": "Ferreira Elisa"
                     },
                     {
                        "MMepId": "5173",
                        "text": "Flašíková Beňová"
                     },
                     {
                        "MMepId": "6012",
                        "text": "Fleckenstein"
                     },
                     {
                        "MMepId": "6620",
                        "text": "Freund"
                     },
                     {
                        "MMepId": "6424",
                        "text": "Frunzulică"
                     },
                     {
                        "MMepId": "5417",
                        "text": "García Pérez"
                     },
                     {
                        "MMepId": "6130",
                        "text": "Gardiazabal Rubial"
                     },
                     {
                        "MMepId": "3827",
                        "text": "Gebhardt"
                     },
                     {
                        "MMepId": "6485",
                        "text": "Gentile"
                     },
                     {
                        "MMepId": "5497",
                        "text": "Gierek"
                     },
                     {
                        "MMepId": "4937",
                        "text": "Gill Neena"
                     },
                     {
                        "MMepId": "6502",
                        "text": "Giuffrida"
                     },
                     {
                        "MMepId": "5425",
                        "text": "Gomes"
                     },
                     {
                        "MMepId": "6580",
                        "text": "Griffin"
                     },
                     {
                        "MMepId": "5692",
                        "text": "Groote"
                     },
                     {
                        "MMepId": "6063",
                        "text": "Gualtieri"
                     },
                     {
                        "MMepId": "5941",
                        "text": "Guerrero Salom"
                     },
                     {
                        "MMepId": "6107",
                        "text": "Guillaume"
                     },
                     {
                        "MMepId": "6603",
                        "text": "Guteland"
                     },
                     {
                        "MMepId": "5261",
                        "text": "Hedh"
                     },
                     {
                        "MMepId": "4103",
                        "text": "Howitt"
                     },
                     {
                        "MMepId": "5738",
                        "text": "Iotova"
                     },
                     {
                        "MMepId": "6029",
                        "text": "Ivan"
                     },
                     {
                        "MMepId": "5870",
                        "text": "Jaakonsaari"
                     },
                     {
                        "MMepId": "6624",
                        "text": "Jongerius"
                     },
                     {
                        "MMepId": "6161",
                        "text": "Jáuregui Atondo"
                     },
                     {
                        "MMepId": "6146",
                        "text": "Kadenbach"
                     },
                     {
                        "MMepId": "6686",
                        "text": "Kaili"
                     },
                     {
                        "MMepId": "3756",
                        "text": "Kaufmann"
                     },
                     {
                        "MMepId": "6326",
                        "text": "Keller Jan"
                     },
                     {
                        "MMepId": "6581",
                        "text": "Khan"
                     },
                     {
                        "MMepId": "6571",
                        "text": "Kirton-Darling"
                     },
                     {
                        "MMepId": "6508",
                        "text": "Kofod"
                     },
                     {
                        "MMepId": "6367",
                        "text": "Kumpula-Natri"
                     },
                     {
                        "MMepId": "6435",
                        "text": "Kyenge"
                     },
                     {
                        "MMepId": "6689",
                        "text": "Kyrkos"
                     },
                     {
                        "MMepId": "3823",
                        "text": "Lange"
                     },
                     {
                        "MMepId": "6329",
                        "text": "Lauristin"
                     },
                     {
                        "MMepId": "5371",
                        "text": "Leichtfried"
                     },
                     {
                        "MMepId": "4685",
                        "text": "Leinen"
                     },
                     {
                        "MMepId": "5094",
                        "text": "Liberadzki"
                     },
                     {
                        "MMepId": "6477",
                        "text": "Lietz"
                     },
                     {
                        "MMepId": "5860",
                        "text": "Ludvigsson"
                     },
                     {
                        "MMepId": "5984",
                        "text": "López Aguilar"
                     },
                     {
                        "MMepId": "6380",
                        "text": "Mamikins"
                     },
                     {
                        "MMepId": "1133",
                        "text": "Martin David"
                     },
                     {
                        "MMepId": "5314",
                        "text": "Maňka"
                     },
                     {
                        "MMepId": "4646",
                        "text": "McAvan"
                     },
                     {
                        "MMepId": "6476",
                        "text": "Melior"
                     },
                     {
                        "MMepId": "6296",
                        "text": "Mizzi"
                     },
                     {
                        "MMepId": "6423",
                        "text": "Moisă"
                     },
                     {
                        "MMepId": "6351",
                        "text": "Molnár"
                     },
                     {
                        "MMepId": "6562",
                        "text": "Moody"
                     },
                     {
                        "MMepId": "4923",
                        "text": "Moraes"
                     },
                     {
                        "MMepId": "6433",
                        "text": "Moretti"
                     },
                     {
                        "MMepId": "6422",
                        "text": "Morgano"
                     },
                     {
                        "MMepId": "6506",
                        "text": "Mosca"
                     },
                     {
                        "MMepId": "6691",
                        "text": "Negrescu"
                     },
                     {
                        "MMepId": "6016",
                        "text": "Neuser"
                     },
                     {
                        "MMepId": "6418",
                        "text": "Nica"
                     },
                     {
                        "MMepId": "6352",
                        "text": "Niedermüller"
                     },
                     {
                        "MMepId": "6242",
                        "text": "Nilsson"
                     },
                     {
                        "MMepId": "6473",
                        "text": "Noichl"
                     },
                     {
                        "MMepId": "5483",
                        "text": "Panzeri"
                     },
                     {
                        "MMepId": "6487",
                        "text": "Paolucci"
                     },
                     {
                        "MMepId": "6323",
                        "text": "Papadakis Demetris"
                     },
                     {
                        "MMepId": "5648",
                        "text": "Paşcu"
                     },
                     {
                        "MMepId": "5296",
                        "text": "Peillon"
                     },
                     {
                        "MMepId": "6484",
                        "text": "Picierno"
                     },
                     {
                        "MMepId": "6261",
                        "text": "Picula"
                     },
                     {
                        "MMepId": "6625",
                        "text": "Piri"
                     },
                     {
                        "MMepId": "6493",
                        "text": "Pirinski"
                     },
                     {
                        "MMepId": "4851",
                        "text": "Pittella"
                     },
                     {
                        "MMepId": "6330",
                        "text": "Poche"
                     },
                     {
                        "MMepId": "6526",
                        "text": "Preuß"
                     },
                     {
                        "MMepId": "6426",
                        "text": "Rebega"
                     },
                     {
                        "MMepId": "6136",
                        "text": "Regner"
                     },
                     {
                        "MMepId": "6316",
                        "text": "Revault D'Allonnes Bonnefoy"
                     },
                     {
                        "MMepId": "6677",
                        "text": "Rodrigues Liliana"
                     },
                     {
                        "MMepId": "6371",
                        "text": "Rodrigues Maria João"
                     },
                     {
                        "MMepId": "6649",
                        "text": "Rodríguez-Piñero Fernández"
                     },
                     {
                        "MMepId": "5813",
                        "text": "Rodust"
                     },
                     {
                        "MMepId": "6357",
                        "text": "Rozière"
                     },
                     {
                        "MMepId": "5713",
                        "text": "Schaldemose"
                     },
                     {
                        "MMepId": "6438",
                        "text": "Schlein"
                     },
                     {
                        "MMepId": "3825",
                        "text": "Schulz"
                     },
                     {
                        "MMepId": "6475",
                        "text": "Schuster"
                     },
                     {
                        "MMepId": "5897",
                        "text": "Sehnalová"
                     },
                     {
                        "MMepId": "6375",
                        "text": "Serrão Santos"
                     },
                     {
                        "MMepId": "6381",
                        "text": "Silva Pereira"
                     },
                     {
                        "MMepId": "6008",
                        "text": "Simon Peter"
                     },
                     {
                        "MMepId": "6093",
                        "text": "Sippel"
                     },
                     {
                        "MMepId": "6498",
                        "text": "Soru"
                     },
                     {
                        "MMepId": "6003",
                        "text": "Steinruck"
                     },
                     {
                        "MMepId": "4949",
                        "text": "Stihler"
                     },
                     {
                        "MMepId": "6334",
                        "text": "Szanyi"
                     },
                     {
                        "MMepId": "5653",
                        "text": "Sârbu"
                     },
                     {
                        "MMepId": "6623",
                        "text": "Tang"
                     },
                     {
                        "MMepId": "6427",
                        "text": "Tapardel"
                     },
                     {
                        "MMepId": "5624",
                        "text": "Tarabella"
                     },
                     {
                        "MMepId": "6282",
                        "text": "Thomas"
                     },
                     {
                        "MMepId": "5459",
                        "text": "Toia"
                     },
                     {
                        "MMepId": "6336",
                        "text": "Ujhelyi"
                     },
                     {
                        "MMepId": "5859",
                        "text": "Ulvskog"
                     },
                     {
                        "MMepId": "4990",
                        "text": "Van Brempt"
                     },
                     {
                        "MMepId": "6086",
                        "text": "Vaughan"
                     },
                     {
                        "MMepId": "6235",
                        "text": "Weidenholzer"
                     },
                     {
                        "MMepId": "6463",
                        "text": "Werner"
                     },
                     {
                        "MMepId": "6011",
                        "text": "Westphal"
                     },
                     {
                        "MMepId": "5698",
                        "text": "Willmott"
                     },
                     {
                        "MMepId": "5845",
                        "text": "Zala"
                     },
                     {
                        "MMepId": "6434",
                        "text": "Zanonato"
                     },
                     {
                        "MMepId": "5957",
                        "text": "Zemke"
                     },
                     {
                        "MMepId": "6373",
                        "text": "Zorrinho"
                     },
                     {
                        "MMepId": "6521",
                        "text": "Łybacka"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6552",
                        "text": "Reimon"
                     },
                     {
                        "MMepId": "6529",
                        "text": "Reintke"
                     },
                     {
                        "MMepId": "6694",
                        "text": "Ropé"
                     }
                  ]
               }
            ]
         },
         "ResultAgainst": {
            "MNumber": "170",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ALDE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6376",
                        "text": "Marinho e Pinto"
                     },
                     {
                        "MMepId": "6614",
                        "text": "Nart"
                     },
                     {
                        "MMepId": "6546",
                        "text": "Sulík"
                     }
                  ]
               },
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5262",
                        "text": "Ashworth"
                     },
                     {
                        "MMepId": "4911",
                        "text": "Belder"
                     },
                     {
                        "MMepId": "4944",
                        "text": "Bradbourn"
                     },
                     {
                        "MMepId": "6081",
                        "text": "Campbell Bannerman"
                     },
                     {
                        "MMepId": "4960",
                        "text": "Deva"
                     },
                     {
                        "MMepId": "6516",
                        "text": "Dohrmann"
                     },
                     {
                        "MMepId": "6579",
                        "text": "Duncan"
                     },
                     {
                        "MMepId": "6511",
                        "text": "Dzhambazki"
                     },
                     {
                        "MMepId": "6104",
                        "text": "Ford"
                     },
                     {
                        "MMepId": "4957",
                        "text": "Foster"
                     },
                     {
                        "MMepId": "6112",
                        "text": "Fox"
                     },
                     {
                        "MMepId": "6452",
                        "text": "Gericke"
                     },
                     {
                        "MMepId": "6111",
                        "text": "Girling"
                     },
                     {
                        "MMepId": "6358",
                        "text": "Halla-aho"
                     },
                     {
                        "MMepId": "4959",
                        "text": "Hannan"
                     },
                     {
                        "MMepId": "6460",
                        "text": "Henkel"
                     },
                     {
                        "MMepId": "5630",
                        "text": "Kamall"
                     },
                     {
                        "MMepId": "6514",
                        "text": "Karlsson"
                     },
                     {
                        "MMepId": "4946",
                        "text": "Kirkhope"
                     },
                     {
                        "MMepId": "6461",
                        "text": "Kölmel"
                     },
                     {
                        "MMepId": "6561",
                        "text": "Lewer"
                     },
                     {
                        "MMepId": "6457",
                        "text": "Lucke"
                     },
                     {
                        "MMepId": "6667",
                        "text": "Marias"
                     },
                     {
                        "MMepId": "6087",
                        "text": "McClarkin"
                     },
                     {
                        "MMepId": "6234",
                        "text": "McIntyre"
                     },
                     {
                        "MMepId": "5851",
                        "text": "Messerschmidt"
                     },
                     {
                        "MMepId": "1230",
                        "text": "Nicholson"
                     },
                     {
                        "MMepId": "6467",
                        "text": "Pretzell"
                     },
                     {
                        "MMepId": "6464",
                        "text": "Starbatty"
                     },
                     {
                        "MMepId": "6088",
                        "text": "Swinburne"
                     },
                     {
                        "MMepId": "4925",
                        "text": "Tannock"
                     },
                     {
                        "MMepId": "6220",
                        "text": "Terho"
                     },
                     {
                        "MMepId": "6300",
                        "text": "Tomašić"
                     },
                     {
                        "MMepId": "4917",
                        "text": "Van Orden"
                     },
                     {
                        "MMepId": "6683",
                        "text": "Van Overtveldt"
                     },
                     {
                        "MMepId": "6513",
                        "text": "Vistisen"
                     },
                     {
                        "MMepId": "5052",
                        "text": "Zahradil"
                     },
                     {
                        "MMepId": "5981",
                        "text": "van Dalen"
                     },
                     {
                        "MMepId": "6545",
                        "text": "Škripek"
                     },
                     {
                        "MMepId": "6540",
                        "text": "Žitňanská"
                     }
                  ]
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6431",
                        "text": "Affronte"
                     },
                     {
                        "MMepId": "6066",
                        "text": "Agnew"
                     },
                     {
                        "MMepId": "6559",
                        "text": "Aker"
                     },
                     {
                        "MMepId": "6577",
                        "text": "Arnott"
                     },
                     {
                        "MMepId": "6574",
                        "text": "Bashir"
                     },
                     {
                        "MMepId": "6583",
                        "text": "Bours"
                     },
                     {
                        "MMepId": "6446",
                        "text": "Castaldo"
                     },
                     {
                        "MMepId": "6586",
                        "text": "Coburn"
                     },
                     {
                        "MMepId": "6494",
                        "text": "Corrao"
                     },
                     {
                        "MMepId": "4929",
                        "text": "Farage"
                     },
                     {
                        "MMepId": "6470",
                        "text": "Ferrara"
                     },
                     {
                        "MMepId": "6558",
                        "text": "Finch"
                     },
                     {
                        "MMepId": "6584",
                        "text": "Gill Nathan"
                     },
                     {
                        "MMepId": "6379",
                        "text": "Grigule"
                     },
                     {
                        "MMepId": "4920",
                        "text": "Helmer"
                     },
                     {
                        "MMepId": "6556",
                        "text": "James"
                     },
                     {
                        "MMepId": "6608",
                        "text": "Lundgren"
                     },
                     {
                        "MMepId": "6448",
                        "text": "Mach"
                     },
                     {
                        "MMepId": "6402",
                        "text": "Mazuronis"
                     },
                     {
                        "MMepId": "5977",
                        "text": "Nuttall"
                     },
                     {
                        "MMepId": "6557",
                        "text": "O'Flynn"
                     },
                     {
                        "MMepId": "6563",
                        "text": "Parker"
                     },
                     {
                        "MMepId": "6564",
                        "text": "Reid"
                     },
                     {
                        "MMepId": "6412",
                        "text": "Valli"
                     },
                     {
                        "MMepId": "6607",
                        "text": "Winberg"
                     },
                     {
                        "MMepId": "6585",
                        "text": "Woolfe"
                     },
                     {
                        "MMepId": "6414",
                        "text": "Zanni"
                     }
                  ]
               },
               {
                  "MIdentifier": "GUE/NGL",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6654",
                        "text": "Albiol Guzmán"
                     },
                     {
                        "MMepId": "6280",
                        "text": "Anderson Martina"
                     },
                     {
                        "MMepId": "6598",
                        "text": "Carthy"
                     },
                     {
                        "MMepId": "6496",
                        "text": "De Masi"
                     },
                     {
                        "MMepId": "6642",
                        "text": "Echenique"
                     },
                     {
                        "MMepId": "6024",
                        "text": "Ernst"
                     },
                     {
                        "MMepId": "5886",
                        "text": "Ferreira João"
                     },
                     {
                        "MMepId": "472",
                        "text": "Glezos"
                     },
                     {
                        "MMepId": "6637",
                        "text": "Iglesias"
                     },
                     {
                        "MMepId": "6657",
                        "text": "Juaristi Abaunz"
                     },
                     {
                        "MMepId": "6313",
                        "text": "Kari"
                     },
                     {
                        "MMepId": "6661",
                        "text": "Katrougkalos"
                     },
                     {
                        "MMepId": "5039",
                        "text": "Konečná"
                     },
                     {
                        "MMepId": "5909",
                        "text": "Le Hyaric"
                     },
                     {
                        "MMepId": "6653",
                        "text": "López Paloma"
                     },
                     {
                        "MMepId": "6692",
                        "text": "Maltese"
                     },
                     {
                        "MMepId": "5992",
                        "text": "Matias"
                     },
                     {
                        "MMepId": "6310",
                        "text": "Michels"
                     },
                     {
                        "MMepId": "5916",
                        "text": "Mélenchon"
                     },
                     {
                        "MMepId": "6599",
                        "text": "Ní Riada"
                     },
                     {
                        "MMepId": "6639",
                        "text": "Rodriguez-Rubio Vázquez"
                     },
                     {
                        "MMepId": "5837",
                        "text": "Scholz"
                     },
                     {
                        "MMepId": "6655",
                        "text": "Senra Rodríguez"
                     },
                     {
                        "MMepId": "6321",
                        "text": "Sylikiotis"
                     },
                     {
                        "MMepId": "6656",
                        "text": "Vallina"
                     },
                     {
                        "MMepId": "6030",
                        "text": "Vergiat"
                     },
                     {
                        "MMepId": "6678",
                        "text": "Viegas"
                     },
                     {
                        "MMepId": "5368",
                        "text": "Zimmer"
                     },
                     {
                        "MMepId": "6253",
                        "text": "Zuber"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6593",
                        "text": "Annemans"
                     },
                     {
                        "MMepId": "6048",
                        "text": "Balczó"
                     },
                     {
                        "MMepId": "6394",
                        "text": "Bay"
                     },
                     {
                        "MMepId": "6405",
                        "text": "Bilde"
                     },
                     {
                        "MMepId": "6179",
                        "text": "Bizzotto"
                     },
                     {
                        "MMepId": "5010",
                        "text": "Borghezio"
                     },
                     {
                        "MMepId": "6387",
                        "text": "Boutonnet"
                     },
                     {
                        "MMepId": "6391",
                        "text": "Briois"
                     },
                     {
                        "MMepId": "6507",
                        "text": "Buonanno"
                     },
                     {
                        "MMepId": "6384",
                        "text": "D'Ornano"
                     },
                     {
                        "MMepId": "6106",
                        "text": "Dodds Diane"
                     },
                     {
                        "MMepId": "6401",
                        "text": "Ferrand"
                     },
                     {
                        "MMepId": "6132",
                        "text": "Fontana"
                     },
                     {
                        "MMepId": "6668",
                        "text": "Fountoulis"
                     },
                     {
                        "MMepId": "6524",
                        "text": "Iwaszkiewicz"
                     },
                     {
                        "MMepId": "6404",
                        "text": "Jalkh"
                     },
                     {
                        "MMepId": "6635",
                        "text": "Jansen"
                     },
                     {
                        "MMepId": "6629",
                        "text": "Kappel"
                     },
                     {
                        "MMepId": "6517",
                        "text": "Korwin-Mikke"
                     },
                     {
                        "MMepId": "6372",
                        "text": "Lebreton"
                     },
                     {
                        "MMepId": "6699",
                        "text": "Loiseau"
                     },
                     {
                        "MMepId": "6633",
                        "text": "Maeijer"
                     },
                     {
                        "MMepId": "6385",
                        "text": "Martin Dominique"
                     },
                     {
                        "MMepId": "6533",
                        "text": "Marusik"
                     },
                     {
                        "MMepId": "6627",
                        "text": "Mayer"
                     },
                     {
                        "MMepId": "6403",
                        "text": "Montel"
                     },
                     {
                        "MMepId": "6399",
                        "text": "Mélin"
                     },
                     {
                        "MMepId": "6676",
                        "text": "Papadakis Konstantinos"
                     },
                     {
                        "MMepId": "5522",
                        "text": "Salvini"
                     },
                     {
                        "MMepId": "6389",
                        "text": "Schaffhauser"
                     },
                     {
                        "MMepId": "6634",
                        "text": "Stuger"
                     },
                     {
                        "MMepId": "6670",
                        "text": "Synadinos"
                     },
                     {
                        "MMepId": "6392",
                        "text": "Troszczynski"
                     },
                     {
                        "MMepId": "6610",
                        "text": "Vilimsky"
                     },
                     {
                        "MMepId": "6469",
                        "text": "Voigt"
                     },
                     {
                        "MMepId": "6673",
                        "text": "Zarianopoulos"
                     },
                     {
                        "MMepId": "6630",
                        "text": "de Graaff"
                     },
                     {
                        "MMepId": "6541",
                        "text": "Żółtek"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6650",
                        "text": "Blanco López"
                     },
                     {
                        "MMepId": "6419",
                        "text": "Grapini"
                     },
                     {
                        "MMepId": "6566",
                        "text": "Simon Siôn"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6606",
                        "text": "Andersson"
                     },
                     {
                        "MMepId": "5291",
                        "text": "Auken"
                     },
                     {
                        "MMepId": "6455",
                        "text": "Buchner"
                     },
                     {
                        "MMepId": "5913",
                        "text": "Bütikofer"
                     },
                     {
                        "MMepId": "6605",
                        "text": "Ceballos"
                     },
                     {
                        "MMepId": "5358",
                        "text": "Cramer"
                     },
                     {
                        "MMepId": "6040",
                        "text": "Delli"
                     },
                     {
                        "MMepId": "6324",
                        "text": "Durand"
                     },
                     {
                        "MMepId": "5899",
                        "text": "Eickhout"
                     },
                     {
                        "MMepId": "6554",
                        "text": "Eriksson"
                     },
                     {
                        "MMepId": "5904",
                        "text": "Giegold"
                     },
                     {
                        "MMepId": "5353",
                        "text": "Harms"
                     },
                     {
                        "MMepId": "5914",
                        "text": "Jadot"
                     },
                     {
                        "MMepId": "5908",
                        "text": "Keller Ska"
                     },
                     {
                        "MMepId": "5838",
                        "text": "Lamberts"
                     },
                     {
                        "MMepId": "5902",
                        "text": "Lochbihler"
                     },
                     {
                        "MMepId": "6149",
                        "text": "Lunacek"
                     },
                     {
                        "MMepId": "6349",
                        "text": "Meszerics"
                     },
                     {
                        "MMepId": "5917",
                        "text": "Rivasi"
                     },
                     {
                        "MMepId": "6560",
                        "text": "Scott Cato"
                     },
                     {
                        "MMepId": "6659",
                        "text": "Sebastià"
                     },
                     {
                        "MMepId": "5571",
                        "text": "Smith"
                     },
                     {
                        "MMepId": "4983",
                        "text": "Staes"
                     },
                     {
                        "MMepId": "6168",
                        "text": "Tarand"
                     },
                     {
                        "MMepId": "6204",
                        "text": "Taylor"
                     },
                     {
                        "MMepId": "5360",
                        "text": "Trüpel"
                     },
                     {
                        "MMepId": "6591",
                        "text": "Urtasun"
                     },
                     {
                        "MMepId": "6551",
                        "text": "Vana"
                     },
                     {
                        "MMepId": "6390",
                        "text": "Škrlec"
                     },
                     {
                        "MMepId": "6612",
                        "text": "Šoltes"
                     }
                  ]
               }
            ]
         },
         "ResultAbstention": {
            "MNumber": "32",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ALDE",
                  "PoliticalGroupMemberName": {
                     "MMepId": "6151",
                     "text": "Vajgl"
                  }
               },
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6531",
                        "text": "Jurek"
                     },
                     {
                        "MMepId": "5892",
                        "text": "Tošenovský"
                     },
                     {
                        "MMepId": "6466",
                        "text": "Trebesius"
                     }
                  ]
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5875",
                        "text": "Paksas"
                     },
                     {
                        "MMepId": "6447",
                        "text": "Tamburrano"
                     }
                  ]
               },
               {
                  "MIdentifier": "GUE/NGL",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6693",
                        "text": "Forenza"
                     },
                     {
                        "MMepId": "6628",
                        "text": "Hazekamp"
                     },
                     {
                        "MMepId": "6675",
                        "text": "Kuneva"
                     },
                     {
                        "MMepId": "6369",
                        "text": "Kyllönen"
                     },
                     {
                        "MMepId": "5044",
                        "text": "Maštálka"
                     },
                     {
                        "MMepId": "6643",
                        "text": "Mineur"
                     },
                     {
                        "MMepId": "5046",
                        "text": "Ransdorf"
                     },
                     {
                        "MMepId": "6444",
                        "text": "Spinelli"
                     },
                     {
                        "MMepId": "5922",
                        "text": "de Jong"
                     }
                  ]
               },
               {
                  "MIdentifier": "PPE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "3845",
                        "text": "Mann"
                     },
                     {
                        "MMepId": "6365",
                        "text": "Ribeiro"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6499",
                        "text": "Chinnici"
                     },
                     {
                        "MMepId": "6014",
                        "text": "Ertug"
                     },
                     {
                        "MMepId": "6609",
                        "text": "Post"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5910",
                        "text": "Albrecht"
                     },
                     {
                        "MMepId": "4954",
                        "text": "Evans"
                     },
                     {
                        "MMepId": "4535",
                        "text": "Hautala"
                     },
                     {
                        "MMepId": "6486",
                        "text": "Heubuch"
                     },
                     {
                        "MMepId": "6350",
                        "text": "Jávor"
                     },
                     {
                        "MMepId": "4935",
                        "text": "Lambert"
                     },
                     {
                        "MMepId": "5862",
                        "text": "Lövin"
                     },
                     {
                        "MMepId": "6550",
                        "text": "Maragall"
                     },
                     {
                        "MMepId": "6453",
                        "text": "Reda"
                     },
                     {
                        "MMepId": "5987",
                        "text": "Sargentini"
                     },
                     {
                        "MMepId": "4847",
                        "text": "Turmes"
                     },
                     {
                        "MMepId": "5602",
                        "text": "Ždanoka"
                     }
                  ]
               }
            ]
         }
      },
      {
         "MDate": "2014-07-16 12:22:49",
         "MIdentifier": "47690",
         "RollCallVoteDescriptionText": "A8-0001/2014 -  Werner Langen - Am 4",
         "ResultFor": {
            "MNumber": "148",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ALDE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6407",
                        "text": "Arthuis"
                     },
                     {
                        "MMepId": "6400",
                        "text": "Auštrevičius"
                     },
                     {
                        "MMepId": "6339",
                        "text": "Charanzová"
                     },
                     {
                        "MMepId": "6679",
                        "text": "Faria"
                     },
                     {
                        "MMepId": "6614",
                        "text": "Nart"
                     },
                     {
                        "MMepId": "6097",
                        "text": "van Baalen"
                     }
                  ]
               },
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": {
                     "MMepId": "5113",
                     "text": "Ujazdowski"
                  }
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6468",
                        "text": "Adinolfi"
                     },
                     {
                        "MMepId": "6431",
                        "text": "Affronte"
                     },
                     {
                        "MMepId": "6445",
                        "text": "Agea"
                     },
                     {
                        "MMepId": "6066",
                        "text": "Agnew"
                     },
                     {
                        "MMepId": "6480",
                        "text": "Aiuto"
                     },
                     {
                        "MMepId": "6559",
                        "text": "Aker"
                     },
                     {
                        "MMepId": "6577",
                        "text": "Arnott"
                     },
                     {
                        "MMepId": "6555",
                        "text": "Atkinson"
                     },
                     {
                        "MMepId": "6574",
                        "text": "Bashir"
                     },
                     {
                        "MMepId": "5567",
                        "text": "Batten"
                     },
                     {
                        "MMepId": "6411",
                        "text": "Beghin"
                     },
                     {
                        "MMepId": "6430",
                        "text": "Borrelli"
                     },
                     {
                        "MMepId": "6583",
                        "text": "Bours"
                     },
                     {
                        "MMepId": "6590",
                        "text": "Carver"
                     },
                     {
                        "MMepId": "6446",
                        "text": "Castaldo"
                     },
                     {
                        "MMepId": "6586",
                        "text": "Coburn"
                     },
                     {
                        "MMepId": "6494",
                        "text": "Corrao"
                     },
                     {
                        "MMepId": "6472",
                        "text": "D'Amato"
                     },
                     {
                        "MMepId": "6569",
                        "text": "Etheridge"
                     },
                     {
                        "MMepId": "6413",
                        "text": "Evi"
                     },
                     {
                        "MMepId": "4929",
                        "text": "Farage"
                     },
                     {
                        "MMepId": "6470",
                        "text": "Ferrara"
                     },
                     {
                        "MMepId": "6558",
                        "text": "Finch"
                     },
                     {
                        "MMepId": "6584",
                        "text": "Gill Nathan"
                     },
                     {
                        "MMepId": "6379",
                        "text": "Grigule"
                     },
                     {
                        "MMepId": "4920",
                        "text": "Helmer"
                     },
                     {
                        "MMepId": "6575",
                        "text": "Hookem"
                     },
                     {
                        "MMepId": "6556",
                        "text": "James"
                     },
                     {
                        "MMepId": "6402",
                        "text": "Mazuronis"
                     },
                     {
                        "MMepId": "6497",
                        "text": "Moi"
                     },
                     {
                        "MMepId": "5977",
                        "text": "Nuttall"
                     },
                     {
                        "MMepId": "6557",
                        "text": "O'Flynn"
                     },
                     {
                        "MMepId": "5875",
                        "text": "Paksas"
                     },
                     {
                        "MMepId": "6563",
                        "text": "Parker"
                     },
                     {
                        "MMepId": "6482",
                        "text": "Pedicini"
                     },
                     {
                        "MMepId": "6564",
                        "text": "Reid"
                     },
                     {
                        "MMepId": "6412",
                        "text": "Valli"
                     },
                     {
                        "MMepId": "6585",
                        "text": "Woolfe"
                     },
                     {
                        "MMepId": "6414",
                        "text": "Zanni"
                     },
                     {
                        "MMepId": "6695",
                        "text": "Zullo"
                     }
                  ]
               },
               {
                  "MIdentifier": "GUE/NGL",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6654",
                        "text": "Albiol Guzmán"
                     },
                     {
                        "MMepId": "6280",
                        "text": "Anderson Martina"
                     },
                     {
                        "MMepId": "6604",
                        "text": "Björk"
                     },
                     {
                        "MMepId": "6595",
                        "text": "Boylan"
                     },
                     {
                        "MMepId": "6598",
                        "text": "Carthy"
                     },
                     {
                        "MMepId": "6701",
                        "text": "Couso Permuy"
                     },
                     {
                        "MMepId": "6496",
                        "text": "De Masi"
                     },
                     {
                        "MMepId": "6642",
                        "text": "Echenique"
                     },
                     {
                        "MMepId": "6024",
                        "text": "Ernst"
                     },
                     {
                        "MMepId": "5886",
                        "text": "Ferreira João"
                     },
                     {
                        "MMepId": "6693",
                        "text": "Forenza"
                     },
                     {
                        "MMepId": "6628",
                        "text": "Hazekamp"
                     },
                     {
                        "MMepId": "6637",
                        "text": "Iglesias"
                     },
                     {
                        "MMepId": "6313",
                        "text": "Kari"
                     },
                     {
                        "MMepId": "5039",
                        "text": "Konečná"
                     },
                     {
                        "MMepId": "6675",
                        "text": "Kuneva"
                     },
                     {
                        "MMepId": "6369",
                        "text": "Kyllönen"
                     },
                     {
                        "MMepId": "5909",
                        "text": "Le Hyaric"
                     },
                     {
                        "MMepId": "6653",
                        "text": "López Paloma"
                     },
                     {
                        "MMepId": "6692",
                        "text": "Maltese"
                     },
                     {
                        "MMepId": "5992",
                        "text": "Matias"
                     },
                     {
                        "MMepId": "5044",
                        "text": "Maštálka"
                     },
                     {
                        "MMepId": "6310",
                        "text": "Michels"
                     },
                     {
                        "MMepId": "6643",
                        "text": "Mineur"
                     },
                     {
                        "MMepId": "5916",
                        "text": "Mélenchon"
                     },
                     {
                        "MMepId": "6599",
                        "text": "Ní Riada"
                     },
                     {
                        "MMepId": "6250",
                        "text": "Omarjee"
                     },
                     {
                        "MMepId": "5596",
                        "text": "Papadimoulis"
                     },
                     {
                        "MMepId": "5046",
                        "text": "Ransdorf"
                     },
                     {
                        "MMepId": "6639",
                        "text": "Rodriguez-Rubio Vázquez"
                     },
                     {
                        "MMepId": "5837",
                        "text": "Scholz"
                     },
                     {
                        "MMepId": "6655",
                        "text": "Senra Rodríguez"
                     },
                     {
                        "MMepId": "6444",
                        "text": "Spinelli"
                     },
                     {
                        "MMepId": "6321",
                        "text": "Sylikiotis"
                     },
                     {
                        "MMepId": "6641",
                        "text": "Sánchez Caldentey"
                     },
                     {
                        "MMepId": "6656",
                        "text": "Vallina"
                     },
                     {
                        "MMepId": "6030",
                        "text": "Vergiat"
                     },
                     {
                        "MMepId": "6678",
                        "text": "Viegas"
                     },
                     {
                        "MMepId": "5368",
                        "text": "Zimmer"
                     },
                     {
                        "MMepId": "6253",
                        "text": "Zuber"
                     },
                     {
                        "MMepId": "5922",
                        "text": "de Jong"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6451",
                        "text": "Aliot"
                     },
                     {
                        "MMepId": "6593",
                        "text": "Annemans"
                     },
                     {
                        "MMepId": "6382",
                        "text": "Arnautu"
                     },
                     {
                        "MMepId": "6048",
                        "text": "Balczó"
                     },
                     {
                        "MMepId": "6405",
                        "text": "Bilde"
                     },
                     {
                        "MMepId": "6179",
                        "text": "Bizzotto"
                     },
                     {
                        "MMepId": "5010",
                        "text": "Borghezio"
                     },
                     {
                        "MMepId": "6387",
                        "text": "Boutonnet"
                     },
                     {
                        "MMepId": "6391",
                        "text": "Briois"
                     },
                     {
                        "MMepId": "6507",
                        "text": "Buonanno"
                     },
                     {
                        "MMepId": "6384",
                        "text": "D'Ornano"
                     },
                     {
                        "MMepId": "6401",
                        "text": "Ferrand"
                     },
                     {
                        "MMepId": "6132",
                        "text": "Fontana"
                     },
                     {
                        "MMepId": "6450",
                        "text": "Goddyn"
                     },
                     {
                        "MMepId": "1380",
                        "text": "Gollnisch"
                     },
                     {
                        "MMepId": "6404",
                        "text": "Jalkh"
                     },
                     {
                        "MMepId": "6635",
                        "text": "Jansen"
                     },
                     {
                        "MMepId": "6629",
                        "text": "Kappel"
                     },
                     {
                        "MMepId": "6203",
                        "text": "Kovács"
                     },
                     {
                        "MMepId": "1529",
                        "text": "Le Pen Jean-Marie"
                     },
                     {
                        "MMepId": "5332",
                        "text": "Le Pen Marine"
                     },
                     {
                        "MMepId": "6372",
                        "text": "Lebreton"
                     },
                     {
                        "MMepId": "6699",
                        "text": "Loiseau"
                     },
                     {
                        "MMepId": "6633",
                        "text": "Maeijer"
                     },
                     {
                        "MMepId": "6385",
                        "text": "Martin Dominique"
                     },
                     {
                        "MMepId": "6627",
                        "text": "Mayer"
                     },
                     {
                        "MMepId": "6395",
                        "text": "Monot"
                     },
                     {
                        "MMepId": "6403",
                        "text": "Montel"
                     },
                     {
                        "MMepId": "6399",
                        "text": "Mélin"
                     },
                     {
                        "MMepId": "6123",
                        "text": "Obermayr"
                     },
                     {
                        "MMepId": "6676",
                        "text": "Papadakis Konstantinos"
                     },
                     {
                        "MMepId": "5522",
                        "text": "Salvini"
                     },
                     {
                        "MMepId": "6389",
                        "text": "Schaffhauser"
                     },
                     {
                        "MMepId": "6634",
                        "text": "Stuger"
                     },
                     {
                        "MMepId": "6670",
                        "text": "Synadinos"
                     },
                     {
                        "MMepId": "6392",
                        "text": "Troszczynski"
                     },
                     {
                        "MMepId": "6610",
                        "text": "Vilimsky"
                     },
                     {
                        "MMepId": "6469",
                        "text": "Voigt"
                     },
                     {
                        "MMepId": "6673",
                        "text": "Zarianopoulos"
                     },
                     {
                        "MMepId": "6630",
                        "text": "de Graaff"
                     }
                  ]
               },
               {
                  "MIdentifier": "PPE",
                  "PoliticalGroupMemberName": {
                     "MMepId": "5512",
                     "text": "Rosati"
                  }
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6421",
                        "text": "Briano"
                     },
                     {
                        "MMepId": "6420",
                        "text": "Drăghici"
                     },
                     {
                        "MMepId": "6326",
                        "text": "Keller Jan"
                     },
                     {
                        "MMepId": "6322",
                        "text": "Mavrides"
                     },
                     {
                        "MMepId": "6476",
                        "text": "Melior"
                     },
                     {
                        "MMepId": "6323",
                        "text": "Papadakis Demetris"
                     },
                     {
                        "MMepId": "5894",
                        "text": "Poc"
                     },
                     {
                        "MMepId": "6426",
                        "text": "Rebega"
                     },
                     {
                        "MMepId": "6677",
                        "text": "Rodrigues Liliana"
                     },
                     {
                        "MMepId": "6427",
                        "text": "Tapardel"
                     },
                     {
                        "MMepId": "6521",
                        "text": "Łybacka"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6606",
                        "text": "Andersson"
                     },
                     {
                        "MMepId": "6605",
                        "text": "Ceballos"
                     },
                     {
                        "MMepId": "6554",
                        "text": "Eriksson"
                     },
                     {
                        "MMepId": "5862",
                        "text": "Lövin"
                     },
                     {
                        "MMepId": "6591",
                        "text": "Urtasun"
                     },
                     {
                        "MMepId": "6551",
                        "text": "Vana"
                     },
                     {
                        "MMepId": "6612",
                        "text": "Šoltes"
                     },
                     {
                        "MMepId": "5602",
                        "text": "Ždanoka"
                     }
                  ]
               }
            ]
         },
         "ResultAgainst": {
            "MNumber": "514",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ALDE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5682",
                        "text": "Ali"
                     },
                     {
                        "MMepId": "6327",
                        "text": "Ansip"
                     },
                     {
                        "MMepId": "6110",
                        "text": "Bearder"
                     },
                     {
                        "MMepId": "6646",
                        "text": "Becerra Basterrechea"
                     },
                     {
                        "MMepId": "6089",
                        "text": "Bilbao Barandica"
                     },
                     {
                        "MMepId": "5328",
                        "text": "Cavada"
                     },
                     {
                        "MMepId": "6226",
                        "text": "De Backer"
                     },
                     {
                        "MMepId": "1060",
                        "text": "Deprez"
                     },
                     {
                        "MMepId": "6439",
                        "text": "Diaconu"
                     },
                     {
                        "MMepId": "6340",
                        "text": "Dlabajová"
                     },
                     {
                        "MMepId": "6601",
                        "text": "Federley"
                     },
                     {
                        "MMepId": "6098",
                        "text": "Gerbrandy"
                     },
                     {
                        "MMepId": "6658",
                        "text": "Girauta Vidal"
                     },
                     {
                        "MMepId": "1793",
                        "text": "Goerens"
                     },
                     {
                        "MMepId": "6169",
                        "text": "Goulard"
                     },
                     {
                        "MMepId": "5330",
                        "text": "Griesbeck"
                     },
                     {
                        "MMepId": "6397",
                        "text": "Guoga"
                     },
                     {
                        "MMepId": "5246",
                        "text": "Harkin"
                     },
                     {
                        "MMepId": "6622",
                        "text": "Huitema"
                     },
                     {
                        "MMepId": "5681",
                        "text": "Hyusmenova"
                     },
                     {
                        "MMepId": "6388",
                        "text": "Jakovčić"
                     },
                     {
                        "MMepId": "6338",
                        "text": "Ježek"
                     },
                     {
                        "MMepId": "5433",
                        "text": "Jäätteenmäki"
                     },
                     {
                        "MMepId": "6328",
                        "text": "Kallas"
                     },
                     {
                        "MMepId": "6504",
                        "text": "Kyuchyuk"
                     },
                     {
                        "MMepId": "5362",
                        "text": "Lambsdorff"
                     },
                     {
                        "MMepId": "6376",
                        "text": "Marinho e Pinto"
                     },
                     {
                        "MMepId": "6645",
                        "text": "Maura Barandiarán"
                     },
                     {
                        "MMepId": "6041",
                        "text": "Meissner"
                     },
                     {
                        "MMepId": "5857",
                        "text": "Michel"
                     },
                     {
                        "MMepId": "6690",
                        "text": "Mihaylova"
                     },
                     {
                        "MMepId": "6631",
                        "text": "Mlinar"
                     },
                     {
                        "MMepId": "6500",
                        "text": "Müller"
                     },
                     {
                        "MMepId": "3899",
                        "text": "Neyts-Uyttebroeck"
                     },
                     {
                        "MMepId": "5997",
                        "text": "Nicolai"
                     },
                     {
                        "MMepId": "6644",
                        "text": "Pagazaurtundúa Ruiz"
                     },
                     {
                        "MMepId": "4693",
                        "text": "Paulsen"
                     },
                     {
                        "MMepId": "6510",
                        "text": "Petersen"
                     },
                     {
                        "MMepId": "6266",
                        "text": "Radoš"
                     },
                     {
                        "MMepId": "4676",
                        "text": "Ries"
                     },
                     {
                        "MMepId": "6056",
                        "text": "Riquet"
                     },
                     {
                        "MMepId": "6154",
                        "text": "Rochefort"
                     },
                     {
                        "MMepId": "5890",
                        "text": "Rohde"
                     },
                     {
                        "MMepId": "6174",
                        "text": "Sosa Wagner"
                     },
                     {
                        "MMepId": "6546",
                        "text": "Sulík"
                     },
                     {
                        "MMepId": "6337",
                        "text": "Telička"
                     },
                     {
                        "MMepId": "6042",
                        "text": "Theurer"
                     },
                     {
                        "MMepId": "6281",
                        "text": "Torvalds"
                     },
                     {
                        "MMepId": "6181",
                        "text": "Tremosa i Balcells"
                     },
                     {
                        "MMepId": "6518",
                        "text": "Tørnæs"
                     },
                     {
                        "MMepId": "6151",
                        "text": "Vajgl"
                     },
                     {
                        "MMepId": "6159",
                        "text": "Verhofstadt"
                     },
                     {
                        "MMepId": "4550",
                        "text": "Väyrynen"
                     },
                     {
                        "MMepId": "5864",
                        "text": "Wikström"
                     },
                     {
                        "MMepId": "4758",
                        "text": "de Sarnez"
                     },
                     {
                        "MMepId": "5385",
                        "text": "in 't Veld"
                     },
                     {
                        "MMepId": "6621",
                        "text": "van Nieuwenhuizen"
                     }
                  ]
               },
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5262",
                        "text": "Ashworth"
                     },
                     {
                        "MMepId": "4911",
                        "text": "Belder"
                     },
                     {
                        "MMepId": "6081",
                        "text": "Campbell Bannerman"
                     },
                     {
                        "MMepId": "5490",
                        "text": "Czarnecki"
                     },
                     {
                        "MMepId": "6291",
                        "text": "Demesmaeker"
                     },
                     {
                        "MMepId": "6516",
                        "text": "Dohrmann"
                     },
                     {
                        "MMepId": "6538",
                        "text": "Duda"
                     },
                     {
                        "MMepId": "6579",
                        "text": "Duncan"
                     },
                     {
                        "MMepId": "6511",
                        "text": "Dzhambazki"
                     },
                     {
                        "MMepId": "6104",
                        "text": "Ford"
                     },
                     {
                        "MMepId": "4957",
                        "text": "Foster"
                     },
                     {
                        "MMepId": "5472",
                        "text": "Fotyga"
                     },
                     {
                        "MMepId": "6112",
                        "text": "Fox"
                     },
                     {
                        "MMepId": "6452",
                        "text": "Gericke"
                     },
                     {
                        "MMepId": "6111",
                        "text": "Girling"
                     },
                     {
                        "MMepId": "6539",
                        "text": "Gosiewska"
                     },
                     {
                        "MMepId": "5963",
                        "text": "Gróbarczyk"
                     },
                     {
                        "MMepId": "4959",
                        "text": "Hannan"
                     },
                     {
                        "MMepId": "6460",
                        "text": "Henkel"
                     },
                     {
                        "MMepId": "6520",
                        "text": "Jackiewicz"
                     },
                     {
                        "MMepId": "6531",
                        "text": "Jurek"
                     },
                     {
                        "MMepId": "5630",
                        "text": "Kamall"
                     },
                     {
                        "MMepId": "5563",
                        "text": "Karim"
                     },
                     {
                        "MMepId": "6514",
                        "text": "Karlsson"
                     },
                     {
                        "MMepId": "6525",
                        "text": "Karski"
                     },
                     {
                        "MMepId": "4946",
                        "text": "Kirkhope"
                     },
                     {
                        "MMepId": "6530",
                        "text": "Krasnodębski"
                     },
                     {
                        "MMepId": "5507",
                        "text": "Kuźmiuk"
                     },
                     {
                        "MMepId": "6461",
                        "text": "Kölmel"
                     },
                     {
                        "MMepId": "6561",
                        "text": "Lewer"
                     },
                     {
                        "MMepId": "6457",
                        "text": "Lucke"
                     },
                     {
                        "MMepId": "6667",
                        "text": "Marias"
                     },
                     {
                        "MMepId": "6087",
                        "text": "McClarkin"
                     },
                     {
                        "MMepId": "6234",
                        "text": "McIntyre"
                     },
                     {
                        "MMepId": "5851",
                        "text": "Messerschmidt"
                     },
                     {
                        "MMepId": "1230",
                        "text": "Nicholson"
                     },
                     {
                        "MMepId": "6536",
                        "text": "Ożóg"
                     },
                     {
                        "MMepId": "6512",
                        "text": "Piecha"
                     },
                     {
                        "MMepId": "5460",
                        "text": "Piotrowski"
                     },
                     {
                        "MMepId": "5974",
                        "text": "Poręba"
                     },
                     {
                        "MMepId": "6467",
                        "text": "Pretzell"
                     },
                     {
                        "MMepId": "6464",
                        "text": "Starbatty"
                     },
                     {
                        "MMepId": "6682",
                        "text": "Stevens"
                     },
                     {
                        "MMepId": "6088",
                        "text": "Swinburne"
                     },
                     {
                        "MMepId": "4925",
                        "text": "Tannock"
                     },
                     {
                        "MMepId": "6220",
                        "text": "Terho"
                     },
                     {
                        "MMepId": "6300",
                        "text": "Tomašić"
                     },
                     {
                        "MMepId": "5892",
                        "text": "Tošenovský"
                     },
                     {
                        "MMepId": "4917",
                        "text": "Van Orden"
                     },
                     {
                        "MMepId": "6683",
                        "text": "Van Overtveldt"
                     },
                     {
                        "MMepId": "6513",
                        "text": "Vistisen"
                     },
                     {
                        "MMepId": "6515",
                        "text": "Wiśniewska"
                     },
                     {
                        "MMepId": "5108",
                        "text": "Wojciechowski"
                     },
                     {
                        "MMepId": "5052",
                        "text": "Zahradil"
                     },
                     {
                        "MMepId": "5598",
                        "text": "Zīle"
                     },
                     {
                        "MMepId": "6522",
                        "text": "Złotowski"
                     },
                     {
                        "MMepId": "5981",
                        "text": "van Dalen"
                     },
                     {
                        "MMepId": "6545",
                        "text": "Škripek"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6394",
                        "text": "Bay"
                     },
                     {
                        "MMepId": "6106",
                        "text": "Dodds Diane"
                     },
                     {
                        "MMepId": "6668",
                        "text": "Fountoulis"
                     }
                  ]
               },
               {
                  "MIdentifier": "PPE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6602",
                        "text": "Adaktusson"
                     },
                     {
                        "MMepId": "1340",
                        "text": "Alliot-Marie"
                     },
                     {
                        "MMepId": "1178",
                        "text": "Arias Cañete"
                     },
                     {
                        "MMepId": "6594",
                        "text": "Arimont"
                     },
                     {
                        "MMepId": "4742",
                        "text": "Ayuso"
                     },
                     {
                        "MMepId": "6191",
                        "text": "Bach"
                     },
                     {
                        "MMepId": "6135",
                        "text": "Balz"
                     },
                     {
                        "MMepId": "6216",
                        "text": "Becker"
                     },
                     {
                        "MMepId": "5377",
                        "text": "Belet"
                     },
                     {
                        "MMepId": "5885",
                        "text": "Bendtsen"
                     },
                     {
                        "MMepId": "6343",
                        "text": "Bocskor"
                     },
                     {
                        "MMepId": "6613",
                        "text": "Bogovič"
                     },
                     {
                        "MMepId": "6535",
                        "text": "Boni"
                     },
                     {
                        "MMepId": "6617",
                        "text": "Buda"
                     },
                     {
                        "MMepId": "1507",
                        "text": "Böge"
                     },
                     {
                        "MMepId": "6021",
                        "text": "Cadec"
                     },
                     {
                        "MMepId": "5252",
                        "text": "Casa"
                     },
                     {
                        "MMepId": "5341",
                        "text": "Caspary"
                     },
                     {
                        "MMepId": "5554",
                        "text": "Cesa"
                     },
                     {
                        "MMepId": "6492",
                        "text": "Cicu"
                     },
                     {
                        "MMepId": "6406",
                        "text": "Cirio"
                     },
                     {
                        "MMepId": "6600",
                        "text": "Clune"
                     },
                     {
                        "MMepId": "3800",
                        "text": "Coelho"
                     },
                     {
                        "MMepId": "6273",
                        "text": "Collin-Langen"
                     },
                     {
                        "MMepId": "5948",
                        "text": "Comi"
                     },
                     {
                        "MMepId": "6587",
                        "text": "Comodini Cachia"
                     },
                     {
                        "MMepId": "6548",
                        "text": "Csáky"
                     },
                     {
                        "MMepId": "5921",
                        "text": "Danjean"
                     },
                     {
                        "MMepId": "6194",
                        "text": "Dantin"
                     },
                     {
                        "MMepId": "6172",
                        "text": "Dati"
                     },
                     {
                        "MMepId": "6364",
                        "text": "Delahaye"
                     },
                     {
                        "MMepId": "6345",
                        "text": "Deli"
                     },
                     {
                        "MMepId": "5998",
                        "text": "Deutsch"
                     },
                     {
                        "MMepId": "5350",
                        "text": "Deß"
                     },
                     {
                        "MMepId": "5601",
                        "text": "Dombrovskis"
                     },
                     {
                        "MMepId": "6481",
                        "text": "Donchev"
                     },
                     {
                        "MMepId": "5960",
                        "text": "Dorfmann"
                     },
                     {
                        "MMepId": "5515",
                        "text": "Díaz de Mera García Consuegra"
                     },
                     {
                        "MMepId": "5348",
                        "text": "Ehler"
                     },
                     {
                        "MMepId": "6047",
                        "text": "Engel"
                     },
                     {
                        "MMepId": "6342",
                        "text": "Erdős"
                     },
                     {
                        "MMepId": "5983",
                        "text": "Estaràs Ferragut"
                     },
                     {
                        "MMepId": "3833",
                        "text": "Ferber"
                     },
                     {
                        "MMepId": "6068",
                        "text": "Fernandes"
                     },
                     {
                        "MMepId": "5903",
                        "text": "Fisas Ayxelà"
                     },
                     {
                        "MMepId": "4880",
                        "text": "Fitto"
                     },
                     {
                        "MMepId": "6020",
                        "text": "Gabriel"
                     },
                     {
                        "MMepId": "4659",
                        "text": "Gahler"
                     },
                     {
                        "MMepId": "6615",
                        "text": "Gambús"
                     },
                     {
                        "MMepId": "5803",
                        "text": "Gardini"
                     },
                     {
                        "MMepId": "6441",
                        "text": "Gieseke"
                     },
                     {
                        "MMepId": "6632",
                        "text": "González Pons"
                     },
                     {
                        "MMepId": "4032",
                        "text": "Grossetête"
                     },
                     {
                        "MMepId": "5109",
                        "text": "Grzyb"
                     },
                     {
                        "MMepId": "5342",
                        "text": "Gräßle"
                     },
                     {
                        "MMepId": "5138",
                        "text": "Gyürk"
                     },
                     {
                        "MMepId": "5280",
                        "text": "Gál"
                     },
                     {
                        "MMepId": "6202",
                        "text": "Gáll-Pelcz"
                     },
                     {
                        "MMepId": "6596",
                        "text": "Hayes"
                     },
                     {
                        "MMepId": "5715",
                        "text": "Hellvig"
                     },
                     {
                        "MMepId": "5018",
                        "text": "Herranz García"
                     },
                     {
                        "MMepId": "6534",
                        "text": "Hetman"
                     },
                     {
                        "MMepId": "5953",
                        "text": "Hohlmeier"
                     },
                     {
                        "MMepId": "4987",
                        "text": "Hortefeux"
                     },
                     {
                        "MMepId": "5254",
                        "text": "Hökmark"
                     },
                     {
                        "MMepId": "6346",
                        "text": "Hölvényi"
                     },
                     {
                        "MMepId": "5952",
                        "text": "Hübner"
                     },
                     {
                        "MMepId": "5945",
                        "text": "Jahr"
                     },
                     {
                        "MMepId": "5954",
                        "text": "Jazłowiecka"
                     },
                     {
                        "MMepId": "5978",
                        "text": "Jiménez-Becerril Barrio"
                     },
                     {
                        "MMepId": "6360",
                        "text": "Joulaud"
                     },
                     {
                        "MMepId": "6055",
                        "text": "Juvin"
                     },
                     {
                        "MMepId": "5944",
                        "text": "Kalinowski"
                     },
                     {
                        "MMepId": "6095",
                        "text": "Kalniete"
                     },
                     {
                        "MMepId": "4669",
                        "text": "Karas"
                     },
                     {
                        "MMepId": "6070",
                        "text": "Kariņš"
                     },
                     {
                        "MMepId": "6666",
                        "text": "Kefalogiannis"
                     },
                     {
                        "MMepId": "5542",
                        "text": "Kelam"
                     },
                     {
                        "MMepId": "5855",
                        "text": "Kelly"
                     },
                     {
                        "MMepId": "3759",
                        "text": "Koch"
                     },
                     {
                        "MMepId": "6196",
                        "text": "Kovatchev"
                     },
                     {
                        "MMepId": "6527",
                        "text": "Kozłowska-Rajewicz"
                     },
                     {
                        "MMepId": "5403",
                        "text": "Kudrycka"
                     },
                     {
                        "MMepId": "5840",
                        "text": "Kukan"
                     },
                     {
                        "MMepId": "6662",
                        "text": "Kyrtsos"
                     },
                     {
                        "MMepId": "6001",
                        "text": "Kósa"
                     },
                     {
                        "MMepId": "6053",
                        "text": "Köstinger"
                     },
                     {
                        "MMepId": "6393",
                        "text": "Landsbergis"
                     },
                     {
                        "MMepId": "3856",
                        "text": "Langen"
                     },
                     {
                        "MMepId": "6363",
                        "text": "Lavrilleux"
                     },
                     {
                        "MMepId": "6200",
                        "text": "Le Grip"
                     },
                     {
                        "MMepId": "6592",
                        "text": "Lenaers"
                     },
                     {
                        "MMepId": "5105",
                        "text": "Lewandowski"
                     },
                     {
                        "MMepId": "3855",
                        "text": "Liese"
                     },
                     {
                        "MMepId": "6442",
                        "text": "Lins"
                     },
                     {
                        "MMepId": "5938",
                        "text": "Lope Fontagné"
                     },
                     {
                        "MMepId": "5996",
                        "text": "Macovei"
                     },
                     {
                        "MMepId": "6303",
                        "text": "Maletić"
                     },
                     {
                        "MMepId": "6245",
                        "text": "Malinov"
                     },
                     {
                        "MMepId": "3845",
                        "text": "Mann"
                     },
                     {
                        "MMepId": "5646",
                        "text": "Marinescu"
                     },
                     {
                        "MMepId": "6465",
                        "text": "Martusciello"
                     },
                     {
                        "MMepId": "6096",
                        "text": "Mato"
                     },
                     {
                        "MMepId": "6440",
                        "text": "McAllister"
                     },
                     {
                        "MMepId": "5245",
                        "text": "McGuinness"
                     },
                     {
                        "MMepId": "6121",
                        "text": "Melo"
                     },
                     {
                        "MMepId": "6297",
                        "text": "Metsola"
                     },
                     {
                        "MMepId": "5308",
                        "text": "Mikolášik"
                     },
                     {
                        "MMepId": "5518",
                        "text": "Millán Mon"
                     },
                     {
                        "MMepId": "6366",
                        "text": "Monteiro de Aguiar"
                     },
                     {
                        "MMepId": "6368",
                        "text": "Morano"
                     },
                     {
                        "MMepId": "5732",
                        "text": "Morin-Chartier"
                     },
                     {
                        "MMepId": "6436",
                        "text": "Mureşan"
                     },
                     {
                        "MMepId": "6361",
                        "text": "Muselier"
                     },
                     {
                        "MMepId": "5547",
                        "text": "Mussolini"
                     },
                     {
                        "MMepId": "5774",
                        "text": "Mănescu"
                     },
                     {
                        "MMepId": "6544",
                        "text": "Nagy"
                     },
                     {
                        "MMepId": "4712",
                        "text": "Niebler"
                     },
                     {
                        "MMepId": "5407",
                        "text": "Olbrycht"
                     },
                     {
                        "MMepId": "6377",
                        "text": "Pabriks"
                     },
                     {
                        "MMepId": "5702",
                        "text": "Patriciello"
                     },
                     {
                        "MMepId": "5033",
                        "text": "Peterle"
                     },
                     {
                        "MMepId": "6383",
                        "text": "Petir"
                     },
                     {
                        "MMepId": "5346",
                        "text": "Pieper"
                     },
                     {
                        "MMepId": "5788",
                        "text": "Pietikäinen"
                     },
                     {
                        "MMepId": "6537",
                        "text": "Pitera"
                     },
                     {
                        "MMepId": "6268",
                        "text": "Plenković"
                     },
                     {
                        "MMepId": "6519",
                        "text": "Plura"
                     },
                     {
                        "MMepId": "6491",
                        "text": "Pogliese"
                     },
                     {
                        "MMepId": "6335",
                        "text": "Polčák"
                     },
                     {
                        "MMepId": "6092",
                        "text": "Ponga"
                     },
                     {
                        "MMepId": "6700",
                        "text": "Pospíšil"
                     },
                     {
                        "MMepId": "6010",
                        "text": "Preda"
                     },
                     {
                        "MMepId": "6224",
                        "text": "Proust"
                     },
                     {
                        "MMepId": "1487",
                        "text": "Quisthoudt-Rowohl"
                     },
                     {
                        "MMepId": "6488",
                        "text": "Radev"
                     },
                     {
                        "MMepId": "6072",
                        "text": "Rangel"
                     },
                     {
                        "MMepId": "5347",
                        "text": "Reul"
                     },
                     {
                        "MMepId": "6365",
                        "text": "Ribeiro"
                     },
                     {
                        "MMepId": "6684",
                        "text": "Rolin"
                     },
                     {
                        "MMepId": "4596",
                        "text": "Rübig"
                     },
                     {
                        "MMepId": "6698",
                        "text": "Salini"
                     },
                     {
                        "MMepId": "6370",
                        "text": "Sander"
                     },
                     {
                        "MMepId": "6259",
                        "text": "Sarvamaa"
                     },
                     {
                        "MMepId": "4768",
                        "text": "Saïfi"
                     },
                     {
                        "MMepId": "6619",
                        "text": "Schmidt"
                     },
                     {
                        "MMepId": "6636",
                        "text": "Schreijer-Pierik"
                     },
                     {
                        "MMepId": "6443",
                        "text": "Schulze"
                     },
                     {
                        "MMepId": "5345",
                        "text": "Schwab"
                     },
                     {
                        "MMepId": "5265",
                        "text": "Schöpflin"
                     },
                     {
                        "MMepId": "6428",
                        "text": "Sernagiotto"
                     },
                     {
                        "MMepId": "5111",
                        "text": "Siekierski"
                     },
                     {
                        "MMepId": "4705",
                        "text": "Sommer"
                     },
                     {
                        "MMepId": "6663",
                        "text": "Spyraki"
                     },
                     {
                        "MMepId": "5778",
                        "text": "Stolojan"
                     },
                     {
                        "MMepId": "6672",
                        "text": "Svoboda"
                     },
                     {
                        "MMepId": "6528",
                        "text": "Szejnfeld"
                     },
                     {
                        "MMepId": "5143",
                        "text": "Szájer"
                     },
                     {
                        "MMepId": "5781",
                        "text": "Sógor"
                     },
                     {
                        "MMepId": "5207",
                        "text": "Theocharous"
                     },
                     {
                        "MMepId": "5949",
                        "text": "Thun und Hohenstein"
                     },
                     {
                        "MMepId": "3738",
                        "text": "Thyssen"
                     },
                     {
                        "MMepId": "6681",
                        "text": "Tomc"
                     },
                     {
                        "MMepId": "6398",
                        "text": "Toti"
                     },
                     {
                        "MMepId": "5783",
                        "text": "Tőkés"
                     },
                     {
                        "MMepId": "6007",
                        "text": "Ungureanu"
                     },
                     {
                        "MMepId": "5734",
                        "text": "Urutchev"
                     },
                     {
                        "MMepId": "6638",
                        "text": "Valcárcel"
                     },
                     {
                        "MMepId": "5930",
                        "text": "Verheyen"
                     },
                     {
                        "MMepId": "6356",
                        "text": "Virkkunen"
                     },
                     {
                        "MMepId": "5934",
                        "text": "Voss"
                     },
                     {
                        "MMepId": "6664",
                        "text": "Vozemberg"
                     },
                     {
                        "MMepId": "5714",
                        "text": "Vălean"
                     },
                     {
                        "MMepId": "5947",
                        "text": "Wałęsa"
                     },
                     {
                        "MMepId": "5770",
                        "text": "Weber Renate"
                     },
                     {
                        "MMepId": "6542",
                        "text": "Wenta"
                     },
                     {
                        "MMepId": "5937",
                        "text": "Winkler Hermann"
                     },
                     {
                        "MMepId": "5782",
                        "text": "Winkler Iuliu"
                     },
                     {
                        "MMepId": "6665",
                        "text": "Zagorakis"
                     },
                     {
                        "MMepId": "5936",
                        "text": "Zalba Bidegain"
                     },
                     {
                        "MMepId": "6344",
                        "text": "Zdechovský"
                     },
                     {
                        "MMepId": "6532",
                        "text": "Zdrojewski"
                     },
                     {
                        "MMepId": "5935",
                        "text": "Zeller"
                     },
                     {
                        "MMepId": "6094",
                        "text": "Zver"
                     },
                     {
                        "MMepId": "5420",
                        "text": "Zwiefka"
                     },
                     {
                        "MMepId": "5189",
                        "text": "Záborská"
                     },
                     {
                        "MMepId": "5511",
                        "text": "de Grandes Pascual"
                     },
                     {
                        "MMepId": "5727",
                        "text": "de Lange"
                     },
                     {
                        "MMepId": "5508",
                        "text": "del Castillo Vera"
                     },
                     {
                        "MMepId": "5295",
                        "text": "van Nistelrooij"
                     },
                     {
                        "MMepId": "5928",
                        "text": "van de Camp"
                     },
                     {
                        "MMepId": "5964",
                        "text": "Łukacijewska"
                     },
                     {
                        "MMepId": "6547",
                        "text": "Štefanec"
                     },
                     {
                        "MMepId": "6301",
                        "text": "Šuica"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6651",
                        "text": "Aguilera García"
                     },
                     {
                        "MMepId": "6567",
                        "text": "Anderson Lucy"
                     },
                     {
                        "MMepId": "6279",
                        "text": "Andrieu"
                     },
                     {
                        "MMepId": "6687",
                        "text": "Androulakis"
                     },
                     {
                        "MMepId": "6553",
                        "text": "Arena"
                     },
                     {
                        "MMepId": "5426",
                        "text": "Assis"
                     },
                     {
                        "MMepId": "5411",
                        "text": "Ayala Sender"
                     },
                     {
                        "MMepId": "6378",
                        "text": "Balas"
                     },
                     {
                        "MMepId": "5866",
                        "text": "Balčytis"
                     },
                     {
                        "MMepId": "6611",
                        "text": "Bayet"
                     },
                     {
                        "MMepId": "6505",
                        "text": "Benifei"
                     },
                     {
                        "MMepId": "3972",
                        "text": "Berès"
                     },
                     {
                        "MMepId": "6456",
                        "text": "Bettini"
                     },
                     {
                        "MMepId": "6650",
                        "text": "Blanco López"
                     },
                     {
                        "MMepId": "5867",
                        "text": "Blinkevičiūtė"
                     },
                     {
                        "MMepId": "6449",
                        "text": "Bonafè"
                     },
                     {
                        "MMepId": "6264",
                        "text": "Borzan"
                     },
                     {
                        "MMepId": "5768",
                        "text": "Boştinaru"
                     },
                     {
                        "MMepId": "6572",
                        "text": "Brannen"
                     },
                     {
                        "MMepId": "5465",
                        "text": "Bresso"
                     },
                     {
                        "MMepId": "6647",
                        "text": "Cabezón Ruiz"
                     },
                     {
                        "MMepId": "6489",
                        "text": "Caputo"
                     },
                     {
                        "MMepId": "5835",
                        "text": "Childers"
                     },
                     {
                        "MMepId": "6499",
                        "text": "Chinnici"
                     },
                     {
                        "MMepId": "5284",
                        "text": "Christensen"
                     },
                     {
                        "MMepId": "6083",
                        "text": "Cofferati"
                     },
                     {
                        "MMepId": "4628",
                        "text": "Corbett"
                     },
                     {
                        "MMepId": "6085",
                        "text": "Costa"
                     },
                     {
                        "MMepId": "6051",
                        "text": "Cozzolino"
                     },
                     {
                        "MMepId": "5661",
                        "text": "Creţu"
                     },
                     {
                        "MMepId": "6616",
                        "text": "Cristea"
                     },
                     {
                        "MMepId": "6589",
                        "text": "Dalli"
                     },
                     {
                        "MMepId": "6570",
                        "text": "Dance"
                     },
                     {
                        "MMepId": "6458",
                        "text": "Danti"
                     },
                     {
                        "MMepId": "6062",
                        "text": "De Castro"
                     },
                     {
                        "MMepId": "6437",
                        "text": "De Monte"
                     },
                     {
                        "MMepId": "6410",
                        "text": "Delvaux-Stehres"
                     },
                     {
                        "MMepId": "5816",
                        "text": "Denanot"
                     },
                     {
                        "MMepId": "6588",
                        "text": "Dodds Anneliese"
                     },
                     {
                        "MMepId": "5829",
                        "text": "Dăncilă"
                     },
                     {
                        "MMepId": "6014",
                        "text": "Ertug"
                     },
                     {
                        "MMepId": "6080",
                        "text": "Fajon"
                     },
                     {
                        "MMepId": "6652",
                        "text": "Fernández"
                     },
                     {
                        "MMepId": "5427",
                        "text": "Ferreira Elisa"
                     },
                     {
                        "MMepId": "5173",
                        "text": "Flašíková Beňová"
                     },
                     {
                        "MMepId": "6012",
                        "text": "Fleckenstein"
                     },
                     {
                        "MMepId": "6620",
                        "text": "Freund"
                     },
                     {
                        "MMepId": "6424",
                        "text": "Frunzulică"
                     },
                     {
                        "MMepId": "5417",
                        "text": "García Pérez"
                     },
                     {
                        "MMepId": "6130",
                        "text": "Gardiazabal Rubial"
                     },
                     {
                        "MMepId": "6454",
                        "text": "Gasbarra"
                     },
                     {
                        "MMepId": "3827",
                        "text": "Gebhardt"
                     },
                     {
                        "MMepId": "6005",
                        "text": "Geier"
                     },
                     {
                        "MMepId": "6485",
                        "text": "Gentile"
                     },
                     {
                        "MMepId": "5495",
                        "text": "Geringer de Oedenberg"
                     },
                     {
                        "MMepId": "5497",
                        "text": "Gierek"
                     },
                     {
                        "MMepId": "4937",
                        "text": "Gill Neena"
                     },
                     {
                        "MMepId": "6502",
                        "text": "Giuffrida"
                     },
                     {
                        "MMepId": "5425",
                        "text": "Gomes"
                     },
                     {
                        "MMepId": "6419",
                        "text": "Grapini"
                     },
                     {
                        "MMepId": "6580",
                        "text": "Griffin"
                     },
                     {
                        "MMepId": "5692",
                        "text": "Groote"
                     },
                     {
                        "MMepId": "6063",
                        "text": "Gualtieri"
                     },
                     {
                        "MMepId": "5941",
                        "text": "Guerrero Salom"
                     },
                     {
                        "MMepId": "6107",
                        "text": "Guillaume"
                     },
                     {
                        "MMepId": "6603",
                        "text": "Guteland"
                     },
                     {
                        "MMepId": "6212",
                        "text": "Gutiérrez Prieto"
                     },
                     {
                        "MMepId": "5261",
                        "text": "Hedh"
                     },
                     {
                        "MMepId": "6479",
                        "text": "Hoffmann"
                     },
                     {
                        "MMepId": "4994",
                        "text": "Honeyball"
                     },
                     {
                        "MMepId": "4103",
                        "text": "Howitt"
                     },
                     {
                        "MMepId": "6029",
                        "text": "Ivan"
                     },
                     {
                        "MMepId": "5870",
                        "text": "Jaakonsaari"
                     },
                     {
                        "MMepId": "6624",
                        "text": "Jongerius"
                     },
                     {
                        "MMepId": "6161",
                        "text": "Jáuregui Atondo"
                     },
                     {
                        "MMepId": "6146",
                        "text": "Kadenbach"
                     },
                     {
                        "MMepId": "6686",
                        "text": "Kaili"
                     },
                     {
                        "MMepId": "6009",
                        "text": "Kammerevert"
                     },
                     {
                        "MMepId": "3756",
                        "text": "Kaufmann"
                     },
                     {
                        "MMepId": "6581",
                        "text": "Khan"
                     },
                     {
                        "MMepId": "6571",
                        "text": "Kirton-Darling"
                     },
                     {
                        "MMepId": "6508",
                        "text": "Kofod"
                     },
                     {
                        "MMepId": "3761",
                        "text": "Krehl"
                     },
                     {
                        "MMepId": "6367",
                        "text": "Kumpula-Natri"
                     },
                     {
                        "MMepId": "6435",
                        "text": "Kyenge"
                     },
                     {
                        "MMepId": "6689",
                        "text": "Kyrkos"
                     },
                     {
                        "MMepId": "6459",
                        "text": "Köster"
                     },
                     {
                        "MMepId": "3823",
                        "text": "Lange"
                     },
                     {
                        "MMepId": "6329",
                        "text": "Lauristin"
                     },
                     {
                        "MMepId": "5371",
                        "text": "Leichtfried"
                     },
                     {
                        "MMepId": "4685",
                        "text": "Leinen"
                     },
                     {
                        "MMepId": "5094",
                        "text": "Liberadzki"
                     },
                     {
                        "MMepId": "6477",
                        "text": "Lietz"
                     },
                     {
                        "MMepId": "5860",
                        "text": "Ludvigsson"
                     },
                     {
                        "MMepId": "6648",
                        "text": "López Javi"
                     },
                     {
                        "MMepId": "5984",
                        "text": "López Aguilar"
                     },
                     {
                        "MMepId": "6380",
                        "text": "Mamikins"
                     },
                     {
                        "MMepId": "1133",
                        "text": "Martin David"
                     },
                     {
                        "MMepId": "5314",
                        "text": "Maňka"
                     },
                     {
                        "MMepId": "4646",
                        "text": "McAvan"
                     },
                     {
                        "MMepId": "6296",
                        "text": "Mizzi"
                     },
                     {
                        "MMepId": "6423",
                        "text": "Moisă"
                     },
                     {
                        "MMepId": "6351",
                        "text": "Molnár"
                     },
                     {
                        "MMepId": "6562",
                        "text": "Moody"
                     },
                     {
                        "MMepId": "4923",
                        "text": "Moraes"
                     },
                     {
                        "MMepId": "6433",
                        "text": "Moretti"
                     },
                     {
                        "MMepId": "6422",
                        "text": "Morgano"
                     },
                     {
                        "MMepId": "6506",
                        "text": "Mosca"
                     },
                     {
                        "MMepId": "6691",
                        "text": "Negrescu"
                     },
                     {
                        "MMepId": "6016",
                        "text": "Neuser"
                     },
                     {
                        "MMepId": "6418",
                        "text": "Nica"
                     },
                     {
                        "MMepId": "6352",
                        "text": "Niedermüller"
                     },
                     {
                        "MMepId": "6242",
                        "text": "Nilsson"
                     },
                     {
                        "MMepId": "6473",
                        "text": "Noichl"
                     },
                     {
                        "MMepId": "5483",
                        "text": "Panzeri"
                     },
                     {
                        "MMepId": "6487",
                        "text": "Paolucci"
                     },
                     {
                        "MMepId": "6103",
                        "text": "Pargneaux"
                     },
                     {
                        "MMepId": "5648",
                        "text": "Paşcu"
                     },
                     {
                        "MMepId": "5296",
                        "text": "Peillon"
                     },
                     {
                        "MMepId": "6484",
                        "text": "Picierno"
                     },
                     {
                        "MMepId": "6261",
                        "text": "Picula"
                     },
                     {
                        "MMepId": "6625",
                        "text": "Piri"
                     },
                     {
                        "MMepId": "6493",
                        "text": "Pirinski"
                     },
                     {
                        "MMepId": "6526",
                        "text": "Preuß"
                     },
                     {
                        "MMepId": "6136",
                        "text": "Regner"
                     },
                     {
                        "MMepId": "6316",
                        "text": "Revault D'Allonnes Bonnefoy"
                     },
                     {
                        "MMepId": "6649",
                        "text": "Rodríguez-Piñero Fernández"
                     },
                     {
                        "MMepId": "5813",
                        "text": "Rodust"
                     },
                     {
                        "MMepId": "6357",
                        "text": "Rozière"
                     },
                     {
                        "MMepId": "5713",
                        "text": "Schaldemose"
                     },
                     {
                        "MMepId": "6438",
                        "text": "Schlein"
                     },
                     {
                        "MMepId": "6475",
                        "text": "Schuster"
                     },
                     {
                        "MMepId": "5897",
                        "text": "Sehnalová"
                     },
                     {
                        "MMepId": "6375",
                        "text": "Serrão Santos"
                     },
                     {
                        "MMepId": "6381",
                        "text": "Silva Pereira"
                     },
                     {
                        "MMepId": "6008",
                        "text": "Simon Peter"
                     },
                     {
                        "MMepId": "6566",
                        "text": "Simon Siôn"
                     },
                     {
                        "MMepId": "6093",
                        "text": "Sippel"
                     },
                     {
                        "MMepId": "5844",
                        "text": "Smolková"
                     },
                     {
                        "MMepId": "6498",
                        "text": "Soru"
                     },
                     {
                        "MMepId": "6003",
                        "text": "Steinruck"
                     },
                     {
                        "MMepId": "4949",
                        "text": "Stihler"
                     },
                     {
                        "MMepId": "6334",
                        "text": "Szanyi"
                     },
                     {
                        "MMepId": "5653",
                        "text": "Sârbu"
                     },
                     {
                        "MMepId": "6623",
                        "text": "Tang"
                     },
                     {
                        "MMepId": "5624",
                        "text": "Tarabella"
                     },
                     {
                        "MMepId": "6282",
                        "text": "Thomas"
                     },
                     {
                        "MMepId": "5459",
                        "text": "Toia"
                     },
                     {
                        "MMepId": "6336",
                        "text": "Ujhelyi"
                     },
                     {
                        "MMepId": "5859",
                        "text": "Ulvskog"
                     },
                     {
                        "MMepId": "4990",
                        "text": "Van Brempt"
                     },
                     {
                        "MMepId": "6086",
                        "text": "Vaughan"
                     },
                     {
                        "MMepId": "6425",
                        "text": "Viotti"
                     },
                     {
                        "MMepId": "6582",
                        "text": "Ward"
                     },
                     {
                        "MMepId": "6235",
                        "text": "Weidenholzer"
                     },
                     {
                        "MMepId": "6463",
                        "text": "Werner"
                     },
                     {
                        "MMepId": "6011",
                        "text": "Westphal"
                     },
                     {
                        "MMepId": "5698",
                        "text": "Willmott"
                     },
                     {
                        "MMepId": "5845",
                        "text": "Zala"
                     },
                     {
                        "MMepId": "6434",
                        "text": "Zanonato"
                     },
                     {
                        "MMepId": "5957",
                        "text": "Zemke"
                     },
                     {
                        "MMepId": "6373",
                        "text": "Zorrinho"
                     },
                     {
                        "MMepId": "6478",
                        "text": "von Weizsäcker"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5910",
                        "text": "Albrecht"
                     },
                     {
                        "MMepId": "5291",
                        "text": "Auken"
                     },
                     {
                        "MMepId": "5918",
                        "text": "Bové"
                     },
                     {
                        "MMepId": "5913",
                        "text": "Bütikofer"
                     },
                     {
                        "MMepId": "5358",
                        "text": "Cramer"
                     },
                     {
                        "MMepId": "6040",
                        "text": "Delli"
                     },
                     {
                        "MMepId": "6324",
                        "text": "Durand"
                     },
                     {
                        "MMepId": "5899",
                        "text": "Eickhout"
                     },
                     {
                        "MMepId": "5904",
                        "text": "Giegold"
                     },
                     {
                        "MMepId": "5353",
                        "text": "Harms"
                     },
                     {
                        "MMepId": "4535",
                        "text": "Hautala"
                     },
                     {
                        "MMepId": "6486",
                        "text": "Heubuch"
                     },
                     {
                        "MMepId": "4655",
                        "text": "Hudghton"
                     },
                     {
                        "MMepId": "5926",
                        "text": "Häusling"
                     },
                     {
                        "MMepId": "5914",
                        "text": "Jadot"
                     },
                     {
                        "MMepId": "6054",
                        "text": "Joly"
                     },
                     {
                        "MMepId": "6350",
                        "text": "Jávor"
                     },
                     {
                        "MMepId": "5908",
                        "text": "Keller Ska"
                     },
                     {
                        "MMepId": "4935",
                        "text": "Lambert"
                     },
                     {
                        "MMepId": "5838",
                        "text": "Lamberts"
                     },
                     {
                        "MMepId": "5902",
                        "text": "Lochbihler"
                     },
                     {
                        "MMepId": "6149",
                        "text": "Lunacek"
                     },
                     {
                        "MMepId": "6550",
                        "text": "Maragall"
                     },
                     {
                        "MMepId": "6349",
                        "text": "Meszerics"
                     },
                     {
                        "MMepId": "6453",
                        "text": "Reda"
                     },
                     {
                        "MMepId": "6552",
                        "text": "Reimon"
                     },
                     {
                        "MMepId": "6529",
                        "text": "Reintke"
                     },
                     {
                        "MMepId": "5917",
                        "text": "Rivasi"
                     },
                     {
                        "MMepId": "6694",
                        "text": "Ropé"
                     },
                     {
                        "MMepId": "5987",
                        "text": "Sargentini"
                     },
                     {
                        "MMepId": "6560",
                        "text": "Scott Cato"
                     },
                     {
                        "MMepId": "6659",
                        "text": "Sebastià"
                     },
                     {
                        "MMepId": "5571",
                        "text": "Smith"
                     },
                     {
                        "MMepId": "4983",
                        "text": "Staes"
                     },
                     {
                        "MMepId": "6168",
                        "text": "Tarand"
                     },
                     {
                        "MMepId": "6204",
                        "text": "Taylor"
                     },
                     {
                        "MMepId": "6549",
                        "text": "Terricabras"
                     },
                     {
                        "MMepId": "5360",
                        "text": "Trüpel"
                     },
                     {
                        "MMepId": "4847",
                        "text": "Turmes"
                     }
                  ]
               }
            ]
         },
         "ResultAbstention": {
            "MNumber": "10",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": {
                     "MMepId": "6466",
                     "text": "Trebesius"
                  }
               },
               {
                  "MIdentifier": "GUE/NGL",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6660",
                        "text": "Chrysogonos"
                     },
                     {
                        "MMepId": "6076",
                        "text": "Hadjigeorgiou"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6524",
                        "text": "Iwaszkiewicz"
                     },
                     {
                        "MMepId": "6533",
                        "text": "Marusik"
                     },
                     {
                        "MMepId": "6541",
                        "text": "Żółtek"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6355",
                        "text": "Manscour"
                     },
                     {
                        "MMepId": "6565",
                        "text": "Martin Edouard"
                     },
                     {
                        "MMepId": "6354",
                        "text": "Maurel"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": {
                     "MMepId": "4954",
                     "text": "Evans"
                  }
               }
            ]
         }
      },
      {
         "MDate": "2014-07-16 12:23:10",
         "MIdentifier": "47691",
         "RollCallVoteDescriptionText": "A8-0001/2014 -  Werner Langen - Am 1",
         "ResultFor": {
            "MNumber": "137",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ALDE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6407",
                        "text": "Arthuis"
                     },
                     {
                        "MMepId": "6100",
                        "text": "Schaake"
                     }
                  ]
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6468",
                        "text": "Adinolfi"
                     },
                     {
                        "MMepId": "6431",
                        "text": "Affronte"
                     },
                     {
                        "MMepId": "6445",
                        "text": "Agea"
                     },
                     {
                        "MMepId": "6066",
                        "text": "Agnew"
                     },
                     {
                        "MMepId": "6480",
                        "text": "Aiuto"
                     },
                     {
                        "MMepId": "6559",
                        "text": "Aker"
                     },
                     {
                        "MMepId": "6577",
                        "text": "Arnott"
                     },
                     {
                        "MMepId": "6555",
                        "text": "Atkinson"
                     },
                     {
                        "MMepId": "6574",
                        "text": "Bashir"
                     },
                     {
                        "MMepId": "5567",
                        "text": "Batten"
                     },
                     {
                        "MMepId": "6411",
                        "text": "Beghin"
                     },
                     {
                        "MMepId": "6430",
                        "text": "Borrelli"
                     },
                     {
                        "MMepId": "6583",
                        "text": "Bours"
                     },
                     {
                        "MMepId": "6590",
                        "text": "Carver"
                     },
                     {
                        "MMepId": "6446",
                        "text": "Castaldo"
                     },
                     {
                        "MMepId": "6586",
                        "text": "Coburn"
                     },
                     {
                        "MMepId": "6573",
                        "text": "Collins"
                     },
                     {
                        "MMepId": "6494",
                        "text": "Corrao"
                     },
                     {
                        "MMepId": "6472",
                        "text": "D'Amato"
                     },
                     {
                        "MMepId": "6569",
                        "text": "Etheridge"
                     },
                     {
                        "MMepId": "6413",
                        "text": "Evi"
                     },
                     {
                        "MMepId": "4929",
                        "text": "Farage"
                     },
                     {
                        "MMepId": "6470",
                        "text": "Ferrara"
                     },
                     {
                        "MMepId": "6558",
                        "text": "Finch"
                     },
                     {
                        "MMepId": "6584",
                        "text": "Gill Nathan"
                     },
                     {
                        "MMepId": "6379",
                        "text": "Grigule"
                     },
                     {
                        "MMepId": "4920",
                        "text": "Helmer"
                     },
                     {
                        "MMepId": "6575",
                        "text": "Hookem"
                     },
                     {
                        "MMepId": "6556",
                        "text": "James"
                     },
                     {
                        "MMepId": "6608",
                        "text": "Lundgren"
                     },
                     {
                        "MMepId": "6402",
                        "text": "Mazuronis"
                     },
                     {
                        "MMepId": "6497",
                        "text": "Moi"
                     },
                     {
                        "MMepId": "5977",
                        "text": "Nuttall"
                     },
                     {
                        "MMepId": "6557",
                        "text": "O'Flynn"
                     },
                     {
                        "MMepId": "5875",
                        "text": "Paksas"
                     },
                     {
                        "MMepId": "6563",
                        "text": "Parker"
                     },
                     {
                        "MMepId": "6482",
                        "text": "Pedicini"
                     },
                     {
                        "MMepId": "6564",
                        "text": "Reid"
                     },
                     {
                        "MMepId": "6412",
                        "text": "Valli"
                     },
                     {
                        "MMepId": "6607",
                        "text": "Winberg"
                     },
                     {
                        "MMepId": "6585",
                        "text": "Woolfe"
                     },
                     {
                        "MMepId": "6414",
                        "text": "Zanni"
                     },
                     {
                        "MMepId": "6695",
                        "text": "Zullo"
                     }
                  ]
               },
               {
                  "MIdentifier": "GUE/NGL",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6654",
                        "text": "Albiol Guzmán"
                     },
                     {
                        "MMepId": "6280",
                        "text": "Anderson Martina"
                     },
                     {
                        "MMepId": "6604",
                        "text": "Björk"
                     },
                     {
                        "MMepId": "6595",
                        "text": "Boylan"
                     },
                     {
                        "MMepId": "6598",
                        "text": "Carthy"
                     },
                     {
                        "MMepId": "6701",
                        "text": "Couso Permuy"
                     },
                     {
                        "MMepId": "6496",
                        "text": "De Masi"
                     },
                     {
                        "MMepId": "6501",
                        "text": "Eck"
                     },
                     {
                        "MMepId": "6024",
                        "text": "Ernst"
                     },
                     {
                        "MMepId": "6693",
                        "text": "Forenza"
                     },
                     {
                        "MMepId": "6628",
                        "text": "Hazekamp"
                     },
                     {
                        "MMepId": "6637",
                        "text": "Iglesias"
                     },
                     {
                        "MMepId": "6657",
                        "text": "Juaristi Abaunz"
                     },
                     {
                        "MMepId": "6313",
                        "text": "Kari"
                     },
                     {
                        "MMepId": "5039",
                        "text": "Konečná"
                     },
                     {
                        "MMepId": "6675",
                        "text": "Kuneva"
                     },
                     {
                        "MMepId": "6369",
                        "text": "Kyllönen"
                     },
                     {
                        "MMepId": "5909",
                        "text": "Le Hyaric"
                     },
                     {
                        "MMepId": "6653",
                        "text": "López Paloma"
                     },
                     {
                        "MMepId": "6692",
                        "text": "Maltese"
                     },
                     {
                        "MMepId": "5992",
                        "text": "Matias"
                     },
                     {
                        "MMepId": "5044",
                        "text": "Maštálka"
                     },
                     {
                        "MMepId": "6310",
                        "text": "Michels"
                     },
                     {
                        "MMepId": "6643",
                        "text": "Mineur"
                     },
                     {
                        "MMepId": "5916",
                        "text": "Mélenchon"
                     },
                     {
                        "MMepId": "6599",
                        "text": "Ní Riada"
                     },
                     {
                        "MMepId": "6250",
                        "text": "Omarjee"
                     },
                     {
                        "MMepId": "5596",
                        "text": "Papadimoulis"
                     },
                     {
                        "MMepId": "5046",
                        "text": "Ransdorf"
                     },
                     {
                        "MMepId": "6639",
                        "text": "Rodriguez-Rubio Vázquez"
                     },
                     {
                        "MMepId": "6674",
                        "text": "Sakorafa"
                     },
                     {
                        "MMepId": "5837",
                        "text": "Scholz"
                     },
                     {
                        "MMepId": "6655",
                        "text": "Senra Rodríguez"
                     },
                     {
                        "MMepId": "6444",
                        "text": "Spinelli"
                     },
                     {
                        "MMepId": "6641",
                        "text": "Sánchez Caldentey"
                     },
                     {
                        "MMepId": "6656",
                        "text": "Vallina"
                     },
                     {
                        "MMepId": "5368",
                        "text": "Zimmer"
                     },
                     {
                        "MMepId": "5922",
                        "text": "de Jong"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6048",
                        "text": "Balczó"
                     },
                     {
                        "MMepId": "6203",
                        "text": "Kovács"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6688",
                        "text": "Grammatikakis"
                     },
                     {
                        "MMepId": "6326",
                        "text": "Keller Jan"
                     },
                     {
                        "MMepId": "6565",
                        "text": "Martin Edouard"
                     },
                     {
                        "MMepId": "6476",
                        "text": "Melior"
                     },
                     {
                        "MMepId": "6618",
                        "text": "Nekov"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5910",
                        "text": "Albrecht"
                     },
                     {
                        "MMepId": "6606",
                        "text": "Andersson"
                     },
                     {
                        "MMepId": "5291",
                        "text": "Auken"
                     },
                     {
                        "MMepId": "5918",
                        "text": "Bové"
                     },
                     {
                        "MMepId": "5913",
                        "text": "Bütikofer"
                     },
                     {
                        "MMepId": "6605",
                        "text": "Ceballos"
                     },
                     {
                        "MMepId": "5358",
                        "text": "Cramer"
                     },
                     {
                        "MMepId": "6040",
                        "text": "Delli"
                     },
                     {
                        "MMepId": "6324",
                        "text": "Durand"
                     },
                     {
                        "MMepId": "5899",
                        "text": "Eickhout"
                     },
                     {
                        "MMepId": "6554",
                        "text": "Eriksson"
                     },
                     {
                        "MMepId": "4954",
                        "text": "Evans"
                     },
                     {
                        "MMepId": "5904",
                        "text": "Giegold"
                     },
                     {
                        "MMepId": "5353",
                        "text": "Harms"
                     },
                     {
                        "MMepId": "4535",
                        "text": "Hautala"
                     },
                     {
                        "MMepId": "6486",
                        "text": "Heubuch"
                     },
                     {
                        "MMepId": "4655",
                        "text": "Hudghton"
                     },
                     {
                        "MMepId": "5926",
                        "text": "Häusling"
                     },
                     {
                        "MMepId": "5914",
                        "text": "Jadot"
                     },
                     {
                        "MMepId": "6054",
                        "text": "Joly"
                     },
                     {
                        "MMepId": "6350",
                        "text": "Jávor"
                     },
                     {
                        "MMepId": "5908",
                        "text": "Keller Ska"
                     },
                     {
                        "MMepId": "4935",
                        "text": "Lambert"
                     },
                     {
                        "MMepId": "5838",
                        "text": "Lamberts"
                     },
                     {
                        "MMepId": "5902",
                        "text": "Lochbihler"
                     },
                     {
                        "MMepId": "6149",
                        "text": "Lunacek"
                     },
                     {
                        "MMepId": "5862",
                        "text": "Lövin"
                     },
                     {
                        "MMepId": "6550",
                        "text": "Maragall"
                     },
                     {
                        "MMepId": "6349",
                        "text": "Meszerics"
                     },
                     {
                        "MMepId": "6453",
                        "text": "Reda"
                     },
                     {
                        "MMepId": "6552",
                        "text": "Reimon"
                     },
                     {
                        "MMepId": "6529",
                        "text": "Reintke"
                     },
                     {
                        "MMepId": "5917",
                        "text": "Rivasi"
                     },
                     {
                        "MMepId": "6694",
                        "text": "Ropé"
                     },
                     {
                        "MMepId": "5987",
                        "text": "Sargentini"
                     },
                     {
                        "MMepId": "6560",
                        "text": "Scott Cato"
                     },
                     {
                        "MMepId": "6659",
                        "text": "Sebastià"
                     },
                     {
                        "MMepId": "5571",
                        "text": "Smith"
                     },
                     {
                        "MMepId": "4983",
                        "text": "Staes"
                     },
                     {
                        "MMepId": "6204",
                        "text": "Taylor"
                     },
                     {
                        "MMepId": "6549",
                        "text": "Terricabras"
                     },
                     {
                        "MMepId": "5360",
                        "text": "Trüpel"
                     },
                     {
                        "MMepId": "4847",
                        "text": "Turmes"
                     },
                     {
                        "MMepId": "6591",
                        "text": "Urtasun"
                     },
                     {
                        "MMepId": "6551",
                        "text": "Vana"
                     },
                     {
                        "MMepId": "6612",
                        "text": "Šoltes"
                     },
                     {
                        "MMepId": "5602",
                        "text": "Ždanoka"
                     }
                  ]
               }
            ]
         },
         "ResultAgainst": {
            "MNumber": "525",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ALDE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5682",
                        "text": "Ali"
                     },
                     {
                        "MMepId": "6327",
                        "text": "Ansip"
                     },
                     {
                        "MMepId": "6400",
                        "text": "Auštrevičius"
                     },
                     {
                        "MMepId": "6110",
                        "text": "Bearder"
                     },
                     {
                        "MMepId": "6646",
                        "text": "Becerra Basterrechea"
                     },
                     {
                        "MMepId": "6089",
                        "text": "Bilbao Barandica"
                     },
                     {
                        "MMepId": "5328",
                        "text": "Cavada"
                     },
                     {
                        "MMepId": "6226",
                        "text": "De Backer"
                     },
                     {
                        "MMepId": "1060",
                        "text": "Deprez"
                     },
                     {
                        "MMepId": "6439",
                        "text": "Diaconu"
                     },
                     {
                        "MMepId": "6340",
                        "text": "Dlabajová"
                     },
                     {
                        "MMepId": "6679",
                        "text": "Faria"
                     },
                     {
                        "MMepId": "6601",
                        "text": "Federley"
                     },
                     {
                        "MMepId": "6098",
                        "text": "Gerbrandy"
                     },
                     {
                        "MMepId": "6658",
                        "text": "Girauta Vidal"
                     },
                     {
                        "MMepId": "1793",
                        "text": "Goerens"
                     },
                     {
                        "MMepId": "6169",
                        "text": "Goulard"
                     },
                     {
                        "MMepId": "5330",
                        "text": "Griesbeck"
                     },
                     {
                        "MMepId": "6397",
                        "text": "Guoga"
                     },
                     {
                        "MMepId": "5246",
                        "text": "Harkin"
                     },
                     {
                        "MMepId": "6622",
                        "text": "Huitema"
                     },
                     {
                        "MMepId": "5681",
                        "text": "Hyusmenova"
                     },
                     {
                        "MMepId": "6388",
                        "text": "Jakovčić"
                     },
                     {
                        "MMepId": "6338",
                        "text": "Ježek"
                     },
                     {
                        "MMepId": "5433",
                        "text": "Jäätteenmäki"
                     },
                     {
                        "MMepId": "6328",
                        "text": "Kallas"
                     },
                     {
                        "MMepId": "6504",
                        "text": "Kyuchyuk"
                     },
                     {
                        "MMepId": "5362",
                        "text": "Lambsdorff"
                     },
                     {
                        "MMepId": "6376",
                        "text": "Marinho e Pinto"
                     },
                     {
                        "MMepId": "6645",
                        "text": "Maura Barandiarán"
                     },
                     {
                        "MMepId": "6041",
                        "text": "Meissner"
                     },
                     {
                        "MMepId": "5857",
                        "text": "Michel"
                     },
                     {
                        "MMepId": "6690",
                        "text": "Mihaylova"
                     },
                     {
                        "MMepId": "6631",
                        "text": "Mlinar"
                     },
                     {
                        "MMepId": "6500",
                        "text": "Müller"
                     },
                     {
                        "MMepId": "6614",
                        "text": "Nart"
                     },
                     {
                        "MMepId": "3899",
                        "text": "Neyts-Uyttebroeck"
                     },
                     {
                        "MMepId": "5997",
                        "text": "Nicolai"
                     },
                     {
                        "MMepId": "6644",
                        "text": "Pagazaurtundúa Ruiz"
                     },
                     {
                        "MMepId": "4693",
                        "text": "Paulsen"
                     },
                     {
                        "MMepId": "6510",
                        "text": "Petersen"
                     },
                     {
                        "MMepId": "6266",
                        "text": "Radoš"
                     },
                     {
                        "MMepId": "4676",
                        "text": "Ries"
                     },
                     {
                        "MMepId": "6056",
                        "text": "Riquet"
                     },
                     {
                        "MMepId": "6154",
                        "text": "Rochefort"
                     },
                     {
                        "MMepId": "5890",
                        "text": "Rohde"
                     },
                     {
                        "MMepId": "6174",
                        "text": "Sosa Wagner"
                     },
                     {
                        "MMepId": "6546",
                        "text": "Sulík"
                     },
                     {
                        "MMepId": "6337",
                        "text": "Telička"
                     },
                     {
                        "MMepId": "6042",
                        "text": "Theurer"
                     },
                     {
                        "MMepId": "6281",
                        "text": "Torvalds"
                     },
                     {
                        "MMepId": "6181",
                        "text": "Tremosa i Balcells"
                     },
                     {
                        "MMepId": "6518",
                        "text": "Tørnæs"
                     },
                     {
                        "MMepId": "6151",
                        "text": "Vajgl"
                     },
                     {
                        "MMepId": "6159",
                        "text": "Verhofstadt"
                     },
                     {
                        "MMepId": "4550",
                        "text": "Väyrynen"
                     },
                     {
                        "MMepId": "5864",
                        "text": "Wikström"
                     },
                     {
                        "MMepId": "4758",
                        "text": "de Sarnez"
                     },
                     {
                        "MMepId": "5385",
                        "text": "in 't Veld"
                     },
                     {
                        "MMepId": "6097",
                        "text": "van Baalen"
                     },
                     {
                        "MMepId": "6621",
                        "text": "van Nieuwenhuizen"
                     }
                  ]
               },
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5262",
                        "text": "Ashworth"
                     },
                     {
                        "MMepId": "4911",
                        "text": "Belder"
                     },
                     {
                        "MMepId": "6081",
                        "text": "Campbell Bannerman"
                     },
                     {
                        "MMepId": "5490",
                        "text": "Czarnecki"
                     },
                     {
                        "MMepId": "6291",
                        "text": "Demesmaeker"
                     },
                     {
                        "MMepId": "6516",
                        "text": "Dohrmann"
                     },
                     {
                        "MMepId": "6538",
                        "text": "Duda"
                     },
                     {
                        "MMepId": "6579",
                        "text": "Duncan"
                     },
                     {
                        "MMepId": "6511",
                        "text": "Dzhambazki"
                     },
                     {
                        "MMepId": "6104",
                        "text": "Ford"
                     },
                     {
                        "MMepId": "4957",
                        "text": "Foster"
                     },
                     {
                        "MMepId": "5472",
                        "text": "Fotyga"
                     },
                     {
                        "MMepId": "6112",
                        "text": "Fox"
                     },
                     {
                        "MMepId": "6452",
                        "text": "Gericke"
                     },
                     {
                        "MMepId": "6111",
                        "text": "Girling"
                     },
                     {
                        "MMepId": "6539",
                        "text": "Gosiewska"
                     },
                     {
                        "MMepId": "5963",
                        "text": "Gróbarczyk"
                     },
                     {
                        "MMepId": "4959",
                        "text": "Hannan"
                     },
                     {
                        "MMepId": "6520",
                        "text": "Jackiewicz"
                     },
                     {
                        "MMepId": "6531",
                        "text": "Jurek"
                     },
                     {
                        "MMepId": "5630",
                        "text": "Kamall"
                     },
                     {
                        "MMepId": "5563",
                        "text": "Karim"
                     },
                     {
                        "MMepId": "6514",
                        "text": "Karlsson"
                     },
                     {
                        "MMepId": "6525",
                        "text": "Karski"
                     },
                     {
                        "MMepId": "4946",
                        "text": "Kirkhope"
                     },
                     {
                        "MMepId": "6530",
                        "text": "Krasnodębski"
                     },
                     {
                        "MMepId": "5507",
                        "text": "Kuźmiuk"
                     },
                     {
                        "MMepId": "6461",
                        "text": "Kölmel"
                     },
                     {
                        "MMepId": "6561",
                        "text": "Lewer"
                     },
                     {
                        "MMepId": "6457",
                        "text": "Lucke"
                     },
                     {
                        "MMepId": "6667",
                        "text": "Marias"
                     },
                     {
                        "MMepId": "6087",
                        "text": "McClarkin"
                     },
                     {
                        "MMepId": "6234",
                        "text": "McIntyre"
                     },
                     {
                        "MMepId": "5851",
                        "text": "Messerschmidt"
                     },
                     {
                        "MMepId": "1230",
                        "text": "Nicholson"
                     },
                     {
                        "MMepId": "6536",
                        "text": "Ożóg"
                     },
                     {
                        "MMepId": "6512",
                        "text": "Piecha"
                     },
                     {
                        "MMepId": "5460",
                        "text": "Piotrowski"
                     },
                     {
                        "MMepId": "5974",
                        "text": "Poręba"
                     },
                     {
                        "MMepId": "6467",
                        "text": "Pretzell"
                     },
                     {
                        "MMepId": "6464",
                        "text": "Starbatty"
                     },
                     {
                        "MMepId": "6682",
                        "text": "Stevens"
                     },
                     {
                        "MMepId": "6088",
                        "text": "Swinburne"
                     },
                     {
                        "MMepId": "4925",
                        "text": "Tannock"
                     },
                     {
                        "MMepId": "6220",
                        "text": "Terho"
                     },
                     {
                        "MMepId": "6300",
                        "text": "Tomašić"
                     },
                     {
                        "MMepId": "5892",
                        "text": "Tošenovský"
                     },
                     {
                        "MMepId": "4917",
                        "text": "Van Orden"
                     },
                     {
                        "MMepId": "6683",
                        "text": "Van Overtveldt"
                     },
                     {
                        "MMepId": "6513",
                        "text": "Vistisen"
                     },
                     {
                        "MMepId": "6515",
                        "text": "Wiśniewska"
                     },
                     {
                        "MMepId": "5108",
                        "text": "Wojciechowski"
                     },
                     {
                        "MMepId": "5052",
                        "text": "Zahradil"
                     },
                     {
                        "MMepId": "5598",
                        "text": "Zīle"
                     },
                     {
                        "MMepId": "6522",
                        "text": "Złotowski"
                     },
                     {
                        "MMepId": "5981",
                        "text": "van Dalen"
                     },
                     {
                        "MMepId": "6545",
                        "text": "Škripek"
                     }
                  ]
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": {
                     "MMepId": "6374",
                     "text": "Bergeron"
                  }
               },
               {
                  "MIdentifier": "GUE/NGL",
                  "PoliticalGroupMemberName": {
                     "MMepId": "6597",
                     "text": "Flanagan"
                  }
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6451",
                        "text": "Aliot"
                     },
                     {
                        "MMepId": "6593",
                        "text": "Annemans"
                     },
                     {
                        "MMepId": "6382",
                        "text": "Arnautu"
                     },
                     {
                        "MMepId": "6394",
                        "text": "Bay"
                     },
                     {
                        "MMepId": "6405",
                        "text": "Bilde"
                     },
                     {
                        "MMepId": "6387",
                        "text": "Boutonnet"
                     },
                     {
                        "MMepId": "6391",
                        "text": "Briois"
                     },
                     {
                        "MMepId": "6384",
                        "text": "D'Ornano"
                     },
                     {
                        "MMepId": "6106",
                        "text": "Dodds Diane"
                     },
                     {
                        "MMepId": "6401",
                        "text": "Ferrand"
                     },
                     {
                        "MMepId": "6668",
                        "text": "Fountoulis"
                     },
                     {
                        "MMepId": "6450",
                        "text": "Goddyn"
                     },
                     {
                        "MMepId": "1380",
                        "text": "Gollnisch"
                     },
                     {
                        "MMepId": "6404",
                        "text": "Jalkh"
                     },
                     {
                        "MMepId": "6635",
                        "text": "Jansen"
                     },
                     {
                        "MMepId": "6629",
                        "text": "Kappel"
                     },
                     {
                        "MMepId": "1529",
                        "text": "Le Pen Jean-Marie"
                     },
                     {
                        "MMepId": "5332",
                        "text": "Le Pen Marine"
                     },
                     {
                        "MMepId": "6372",
                        "text": "Lebreton"
                     },
                     {
                        "MMepId": "6699",
                        "text": "Loiseau"
                     },
                     {
                        "MMepId": "6633",
                        "text": "Maeijer"
                     },
                     {
                        "MMepId": "6385",
                        "text": "Martin Dominique"
                     },
                     {
                        "MMepId": "6627",
                        "text": "Mayer"
                     },
                     {
                        "MMepId": "6395",
                        "text": "Monot"
                     },
                     {
                        "MMepId": "6403",
                        "text": "Montel"
                     },
                     {
                        "MMepId": "6399",
                        "text": "Mélin"
                     },
                     {
                        "MMepId": "6123",
                        "text": "Obermayr"
                     },
                     {
                        "MMepId": "6676",
                        "text": "Papadakis Konstantinos"
                     },
                     {
                        "MMepId": "6634",
                        "text": "Stuger"
                     },
                     {
                        "MMepId": "6670",
                        "text": "Synadinos"
                     },
                     {
                        "MMepId": "6392",
                        "text": "Troszczynski"
                     },
                     {
                        "MMepId": "6610",
                        "text": "Vilimsky"
                     },
                     {
                        "MMepId": "6469",
                        "text": "Voigt"
                     },
                     {
                        "MMepId": "6673",
                        "text": "Zarianopoulos"
                     },
                     {
                        "MMepId": "6630",
                        "text": "de Graaff"
                     }
                  ]
               },
               {
                  "MIdentifier": "PPE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6602",
                        "text": "Adaktusson"
                     },
                     {
                        "MMepId": "1340",
                        "text": "Alliot-Marie"
                     },
                     {
                        "MMepId": "1178",
                        "text": "Arias Cañete"
                     },
                     {
                        "MMepId": "6594",
                        "text": "Arimont"
                     },
                     {
                        "MMepId": "4742",
                        "text": "Ayuso"
                     },
                     {
                        "MMepId": "6191",
                        "text": "Bach"
                     },
                     {
                        "MMepId": "6135",
                        "text": "Balz"
                     },
                     {
                        "MMepId": "6216",
                        "text": "Becker"
                     },
                     {
                        "MMepId": "5377",
                        "text": "Belet"
                     },
                     {
                        "MMepId": "5885",
                        "text": "Bendtsen"
                     },
                     {
                        "MMepId": "6343",
                        "text": "Bocskor"
                     },
                     {
                        "MMepId": "6613",
                        "text": "Bogovič"
                     },
                     {
                        "MMepId": "6535",
                        "text": "Boni"
                     },
                     {
                        "MMepId": "6617",
                        "text": "Buda"
                     },
                     {
                        "MMepId": "5388",
                        "text": "Buzek"
                     },
                     {
                        "MMepId": "1507",
                        "text": "Böge"
                     },
                     {
                        "MMepId": "6021",
                        "text": "Cadec"
                     },
                     {
                        "MMepId": "5252",
                        "text": "Casa"
                     },
                     {
                        "MMepId": "5341",
                        "text": "Caspary"
                     },
                     {
                        "MMepId": "5554",
                        "text": "Cesa"
                     },
                     {
                        "MMepId": "6492",
                        "text": "Cicu"
                     },
                     {
                        "MMepId": "6406",
                        "text": "Cirio"
                     },
                     {
                        "MMepId": "6600",
                        "text": "Clune"
                     },
                     {
                        "MMepId": "3800",
                        "text": "Coelho"
                     },
                     {
                        "MMepId": "6273",
                        "text": "Collin-Langen"
                     },
                     {
                        "MMepId": "5948",
                        "text": "Comi"
                     },
                     {
                        "MMepId": "6587",
                        "text": "Comodini Cachia"
                     },
                     {
                        "MMepId": "6548",
                        "text": "Csáky"
                     },
                     {
                        "MMepId": "5921",
                        "text": "Danjean"
                     },
                     {
                        "MMepId": "6194",
                        "text": "Dantin"
                     },
                     {
                        "MMepId": "6172",
                        "text": "Dati"
                     },
                     {
                        "MMepId": "6364",
                        "text": "Delahaye"
                     },
                     {
                        "MMepId": "6345",
                        "text": "Deli"
                     },
                     {
                        "MMepId": "5998",
                        "text": "Deutsch"
                     },
                     {
                        "MMepId": "5350",
                        "text": "Deß"
                     },
                     {
                        "MMepId": "5601",
                        "text": "Dombrovskis"
                     },
                     {
                        "MMepId": "6481",
                        "text": "Donchev"
                     },
                     {
                        "MMepId": "5960",
                        "text": "Dorfmann"
                     },
                     {
                        "MMepId": "5515",
                        "text": "Díaz de Mera García Consuegra"
                     },
                     {
                        "MMepId": "5348",
                        "text": "Ehler"
                     },
                     {
                        "MMepId": "6047",
                        "text": "Engel"
                     },
                     {
                        "MMepId": "6342",
                        "text": "Erdős"
                     },
                     {
                        "MMepId": "5983",
                        "text": "Estaràs Ferragut"
                     },
                     {
                        "MMepId": "3833",
                        "text": "Ferber"
                     },
                     {
                        "MMepId": "6068",
                        "text": "Fernandes"
                     },
                     {
                        "MMepId": "5903",
                        "text": "Fisas Ayxelà"
                     },
                     {
                        "MMepId": "4880",
                        "text": "Fitto"
                     },
                     {
                        "MMepId": "6020",
                        "text": "Gabriel"
                     },
                     {
                        "MMepId": "4659",
                        "text": "Gahler"
                     },
                     {
                        "MMepId": "6615",
                        "text": "Gambús"
                     },
                     {
                        "MMepId": "5803",
                        "text": "Gardini"
                     },
                     {
                        "MMepId": "6441",
                        "text": "Gieseke"
                     },
                     {
                        "MMepId": "6632",
                        "text": "González Pons"
                     },
                     {
                        "MMepId": "4032",
                        "text": "Grossetête"
                     },
                     {
                        "MMepId": "5109",
                        "text": "Grzyb"
                     },
                     {
                        "MMepId": "5342",
                        "text": "Gräßle"
                     },
                     {
                        "MMepId": "5138",
                        "text": "Gyürk"
                     },
                     {
                        "MMepId": "5280",
                        "text": "Gál"
                     },
                     {
                        "MMepId": "6202",
                        "text": "Gáll-Pelcz"
                     },
                     {
                        "MMepId": "6596",
                        "text": "Hayes"
                     },
                     {
                        "MMepId": "5715",
                        "text": "Hellvig"
                     },
                     {
                        "MMepId": "5018",
                        "text": "Herranz García"
                     },
                     {
                        "MMepId": "6534",
                        "text": "Hetman"
                     },
                     {
                        "MMepId": "5953",
                        "text": "Hohlmeier"
                     },
                     {
                        "MMepId": "4987",
                        "text": "Hortefeux"
                     },
                     {
                        "MMepId": "5254",
                        "text": "Hökmark"
                     },
                     {
                        "MMepId": "6346",
                        "text": "Hölvényi"
                     },
                     {
                        "MMepId": "5952",
                        "text": "Hübner"
                     },
                     {
                        "MMepId": "5945",
                        "text": "Jahr"
                     },
                     {
                        "MMepId": "5954",
                        "text": "Jazłowiecka"
                     },
                     {
                        "MMepId": "5978",
                        "text": "Jiménez-Becerril Barrio"
                     },
                     {
                        "MMepId": "6360",
                        "text": "Joulaud"
                     },
                     {
                        "MMepId": "6055",
                        "text": "Juvin"
                     },
                     {
                        "MMepId": "5944",
                        "text": "Kalinowski"
                     },
                     {
                        "MMepId": "6095",
                        "text": "Kalniete"
                     },
                     {
                        "MMepId": "4669",
                        "text": "Karas"
                     },
                     {
                        "MMepId": "6070",
                        "text": "Kariņš"
                     },
                     {
                        "MMepId": "6666",
                        "text": "Kefalogiannis"
                     },
                     {
                        "MMepId": "5542",
                        "text": "Kelam"
                     },
                     {
                        "MMepId": "5855",
                        "text": "Kelly"
                     },
                     {
                        "MMepId": "3759",
                        "text": "Koch"
                     },
                     {
                        "MMepId": "6196",
                        "text": "Kovatchev"
                     },
                     {
                        "MMepId": "6527",
                        "text": "Kozłowska-Rajewicz"
                     },
                     {
                        "MMepId": "5403",
                        "text": "Kudrycka"
                     },
                     {
                        "MMepId": "5840",
                        "text": "Kukan"
                     },
                     {
                        "MMepId": "6662",
                        "text": "Kyrtsos"
                     },
                     {
                        "MMepId": "6001",
                        "text": "Kósa"
                     },
                     {
                        "MMepId": "6053",
                        "text": "Köstinger"
                     },
                     {
                        "MMepId": "6393",
                        "text": "Landsbergis"
                     },
                     {
                        "MMepId": "3856",
                        "text": "Langen"
                     },
                     {
                        "MMepId": "6363",
                        "text": "Lavrilleux"
                     },
                     {
                        "MMepId": "6200",
                        "text": "Le Grip"
                     },
                     {
                        "MMepId": "6592",
                        "text": "Lenaers"
                     },
                     {
                        "MMepId": "5105",
                        "text": "Lewandowski"
                     },
                     {
                        "MMepId": "3855",
                        "text": "Liese"
                     },
                     {
                        "MMepId": "6442",
                        "text": "Lins"
                     },
                     {
                        "MMepId": "5938",
                        "text": "Lope Fontagné"
                     },
                     {
                        "MMepId": "5996",
                        "text": "Macovei"
                     },
                     {
                        "MMepId": "6303",
                        "text": "Maletić"
                     },
                     {
                        "MMepId": "6245",
                        "text": "Malinov"
                     },
                     {
                        "MMepId": "3845",
                        "text": "Mann"
                     },
                     {
                        "MMepId": "5646",
                        "text": "Marinescu"
                     },
                     {
                        "MMepId": "6465",
                        "text": "Martusciello"
                     },
                     {
                        "MMepId": "6096",
                        "text": "Mato"
                     },
                     {
                        "MMepId": "6440",
                        "text": "McAllister"
                     },
                     {
                        "MMepId": "5245",
                        "text": "McGuinness"
                     },
                     {
                        "MMepId": "6121",
                        "text": "Melo"
                     },
                     {
                        "MMepId": "6297",
                        "text": "Metsola"
                     },
                     {
                        "MMepId": "5308",
                        "text": "Mikolášik"
                     },
                     {
                        "MMepId": "5518",
                        "text": "Millán Mon"
                     },
                     {
                        "MMepId": "6366",
                        "text": "Monteiro de Aguiar"
                     },
                     {
                        "MMepId": "6368",
                        "text": "Morano"
                     },
                     {
                        "MMepId": "5732",
                        "text": "Morin-Chartier"
                     },
                     {
                        "MMepId": "6436",
                        "text": "Mureşan"
                     },
                     {
                        "MMepId": "6361",
                        "text": "Muselier"
                     },
                     {
                        "MMepId": "5547",
                        "text": "Mussolini"
                     },
                     {
                        "MMepId": "5774",
                        "text": "Mănescu"
                     },
                     {
                        "MMepId": "6544",
                        "text": "Nagy"
                     },
                     {
                        "MMepId": "4712",
                        "text": "Niebler"
                     },
                     {
                        "MMepId": "6332",
                        "text": "Niedermayer"
                     },
                     {
                        "MMepId": "5407",
                        "text": "Olbrycht"
                     },
                     {
                        "MMepId": "6377",
                        "text": "Pabriks"
                     },
                     {
                        "MMepId": "5702",
                        "text": "Patriciello"
                     },
                     {
                        "MMepId": "5033",
                        "text": "Peterle"
                     },
                     {
                        "MMepId": "6383",
                        "text": "Petir"
                     },
                     {
                        "MMepId": "5346",
                        "text": "Pieper"
                     },
                     {
                        "MMepId": "5788",
                        "text": "Pietikäinen"
                     },
                     {
                        "MMepId": "6537",
                        "text": "Pitera"
                     },
                     {
                        "MMepId": "6268",
                        "text": "Plenković"
                     },
                     {
                        "MMepId": "6519",
                        "text": "Plura"
                     },
                     {
                        "MMepId": "6491",
                        "text": "Pogliese"
                     },
                     {
                        "MMepId": "6335",
                        "text": "Polčák"
                     },
                     {
                        "MMepId": "6092",
                        "text": "Ponga"
                     },
                     {
                        "MMepId": "6700",
                        "text": "Pospíšil"
                     },
                     {
                        "MMepId": "6010",
                        "text": "Preda"
                     },
                     {
                        "MMepId": "6224",
                        "text": "Proust"
                     },
                     {
                        "MMepId": "1487",
                        "text": "Quisthoudt-Rowohl"
                     },
                     {
                        "MMepId": "6488",
                        "text": "Radev"
                     },
                     {
                        "MMepId": "6072",
                        "text": "Rangel"
                     },
                     {
                        "MMepId": "5347",
                        "text": "Reul"
                     },
                     {
                        "MMepId": "6365",
                        "text": "Ribeiro"
                     },
                     {
                        "MMepId": "6684",
                        "text": "Rolin"
                     },
                     {
                        "MMepId": "5512",
                        "text": "Rosati"
                     },
                     {
                        "MMepId": "4596",
                        "text": "Rübig"
                     },
                     {
                        "MMepId": "6698",
                        "text": "Salini"
                     },
                     {
                        "MMepId": "6370",
                        "text": "Sander"
                     },
                     {
                        "MMepId": "6259",
                        "text": "Sarvamaa"
                     },
                     {
                        "MMepId": "4768",
                        "text": "Saïfi"
                     },
                     {
                        "MMepId": "6619",
                        "text": "Schmidt"
                     },
                     {
                        "MMepId": "6636",
                        "text": "Schreijer-Pierik"
                     },
                     {
                        "MMepId": "6443",
                        "text": "Schulze"
                     },
                     {
                        "MMepId": "5345",
                        "text": "Schwab"
                     },
                     {
                        "MMepId": "5265",
                        "text": "Schöpflin"
                     },
                     {
                        "MMepId": "6428",
                        "text": "Sernagiotto"
                     },
                     {
                        "MMepId": "5111",
                        "text": "Siekierski"
                     },
                     {
                        "MMepId": "4705",
                        "text": "Sommer"
                     },
                     {
                        "MMepId": "6663",
                        "text": "Spyraki"
                     },
                     {
                        "MMepId": "5778",
                        "text": "Stolojan"
                     },
                     {
                        "MMepId": "6320",
                        "text": "Stylianides"
                     },
                     {
                        "MMepId": "6672",
                        "text": "Svoboda"
                     },
                     {
                        "MMepId": "6528",
                        "text": "Szejnfeld"
                     },
                     {
                        "MMepId": "5143",
                        "text": "Szájer"
                     },
                     {
                        "MMepId": "5781",
                        "text": "Sógor"
                     },
                     {
                        "MMepId": "5207",
                        "text": "Theocharous"
                     },
                     {
                        "MMepId": "5949",
                        "text": "Thun und Hohenstein"
                     },
                     {
                        "MMepId": "3738",
                        "text": "Thyssen"
                     },
                     {
                        "MMepId": "6681",
                        "text": "Tomc"
                     },
                     {
                        "MMepId": "6398",
                        "text": "Toti"
                     },
                     {
                        "MMepId": "5783",
                        "text": "Tőkés"
                     },
                     {
                        "MMepId": "6007",
                        "text": "Ungureanu"
                     },
                     {
                        "MMepId": "5734",
                        "text": "Urutchev"
                     },
                     {
                        "MMepId": "6638",
                        "text": "Valcárcel"
                     },
                     {
                        "MMepId": "5930",
                        "text": "Verheyen"
                     },
                     {
                        "MMepId": "6356",
                        "text": "Virkkunen"
                     },
                     {
                        "MMepId": "5934",
                        "text": "Voss"
                     },
                     {
                        "MMepId": "6664",
                        "text": "Vozemberg"
                     },
                     {
                        "MMepId": "5714",
                        "text": "Vălean"
                     },
                     {
                        "MMepId": "5947",
                        "text": "Wałęsa"
                     },
                     {
                        "MMepId": "5770",
                        "text": "Weber Renate"
                     },
                     {
                        "MMepId": "6542",
                        "text": "Wenta"
                     },
                     {
                        "MMepId": "5937",
                        "text": "Winkler Hermann"
                     },
                     {
                        "MMepId": "5782",
                        "text": "Winkler Iuliu"
                     },
                     {
                        "MMepId": "6665",
                        "text": "Zagorakis"
                     },
                     {
                        "MMepId": "5936",
                        "text": "Zalba Bidegain"
                     },
                     {
                        "MMepId": "6344",
                        "text": "Zdechovský"
                     },
                     {
                        "MMepId": "6532",
                        "text": "Zdrojewski"
                     },
                     {
                        "MMepId": "5935",
                        "text": "Zeller"
                     },
                     {
                        "MMepId": "6094",
                        "text": "Zver"
                     },
                     {
                        "MMepId": "5420",
                        "text": "Zwiefka"
                     },
                     {
                        "MMepId": "5189",
                        "text": "Záborská"
                     },
                     {
                        "MMepId": "5511",
                        "text": "de Grandes Pascual"
                     },
                     {
                        "MMepId": "5727",
                        "text": "de Lange"
                     },
                     {
                        "MMepId": "5508",
                        "text": "del Castillo Vera"
                     },
                     {
                        "MMepId": "5295",
                        "text": "van Nistelrooij"
                     },
                     {
                        "MMepId": "5928",
                        "text": "van de Camp"
                     },
                     {
                        "MMepId": "5964",
                        "text": "Łukacijewska"
                     },
                     {
                        "MMepId": "6341",
                        "text": "Šojdrová"
                     },
                     {
                        "MMepId": "6547",
                        "text": "Štefanec"
                     },
                     {
                        "MMepId": "6301",
                        "text": "Šuica"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6651",
                        "text": "Aguilera García"
                     },
                     {
                        "MMepId": "6567",
                        "text": "Anderson Lucy"
                     },
                     {
                        "MMepId": "6279",
                        "text": "Andrieu"
                     },
                     {
                        "MMepId": "6687",
                        "text": "Androulakis"
                     },
                     {
                        "MMepId": "6553",
                        "text": "Arena"
                     },
                     {
                        "MMepId": "5426",
                        "text": "Assis"
                     },
                     {
                        "MMepId": "5411",
                        "text": "Ayala Sender"
                     },
                     {
                        "MMepId": "6378",
                        "text": "Balas"
                     },
                     {
                        "MMepId": "5866",
                        "text": "Balčytis"
                     },
                     {
                        "MMepId": "6611",
                        "text": "Bayet"
                     },
                     {
                        "MMepId": "6505",
                        "text": "Benifei"
                     },
                     {
                        "MMepId": "3972",
                        "text": "Berès"
                     },
                     {
                        "MMepId": "6456",
                        "text": "Bettini"
                     },
                     {
                        "MMepId": "6650",
                        "text": "Blanco López"
                     },
                     {
                        "MMepId": "5867",
                        "text": "Blinkevičiūtė"
                     },
                     {
                        "MMepId": "6449",
                        "text": "Bonafè"
                     },
                     {
                        "MMepId": "6264",
                        "text": "Borzan"
                     },
                     {
                        "MMepId": "5768",
                        "text": "Boştinaru"
                     },
                     {
                        "MMepId": "6572",
                        "text": "Brannen"
                     },
                     {
                        "MMepId": "5465",
                        "text": "Bresso"
                     },
                     {
                        "MMepId": "6421",
                        "text": "Briano"
                     },
                     {
                        "MMepId": "6647",
                        "text": "Cabezón Ruiz"
                     },
                     {
                        "MMepId": "6489",
                        "text": "Caputo"
                     },
                     {
                        "MMepId": "5835",
                        "text": "Childers"
                     },
                     {
                        "MMepId": "6499",
                        "text": "Chinnici"
                     },
                     {
                        "MMepId": "5284",
                        "text": "Christensen"
                     },
                     {
                        "MMepId": "6083",
                        "text": "Cofferati"
                     },
                     {
                        "MMepId": "4628",
                        "text": "Corbett"
                     },
                     {
                        "MMepId": "6085",
                        "text": "Costa"
                     },
                     {
                        "MMepId": "6051",
                        "text": "Cozzolino"
                     },
                     {
                        "MMepId": "5661",
                        "text": "Creţu"
                     },
                     {
                        "MMepId": "6616",
                        "text": "Cristea"
                     },
                     {
                        "MMepId": "6589",
                        "text": "Dalli"
                     },
                     {
                        "MMepId": "6570",
                        "text": "Dance"
                     },
                     {
                        "MMepId": "6458",
                        "text": "Danti"
                     },
                     {
                        "MMepId": "6062",
                        "text": "De Castro"
                     },
                     {
                        "MMepId": "6437",
                        "text": "De Monte"
                     },
                     {
                        "MMepId": "6410",
                        "text": "Delvaux-Stehres"
                     },
                     {
                        "MMepId": "5816",
                        "text": "Denanot"
                     },
                     {
                        "MMepId": "6588",
                        "text": "Dodds Anneliese"
                     },
                     {
                        "MMepId": "6420",
                        "text": "Drăghici"
                     },
                     {
                        "MMepId": "5829",
                        "text": "Dăncilă"
                     },
                     {
                        "MMepId": "6080",
                        "text": "Fajon"
                     },
                     {
                        "MMepId": "6652",
                        "text": "Fernández"
                     },
                     {
                        "MMepId": "5427",
                        "text": "Ferreira Elisa"
                     },
                     {
                        "MMepId": "5173",
                        "text": "Flašíková Beňová"
                     },
                     {
                        "MMepId": "6012",
                        "text": "Fleckenstein"
                     },
                     {
                        "MMepId": "6620",
                        "text": "Freund"
                     },
                     {
                        "MMepId": "6424",
                        "text": "Frunzulică"
                     },
                     {
                        "MMepId": "5417",
                        "text": "García Pérez"
                     },
                     {
                        "MMepId": "6130",
                        "text": "Gardiazabal Rubial"
                     },
                     {
                        "MMepId": "6454",
                        "text": "Gasbarra"
                     },
                     {
                        "MMepId": "3827",
                        "text": "Gebhardt"
                     },
                     {
                        "MMepId": "6005",
                        "text": "Geier"
                     },
                     {
                        "MMepId": "6485",
                        "text": "Gentile"
                     },
                     {
                        "MMepId": "5495",
                        "text": "Geringer de Oedenberg"
                     },
                     {
                        "MMepId": "5497",
                        "text": "Gierek"
                     },
                     {
                        "MMepId": "4937",
                        "text": "Gill Neena"
                     },
                     {
                        "MMepId": "6502",
                        "text": "Giuffrida"
                     },
                     {
                        "MMepId": "5425",
                        "text": "Gomes"
                     },
                     {
                        "MMepId": "6419",
                        "text": "Grapini"
                     },
                     {
                        "MMepId": "6580",
                        "text": "Griffin"
                     },
                     {
                        "MMepId": "5692",
                        "text": "Groote"
                     },
                     {
                        "MMepId": "6063",
                        "text": "Gualtieri"
                     },
                     {
                        "MMepId": "5941",
                        "text": "Guerrero Salom"
                     },
                     {
                        "MMepId": "6107",
                        "text": "Guillaume"
                     },
                     {
                        "MMepId": "6603",
                        "text": "Guteland"
                     },
                     {
                        "MMepId": "6212",
                        "text": "Gutiérrez Prieto"
                     },
                     {
                        "MMepId": "5261",
                        "text": "Hedh"
                     },
                     {
                        "MMepId": "6479",
                        "text": "Hoffmann"
                     },
                     {
                        "MMepId": "4994",
                        "text": "Honeyball"
                     },
                     {
                        "MMepId": "4103",
                        "text": "Howitt"
                     },
                     {
                        "MMepId": "6029",
                        "text": "Ivan"
                     },
                     {
                        "MMepId": "5870",
                        "text": "Jaakonsaari"
                     },
                     {
                        "MMepId": "6624",
                        "text": "Jongerius"
                     },
                     {
                        "MMepId": "6161",
                        "text": "Jáuregui Atondo"
                     },
                     {
                        "MMepId": "6146",
                        "text": "Kadenbach"
                     },
                     {
                        "MMepId": "6686",
                        "text": "Kaili"
                     },
                     {
                        "MMepId": "6009",
                        "text": "Kammerevert"
                     },
                     {
                        "MMepId": "3756",
                        "text": "Kaufmann"
                     },
                     {
                        "MMepId": "6581",
                        "text": "Khan"
                     },
                     {
                        "MMepId": "6571",
                        "text": "Kirton-Darling"
                     },
                     {
                        "MMepId": "6508",
                        "text": "Kofod"
                     },
                     {
                        "MMepId": "3761",
                        "text": "Krehl"
                     },
                     {
                        "MMepId": "6367",
                        "text": "Kumpula-Natri"
                     },
                     {
                        "MMepId": "6435",
                        "text": "Kyenge"
                     },
                     {
                        "MMepId": "6689",
                        "text": "Kyrkos"
                     },
                     {
                        "MMepId": "6459",
                        "text": "Köster"
                     },
                     {
                        "MMepId": "3823",
                        "text": "Lange"
                     },
                     {
                        "MMepId": "6329",
                        "text": "Lauristin"
                     },
                     {
                        "MMepId": "4685",
                        "text": "Leinen"
                     },
                     {
                        "MMepId": "5094",
                        "text": "Liberadzki"
                     },
                     {
                        "MMepId": "6477",
                        "text": "Lietz"
                     },
                     {
                        "MMepId": "5860",
                        "text": "Ludvigsson"
                     },
                     {
                        "MMepId": "6648",
                        "text": "López Javi"
                     },
                     {
                        "MMepId": "5984",
                        "text": "López Aguilar"
                     },
                     {
                        "MMepId": "6380",
                        "text": "Mamikins"
                     },
                     {
                        "MMepId": "6355",
                        "text": "Manscour"
                     },
                     {
                        "MMepId": "1133",
                        "text": "Martin David"
                     },
                     {
                        "MMepId": "6354",
                        "text": "Maurel"
                     },
                     {
                        "MMepId": "5314",
                        "text": "Maňka"
                     },
                     {
                        "MMepId": "4646",
                        "text": "McAvan"
                     },
                     {
                        "MMepId": "6296",
                        "text": "Mizzi"
                     },
                     {
                        "MMepId": "6423",
                        "text": "Moisă"
                     },
                     {
                        "MMepId": "6351",
                        "text": "Molnár"
                     },
                     {
                        "MMepId": "6562",
                        "text": "Moody"
                     },
                     {
                        "MMepId": "4923",
                        "text": "Moraes"
                     },
                     {
                        "MMepId": "6433",
                        "text": "Moretti"
                     },
                     {
                        "MMepId": "6422",
                        "text": "Morgano"
                     },
                     {
                        "MMepId": "6506",
                        "text": "Mosca"
                     },
                     {
                        "MMepId": "6691",
                        "text": "Negrescu"
                     },
                     {
                        "MMepId": "6016",
                        "text": "Neuser"
                     },
                     {
                        "MMepId": "6418",
                        "text": "Nica"
                     },
                     {
                        "MMepId": "6352",
                        "text": "Niedermüller"
                     },
                     {
                        "MMepId": "6242",
                        "text": "Nilsson"
                     },
                     {
                        "MMepId": "6473",
                        "text": "Noichl"
                     },
                     {
                        "MMepId": "5483",
                        "text": "Panzeri"
                     },
                     {
                        "MMepId": "6487",
                        "text": "Paolucci"
                     },
                     {
                        "MMepId": "6323",
                        "text": "Papadakis Demetris"
                     },
                     {
                        "MMepId": "6103",
                        "text": "Pargneaux"
                     },
                     {
                        "MMepId": "5648",
                        "text": "Paşcu"
                     },
                     {
                        "MMepId": "5296",
                        "text": "Peillon"
                     },
                     {
                        "MMepId": "6484",
                        "text": "Picierno"
                     },
                     {
                        "MMepId": "6261",
                        "text": "Picula"
                     },
                     {
                        "MMepId": "6625",
                        "text": "Piri"
                     },
                     {
                        "MMepId": "6493",
                        "text": "Pirinski"
                     },
                     {
                        "MMepId": "5894",
                        "text": "Poc"
                     },
                     {
                        "MMepId": "6136",
                        "text": "Regner"
                     },
                     {
                        "MMepId": "6316",
                        "text": "Revault D'Allonnes Bonnefoy"
                     },
                     {
                        "MMepId": "6677",
                        "text": "Rodrigues Liliana"
                     },
                     {
                        "MMepId": "6649",
                        "text": "Rodríguez-Piñero Fernández"
                     },
                     {
                        "MMepId": "5813",
                        "text": "Rodust"
                     },
                     {
                        "MMepId": "6357",
                        "text": "Rozière"
                     },
                     {
                        "MMepId": "6415",
                        "text": "Sant"
                     },
                     {
                        "MMepId": "5713",
                        "text": "Schaldemose"
                     },
                     {
                        "MMepId": "6438",
                        "text": "Schlein"
                     },
                     {
                        "MMepId": "6475",
                        "text": "Schuster"
                     },
                     {
                        "MMepId": "5897",
                        "text": "Sehnalová"
                     },
                     {
                        "MMepId": "6375",
                        "text": "Serrão Santos"
                     },
                     {
                        "MMepId": "6381",
                        "text": "Silva Pereira"
                     },
                     {
                        "MMepId": "6008",
                        "text": "Simon Peter"
                     },
                     {
                        "MMepId": "6566",
                        "text": "Simon Siôn"
                     },
                     {
                        "MMepId": "6093",
                        "text": "Sippel"
                     },
                     {
                        "MMepId": "5844",
                        "text": "Smolková"
                     },
                     {
                        "MMepId": "6498",
                        "text": "Soru"
                     },
                     {
                        "MMepId": "6003",
                        "text": "Steinruck"
                     },
                     {
                        "MMepId": "4949",
                        "text": "Stihler"
                     },
                     {
                        "MMepId": "6334",
                        "text": "Szanyi"
                     },
                     {
                        "MMepId": "5653",
                        "text": "Sârbu"
                     },
                     {
                        "MMepId": "6623",
                        "text": "Tang"
                     },
                     {
                        "MMepId": "6427",
                        "text": "Tapardel"
                     },
                     {
                        "MMepId": "5624",
                        "text": "Tarabella"
                     },
                     {
                        "MMepId": "6282",
                        "text": "Thomas"
                     },
                     {
                        "MMepId": "5459",
                        "text": "Toia"
                     },
                     {
                        "MMepId": "6336",
                        "text": "Ujhelyi"
                     },
                     {
                        "MMepId": "5859",
                        "text": "Ulvskog"
                     },
                     {
                        "MMepId": "4990",
                        "text": "Van Brempt"
                     },
                     {
                        "MMepId": "6086",
                        "text": "Vaughan"
                     },
                     {
                        "MMepId": "6425",
                        "text": "Viotti"
                     },
                     {
                        "MMepId": "6582",
                        "text": "Ward"
                     },
                     {
                        "MMepId": "6235",
                        "text": "Weidenholzer"
                     },
                     {
                        "MMepId": "6463",
                        "text": "Werner"
                     },
                     {
                        "MMepId": "6011",
                        "text": "Westphal"
                     },
                     {
                        "MMepId": "5698",
                        "text": "Willmott"
                     },
                     {
                        "MMepId": "5845",
                        "text": "Zala"
                     },
                     {
                        "MMepId": "6434",
                        "text": "Zanonato"
                     },
                     {
                        "MMepId": "5957",
                        "text": "Zemke"
                     },
                     {
                        "MMepId": "6373",
                        "text": "Zorrinho"
                     },
                     {
                        "MMepId": "6478",
                        "text": "von Weizsäcker"
                     },
                     {
                        "MMepId": "6521",
                        "text": "Łybacka"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": {
                     "MMepId": "6168",
                     "text": "Tarand"
                  }
               }
            ]
         },
         "ResultAbstention": {
            "MNumber": "19",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ALDE",
                  "PoliticalGroupMemberName": {
                     "MMepId": "6339",
                     "text": "Charanzová"
                  }
               },
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6466",
                        "text": "Trebesius"
                     },
                     {
                        "MMepId": "5113",
                        "text": "Ujazdowski"
                     }
                  ]
               },
               {
                  "MIdentifier": "GUE/NGL",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5886",
                        "text": "Ferreira João"
                     },
                     {
                        "MMepId": "6076",
                        "text": "Hadjigeorgiou"
                     },
                     {
                        "MMepId": "6321",
                        "text": "Sylikiotis"
                     },
                     {
                        "MMepId": "6030",
                        "text": "Vergiat"
                     },
                     {
                        "MMepId": "6678",
                        "text": "Viegas"
                     },
                     {
                        "MMepId": "6253",
                        "text": "Zuber"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6179",
                        "text": "Bizzotto"
                     },
                     {
                        "MMepId": "5010",
                        "text": "Borghezio"
                     },
                     {
                        "MMepId": "6507",
                        "text": "Buonanno"
                     },
                     {
                        "MMepId": "6132",
                        "text": "Fontana"
                     },
                     {
                        "MMepId": "6524",
                        "text": "Iwaszkiewicz"
                     },
                     {
                        "MMepId": "6517",
                        "text": "Korwin-Mikke"
                     },
                     {
                        "MMepId": "6533",
                        "text": "Marusik"
                     },
                     {
                        "MMepId": "5522",
                        "text": "Salvini"
                     },
                     {
                        "MMepId": "6389",
                        "text": "Schaffhauser"
                     },
                     {
                        "MMepId": "6541",
                        "text": "Żółtek"
                     }
                  ]
               }
            ]
         }
      },
      {
         "MDate": "2014-07-16 12:23:45",
         "MIdentifier": "47692",
         "RollCallVoteDescriptionText": "A8-0001/2014 -  Werner Langen - Am 3",
         "ResultFor": {
            "MNumber": "93",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": {
                     "MMepId": "6667",
                     "text": "Marias"
                  }
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6468",
                        "text": "Adinolfi"
                     },
                     {
                        "MMepId": "6431",
                        "text": "Affronte"
                     },
                     {
                        "MMepId": "6445",
                        "text": "Agea"
                     },
                     {
                        "MMepId": "6066",
                        "text": "Agnew"
                     },
                     {
                        "MMepId": "6480",
                        "text": "Aiuto"
                     },
                     {
                        "MMepId": "6559",
                        "text": "Aker"
                     },
                     {
                        "MMepId": "6577",
                        "text": "Arnott"
                     },
                     {
                        "MMepId": "6555",
                        "text": "Atkinson"
                     },
                     {
                        "MMepId": "6574",
                        "text": "Bashir"
                     },
                     {
                        "MMepId": "6411",
                        "text": "Beghin"
                     },
                     {
                        "MMepId": "6374",
                        "text": "Bergeron"
                     },
                     {
                        "MMepId": "6430",
                        "text": "Borrelli"
                     },
                     {
                        "MMepId": "6583",
                        "text": "Bours"
                     },
                     {
                        "MMepId": "6590",
                        "text": "Carver"
                     },
                     {
                        "MMepId": "6446",
                        "text": "Castaldo"
                     },
                     {
                        "MMepId": "6586",
                        "text": "Coburn"
                     },
                     {
                        "MMepId": "6573",
                        "text": "Collins"
                     },
                     {
                        "MMepId": "6494",
                        "text": "Corrao"
                     },
                     {
                        "MMepId": "6472",
                        "text": "D'Amato"
                     },
                     {
                        "MMepId": "6569",
                        "text": "Etheridge"
                     },
                     {
                        "MMepId": "6413",
                        "text": "Evi"
                     },
                     {
                        "MMepId": "4929",
                        "text": "Farage"
                     },
                     {
                        "MMepId": "6470",
                        "text": "Ferrara"
                     },
                     {
                        "MMepId": "6558",
                        "text": "Finch"
                     },
                     {
                        "MMepId": "6584",
                        "text": "Gill Nathan"
                     },
                     {
                        "MMepId": "6379",
                        "text": "Grigule"
                     },
                     {
                        "MMepId": "4920",
                        "text": "Helmer"
                     },
                     {
                        "MMepId": "6575",
                        "text": "Hookem"
                     },
                     {
                        "MMepId": "6556",
                        "text": "James"
                     },
                     {
                        "MMepId": "6608",
                        "text": "Lundgren"
                     },
                     {
                        "MMepId": "6448",
                        "text": "Mach"
                     },
                     {
                        "MMepId": "6497",
                        "text": "Moi"
                     },
                     {
                        "MMepId": "5977",
                        "text": "Nuttall"
                     },
                     {
                        "MMepId": "6557",
                        "text": "O'Flynn"
                     },
                     {
                        "MMepId": "6563",
                        "text": "Parker"
                     },
                     {
                        "MMepId": "6482",
                        "text": "Pedicini"
                     },
                     {
                        "MMepId": "6564",
                        "text": "Reid"
                     },
                     {
                        "MMepId": "6412",
                        "text": "Valli"
                     },
                     {
                        "MMepId": "6607",
                        "text": "Winberg"
                     },
                     {
                        "MMepId": "6585",
                        "text": "Woolfe"
                     },
                     {
                        "MMepId": "6414",
                        "text": "Zanni"
                     },
                     {
                        "MMepId": "6695",
                        "text": "Zullo"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6048",
                        "text": "Balczó"
                     },
                     {
                        "MMepId": "6203",
                        "text": "Kovács"
                     },
                     {
                        "MMepId": "6389",
                        "text": "Schaffhauser"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6326",
                        "text": "Keller Jan"
                     },
                     {
                        "MMepId": "6476",
                        "text": "Melior"
                     },
                     {
                        "MMepId": "6426",
                        "text": "Rebega"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5910",
                        "text": "Albrecht"
                     },
                     {
                        "MMepId": "5291",
                        "text": "Auken"
                     },
                     {
                        "MMepId": "5918",
                        "text": "Bové"
                     },
                     {
                        "MMepId": "5913",
                        "text": "Bütikofer"
                     },
                     {
                        "MMepId": "5358",
                        "text": "Cramer"
                     },
                     {
                        "MMepId": "6040",
                        "text": "Delli"
                     },
                     {
                        "MMepId": "6324",
                        "text": "Durand"
                     },
                     {
                        "MMepId": "5899",
                        "text": "Eickhout"
                     },
                     {
                        "MMepId": "4954",
                        "text": "Evans"
                     },
                     {
                        "MMepId": "5904",
                        "text": "Giegold"
                     },
                     {
                        "MMepId": "5353",
                        "text": "Harms"
                     },
                     {
                        "MMepId": "4535",
                        "text": "Hautala"
                     },
                     {
                        "MMepId": "6486",
                        "text": "Heubuch"
                     },
                     {
                        "MMepId": "4655",
                        "text": "Hudghton"
                     },
                     {
                        "MMepId": "5926",
                        "text": "Häusling"
                     },
                     {
                        "MMepId": "5914",
                        "text": "Jadot"
                     },
                     {
                        "MMepId": "6054",
                        "text": "Joly"
                     },
                     {
                        "MMepId": "6350",
                        "text": "Jávor"
                     },
                     {
                        "MMepId": "5908",
                        "text": "Keller Ska"
                     },
                     {
                        "MMepId": "4935",
                        "text": "Lambert"
                     },
                     {
                        "MMepId": "5838",
                        "text": "Lamberts"
                     },
                     {
                        "MMepId": "5902",
                        "text": "Lochbihler"
                     },
                     {
                        "MMepId": "6149",
                        "text": "Lunacek"
                     },
                     {
                        "MMepId": "6550",
                        "text": "Maragall"
                     },
                     {
                        "MMepId": "6349",
                        "text": "Meszerics"
                     },
                     {
                        "MMepId": "6453",
                        "text": "Reda"
                     },
                     {
                        "MMepId": "6552",
                        "text": "Reimon"
                     },
                     {
                        "MMepId": "6529",
                        "text": "Reintke"
                     },
                     {
                        "MMepId": "5917",
                        "text": "Rivasi"
                     },
                     {
                        "MMepId": "6694",
                        "text": "Ropé"
                     },
                     {
                        "MMepId": "5987",
                        "text": "Sargentini"
                     },
                     {
                        "MMepId": "6560",
                        "text": "Scott Cato"
                     },
                     {
                        "MMepId": "6659",
                        "text": "Sebastià"
                     },
                     {
                        "MMepId": "5571",
                        "text": "Smith"
                     },
                     {
                        "MMepId": "4983",
                        "text": "Staes"
                     },
                     {
                        "MMepId": "6168",
                        "text": "Tarand"
                     },
                     {
                        "MMepId": "6204",
                        "text": "Taylor"
                     },
                     {
                        "MMepId": "6549",
                        "text": "Terricabras"
                     },
                     {
                        "MMepId": "5360",
                        "text": "Trüpel"
                     },
                     {
                        "MMepId": "4847",
                        "text": "Turmes"
                     },
                     {
                        "MMepId": "6591",
                        "text": "Urtasun"
                     },
                     {
                        "MMepId": "6551",
                        "text": "Vana"
                     },
                     {
                        "MMepId": "6612",
                        "text": "Šoltes"
                     },
                     {
                        "MMepId": "5602",
                        "text": "Ždanoka"
                     }
                  ]
               }
            ]
         },
         "ResultAgainst": {
            "MNumber": "550",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ALDE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5682",
                        "text": "Ali"
                     },
                     {
                        "MMepId": "6327",
                        "text": "Ansip"
                     },
                     {
                        "MMepId": "6407",
                        "text": "Arthuis"
                     },
                     {
                        "MMepId": "6400",
                        "text": "Auštrevičius"
                     },
                     {
                        "MMepId": "6110",
                        "text": "Bearder"
                     },
                     {
                        "MMepId": "6646",
                        "text": "Becerra Basterrechea"
                     },
                     {
                        "MMepId": "6089",
                        "text": "Bilbao Barandica"
                     },
                     {
                        "MMepId": "5328",
                        "text": "Cavada"
                     },
                     {
                        "MMepId": "6339",
                        "text": "Charanzová"
                     },
                     {
                        "MMepId": "6226",
                        "text": "De Backer"
                     },
                     {
                        "MMepId": "1060",
                        "text": "Deprez"
                     },
                     {
                        "MMepId": "6439",
                        "text": "Diaconu"
                     },
                     {
                        "MMepId": "6340",
                        "text": "Dlabajová"
                     },
                     {
                        "MMepId": "6679",
                        "text": "Faria"
                     },
                     {
                        "MMepId": "6601",
                        "text": "Federley"
                     },
                     {
                        "MMepId": "6098",
                        "text": "Gerbrandy"
                     },
                     {
                        "MMepId": "6658",
                        "text": "Girauta Vidal"
                     },
                     {
                        "MMepId": "1793",
                        "text": "Goerens"
                     },
                     {
                        "MMepId": "6169",
                        "text": "Goulard"
                     },
                     {
                        "MMepId": "5330",
                        "text": "Griesbeck"
                     },
                     {
                        "MMepId": "6397",
                        "text": "Guoga"
                     },
                     {
                        "MMepId": "5246",
                        "text": "Harkin"
                     },
                     {
                        "MMepId": "6622",
                        "text": "Huitema"
                     },
                     {
                        "MMepId": "5681",
                        "text": "Hyusmenova"
                     },
                     {
                        "MMepId": "6388",
                        "text": "Jakovčić"
                     },
                     {
                        "MMepId": "6338",
                        "text": "Ježek"
                     },
                     {
                        "MMepId": "5433",
                        "text": "Jäätteenmäki"
                     },
                     {
                        "MMepId": "6328",
                        "text": "Kallas"
                     },
                     {
                        "MMepId": "6504",
                        "text": "Kyuchyuk"
                     },
                     {
                        "MMepId": "5362",
                        "text": "Lambsdorff"
                     },
                     {
                        "MMepId": "6376",
                        "text": "Marinho e Pinto"
                     },
                     {
                        "MMepId": "6645",
                        "text": "Maura Barandiarán"
                     },
                     {
                        "MMepId": "6041",
                        "text": "Meissner"
                     },
                     {
                        "MMepId": "5857",
                        "text": "Michel"
                     },
                     {
                        "MMepId": "6690",
                        "text": "Mihaylova"
                     },
                     {
                        "MMepId": "6631",
                        "text": "Mlinar"
                     },
                     {
                        "MMepId": "6500",
                        "text": "Müller"
                     },
                     {
                        "MMepId": "6614",
                        "text": "Nart"
                     },
                     {
                        "MMepId": "3899",
                        "text": "Neyts-Uyttebroeck"
                     },
                     {
                        "MMepId": "5997",
                        "text": "Nicolai"
                     },
                     {
                        "MMepId": "6644",
                        "text": "Pagazaurtundúa Ruiz"
                     },
                     {
                        "MMepId": "4693",
                        "text": "Paulsen"
                     },
                     {
                        "MMepId": "6510",
                        "text": "Petersen"
                     },
                     {
                        "MMepId": "6266",
                        "text": "Radoš"
                     },
                     {
                        "MMepId": "4544",
                        "text": "Rehn"
                     },
                     {
                        "MMepId": "4676",
                        "text": "Ries"
                     },
                     {
                        "MMepId": "6056",
                        "text": "Riquet"
                     },
                     {
                        "MMepId": "6154",
                        "text": "Rochefort"
                     },
                     {
                        "MMepId": "5890",
                        "text": "Rohde"
                     },
                     {
                        "MMepId": "6100",
                        "text": "Schaake"
                     },
                     {
                        "MMepId": "6174",
                        "text": "Sosa Wagner"
                     },
                     {
                        "MMepId": "6546",
                        "text": "Sulík"
                     },
                     {
                        "MMepId": "6337",
                        "text": "Telička"
                     },
                     {
                        "MMepId": "6042",
                        "text": "Theurer"
                     },
                     {
                        "MMepId": "6281",
                        "text": "Torvalds"
                     },
                     {
                        "MMepId": "6181",
                        "text": "Tremosa i Balcells"
                     },
                     {
                        "MMepId": "6518",
                        "text": "Tørnæs"
                     },
                     {
                        "MMepId": "6151",
                        "text": "Vajgl"
                     },
                     {
                        "MMepId": "6159",
                        "text": "Verhofstadt"
                     },
                     {
                        "MMepId": "4550",
                        "text": "Väyrynen"
                     },
                     {
                        "MMepId": "5864",
                        "text": "Wikström"
                     },
                     {
                        "MMepId": "4758",
                        "text": "de Sarnez"
                     },
                     {
                        "MMepId": "5385",
                        "text": "in 't Veld"
                     },
                     {
                        "MMepId": "6097",
                        "text": "van Baalen"
                     },
                     {
                        "MMepId": "6621",
                        "text": "van Nieuwenhuizen"
                     }
                  ]
               },
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5262",
                        "text": "Ashworth"
                     },
                     {
                        "MMepId": "4911",
                        "text": "Belder"
                     },
                     {
                        "MMepId": "6081",
                        "text": "Campbell Bannerman"
                     },
                     {
                        "MMepId": "5490",
                        "text": "Czarnecki"
                     },
                     {
                        "MMepId": "6291",
                        "text": "Demesmaeker"
                     },
                     {
                        "MMepId": "6516",
                        "text": "Dohrmann"
                     },
                     {
                        "MMepId": "6538",
                        "text": "Duda"
                     },
                     {
                        "MMepId": "6579",
                        "text": "Duncan"
                     },
                     {
                        "MMepId": "6511",
                        "text": "Dzhambazki"
                     },
                     {
                        "MMepId": "6104",
                        "text": "Ford"
                     },
                     {
                        "MMepId": "4957",
                        "text": "Foster"
                     },
                     {
                        "MMepId": "5472",
                        "text": "Fotyga"
                     },
                     {
                        "MMepId": "6112",
                        "text": "Fox"
                     },
                     {
                        "MMepId": "6452",
                        "text": "Gericke"
                     },
                     {
                        "MMepId": "6111",
                        "text": "Girling"
                     },
                     {
                        "MMepId": "6539",
                        "text": "Gosiewska"
                     },
                     {
                        "MMepId": "5963",
                        "text": "Gróbarczyk"
                     },
                     {
                        "MMepId": "4959",
                        "text": "Hannan"
                     },
                     {
                        "MMepId": "6520",
                        "text": "Jackiewicz"
                     },
                     {
                        "MMepId": "6531",
                        "text": "Jurek"
                     },
                     {
                        "MMepId": "5630",
                        "text": "Kamall"
                     },
                     {
                        "MMepId": "5563",
                        "text": "Karim"
                     },
                     {
                        "MMepId": "6514",
                        "text": "Karlsson"
                     },
                     {
                        "MMepId": "6525",
                        "text": "Karski"
                     },
                     {
                        "MMepId": "4946",
                        "text": "Kirkhope"
                     },
                     {
                        "MMepId": "6530",
                        "text": "Krasnodębski"
                     },
                     {
                        "MMepId": "5507",
                        "text": "Kuźmiuk"
                     },
                     {
                        "MMepId": "6461",
                        "text": "Kölmel"
                     },
                     {
                        "MMepId": "6561",
                        "text": "Lewer"
                     },
                     {
                        "MMepId": "6087",
                        "text": "McClarkin"
                     },
                     {
                        "MMepId": "6234",
                        "text": "McIntyre"
                     },
                     {
                        "MMepId": "5851",
                        "text": "Messerschmidt"
                     },
                     {
                        "MMepId": "1230",
                        "text": "Nicholson"
                     },
                     {
                        "MMepId": "6536",
                        "text": "Ożóg"
                     },
                     {
                        "MMepId": "6512",
                        "text": "Piecha"
                     },
                     {
                        "MMepId": "5460",
                        "text": "Piotrowski"
                     },
                     {
                        "MMepId": "5974",
                        "text": "Poręba"
                     },
                     {
                        "MMepId": "6467",
                        "text": "Pretzell"
                     },
                     {
                        "MMepId": "6464",
                        "text": "Starbatty"
                     },
                     {
                        "MMepId": "6682",
                        "text": "Stevens"
                     },
                     {
                        "MMepId": "6088",
                        "text": "Swinburne"
                     },
                     {
                        "MMepId": "4925",
                        "text": "Tannock"
                     },
                     {
                        "MMepId": "6220",
                        "text": "Terho"
                     },
                     {
                        "MMepId": "6300",
                        "text": "Tomašić"
                     },
                     {
                        "MMepId": "5892",
                        "text": "Tošenovský"
                     },
                     {
                        "MMepId": "5113",
                        "text": "Ujazdowski"
                     },
                     {
                        "MMepId": "4917",
                        "text": "Van Orden"
                     },
                     {
                        "MMepId": "6683",
                        "text": "Van Overtveldt"
                     },
                     {
                        "MMepId": "6513",
                        "text": "Vistisen"
                     },
                     {
                        "MMepId": "6515",
                        "text": "Wiśniewska"
                     },
                     {
                        "MMepId": "5108",
                        "text": "Wojciechowski"
                     },
                     {
                        "MMepId": "5052",
                        "text": "Zahradil"
                     },
                     {
                        "MMepId": "5598",
                        "text": "Zīle"
                     },
                     {
                        "MMepId": "6522",
                        "text": "Złotowski"
                     },
                     {
                        "MMepId": "5981",
                        "text": "van Dalen"
                     },
                     {
                        "MMepId": "6545",
                        "text": "Škripek"
                     },
                     {
                        "MMepId": "6540",
                        "text": "Žitňanská"
                     }
                  ]
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": {
                     "MMepId": "6402",
                     "text": "Mazuronis"
                  }
               },
               {
                  "MIdentifier": "GUE/NGL",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6654",
                        "text": "Albiol Guzmán"
                     },
                     {
                        "MMepId": "6280",
                        "text": "Anderson Martina"
                     },
                     {
                        "MMepId": "6595",
                        "text": "Boylan"
                     },
                     {
                        "MMepId": "6598",
                        "text": "Carthy"
                     },
                     {
                        "MMepId": "6701",
                        "text": "Couso Permuy"
                     },
                     {
                        "MMepId": "5886",
                        "text": "Ferreira João"
                     },
                     {
                        "MMepId": "6628",
                        "text": "Hazekamp"
                     },
                     {
                        "MMepId": "6653",
                        "text": "López Paloma"
                     },
                     {
                        "MMepId": "6643",
                        "text": "Mineur"
                     },
                     {
                        "MMepId": "6599",
                        "text": "Ní Riada"
                     },
                     {
                        "MMepId": "6655",
                        "text": "Senra Rodríguez"
                     },
                     {
                        "MMepId": "6656",
                        "text": "Vallina"
                     },
                     {
                        "MMepId": "6678",
                        "text": "Viegas"
                     },
                     {
                        "MMepId": "6253",
                        "text": "Zuber"
                     },
                     {
                        "MMepId": "5922",
                        "text": "de Jong"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6451",
                        "text": "Aliot"
                     },
                     {
                        "MMepId": "6593",
                        "text": "Annemans"
                     },
                     {
                        "MMepId": "6382",
                        "text": "Arnautu"
                     },
                     {
                        "MMepId": "6394",
                        "text": "Bay"
                     },
                     {
                        "MMepId": "6405",
                        "text": "Bilde"
                     },
                     {
                        "MMepId": "6179",
                        "text": "Bizzotto"
                     },
                     {
                        "MMepId": "5010",
                        "text": "Borghezio"
                     },
                     {
                        "MMepId": "6387",
                        "text": "Boutonnet"
                     },
                     {
                        "MMepId": "6391",
                        "text": "Briois"
                     },
                     {
                        "MMepId": "6507",
                        "text": "Buonanno"
                     },
                     {
                        "MMepId": "6384",
                        "text": "D'Ornano"
                     },
                     {
                        "MMepId": "6106",
                        "text": "Dodds Diane"
                     },
                     {
                        "MMepId": "6401",
                        "text": "Ferrand"
                     },
                     {
                        "MMepId": "6132",
                        "text": "Fontana"
                     },
                     {
                        "MMepId": "6668",
                        "text": "Fountoulis"
                     },
                     {
                        "MMepId": "6450",
                        "text": "Goddyn"
                     },
                     {
                        "MMepId": "1380",
                        "text": "Gollnisch"
                     },
                     {
                        "MMepId": "6404",
                        "text": "Jalkh"
                     },
                     {
                        "MMepId": "6635",
                        "text": "Jansen"
                     },
                     {
                        "MMepId": "6629",
                        "text": "Kappel"
                     },
                     {
                        "MMepId": "1529",
                        "text": "Le Pen Jean-Marie"
                     },
                     {
                        "MMepId": "5332",
                        "text": "Le Pen Marine"
                     },
                     {
                        "MMepId": "6372",
                        "text": "Lebreton"
                     },
                     {
                        "MMepId": "6699",
                        "text": "Loiseau"
                     },
                     {
                        "MMepId": "6633",
                        "text": "Maeijer"
                     },
                     {
                        "MMepId": "6385",
                        "text": "Martin Dominique"
                     },
                     {
                        "MMepId": "6627",
                        "text": "Mayer"
                     },
                     {
                        "MMepId": "6395",
                        "text": "Monot"
                     },
                     {
                        "MMepId": "6403",
                        "text": "Montel"
                     },
                     {
                        "MMepId": "6399",
                        "text": "Mélin"
                     },
                     {
                        "MMepId": "6123",
                        "text": "Obermayr"
                     },
                     {
                        "MMepId": "6676",
                        "text": "Papadakis Konstantinos"
                     },
                     {
                        "MMepId": "5522",
                        "text": "Salvini"
                     },
                     {
                        "MMepId": "6634",
                        "text": "Stuger"
                     },
                     {
                        "MMepId": "6670",
                        "text": "Synadinos"
                     },
                     {
                        "MMepId": "6392",
                        "text": "Troszczynski"
                     },
                     {
                        "MMepId": "6610",
                        "text": "Vilimsky"
                     },
                     {
                        "MMepId": "6469",
                        "text": "Voigt"
                     },
                     {
                        "MMepId": "6673",
                        "text": "Zarianopoulos"
                     },
                     {
                        "MMepId": "6630",
                        "text": "de Graaff"
                     }
                  ]
               },
               {
                  "MIdentifier": "PPE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6602",
                        "text": "Adaktusson"
                     },
                     {
                        "MMepId": "1340",
                        "text": "Alliot-Marie"
                     },
                     {
                        "MMepId": "1178",
                        "text": "Arias Cañete"
                     },
                     {
                        "MMepId": "6594",
                        "text": "Arimont"
                     },
                     {
                        "MMepId": "4742",
                        "text": "Ayuso"
                     },
                     {
                        "MMepId": "6191",
                        "text": "Bach"
                     },
                     {
                        "MMepId": "6135",
                        "text": "Balz"
                     },
                     {
                        "MMepId": "6216",
                        "text": "Becker"
                     },
                     {
                        "MMepId": "5377",
                        "text": "Belet"
                     },
                     {
                        "MMepId": "5885",
                        "text": "Bendtsen"
                     },
                     {
                        "MMepId": "6343",
                        "text": "Bocskor"
                     },
                     {
                        "MMepId": "6613",
                        "text": "Bogovič"
                     },
                     {
                        "MMepId": "6535",
                        "text": "Boni"
                     },
                     {
                        "MMepId": "6617",
                        "text": "Buda"
                     },
                     {
                        "MMepId": "5388",
                        "text": "Buzek"
                     },
                     {
                        "MMepId": "1507",
                        "text": "Böge"
                     },
                     {
                        "MMepId": "6021",
                        "text": "Cadec"
                     },
                     {
                        "MMepId": "5252",
                        "text": "Casa"
                     },
                     {
                        "MMepId": "5341",
                        "text": "Caspary"
                     },
                     {
                        "MMepId": "5554",
                        "text": "Cesa"
                     },
                     {
                        "MMepId": "6492",
                        "text": "Cicu"
                     },
                     {
                        "MMepId": "6406",
                        "text": "Cirio"
                     },
                     {
                        "MMepId": "6600",
                        "text": "Clune"
                     },
                     {
                        "MMepId": "3800",
                        "text": "Coelho"
                     },
                     {
                        "MMepId": "6273",
                        "text": "Collin-Langen"
                     },
                     {
                        "MMepId": "5948",
                        "text": "Comi"
                     },
                     {
                        "MMepId": "6587",
                        "text": "Comodini Cachia"
                     },
                     {
                        "MMepId": "6548",
                        "text": "Csáky"
                     },
                     {
                        "MMepId": "5921",
                        "text": "Danjean"
                     },
                     {
                        "MMepId": "6194",
                        "text": "Dantin"
                     },
                     {
                        "MMepId": "6172",
                        "text": "Dati"
                     },
                     {
                        "MMepId": "6364",
                        "text": "Delahaye"
                     },
                     {
                        "MMepId": "6345",
                        "text": "Deli"
                     },
                     {
                        "MMepId": "5998",
                        "text": "Deutsch"
                     },
                     {
                        "MMepId": "5350",
                        "text": "Deß"
                     },
                     {
                        "MMepId": "5601",
                        "text": "Dombrovskis"
                     },
                     {
                        "MMepId": "6481",
                        "text": "Donchev"
                     },
                     {
                        "MMepId": "5960",
                        "text": "Dorfmann"
                     },
                     {
                        "MMepId": "5515",
                        "text": "Díaz de Mera García Consuegra"
                     },
                     {
                        "MMepId": "5348",
                        "text": "Ehler"
                     },
                     {
                        "MMepId": "6047",
                        "text": "Engel"
                     },
                     {
                        "MMepId": "6342",
                        "text": "Erdős"
                     },
                     {
                        "MMepId": "5983",
                        "text": "Estaràs Ferragut"
                     },
                     {
                        "MMepId": "3833",
                        "text": "Ferber"
                     },
                     {
                        "MMepId": "6068",
                        "text": "Fernandes"
                     },
                     {
                        "MMepId": "5903",
                        "text": "Fisas Ayxelà"
                     },
                     {
                        "MMepId": "4880",
                        "text": "Fitto"
                     },
                     {
                        "MMepId": "6020",
                        "text": "Gabriel"
                     },
                     {
                        "MMepId": "4659",
                        "text": "Gahler"
                     },
                     {
                        "MMepId": "6615",
                        "text": "Gambús"
                     },
                     {
                        "MMepId": "5803",
                        "text": "Gardini"
                     },
                     {
                        "MMepId": "6441",
                        "text": "Gieseke"
                     },
                     {
                        "MMepId": "6632",
                        "text": "González Pons"
                     },
                     {
                        "MMepId": "4032",
                        "text": "Grossetête"
                     },
                     {
                        "MMepId": "5109",
                        "text": "Grzyb"
                     },
                     {
                        "MMepId": "5342",
                        "text": "Gräßle"
                     },
                     {
                        "MMepId": "5138",
                        "text": "Gyürk"
                     },
                     {
                        "MMepId": "5280",
                        "text": "Gál"
                     },
                     {
                        "MMepId": "6202",
                        "text": "Gáll-Pelcz"
                     },
                     {
                        "MMepId": "6596",
                        "text": "Hayes"
                     },
                     {
                        "MMepId": "5715",
                        "text": "Hellvig"
                     },
                     {
                        "MMepId": "5018",
                        "text": "Herranz García"
                     },
                     {
                        "MMepId": "6534",
                        "text": "Hetman"
                     },
                     {
                        "MMepId": "5953",
                        "text": "Hohlmeier"
                     },
                     {
                        "MMepId": "4987",
                        "text": "Hortefeux"
                     },
                     {
                        "MMepId": "5254",
                        "text": "Hökmark"
                     },
                     {
                        "MMepId": "6346",
                        "text": "Hölvényi"
                     },
                     {
                        "MMepId": "5952",
                        "text": "Hübner"
                     },
                     {
                        "MMepId": "5945",
                        "text": "Jahr"
                     },
                     {
                        "MMepId": "5954",
                        "text": "Jazłowiecka"
                     },
                     {
                        "MMepId": "5978",
                        "text": "Jiménez-Becerril Barrio"
                     },
                     {
                        "MMepId": "6360",
                        "text": "Joulaud"
                     },
                     {
                        "MMepId": "6055",
                        "text": "Juvin"
                     },
                     {
                        "MMepId": "5944",
                        "text": "Kalinowski"
                     },
                     {
                        "MMepId": "6095",
                        "text": "Kalniete"
                     },
                     {
                        "MMepId": "4669",
                        "text": "Karas"
                     },
                     {
                        "MMepId": "6070",
                        "text": "Kariņš"
                     },
                     {
                        "MMepId": "6666",
                        "text": "Kefalogiannis"
                     },
                     {
                        "MMepId": "5542",
                        "text": "Kelam"
                     },
                     {
                        "MMepId": "5855",
                        "text": "Kelly"
                     },
                     {
                        "MMepId": "3759",
                        "text": "Koch"
                     },
                     {
                        "MMepId": "6196",
                        "text": "Kovatchev"
                     },
                     {
                        "MMepId": "6527",
                        "text": "Kozłowska-Rajewicz"
                     },
                     {
                        "MMepId": "5403",
                        "text": "Kudrycka"
                     },
                     {
                        "MMepId": "5840",
                        "text": "Kukan"
                     },
                     {
                        "MMepId": "6662",
                        "text": "Kyrtsos"
                     },
                     {
                        "MMepId": "6001",
                        "text": "Kósa"
                     },
                     {
                        "MMepId": "6053",
                        "text": "Köstinger"
                     },
                     {
                        "MMepId": "6393",
                        "text": "Landsbergis"
                     },
                     {
                        "MMepId": "3856",
                        "text": "Langen"
                     },
                     {
                        "MMepId": "6363",
                        "text": "Lavrilleux"
                     },
                     {
                        "MMepId": "6200",
                        "text": "Le Grip"
                     },
                     {
                        "MMepId": "6592",
                        "text": "Lenaers"
                     },
                     {
                        "MMepId": "5105",
                        "text": "Lewandowski"
                     },
                     {
                        "MMepId": "3855",
                        "text": "Liese"
                     },
                     {
                        "MMepId": "6442",
                        "text": "Lins"
                     },
                     {
                        "MMepId": "5938",
                        "text": "Lope Fontagné"
                     },
                     {
                        "MMepId": "5996",
                        "text": "Macovei"
                     },
                     {
                        "MMepId": "6303",
                        "text": "Maletić"
                     },
                     {
                        "MMepId": "6245",
                        "text": "Malinov"
                     },
                     {
                        "MMepId": "3845",
                        "text": "Mann"
                     },
                     {
                        "MMepId": "5646",
                        "text": "Marinescu"
                     },
                     {
                        "MMepId": "6465",
                        "text": "Martusciello"
                     },
                     {
                        "MMepId": "6096",
                        "text": "Mato"
                     },
                     {
                        "MMepId": "6440",
                        "text": "McAllister"
                     },
                     {
                        "MMepId": "5245",
                        "text": "McGuinness"
                     },
                     {
                        "MMepId": "6121",
                        "text": "Melo"
                     },
                     {
                        "MMepId": "6297",
                        "text": "Metsola"
                     },
                     {
                        "MMepId": "5308",
                        "text": "Mikolášik"
                     },
                     {
                        "MMepId": "5518",
                        "text": "Millán Mon"
                     },
                     {
                        "MMepId": "6366",
                        "text": "Monteiro de Aguiar"
                     },
                     {
                        "MMepId": "6368",
                        "text": "Morano"
                     },
                     {
                        "MMepId": "5732",
                        "text": "Morin-Chartier"
                     },
                     {
                        "MMepId": "6436",
                        "text": "Mureşan"
                     },
                     {
                        "MMepId": "6361",
                        "text": "Muselier"
                     },
                     {
                        "MMepId": "5547",
                        "text": "Mussolini"
                     },
                     {
                        "MMepId": "5774",
                        "text": "Mănescu"
                     },
                     {
                        "MMepId": "6544",
                        "text": "Nagy"
                     },
                     {
                        "MMepId": "4712",
                        "text": "Niebler"
                     },
                     {
                        "MMepId": "6332",
                        "text": "Niedermayer"
                     },
                     {
                        "MMepId": "5407",
                        "text": "Olbrycht"
                     },
                     {
                        "MMepId": "6377",
                        "text": "Pabriks"
                     },
                     {
                        "MMepId": "5702",
                        "text": "Patriciello"
                     },
                     {
                        "MMepId": "5033",
                        "text": "Peterle"
                     },
                     {
                        "MMepId": "6383",
                        "text": "Petir"
                     },
                     {
                        "MMepId": "5346",
                        "text": "Pieper"
                     },
                     {
                        "MMepId": "5788",
                        "text": "Pietikäinen"
                     },
                     {
                        "MMepId": "6537",
                        "text": "Pitera"
                     },
                     {
                        "MMepId": "6268",
                        "text": "Plenković"
                     },
                     {
                        "MMepId": "6519",
                        "text": "Plura"
                     },
                     {
                        "MMepId": "6491",
                        "text": "Pogliese"
                     },
                     {
                        "MMepId": "6335",
                        "text": "Polčák"
                     },
                     {
                        "MMepId": "6092",
                        "text": "Ponga"
                     },
                     {
                        "MMepId": "6700",
                        "text": "Pospíšil"
                     },
                     {
                        "MMepId": "6010",
                        "text": "Preda"
                     },
                     {
                        "MMepId": "6224",
                        "text": "Proust"
                     },
                     {
                        "MMepId": "1487",
                        "text": "Quisthoudt-Rowohl"
                     },
                     {
                        "MMepId": "6488",
                        "text": "Radev"
                     },
                     {
                        "MMepId": "6072",
                        "text": "Rangel"
                     },
                     {
                        "MMepId": "5347",
                        "text": "Reul"
                     },
                     {
                        "MMepId": "6365",
                        "text": "Ribeiro"
                     },
                     {
                        "MMepId": "6684",
                        "text": "Rolin"
                     },
                     {
                        "MMepId": "5512",
                        "text": "Rosati"
                     },
                     {
                        "MMepId": "4596",
                        "text": "Rübig"
                     },
                     {
                        "MMepId": "6698",
                        "text": "Salini"
                     },
                     {
                        "MMepId": "6370",
                        "text": "Sander"
                     },
                     {
                        "MMepId": "6259",
                        "text": "Sarvamaa"
                     },
                     {
                        "MMepId": "4768",
                        "text": "Saïfi"
                     },
                     {
                        "MMepId": "6619",
                        "text": "Schmidt"
                     },
                     {
                        "MMepId": "6636",
                        "text": "Schreijer-Pierik"
                     },
                     {
                        "MMepId": "6443",
                        "text": "Schulze"
                     },
                     {
                        "MMepId": "5345",
                        "text": "Schwab"
                     },
                     {
                        "MMepId": "5265",
                        "text": "Schöpflin"
                     },
                     {
                        "MMepId": "6428",
                        "text": "Sernagiotto"
                     },
                     {
                        "MMepId": "5111",
                        "text": "Siekierski"
                     },
                     {
                        "MMepId": "4705",
                        "text": "Sommer"
                     },
                     {
                        "MMepId": "6663",
                        "text": "Spyraki"
                     },
                     {
                        "MMepId": "5778",
                        "text": "Stolojan"
                     },
                     {
                        "MMepId": "6320",
                        "text": "Stylianides"
                     },
                     {
                        "MMepId": "6672",
                        "text": "Svoboda"
                     },
                     {
                        "MMepId": "6528",
                        "text": "Szejnfeld"
                     },
                     {
                        "MMepId": "5143",
                        "text": "Szájer"
                     },
                     {
                        "MMepId": "5781",
                        "text": "Sógor"
                     },
                     {
                        "MMepId": "5207",
                        "text": "Theocharous"
                     },
                     {
                        "MMepId": "5949",
                        "text": "Thun und Hohenstein"
                     },
                     {
                        "MMepId": "3738",
                        "text": "Thyssen"
                     },
                     {
                        "MMepId": "6681",
                        "text": "Tomc"
                     },
                     {
                        "MMepId": "6398",
                        "text": "Toti"
                     },
                     {
                        "MMepId": "5783",
                        "text": "Tőkés"
                     },
                     {
                        "MMepId": "6007",
                        "text": "Ungureanu"
                     },
                     {
                        "MMepId": "5734",
                        "text": "Urutchev"
                     },
                     {
                        "MMepId": "6638",
                        "text": "Valcárcel"
                     },
                     {
                        "MMepId": "5930",
                        "text": "Verheyen"
                     },
                     {
                        "MMepId": "6356",
                        "text": "Virkkunen"
                     },
                     {
                        "MMepId": "5934",
                        "text": "Voss"
                     },
                     {
                        "MMepId": "6664",
                        "text": "Vozemberg"
                     },
                     {
                        "MMepId": "5714",
                        "text": "Vălean"
                     },
                     {
                        "MMepId": "5947",
                        "text": "Wałęsa"
                     },
                     {
                        "MMepId": "5770",
                        "text": "Weber Renate"
                     },
                     {
                        "MMepId": "6542",
                        "text": "Wenta"
                     },
                     {
                        "MMepId": "4643",
                        "text": "Wieland"
                     },
                     {
                        "MMepId": "5937",
                        "text": "Winkler Hermann"
                     },
                     {
                        "MMepId": "5782",
                        "text": "Winkler Iuliu"
                     },
                     {
                        "MMepId": "6665",
                        "text": "Zagorakis"
                     },
                     {
                        "MMepId": "5936",
                        "text": "Zalba Bidegain"
                     },
                     {
                        "MMepId": "6344",
                        "text": "Zdechovský"
                     },
                     {
                        "MMepId": "6532",
                        "text": "Zdrojewski"
                     },
                     {
                        "MMepId": "5935",
                        "text": "Zeller"
                     },
                     {
                        "MMepId": "6094",
                        "text": "Zver"
                     },
                     {
                        "MMepId": "5420",
                        "text": "Zwiefka"
                     },
                     {
                        "MMepId": "5189",
                        "text": "Záborská"
                     },
                     {
                        "MMepId": "5511",
                        "text": "de Grandes Pascual"
                     },
                     {
                        "MMepId": "5727",
                        "text": "de Lange"
                     },
                     {
                        "MMepId": "5508",
                        "text": "del Castillo Vera"
                     },
                     {
                        "MMepId": "5295",
                        "text": "van Nistelrooij"
                     },
                     {
                        "MMepId": "5928",
                        "text": "van de Camp"
                     },
                     {
                        "MMepId": "5964",
                        "text": "Łukacijewska"
                     },
                     {
                        "MMepId": "6341",
                        "text": "Šojdrová"
                     },
                     {
                        "MMepId": "6547",
                        "text": "Štefanec"
                     },
                     {
                        "MMepId": "6301",
                        "text": "Šuica"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6651",
                        "text": "Aguilera García"
                     },
                     {
                        "MMepId": "6567",
                        "text": "Anderson Lucy"
                     },
                     {
                        "MMepId": "6279",
                        "text": "Andrieu"
                     },
                     {
                        "MMepId": "6687",
                        "text": "Androulakis"
                     },
                     {
                        "MMepId": "6553",
                        "text": "Arena"
                     },
                     {
                        "MMepId": "5426",
                        "text": "Assis"
                     },
                     {
                        "MMepId": "5411",
                        "text": "Ayala Sender"
                     },
                     {
                        "MMepId": "6378",
                        "text": "Balas"
                     },
                     {
                        "MMepId": "5866",
                        "text": "Balčytis"
                     },
                     {
                        "MMepId": "6611",
                        "text": "Bayet"
                     },
                     {
                        "MMepId": "6505",
                        "text": "Benifei"
                     },
                     {
                        "MMepId": "3972",
                        "text": "Berès"
                     },
                     {
                        "MMepId": "6456",
                        "text": "Bettini"
                     },
                     {
                        "MMepId": "6650",
                        "text": "Blanco López"
                     },
                     {
                        "MMepId": "5867",
                        "text": "Blinkevičiūtė"
                     },
                     {
                        "MMepId": "6449",
                        "text": "Bonafè"
                     },
                     {
                        "MMepId": "6264",
                        "text": "Borzan"
                     },
                     {
                        "MMepId": "5768",
                        "text": "Boştinaru"
                     },
                     {
                        "MMepId": "6572",
                        "text": "Brannen"
                     },
                     {
                        "MMepId": "5465",
                        "text": "Bresso"
                     },
                     {
                        "MMepId": "6421",
                        "text": "Briano"
                     },
                     {
                        "MMepId": "6647",
                        "text": "Cabezón Ruiz"
                     },
                     {
                        "MMepId": "6489",
                        "text": "Caputo"
                     },
                     {
                        "MMepId": "5835",
                        "text": "Childers"
                     },
                     {
                        "MMepId": "6499",
                        "text": "Chinnici"
                     },
                     {
                        "MMepId": "5284",
                        "text": "Christensen"
                     },
                     {
                        "MMepId": "6083",
                        "text": "Cofferati"
                     },
                     {
                        "MMepId": "4628",
                        "text": "Corbett"
                     },
                     {
                        "MMepId": "6085",
                        "text": "Costa"
                     },
                     {
                        "MMepId": "6051",
                        "text": "Cozzolino"
                     },
                     {
                        "MMepId": "5661",
                        "text": "Creţu"
                     },
                     {
                        "MMepId": "6616",
                        "text": "Cristea"
                     },
                     {
                        "MMepId": "6589",
                        "text": "Dalli"
                     },
                     {
                        "MMepId": "6570",
                        "text": "Dance"
                     },
                     {
                        "MMepId": "6458",
                        "text": "Danti"
                     },
                     {
                        "MMepId": "6062",
                        "text": "De Castro"
                     },
                     {
                        "MMepId": "6437",
                        "text": "De Monte"
                     },
                     {
                        "MMepId": "6410",
                        "text": "Delvaux-Stehres"
                     },
                     {
                        "MMepId": "5816",
                        "text": "Denanot"
                     },
                     {
                        "MMepId": "6588",
                        "text": "Dodds Anneliese"
                     },
                     {
                        "MMepId": "6420",
                        "text": "Drăghici"
                     },
                     {
                        "MMepId": "5829",
                        "text": "Dăncilă"
                     },
                     {
                        "MMepId": "6014",
                        "text": "Ertug"
                     },
                     {
                        "MMepId": "6080",
                        "text": "Fajon"
                     },
                     {
                        "MMepId": "6652",
                        "text": "Fernández"
                     },
                     {
                        "MMepId": "5427",
                        "text": "Ferreira Elisa"
                     },
                     {
                        "MMepId": "5173",
                        "text": "Flašíková Beňová"
                     },
                     {
                        "MMepId": "6012",
                        "text": "Fleckenstein"
                     },
                     {
                        "MMepId": "6620",
                        "text": "Freund"
                     },
                     {
                        "MMepId": "6424",
                        "text": "Frunzulică"
                     },
                     {
                        "MMepId": "5417",
                        "text": "García Pérez"
                     },
                     {
                        "MMepId": "6130",
                        "text": "Gardiazabal Rubial"
                     },
                     {
                        "MMepId": "6454",
                        "text": "Gasbarra"
                     },
                     {
                        "MMepId": "3827",
                        "text": "Gebhardt"
                     },
                     {
                        "MMepId": "6005",
                        "text": "Geier"
                     },
                     {
                        "MMepId": "6485",
                        "text": "Gentile"
                     },
                     {
                        "MMepId": "5495",
                        "text": "Geringer de Oedenberg"
                     },
                     {
                        "MMepId": "5497",
                        "text": "Gierek"
                     },
                     {
                        "MMepId": "4937",
                        "text": "Gill Neena"
                     },
                     {
                        "MMepId": "6502",
                        "text": "Giuffrida"
                     },
                     {
                        "MMepId": "5425",
                        "text": "Gomes"
                     },
                     {
                        "MMepId": "6688",
                        "text": "Grammatikakis"
                     },
                     {
                        "MMepId": "6419",
                        "text": "Grapini"
                     },
                     {
                        "MMepId": "6580",
                        "text": "Griffin"
                     },
                     {
                        "MMepId": "5692",
                        "text": "Groote"
                     },
                     {
                        "MMepId": "6063",
                        "text": "Gualtieri"
                     },
                     {
                        "MMepId": "5941",
                        "text": "Guerrero Salom"
                     },
                     {
                        "MMepId": "6107",
                        "text": "Guillaume"
                     },
                     {
                        "MMepId": "6603",
                        "text": "Guteland"
                     },
                     {
                        "MMepId": "6212",
                        "text": "Gutiérrez Prieto"
                     },
                     {
                        "MMepId": "5261",
                        "text": "Hedh"
                     },
                     {
                        "MMepId": "6479",
                        "text": "Hoffmann"
                     },
                     {
                        "MMepId": "4994",
                        "text": "Honeyball"
                     },
                     {
                        "MMepId": "4103",
                        "text": "Howitt"
                     },
                     {
                        "MMepId": "6029",
                        "text": "Ivan"
                     },
                     {
                        "MMepId": "5870",
                        "text": "Jaakonsaari"
                     },
                     {
                        "MMepId": "6624",
                        "text": "Jongerius"
                     },
                     {
                        "MMepId": "6161",
                        "text": "Jáuregui Atondo"
                     },
                     {
                        "MMepId": "6146",
                        "text": "Kadenbach"
                     },
                     {
                        "MMepId": "6686",
                        "text": "Kaili"
                     },
                     {
                        "MMepId": "6009",
                        "text": "Kammerevert"
                     },
                     {
                        "MMepId": "3756",
                        "text": "Kaufmann"
                     },
                     {
                        "MMepId": "6581",
                        "text": "Khan"
                     },
                     {
                        "MMepId": "6571",
                        "text": "Kirton-Darling"
                     },
                     {
                        "MMepId": "6508",
                        "text": "Kofod"
                     },
                     {
                        "MMepId": "3761",
                        "text": "Krehl"
                     },
                     {
                        "MMepId": "6367",
                        "text": "Kumpula-Natri"
                     },
                     {
                        "MMepId": "6435",
                        "text": "Kyenge"
                     },
                     {
                        "MMepId": "6689",
                        "text": "Kyrkos"
                     },
                     {
                        "MMepId": "3823",
                        "text": "Lange"
                     },
                     {
                        "MMepId": "6329",
                        "text": "Lauristin"
                     },
                     {
                        "MMepId": "5371",
                        "text": "Leichtfried"
                     },
                     {
                        "MMepId": "4685",
                        "text": "Leinen"
                     },
                     {
                        "MMepId": "5094",
                        "text": "Liberadzki"
                     },
                     {
                        "MMepId": "6477",
                        "text": "Lietz"
                     },
                     {
                        "MMepId": "5860",
                        "text": "Ludvigsson"
                     },
                     {
                        "MMepId": "6648",
                        "text": "López Javi"
                     },
                     {
                        "MMepId": "5984",
                        "text": "López Aguilar"
                     },
                     {
                        "MMepId": "6380",
                        "text": "Mamikins"
                     },
                     {
                        "MMepId": "6355",
                        "text": "Manscour"
                     },
                     {
                        "MMepId": "1133",
                        "text": "Martin David"
                     },
                     {
                        "MMepId": "6565",
                        "text": "Martin Edouard"
                     },
                     {
                        "MMepId": "5314",
                        "text": "Maňka"
                     },
                     {
                        "MMepId": "4646",
                        "text": "McAvan"
                     },
                     {
                        "MMepId": "6296",
                        "text": "Mizzi"
                     },
                     {
                        "MMepId": "6423",
                        "text": "Moisă"
                     },
                     {
                        "MMepId": "6351",
                        "text": "Molnár"
                     },
                     {
                        "MMepId": "6562",
                        "text": "Moody"
                     },
                     {
                        "MMepId": "4923",
                        "text": "Moraes"
                     },
                     {
                        "MMepId": "6433",
                        "text": "Moretti"
                     },
                     {
                        "MMepId": "6422",
                        "text": "Morgano"
                     },
                     {
                        "MMepId": "6506",
                        "text": "Mosca"
                     },
                     {
                        "MMepId": "6691",
                        "text": "Negrescu"
                     },
                     {
                        "MMepId": "6618",
                        "text": "Nekov"
                     },
                     {
                        "MMepId": "6016",
                        "text": "Neuser"
                     },
                     {
                        "MMepId": "6418",
                        "text": "Nica"
                     },
                     {
                        "MMepId": "6352",
                        "text": "Niedermüller"
                     },
                     {
                        "MMepId": "6242",
                        "text": "Nilsson"
                     },
                     {
                        "MMepId": "6473",
                        "text": "Noichl"
                     },
                     {
                        "MMepId": "5483",
                        "text": "Panzeri"
                     },
                     {
                        "MMepId": "6487",
                        "text": "Paolucci"
                     },
                     {
                        "MMepId": "6323",
                        "text": "Papadakis Demetris"
                     },
                     {
                        "MMepId": "6103",
                        "text": "Pargneaux"
                     },
                     {
                        "MMepId": "5648",
                        "text": "Paşcu"
                     },
                     {
                        "MMepId": "5296",
                        "text": "Peillon"
                     },
                     {
                        "MMepId": "6484",
                        "text": "Picierno"
                     },
                     {
                        "MMepId": "6261",
                        "text": "Picula"
                     },
                     {
                        "MMepId": "6625",
                        "text": "Piri"
                     },
                     {
                        "MMepId": "6493",
                        "text": "Pirinski"
                     },
                     {
                        "MMepId": "5894",
                        "text": "Poc"
                     },
                     {
                        "MMepId": "6330",
                        "text": "Poche"
                     },
                     {
                        "MMepId": "6526",
                        "text": "Preuß"
                     },
                     {
                        "MMepId": "6136",
                        "text": "Regner"
                     },
                     {
                        "MMepId": "6316",
                        "text": "Revault D'Allonnes Bonnefoy"
                     },
                     {
                        "MMepId": "6677",
                        "text": "Rodrigues Liliana"
                     },
                     {
                        "MMepId": "6649",
                        "text": "Rodríguez-Piñero Fernández"
                     },
                     {
                        "MMepId": "5813",
                        "text": "Rodust"
                     },
                     {
                        "MMepId": "6415",
                        "text": "Sant"
                     },
                     {
                        "MMepId": "5713",
                        "text": "Schaldemose"
                     },
                     {
                        "MMepId": "6438",
                        "text": "Schlein"
                     },
                     {
                        "MMepId": "6475",
                        "text": "Schuster"
                     },
                     {
                        "MMepId": "5897",
                        "text": "Sehnalová"
                     },
                     {
                        "MMepId": "6375",
                        "text": "Serrão Santos"
                     },
                     {
                        "MMepId": "6381",
                        "text": "Silva Pereira"
                     },
                     {
                        "MMepId": "6008",
                        "text": "Simon Peter"
                     },
                     {
                        "MMepId": "6566",
                        "text": "Simon Siôn"
                     },
                     {
                        "MMepId": "6093",
                        "text": "Sippel"
                     },
                     {
                        "MMepId": "5844",
                        "text": "Smolková"
                     },
                     {
                        "MMepId": "6498",
                        "text": "Soru"
                     },
                     {
                        "MMepId": "6003",
                        "text": "Steinruck"
                     },
                     {
                        "MMepId": "4949",
                        "text": "Stihler"
                     },
                     {
                        "MMepId": "6334",
                        "text": "Szanyi"
                     },
                     {
                        "MMepId": "5653",
                        "text": "Sârbu"
                     },
                     {
                        "MMepId": "6623",
                        "text": "Tang"
                     },
                     {
                        "MMepId": "6427",
                        "text": "Tapardel"
                     },
                     {
                        "MMepId": "5624",
                        "text": "Tarabella"
                     },
                     {
                        "MMepId": "6282",
                        "text": "Thomas"
                     },
                     {
                        "MMepId": "5459",
                        "text": "Toia"
                     },
                     {
                        "MMepId": "6336",
                        "text": "Ujhelyi"
                     },
                     {
                        "MMepId": "4990",
                        "text": "Van Brempt"
                     },
                     {
                        "MMepId": "6086",
                        "text": "Vaughan"
                     },
                     {
                        "MMepId": "6425",
                        "text": "Viotti"
                     },
                     {
                        "MMepId": "6582",
                        "text": "Ward"
                     },
                     {
                        "MMepId": "6235",
                        "text": "Weidenholzer"
                     },
                     {
                        "MMepId": "6463",
                        "text": "Werner"
                     },
                     {
                        "MMepId": "6011",
                        "text": "Westphal"
                     },
                     {
                        "MMepId": "5698",
                        "text": "Willmott"
                     },
                     {
                        "MMepId": "5845",
                        "text": "Zala"
                     },
                     {
                        "MMepId": "6434",
                        "text": "Zanonato"
                     },
                     {
                        "MMepId": "5957",
                        "text": "Zemke"
                     },
                     {
                        "MMepId": "6373",
                        "text": "Zorrinho"
                     },
                     {
                        "MMepId": "6478",
                        "text": "von Weizsäcker"
                     }
                  ]
               }
            ]
         },
         "ResultAbstention": {
            "MNumber": "40",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": {
                     "MMepId": "6466",
                     "text": "Trebesius"
                  }
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5567",
                        "text": "Batten"
                     },
                     {
                        "MMepId": "5875",
                        "text": "Paksas"
                     }
                  ]
               },
               {
                  "MIdentifier": "GUE/NGL",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6604",
                        "text": "Björk"
                     },
                     {
                        "MMepId": "6024",
                        "text": "Ernst"
                     },
                     {
                        "MMepId": "6597",
                        "text": "Flanagan"
                     },
                     {
                        "MMepId": "6693",
                        "text": "Forenza"
                     },
                     {
                        "MMepId": "6076",
                        "text": "Hadjigeorgiou"
                     },
                     {
                        "MMepId": "6637",
                        "text": "Iglesias"
                     },
                     {
                        "MMepId": "6640",
                        "text": "Jiménez Villarejo"
                     },
                     {
                        "MMepId": "6657",
                        "text": "Juaristi Abaunz"
                     },
                     {
                        "MMepId": "6313",
                        "text": "Kari"
                     },
                     {
                        "MMepId": "5039",
                        "text": "Konečná"
                     },
                     {
                        "MMepId": "6675",
                        "text": "Kuneva"
                     },
                     {
                        "MMepId": "6369",
                        "text": "Kyllönen"
                     },
                     {
                        "MMepId": "5909",
                        "text": "Le Hyaric"
                     },
                     {
                        "MMepId": "6692",
                        "text": "Maltese"
                     },
                     {
                        "MMepId": "5992",
                        "text": "Matias"
                     },
                     {
                        "MMepId": "5044",
                        "text": "Maštálka"
                     },
                     {
                        "MMepId": "6310",
                        "text": "Michels"
                     },
                     {
                        "MMepId": "5916",
                        "text": "Mélenchon"
                     },
                     {
                        "MMepId": "6250",
                        "text": "Omarjee"
                     },
                     {
                        "MMepId": "5596",
                        "text": "Papadimoulis"
                     },
                     {
                        "MMepId": "5046",
                        "text": "Ransdorf"
                     },
                     {
                        "MMepId": "5837",
                        "text": "Scholz"
                     },
                     {
                        "MMepId": "6321",
                        "text": "Sylikiotis"
                     },
                     {
                        "MMepId": "6641",
                        "text": "Sánchez Caldentey"
                     },
                     {
                        "MMepId": "6030",
                        "text": "Vergiat"
                     },
                     {
                        "MMepId": "5368",
                        "text": "Zimmer"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6669",
                        "text": "Epitideios"
                     },
                     {
                        "MMepId": "6524",
                        "text": "Iwaszkiewicz"
                     },
                     {
                        "MMepId": "6517",
                        "text": "Korwin-Mikke"
                     },
                     {
                        "MMepId": "6533",
                        "text": "Marusik"
                     },
                     {
                        "MMepId": "6541",
                        "text": "Żółtek"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6354",
                        "text": "Maurel"
                     },
                     {
                        "MMepId": "6521",
                        "text": "Łybacka"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6606",
                        "text": "Andersson"
                     },
                     {
                        "MMepId": "6605",
                        "text": "Ceballos"
                     },
                     {
                        "MMepId": "6554",
                        "text": "Eriksson"
                     },
                     {
                        "MMepId": "5862",
                        "text": "Lövin"
                     }
                  ]
               }
            ]
         }
      },
      {
         "MDate": "2014-07-16 12:24:53",
         "MIdentifier": "47689",
         "RollCallVoteDescriptionText": "A8-0001/2014 -  Werner Langen - Résolution législative",
         "ResultFor": {
            "MNumber": "545",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ALDE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5682",
                        "text": "Ali"
                     },
                     {
                        "MMepId": "6327",
                        "text": "Ansip"
                     },
                     {
                        "MMepId": "6407",
                        "text": "Arthuis"
                     },
                     {
                        "MMepId": "6400",
                        "text": "Auštrevičius"
                     },
                     {
                        "MMepId": "6110",
                        "text": "Bearder"
                     },
                     {
                        "MMepId": "6646",
                        "text": "Becerra Basterrechea"
                     },
                     {
                        "MMepId": "6089",
                        "text": "Bilbao Barandica"
                     },
                     {
                        "MMepId": "5328",
                        "text": "Cavada"
                     },
                     {
                        "MMepId": "6339",
                        "text": "Charanzová"
                     },
                     {
                        "MMepId": "6226",
                        "text": "De Backer"
                     },
                     {
                        "MMepId": "1060",
                        "text": "Deprez"
                     },
                     {
                        "MMepId": "6439",
                        "text": "Diaconu"
                     },
                     {
                        "MMepId": "6340",
                        "text": "Dlabajová"
                     },
                     {
                        "MMepId": "6679",
                        "text": "Faria"
                     },
                     {
                        "MMepId": "6601",
                        "text": "Federley"
                     },
                     {
                        "MMepId": "6098",
                        "text": "Gerbrandy"
                     },
                     {
                        "MMepId": "6658",
                        "text": "Girauta Vidal"
                     },
                     {
                        "MMepId": "1793",
                        "text": "Goerens"
                     },
                     {
                        "MMepId": "6169",
                        "text": "Goulard"
                     },
                     {
                        "MMepId": "5330",
                        "text": "Griesbeck"
                     },
                     {
                        "MMepId": "6397",
                        "text": "Guoga"
                     },
                     {
                        "MMepId": "5246",
                        "text": "Harkin"
                     },
                     {
                        "MMepId": "6622",
                        "text": "Huitema"
                     },
                     {
                        "MMepId": "5681",
                        "text": "Hyusmenova"
                     },
                     {
                        "MMepId": "6388",
                        "text": "Jakovčić"
                     },
                     {
                        "MMepId": "6338",
                        "text": "Ježek"
                     },
                     {
                        "MMepId": "5433",
                        "text": "Jäätteenmäki"
                     },
                     {
                        "MMepId": "6328",
                        "text": "Kallas"
                     },
                     {
                        "MMepId": "6504",
                        "text": "Kyuchyuk"
                     },
                     {
                        "MMepId": "5362",
                        "text": "Lambsdorff"
                     },
                     {
                        "MMepId": "6376",
                        "text": "Marinho e Pinto"
                     },
                     {
                        "MMepId": "6645",
                        "text": "Maura Barandiarán"
                     },
                     {
                        "MMepId": "6041",
                        "text": "Meissner"
                     },
                     {
                        "MMepId": "5857",
                        "text": "Michel"
                     },
                     {
                        "MMepId": "6690",
                        "text": "Mihaylova"
                     },
                     {
                        "MMepId": "6631",
                        "text": "Mlinar"
                     },
                     {
                        "MMepId": "6500",
                        "text": "Müller"
                     },
                     {
                        "MMepId": "6614",
                        "text": "Nart"
                     },
                     {
                        "MMepId": "3899",
                        "text": "Neyts-Uyttebroeck"
                     },
                     {
                        "MMepId": "6644",
                        "text": "Pagazaurtundúa Ruiz"
                     },
                     {
                        "MMepId": "4693",
                        "text": "Paulsen"
                     },
                     {
                        "MMepId": "6510",
                        "text": "Petersen"
                     },
                     {
                        "MMepId": "6266",
                        "text": "Radoš"
                     },
                     {
                        "MMepId": "4544",
                        "text": "Rehn"
                     },
                     {
                        "MMepId": "4676",
                        "text": "Ries"
                     },
                     {
                        "MMepId": "6056",
                        "text": "Riquet"
                     },
                     {
                        "MMepId": "6154",
                        "text": "Rochefort"
                     },
                     {
                        "MMepId": "5890",
                        "text": "Rohde"
                     },
                     {
                        "MMepId": "6100",
                        "text": "Schaake"
                     },
                     {
                        "MMepId": "6174",
                        "text": "Sosa Wagner"
                     },
                     {
                        "MMepId": "6546",
                        "text": "Sulík"
                     },
                     {
                        "MMepId": "6337",
                        "text": "Telička"
                     },
                     {
                        "MMepId": "6042",
                        "text": "Theurer"
                     },
                     {
                        "MMepId": "6281",
                        "text": "Torvalds"
                     },
                     {
                        "MMepId": "6181",
                        "text": "Tremosa i Balcells"
                     },
                     {
                        "MMepId": "6518",
                        "text": "Tørnæs"
                     },
                     {
                        "MMepId": "6151",
                        "text": "Vajgl"
                     },
                     {
                        "MMepId": "6159",
                        "text": "Verhofstadt"
                     },
                     {
                        "MMepId": "4550",
                        "text": "Väyrynen"
                     },
                     {
                        "MMepId": "5864",
                        "text": "Wikström"
                     },
                     {
                        "MMepId": "4758",
                        "text": "de Sarnez"
                     },
                     {
                        "MMepId": "5385",
                        "text": "in 't Veld"
                     },
                     {
                        "MMepId": "6097",
                        "text": "van Baalen"
                     },
                     {
                        "MMepId": "6621",
                        "text": "van Nieuwenhuizen"
                     }
                  ]
               },
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5262",
                        "text": "Ashworth"
                     },
                     {
                        "MMepId": "4911",
                        "text": "Belder"
                     },
                     {
                        "MMepId": "6081",
                        "text": "Campbell Bannerman"
                     },
                     {
                        "MMepId": "5490",
                        "text": "Czarnecki"
                     },
                     {
                        "MMepId": "6291",
                        "text": "Demesmaeker"
                     },
                     {
                        "MMepId": "6516",
                        "text": "Dohrmann"
                     },
                     {
                        "MMepId": "6538",
                        "text": "Duda"
                     },
                     {
                        "MMepId": "6579",
                        "text": "Duncan"
                     },
                     {
                        "MMepId": "6511",
                        "text": "Dzhambazki"
                     },
                     {
                        "MMepId": "6104",
                        "text": "Ford"
                     },
                     {
                        "MMepId": "4957",
                        "text": "Foster"
                     },
                     {
                        "MMepId": "5472",
                        "text": "Fotyga"
                     },
                     {
                        "MMepId": "6112",
                        "text": "Fox"
                     },
                     {
                        "MMepId": "6111",
                        "text": "Girling"
                     },
                     {
                        "MMepId": "6539",
                        "text": "Gosiewska"
                     },
                     {
                        "MMepId": "5963",
                        "text": "Gróbarczyk"
                     },
                     {
                        "MMepId": "4959",
                        "text": "Hannan"
                     },
                     {
                        "MMepId": "6520",
                        "text": "Jackiewicz"
                     },
                     {
                        "MMepId": "6531",
                        "text": "Jurek"
                     },
                     {
                        "MMepId": "5630",
                        "text": "Kamall"
                     },
                     {
                        "MMepId": "5563",
                        "text": "Karim"
                     },
                     {
                        "MMepId": "6514",
                        "text": "Karlsson"
                     },
                     {
                        "MMepId": "6525",
                        "text": "Karski"
                     },
                     {
                        "MMepId": "4946",
                        "text": "Kirkhope"
                     },
                     {
                        "MMepId": "6530",
                        "text": "Krasnodębski"
                     },
                     {
                        "MMepId": "5507",
                        "text": "Kuźmiuk"
                     },
                     {
                        "MMepId": "6561",
                        "text": "Lewer"
                     },
                     {
                        "MMepId": "6667",
                        "text": "Marias"
                     },
                     {
                        "MMepId": "6234",
                        "text": "McIntyre"
                     },
                     {
                        "MMepId": "5851",
                        "text": "Messerschmidt"
                     },
                     {
                        "MMepId": "1230",
                        "text": "Nicholson"
                     },
                     {
                        "MMepId": "6536",
                        "text": "Ożóg"
                     },
                     {
                        "MMepId": "6512",
                        "text": "Piecha"
                     },
                     {
                        "MMepId": "5974",
                        "text": "Poręba"
                     },
                     {
                        "MMepId": "6682",
                        "text": "Stevens"
                     },
                     {
                        "MMepId": "6088",
                        "text": "Swinburne"
                     },
                     {
                        "MMepId": "4925",
                        "text": "Tannock"
                     },
                     {
                        "MMepId": "6220",
                        "text": "Terho"
                     },
                     {
                        "MMepId": "5892",
                        "text": "Tošenovský"
                     },
                     {
                        "MMepId": "5113",
                        "text": "Ujazdowski"
                     },
                     {
                        "MMepId": "4917",
                        "text": "Van Orden"
                     },
                     {
                        "MMepId": "6513",
                        "text": "Vistisen"
                     },
                     {
                        "MMepId": "6515",
                        "text": "Wiśniewska"
                     },
                     {
                        "MMepId": "5108",
                        "text": "Wojciechowski"
                     },
                     {
                        "MMepId": "5598",
                        "text": "Zīle"
                     },
                     {
                        "MMepId": "6522",
                        "text": "Złotowski"
                     },
                     {
                        "MMepId": "5981",
                        "text": "van Dalen"
                     },
                     {
                        "MMepId": "6545",
                        "text": "Škripek"
                     },
                     {
                        "MMepId": "6540",
                        "text": "Žitňanská"
                     }
                  ]
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6448",
                        "text": "Mach"
                     },
                     {
                        "MMepId": "6402",
                        "text": "Mazuronis"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6048",
                        "text": "Balczó"
                     },
                     {
                        "MMepId": "6203",
                        "text": "Kovács"
                     }
                  ]
               },
               {
                  "MIdentifier": "PPE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6602",
                        "text": "Adaktusson"
                     },
                     {
                        "MMepId": "1340",
                        "text": "Alliot-Marie"
                     },
                     {
                        "MMepId": "1178",
                        "text": "Arias Cañete"
                     },
                     {
                        "MMepId": "6594",
                        "text": "Arimont"
                     },
                     {
                        "MMepId": "4742",
                        "text": "Ayuso"
                     },
                     {
                        "MMepId": "6191",
                        "text": "Bach"
                     },
                     {
                        "MMepId": "6135",
                        "text": "Balz"
                     },
                     {
                        "MMepId": "6216",
                        "text": "Becker"
                     },
                     {
                        "MMepId": "5377",
                        "text": "Belet"
                     },
                     {
                        "MMepId": "5885",
                        "text": "Bendtsen"
                     },
                     {
                        "MMepId": "6343",
                        "text": "Bocskor"
                     },
                     {
                        "MMepId": "6613",
                        "text": "Bogovič"
                     },
                     {
                        "MMepId": "6535",
                        "text": "Boni"
                     },
                     {
                        "MMepId": "6617",
                        "text": "Buda"
                     },
                     {
                        "MMepId": "5388",
                        "text": "Buzek"
                     },
                     {
                        "MMepId": "1507",
                        "text": "Böge"
                     },
                     {
                        "MMepId": "6021",
                        "text": "Cadec"
                     },
                     {
                        "MMepId": "5341",
                        "text": "Caspary"
                     },
                     {
                        "MMepId": "5554",
                        "text": "Cesa"
                     },
                     {
                        "MMepId": "6492",
                        "text": "Cicu"
                     },
                     {
                        "MMepId": "6406",
                        "text": "Cirio"
                     },
                     {
                        "MMepId": "6600",
                        "text": "Clune"
                     },
                     {
                        "MMepId": "3800",
                        "text": "Coelho"
                     },
                     {
                        "MMepId": "6273",
                        "text": "Collin-Langen"
                     },
                     {
                        "MMepId": "5948",
                        "text": "Comi"
                     },
                     {
                        "MMepId": "6587",
                        "text": "Comodini Cachia"
                     },
                     {
                        "MMepId": "6548",
                        "text": "Csáky"
                     },
                     {
                        "MMepId": "5921",
                        "text": "Danjean"
                     },
                     {
                        "MMepId": "6194",
                        "text": "Dantin"
                     },
                     {
                        "MMepId": "6172",
                        "text": "Dati"
                     },
                     {
                        "MMepId": "6364",
                        "text": "Delahaye"
                     },
                     {
                        "MMepId": "6345",
                        "text": "Deli"
                     },
                     {
                        "MMepId": "5998",
                        "text": "Deutsch"
                     },
                     {
                        "MMepId": "5350",
                        "text": "Deß"
                     },
                     {
                        "MMepId": "5601",
                        "text": "Dombrovskis"
                     },
                     {
                        "MMepId": "6481",
                        "text": "Donchev"
                     },
                     {
                        "MMepId": "5960",
                        "text": "Dorfmann"
                     },
                     {
                        "MMepId": "5515",
                        "text": "Díaz de Mera García Consuegra"
                     },
                     {
                        "MMepId": "5348",
                        "text": "Ehler"
                     },
                     {
                        "MMepId": "6047",
                        "text": "Engel"
                     },
                     {
                        "MMepId": "6342",
                        "text": "Erdős"
                     },
                     {
                        "MMepId": "5983",
                        "text": "Estaràs Ferragut"
                     },
                     {
                        "MMepId": "3833",
                        "text": "Ferber"
                     },
                     {
                        "MMepId": "6068",
                        "text": "Fernandes"
                     },
                     {
                        "MMepId": "5903",
                        "text": "Fisas Ayxelà"
                     },
                     {
                        "MMepId": "4880",
                        "text": "Fitto"
                     },
                     {
                        "MMepId": "6020",
                        "text": "Gabriel"
                     },
                     {
                        "MMepId": "4659",
                        "text": "Gahler"
                     },
                     {
                        "MMepId": "6615",
                        "text": "Gambús"
                     },
                     {
                        "MMepId": "5803",
                        "text": "Gardini"
                     },
                     {
                        "MMepId": "6441",
                        "text": "Gieseke"
                     },
                     {
                        "MMepId": "6632",
                        "text": "González Pons"
                     },
                     {
                        "MMepId": "4032",
                        "text": "Grossetête"
                     },
                     {
                        "MMepId": "5109",
                        "text": "Grzyb"
                     },
                     {
                        "MMepId": "5342",
                        "text": "Gräßle"
                     },
                     {
                        "MMepId": "5138",
                        "text": "Gyürk"
                     },
                     {
                        "MMepId": "5280",
                        "text": "Gál"
                     },
                     {
                        "MMepId": "6202",
                        "text": "Gáll-Pelcz"
                     },
                     {
                        "MMepId": "6596",
                        "text": "Hayes"
                     },
                     {
                        "MMepId": "5715",
                        "text": "Hellvig"
                     },
                     {
                        "MMepId": "5018",
                        "text": "Herranz García"
                     },
                     {
                        "MMepId": "6534",
                        "text": "Hetman"
                     },
                     {
                        "MMepId": "5953",
                        "text": "Hohlmeier"
                     },
                     {
                        "MMepId": "4987",
                        "text": "Hortefeux"
                     },
                     {
                        "MMepId": "5254",
                        "text": "Hökmark"
                     },
                     {
                        "MMepId": "5952",
                        "text": "Hübner"
                     },
                     {
                        "MMepId": "5945",
                        "text": "Jahr"
                     },
                     {
                        "MMepId": "5954",
                        "text": "Jazłowiecka"
                     },
                     {
                        "MMepId": "5978",
                        "text": "Jiménez-Becerril Barrio"
                     },
                     {
                        "MMepId": "6360",
                        "text": "Joulaud"
                     },
                     {
                        "MMepId": "6055",
                        "text": "Juvin"
                     },
                     {
                        "MMepId": "5944",
                        "text": "Kalinowski"
                     },
                     {
                        "MMepId": "6095",
                        "text": "Kalniete"
                     },
                     {
                        "MMepId": "4669",
                        "text": "Karas"
                     },
                     {
                        "MMepId": "6070",
                        "text": "Kariņš"
                     },
                     {
                        "MMepId": "6666",
                        "text": "Kefalogiannis"
                     },
                     {
                        "MMepId": "5542",
                        "text": "Kelam"
                     },
                     {
                        "MMepId": "5855",
                        "text": "Kelly"
                     },
                     {
                        "MMepId": "3759",
                        "text": "Koch"
                     },
                     {
                        "MMepId": "6196",
                        "text": "Kovatchev"
                     },
                     {
                        "MMepId": "6527",
                        "text": "Kozłowska-Rajewicz"
                     },
                     {
                        "MMepId": "5403",
                        "text": "Kudrycka"
                     },
                     {
                        "MMepId": "5840",
                        "text": "Kukan"
                     },
                     {
                        "MMepId": "6662",
                        "text": "Kyrtsos"
                     },
                     {
                        "MMepId": "6001",
                        "text": "Kósa"
                     },
                     {
                        "MMepId": "6053",
                        "text": "Köstinger"
                     },
                     {
                        "MMepId": "6393",
                        "text": "Landsbergis"
                     },
                     {
                        "MMepId": "3856",
                        "text": "Langen"
                     },
                     {
                        "MMepId": "6363",
                        "text": "Lavrilleux"
                     },
                     {
                        "MMepId": "6200",
                        "text": "Le Grip"
                     },
                     {
                        "MMepId": "6592",
                        "text": "Lenaers"
                     },
                     {
                        "MMepId": "5105",
                        "text": "Lewandowski"
                     },
                     {
                        "MMepId": "3855",
                        "text": "Liese"
                     },
                     {
                        "MMepId": "6442",
                        "text": "Lins"
                     },
                     {
                        "MMepId": "5938",
                        "text": "Lope Fontagné"
                     },
                     {
                        "MMepId": "5996",
                        "text": "Macovei"
                     },
                     {
                        "MMepId": "6303",
                        "text": "Maletić"
                     },
                     {
                        "MMepId": "6245",
                        "text": "Malinov"
                     },
                     {
                        "MMepId": "3845",
                        "text": "Mann"
                     },
                     {
                        "MMepId": "5646",
                        "text": "Marinescu"
                     },
                     {
                        "MMepId": "6465",
                        "text": "Martusciello"
                     },
                     {
                        "MMepId": "6096",
                        "text": "Mato"
                     },
                     {
                        "MMepId": "6440",
                        "text": "McAllister"
                     },
                     {
                        "MMepId": "5245",
                        "text": "McGuinness"
                     },
                     {
                        "MMepId": "6121",
                        "text": "Melo"
                     },
                     {
                        "MMepId": "6297",
                        "text": "Metsola"
                     },
                     {
                        "MMepId": "5308",
                        "text": "Mikolášik"
                     },
                     {
                        "MMepId": "5518",
                        "text": "Millán Mon"
                     },
                     {
                        "MMepId": "6366",
                        "text": "Monteiro de Aguiar"
                     },
                     {
                        "MMepId": "6368",
                        "text": "Morano"
                     },
                     {
                        "MMepId": "5732",
                        "text": "Morin-Chartier"
                     },
                     {
                        "MMepId": "6436",
                        "text": "Mureşan"
                     },
                     {
                        "MMepId": "6361",
                        "text": "Muselier"
                     },
                     {
                        "MMepId": "5547",
                        "text": "Mussolini"
                     },
                     {
                        "MMepId": "5774",
                        "text": "Mănescu"
                     },
                     {
                        "MMepId": "6544",
                        "text": "Nagy"
                     },
                     {
                        "MMepId": "4712",
                        "text": "Niebler"
                     },
                     {
                        "MMepId": "6332",
                        "text": "Niedermayer"
                     },
                     {
                        "MMepId": "5407",
                        "text": "Olbrycht"
                     },
                     {
                        "MMepId": "6377",
                        "text": "Pabriks"
                     },
                     {
                        "MMepId": "5702",
                        "text": "Patriciello"
                     },
                     {
                        "MMepId": "5033",
                        "text": "Peterle"
                     },
                     {
                        "MMepId": "6383",
                        "text": "Petir"
                     },
                     {
                        "MMepId": "5346",
                        "text": "Pieper"
                     },
                     {
                        "MMepId": "5788",
                        "text": "Pietikäinen"
                     },
                     {
                        "MMepId": "6537",
                        "text": "Pitera"
                     },
                     {
                        "MMepId": "6268",
                        "text": "Plenković"
                     },
                     {
                        "MMepId": "6519",
                        "text": "Plura"
                     },
                     {
                        "MMepId": "6491",
                        "text": "Pogliese"
                     },
                     {
                        "MMepId": "6335",
                        "text": "Polčák"
                     },
                     {
                        "MMepId": "6092",
                        "text": "Ponga"
                     },
                     {
                        "MMepId": "6700",
                        "text": "Pospíšil"
                     },
                     {
                        "MMepId": "6010",
                        "text": "Preda"
                     },
                     {
                        "MMepId": "6224",
                        "text": "Proust"
                     },
                     {
                        "MMepId": "1487",
                        "text": "Quisthoudt-Rowohl"
                     },
                     {
                        "MMepId": "6488",
                        "text": "Radev"
                     },
                     {
                        "MMepId": "6072",
                        "text": "Rangel"
                     },
                     {
                        "MMepId": "5347",
                        "text": "Reul"
                     },
                     {
                        "MMepId": "6365",
                        "text": "Ribeiro"
                     },
                     {
                        "MMepId": "6684",
                        "text": "Rolin"
                     },
                     {
                        "MMepId": "5512",
                        "text": "Rosati"
                     },
                     {
                        "MMepId": "6362",
                        "text": "Ruas"
                     },
                     {
                        "MMepId": "4596",
                        "text": "Rübig"
                     },
                     {
                        "MMepId": "6698",
                        "text": "Salini"
                     },
                     {
                        "MMepId": "6370",
                        "text": "Sander"
                     },
                     {
                        "MMepId": "6259",
                        "text": "Sarvamaa"
                     },
                     {
                        "MMepId": "4768",
                        "text": "Saïfi"
                     },
                     {
                        "MMepId": "6619",
                        "text": "Schmidt"
                     },
                     {
                        "MMepId": "6636",
                        "text": "Schreijer-Pierik"
                     },
                     {
                        "MMepId": "6443",
                        "text": "Schulze"
                     },
                     {
                        "MMepId": "5345",
                        "text": "Schwab"
                     },
                     {
                        "MMepId": "5265",
                        "text": "Schöpflin"
                     },
                     {
                        "MMepId": "6428",
                        "text": "Sernagiotto"
                     },
                     {
                        "MMepId": "5111",
                        "text": "Siekierski"
                     },
                     {
                        "MMepId": "4705",
                        "text": "Sommer"
                     },
                     {
                        "MMepId": "6663",
                        "text": "Spyraki"
                     },
                     {
                        "MMepId": "5778",
                        "text": "Stolojan"
                     },
                     {
                        "MMepId": "6320",
                        "text": "Stylianides"
                     },
                     {
                        "MMepId": "6672",
                        "text": "Svoboda"
                     },
                     {
                        "MMepId": "6528",
                        "text": "Szejnfeld"
                     },
                     {
                        "MMepId": "5143",
                        "text": "Szájer"
                     },
                     {
                        "MMepId": "5781",
                        "text": "Sógor"
                     },
                     {
                        "MMepId": "5207",
                        "text": "Theocharous"
                     },
                     {
                        "MMepId": "5949",
                        "text": "Thun und Hohenstein"
                     },
                     {
                        "MMepId": "3738",
                        "text": "Thyssen"
                     },
                     {
                        "MMepId": "6681",
                        "text": "Tomc"
                     },
                     {
                        "MMepId": "6398",
                        "text": "Toti"
                     },
                     {
                        "MMepId": "5783",
                        "text": "Tőkés"
                     },
                     {
                        "MMepId": "6007",
                        "text": "Ungureanu"
                     },
                     {
                        "MMepId": "5734",
                        "text": "Urutchev"
                     },
                     {
                        "MMepId": "6638",
                        "text": "Valcárcel"
                     },
                     {
                        "MMepId": "5930",
                        "text": "Verheyen"
                     },
                     {
                        "MMepId": "6356",
                        "text": "Virkkunen"
                     },
                     {
                        "MMepId": "5934",
                        "text": "Voss"
                     },
                     {
                        "MMepId": "6664",
                        "text": "Vozemberg"
                     },
                     {
                        "MMepId": "5714",
                        "text": "Vălean"
                     },
                     {
                        "MMepId": "5947",
                        "text": "Wałęsa"
                     },
                     {
                        "MMepId": "5770",
                        "text": "Weber Renate"
                     },
                     {
                        "MMepId": "6542",
                        "text": "Wenta"
                     },
                     {
                        "MMepId": "4643",
                        "text": "Wieland"
                     },
                     {
                        "MMepId": "5937",
                        "text": "Winkler Hermann"
                     },
                     {
                        "MMepId": "5782",
                        "text": "Winkler Iuliu"
                     },
                     {
                        "MMepId": "6665",
                        "text": "Zagorakis"
                     },
                     {
                        "MMepId": "5936",
                        "text": "Zalba Bidegain"
                     },
                     {
                        "MMepId": "6344",
                        "text": "Zdechovský"
                     },
                     {
                        "MMepId": "6532",
                        "text": "Zdrojewski"
                     },
                     {
                        "MMepId": "5935",
                        "text": "Zeller"
                     },
                     {
                        "MMepId": "6094",
                        "text": "Zver"
                     },
                     {
                        "MMepId": "5420",
                        "text": "Zwiefka"
                     },
                     {
                        "MMepId": "5189",
                        "text": "Záborská"
                     },
                     {
                        "MMepId": "5511",
                        "text": "de Grandes Pascual"
                     },
                     {
                        "MMepId": "5727",
                        "text": "de Lange"
                     },
                     {
                        "MMepId": "5508",
                        "text": "del Castillo Vera"
                     },
                     {
                        "MMepId": "5295",
                        "text": "van Nistelrooij"
                     },
                     {
                        "MMepId": "5928",
                        "text": "van de Camp"
                     },
                     {
                        "MMepId": "5964",
                        "text": "Łukacijewska"
                     },
                     {
                        "MMepId": "6341",
                        "text": "Šojdrová"
                     },
                     {
                        "MMepId": "6547",
                        "text": "Štefanec"
                     },
                     {
                        "MMepId": "6333",
                        "text": "Štětina"
                     },
                     {
                        "MMepId": "6301",
                        "text": "Šuica"
                     },
                     {
                        "MMepId": "6680",
                        "text": "Šulin"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6651",
                        "text": "Aguilera García"
                     },
                     {
                        "MMepId": "6567",
                        "text": "Anderson Lucy"
                     },
                     {
                        "MMepId": "6279",
                        "text": "Andrieu"
                     },
                     {
                        "MMepId": "6687",
                        "text": "Androulakis"
                     },
                     {
                        "MMepId": "6553",
                        "text": "Arena"
                     },
                     {
                        "MMepId": "5426",
                        "text": "Assis"
                     },
                     {
                        "MMepId": "5411",
                        "text": "Ayala Sender"
                     },
                     {
                        "MMepId": "6378",
                        "text": "Balas"
                     },
                     {
                        "MMepId": "5866",
                        "text": "Balčytis"
                     },
                     {
                        "MMepId": "6611",
                        "text": "Bayet"
                     },
                     {
                        "MMepId": "6505",
                        "text": "Benifei"
                     },
                     {
                        "MMepId": "3972",
                        "text": "Berès"
                     },
                     {
                        "MMepId": "6456",
                        "text": "Bettini"
                     },
                     {
                        "MMepId": "6650",
                        "text": "Blanco López"
                     },
                     {
                        "MMepId": "5867",
                        "text": "Blinkevičiūtė"
                     },
                     {
                        "MMepId": "6449",
                        "text": "Bonafè"
                     },
                     {
                        "MMepId": "5768",
                        "text": "Boştinaru"
                     },
                     {
                        "MMepId": "6572",
                        "text": "Brannen"
                     },
                     {
                        "MMepId": "5465",
                        "text": "Bresso"
                     },
                     {
                        "MMepId": "6421",
                        "text": "Briano"
                     },
                     {
                        "MMepId": "6647",
                        "text": "Cabezón Ruiz"
                     },
                     {
                        "MMepId": "6489",
                        "text": "Caputo"
                     },
                     {
                        "MMepId": "5835",
                        "text": "Childers"
                     },
                     {
                        "MMepId": "6499",
                        "text": "Chinnici"
                     },
                     {
                        "MMepId": "5284",
                        "text": "Christensen"
                     },
                     {
                        "MMepId": "6083",
                        "text": "Cofferati"
                     },
                     {
                        "MMepId": "4628",
                        "text": "Corbett"
                     },
                     {
                        "MMepId": "6085",
                        "text": "Costa"
                     },
                     {
                        "MMepId": "6051",
                        "text": "Cozzolino"
                     },
                     {
                        "MMepId": "5661",
                        "text": "Creţu"
                     },
                     {
                        "MMepId": "6616",
                        "text": "Cristea"
                     },
                     {
                        "MMepId": "6589",
                        "text": "Dalli"
                     },
                     {
                        "MMepId": "6570",
                        "text": "Dance"
                     },
                     {
                        "MMepId": "6458",
                        "text": "Danti"
                     },
                     {
                        "MMepId": "6062",
                        "text": "De Castro"
                     },
                     {
                        "MMepId": "6437",
                        "text": "De Monte"
                     },
                     {
                        "MMepId": "6410",
                        "text": "Delvaux-Stehres"
                     },
                     {
                        "MMepId": "5816",
                        "text": "Denanot"
                     },
                     {
                        "MMepId": "6588",
                        "text": "Dodds Anneliese"
                     },
                     {
                        "MMepId": "6420",
                        "text": "Drăghici"
                     },
                     {
                        "MMepId": "5829",
                        "text": "Dăncilă"
                     },
                     {
                        "MMepId": "6014",
                        "text": "Ertug"
                     },
                     {
                        "MMepId": "6080",
                        "text": "Fajon"
                     },
                     {
                        "MMepId": "6652",
                        "text": "Fernández"
                     },
                     {
                        "MMepId": "5427",
                        "text": "Ferreira Elisa"
                     },
                     {
                        "MMepId": "5173",
                        "text": "Flašíková Beňová"
                     },
                     {
                        "MMepId": "6012",
                        "text": "Fleckenstein"
                     },
                     {
                        "MMepId": "6620",
                        "text": "Freund"
                     },
                     {
                        "MMepId": "6424",
                        "text": "Frunzulică"
                     },
                     {
                        "MMepId": "5417",
                        "text": "García Pérez"
                     },
                     {
                        "MMepId": "6130",
                        "text": "Gardiazabal Rubial"
                     },
                     {
                        "MMepId": "6454",
                        "text": "Gasbarra"
                     },
                     {
                        "MMepId": "3827",
                        "text": "Gebhardt"
                     },
                     {
                        "MMepId": "6005",
                        "text": "Geier"
                     },
                     {
                        "MMepId": "6485",
                        "text": "Gentile"
                     },
                     {
                        "MMepId": "5495",
                        "text": "Geringer de Oedenberg"
                     },
                     {
                        "MMepId": "5497",
                        "text": "Gierek"
                     },
                     {
                        "MMepId": "4937",
                        "text": "Gill Neena"
                     },
                     {
                        "MMepId": "6502",
                        "text": "Giuffrida"
                     },
                     {
                        "MMepId": "5425",
                        "text": "Gomes"
                     },
                     {
                        "MMepId": "6688",
                        "text": "Grammatikakis"
                     },
                     {
                        "MMepId": "6419",
                        "text": "Grapini"
                     },
                     {
                        "MMepId": "6580",
                        "text": "Griffin"
                     },
                     {
                        "MMepId": "5692",
                        "text": "Groote"
                     },
                     {
                        "MMepId": "6063",
                        "text": "Gualtieri"
                     },
                     {
                        "MMepId": "5941",
                        "text": "Guerrero Salom"
                     },
                     {
                        "MMepId": "6107",
                        "text": "Guillaume"
                     },
                     {
                        "MMepId": "6603",
                        "text": "Guteland"
                     },
                     {
                        "MMepId": "6212",
                        "text": "Gutiérrez Prieto"
                     },
                     {
                        "MMepId": "5261",
                        "text": "Hedh"
                     },
                     {
                        "MMepId": "4994",
                        "text": "Honeyball"
                     },
                     {
                        "MMepId": "4103",
                        "text": "Howitt"
                     },
                     {
                        "MMepId": "6029",
                        "text": "Ivan"
                     },
                     {
                        "MMepId": "5870",
                        "text": "Jaakonsaari"
                     },
                     {
                        "MMepId": "6624",
                        "text": "Jongerius"
                     },
                     {
                        "MMepId": "6161",
                        "text": "Jáuregui Atondo"
                     },
                     {
                        "MMepId": "6146",
                        "text": "Kadenbach"
                     },
                     {
                        "MMepId": "6686",
                        "text": "Kaili"
                     },
                     {
                        "MMepId": "6009",
                        "text": "Kammerevert"
                     },
                     {
                        "MMepId": "3756",
                        "text": "Kaufmann"
                     },
                     {
                        "MMepId": "6326",
                        "text": "Keller Jan"
                     },
                     {
                        "MMepId": "6581",
                        "text": "Khan"
                     },
                     {
                        "MMepId": "6571",
                        "text": "Kirton-Darling"
                     },
                     {
                        "MMepId": "6508",
                        "text": "Kofod"
                     },
                     {
                        "MMepId": "3761",
                        "text": "Krehl"
                     },
                     {
                        "MMepId": "6367",
                        "text": "Kumpula-Natri"
                     },
                     {
                        "MMepId": "6435",
                        "text": "Kyenge"
                     },
                     {
                        "MMepId": "6689",
                        "text": "Kyrkos"
                     },
                     {
                        "MMepId": "6459",
                        "text": "Köster"
                     },
                     {
                        "MMepId": "3823",
                        "text": "Lange"
                     },
                     {
                        "MMepId": "6329",
                        "text": "Lauristin"
                     },
                     {
                        "MMepId": "5371",
                        "text": "Leichtfried"
                     },
                     {
                        "MMepId": "4685",
                        "text": "Leinen"
                     },
                     {
                        "MMepId": "5094",
                        "text": "Liberadzki"
                     },
                     {
                        "MMepId": "6477",
                        "text": "Lietz"
                     },
                     {
                        "MMepId": "5860",
                        "text": "Ludvigsson"
                     },
                     {
                        "MMepId": "6648",
                        "text": "López Javi"
                     },
                     {
                        "MMepId": "5984",
                        "text": "López Aguilar"
                     },
                     {
                        "MMepId": "6380",
                        "text": "Mamikins"
                     },
                     {
                        "MMepId": "6355",
                        "text": "Manscour"
                     },
                     {
                        "MMepId": "1133",
                        "text": "Martin David"
                     },
                     {
                        "MMepId": "6565",
                        "text": "Martin Edouard"
                     },
                     {
                        "MMepId": "6354",
                        "text": "Maurel"
                     },
                     {
                        "MMepId": "6322",
                        "text": "Mavrides"
                     },
                     {
                        "MMepId": "5314",
                        "text": "Maňka"
                     },
                     {
                        "MMepId": "4646",
                        "text": "McAvan"
                     },
                     {
                        "MMepId": "6476",
                        "text": "Melior"
                     },
                     {
                        "MMepId": "6296",
                        "text": "Mizzi"
                     },
                     {
                        "MMepId": "6423",
                        "text": "Moisă"
                     },
                     {
                        "MMepId": "6351",
                        "text": "Molnár"
                     },
                     {
                        "MMepId": "6562",
                        "text": "Moody"
                     },
                     {
                        "MMepId": "4923",
                        "text": "Moraes"
                     },
                     {
                        "MMepId": "6433",
                        "text": "Moretti"
                     },
                     {
                        "MMepId": "6422",
                        "text": "Morgano"
                     },
                     {
                        "MMepId": "6506",
                        "text": "Mosca"
                     },
                     {
                        "MMepId": "6691",
                        "text": "Negrescu"
                     },
                     {
                        "MMepId": "6618",
                        "text": "Nekov"
                     },
                     {
                        "MMepId": "6016",
                        "text": "Neuser"
                     },
                     {
                        "MMepId": "6418",
                        "text": "Nica"
                     },
                     {
                        "MMepId": "6352",
                        "text": "Niedermüller"
                     },
                     {
                        "MMepId": "6242",
                        "text": "Nilsson"
                     },
                     {
                        "MMepId": "6473",
                        "text": "Noichl"
                     },
                     {
                        "MMepId": "5483",
                        "text": "Panzeri"
                     },
                     {
                        "MMepId": "6487",
                        "text": "Paolucci"
                     },
                     {
                        "MMepId": "6323",
                        "text": "Papadakis Demetris"
                     },
                     {
                        "MMepId": "6103",
                        "text": "Pargneaux"
                     },
                     {
                        "MMepId": "5648",
                        "text": "Paşcu"
                     },
                     {
                        "MMepId": "5296",
                        "text": "Peillon"
                     },
                     {
                        "MMepId": "6484",
                        "text": "Picierno"
                     },
                     {
                        "MMepId": "6261",
                        "text": "Picula"
                     },
                     {
                        "MMepId": "6625",
                        "text": "Piri"
                     },
                     {
                        "MMepId": "6493",
                        "text": "Pirinski"
                     },
                     {
                        "MMepId": "5894",
                        "text": "Poc"
                     },
                     {
                        "MMepId": "6330",
                        "text": "Poche"
                     },
                     {
                        "MMepId": "6609",
                        "text": "Post"
                     },
                     {
                        "MMepId": "6526",
                        "text": "Preuß"
                     },
                     {
                        "MMepId": "6426",
                        "text": "Rebega"
                     },
                     {
                        "MMepId": "6136",
                        "text": "Regner"
                     },
                     {
                        "MMepId": "6316",
                        "text": "Revault D'Allonnes Bonnefoy"
                     },
                     {
                        "MMepId": "6677",
                        "text": "Rodrigues Liliana"
                     },
                     {
                        "MMepId": "6649",
                        "text": "Rodríguez-Piñero Fernández"
                     },
                     {
                        "MMepId": "5813",
                        "text": "Rodust"
                     },
                     {
                        "MMepId": "6357",
                        "text": "Rozière"
                     },
                     {
                        "MMepId": "6415",
                        "text": "Sant"
                     },
                     {
                        "MMepId": "5713",
                        "text": "Schaldemose"
                     },
                     {
                        "MMepId": "6438",
                        "text": "Schlein"
                     },
                     {
                        "MMepId": "6475",
                        "text": "Schuster"
                     },
                     {
                        "MMepId": "5897",
                        "text": "Sehnalová"
                     },
                     {
                        "MMepId": "6375",
                        "text": "Serrão Santos"
                     },
                     {
                        "MMepId": "6381",
                        "text": "Silva Pereira"
                     },
                     {
                        "MMepId": "6008",
                        "text": "Simon Peter"
                     },
                     {
                        "MMepId": "6566",
                        "text": "Simon Siôn"
                     },
                     {
                        "MMepId": "6093",
                        "text": "Sippel"
                     },
                     {
                        "MMepId": "5844",
                        "text": "Smolková"
                     },
                     {
                        "MMepId": "6498",
                        "text": "Soru"
                     },
                     {
                        "MMepId": "6003",
                        "text": "Steinruck"
                     },
                     {
                        "MMepId": "4949",
                        "text": "Stihler"
                     },
                     {
                        "MMepId": "6334",
                        "text": "Szanyi"
                     },
                     {
                        "MMepId": "5653",
                        "text": "Sârbu"
                     },
                     {
                        "MMepId": "6623",
                        "text": "Tang"
                     },
                     {
                        "MMepId": "6427",
                        "text": "Tapardel"
                     },
                     {
                        "MMepId": "5624",
                        "text": "Tarabella"
                     },
                     {
                        "MMepId": "6282",
                        "text": "Thomas"
                     },
                     {
                        "MMepId": "5459",
                        "text": "Toia"
                     },
                     {
                        "MMepId": "6336",
                        "text": "Ujhelyi"
                     },
                     {
                        "MMepId": "5859",
                        "text": "Ulvskog"
                     },
                     {
                        "MMepId": "4990",
                        "text": "Van Brempt"
                     },
                     {
                        "MMepId": "6086",
                        "text": "Vaughan"
                     },
                     {
                        "MMepId": "6425",
                        "text": "Viotti"
                     },
                     {
                        "MMepId": "6582",
                        "text": "Ward"
                     },
                     {
                        "MMepId": "6235",
                        "text": "Weidenholzer"
                     },
                     {
                        "MMepId": "6463",
                        "text": "Werner"
                     },
                     {
                        "MMepId": "6011",
                        "text": "Westphal"
                     },
                     {
                        "MMepId": "5698",
                        "text": "Willmott"
                     },
                     {
                        "MMepId": "5845",
                        "text": "Zala"
                     },
                     {
                        "MMepId": "6434",
                        "text": "Zanonato"
                     },
                     {
                        "MMepId": "5957",
                        "text": "Zemke"
                     },
                     {
                        "MMepId": "6373",
                        "text": "Zorrinho"
                     },
                     {
                        "MMepId": "6478",
                        "text": "von Weizsäcker"
                     },
                     {
                        "MMepId": "6521",
                        "text": "Łybacka"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "5910",
                        "text": "Albrecht"
                     },
                     {
                        "MMepId": "5291",
                        "text": "Auken"
                     },
                     {
                        "MMepId": "5918",
                        "text": "Bové"
                     },
                     {
                        "MMepId": "5913",
                        "text": "Bütikofer"
                     },
                     {
                        "MMepId": "6605",
                        "text": "Ceballos"
                     },
                     {
                        "MMepId": "5358",
                        "text": "Cramer"
                     },
                     {
                        "MMepId": "6040",
                        "text": "Delli"
                     },
                     {
                        "MMepId": "6324",
                        "text": "Durand"
                     },
                     {
                        "MMepId": "5899",
                        "text": "Eickhout"
                     },
                     {
                        "MMepId": "6554",
                        "text": "Eriksson"
                     },
                     {
                        "MMepId": "4954",
                        "text": "Evans"
                     },
                     {
                        "MMepId": "5904",
                        "text": "Giegold"
                     },
                     {
                        "MMepId": "5353",
                        "text": "Harms"
                     },
                     {
                        "MMepId": "4535",
                        "text": "Hautala"
                     },
                     {
                        "MMepId": "6486",
                        "text": "Heubuch"
                     },
                     {
                        "MMepId": "4655",
                        "text": "Hudghton"
                     },
                     {
                        "MMepId": "5926",
                        "text": "Häusling"
                     },
                     {
                        "MMepId": "5914",
                        "text": "Jadot"
                     },
                     {
                        "MMepId": "6054",
                        "text": "Joly"
                     },
                     {
                        "MMepId": "6350",
                        "text": "Jávor"
                     },
                     {
                        "MMepId": "5908",
                        "text": "Keller Ska"
                     },
                     {
                        "MMepId": "4935",
                        "text": "Lambert"
                     },
                     {
                        "MMepId": "5838",
                        "text": "Lamberts"
                     },
                     {
                        "MMepId": "5902",
                        "text": "Lochbihler"
                     },
                     {
                        "MMepId": "6149",
                        "text": "Lunacek"
                     },
                     {
                        "MMepId": "5862",
                        "text": "Lövin"
                     },
                     {
                        "MMepId": "6550",
                        "text": "Maragall"
                     },
                     {
                        "MMepId": "6349",
                        "text": "Meszerics"
                     },
                     {
                        "MMepId": "6453",
                        "text": "Reda"
                     },
                     {
                        "MMepId": "6552",
                        "text": "Reimon"
                     },
                     {
                        "MMepId": "6529",
                        "text": "Reintke"
                     },
                     {
                        "MMepId": "5917",
                        "text": "Rivasi"
                     },
                     {
                        "MMepId": "6694",
                        "text": "Ropé"
                     },
                     {
                        "MMepId": "5987",
                        "text": "Sargentini"
                     },
                     {
                        "MMepId": "6560",
                        "text": "Scott Cato"
                     },
                     {
                        "MMepId": "6659",
                        "text": "Sebastià"
                     },
                     {
                        "MMepId": "5571",
                        "text": "Smith"
                     },
                     {
                        "MMepId": "4983",
                        "text": "Staes"
                     },
                     {
                        "MMepId": "6168",
                        "text": "Tarand"
                     },
                     {
                        "MMepId": "6204",
                        "text": "Taylor"
                     },
                     {
                        "MMepId": "6549",
                        "text": "Terricabras"
                     },
                     {
                        "MMepId": "5360",
                        "text": "Trüpel"
                     },
                     {
                        "MMepId": "4847",
                        "text": "Turmes"
                     },
                     {
                        "MMepId": "6591",
                        "text": "Urtasun"
                     },
                     {
                        "MMepId": "6551",
                        "text": "Vana"
                     },
                     {
                        "MMepId": "6390",
                        "text": "Škrlec"
                     },
                     {
                        "MMepId": "6612",
                        "text": "Šoltes"
                     }
                  ]
               }
            ]
         },
         "ResultAgainst": {
            "MNumber": "116",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ALDE",
                  "PoliticalGroupMemberName": {
                     "MMepId": "5997",
                     "text": "Nicolai"
                  }
               },
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6452",
                        "text": "Gericke"
                     },
                     {
                        "MMepId": "6460",
                        "text": "Henkel"
                     },
                     {
                        "MMepId": "6461",
                        "text": "Kölmel"
                     },
                     {
                        "MMepId": "6457",
                        "text": "Lucke"
                     },
                     {
                        "MMepId": "5460",
                        "text": "Piotrowski"
                     },
                     {
                        "MMepId": "6467",
                        "text": "Pretzell"
                     },
                     {
                        "MMepId": "6464",
                        "text": "Starbatty"
                     },
                     {
                        "MMepId": "6300",
                        "text": "Tomašić"
                     },
                     {
                        "MMepId": "6683",
                        "text": "Van Overtveldt"
                     }
                  ]
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6431",
                        "text": "Affronte"
                     },
                     {
                        "MMepId": "6445",
                        "text": "Agea"
                     },
                     {
                        "MMepId": "6066",
                        "text": "Agnew"
                     },
                     {
                        "MMepId": "6480",
                        "text": "Aiuto"
                     },
                     {
                        "MMepId": "6559",
                        "text": "Aker"
                     },
                     {
                        "MMepId": "6577",
                        "text": "Arnott"
                     },
                     {
                        "MMepId": "6555",
                        "text": "Atkinson"
                     },
                     {
                        "MMepId": "6574",
                        "text": "Bashir"
                     },
                     {
                        "MMepId": "5567",
                        "text": "Batten"
                     },
                     {
                        "MMepId": "6411",
                        "text": "Beghin"
                     },
                     {
                        "MMepId": "6583",
                        "text": "Bours"
                     },
                     {
                        "MMepId": "6590",
                        "text": "Carver"
                     },
                     {
                        "MMepId": "6446",
                        "text": "Castaldo"
                     },
                     {
                        "MMepId": "6586",
                        "text": "Coburn"
                     },
                     {
                        "MMepId": "6573",
                        "text": "Collins"
                     },
                     {
                        "MMepId": "6494",
                        "text": "Corrao"
                     },
                     {
                        "MMepId": "6472",
                        "text": "D'Amato"
                     },
                     {
                        "MMepId": "6569",
                        "text": "Etheridge"
                     },
                     {
                        "MMepId": "6413",
                        "text": "Evi"
                     },
                     {
                        "MMepId": "4929",
                        "text": "Farage"
                     },
                     {
                        "MMepId": "6470",
                        "text": "Ferrara"
                     },
                     {
                        "MMepId": "6558",
                        "text": "Finch"
                     },
                     {
                        "MMepId": "6584",
                        "text": "Gill Nathan"
                     },
                     {
                        "MMepId": "4920",
                        "text": "Helmer"
                     },
                     {
                        "MMepId": "6575",
                        "text": "Hookem"
                     },
                     {
                        "MMepId": "6556",
                        "text": "James"
                     },
                     {
                        "MMepId": "6608",
                        "text": "Lundgren"
                     },
                     {
                        "MMepId": "6497",
                        "text": "Moi"
                     },
                     {
                        "MMepId": "5977",
                        "text": "Nuttall"
                     },
                     {
                        "MMepId": "6557",
                        "text": "O'Flynn"
                     },
                     {
                        "MMepId": "5875",
                        "text": "Paksas"
                     },
                     {
                        "MMepId": "6563",
                        "text": "Parker"
                     },
                     {
                        "MMepId": "6482",
                        "text": "Pedicini"
                     },
                     {
                        "MMepId": "6564",
                        "text": "Reid"
                     },
                     {
                        "MMepId": "6447",
                        "text": "Tamburrano"
                     },
                     {
                        "MMepId": "6412",
                        "text": "Valli"
                     },
                     {
                        "MMepId": "6585",
                        "text": "Woolfe"
                     },
                     {
                        "MMepId": "6414",
                        "text": "Zanni"
                     },
                     {
                        "MMepId": "6695",
                        "text": "Zullo"
                     }
                  ]
               },
               {
                  "MIdentifier": "GUE/NGL",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6654",
                        "text": "Albiol Guzmán"
                     },
                     {
                        "MMepId": "6604",
                        "text": "Björk"
                     },
                     {
                        "MMepId": "6701",
                        "text": "Couso Permuy"
                     },
                     {
                        "MMepId": "6501",
                        "text": "Eck"
                     },
                     {
                        "MMepId": "5886",
                        "text": "Ferreira João"
                     },
                     {
                        "MMepId": "6628",
                        "text": "Hazekamp"
                     },
                     {
                        "MMepId": "6637",
                        "text": "Iglesias"
                     },
                     {
                        "MMepId": "6657",
                        "text": "Juaristi Abaunz"
                     },
                     {
                        "MMepId": "6313",
                        "text": "Kari"
                     },
                     {
                        "MMepId": "6369",
                        "text": "Kyllönen"
                     },
                     {
                        "MMepId": "6653",
                        "text": "López Paloma"
                     },
                     {
                        "MMepId": "6643",
                        "text": "Mineur"
                     },
                     {
                        "MMepId": "5916",
                        "text": "Mélenchon"
                     },
                     {
                        "MMepId": "6250",
                        "text": "Omarjee"
                     },
                     {
                        "MMepId": "6674",
                        "text": "Sakorafa"
                     },
                     {
                        "MMepId": "6655",
                        "text": "Senra Rodríguez"
                     },
                     {
                        "MMepId": "6641",
                        "text": "Sánchez Caldentey"
                     },
                     {
                        "MMepId": "6656",
                        "text": "Vallina"
                     },
                     {
                        "MMepId": "6678",
                        "text": "Viegas"
                     },
                     {
                        "MMepId": "6253",
                        "text": "Zuber"
                     },
                     {
                        "MMepId": "5922",
                        "text": "de Jong"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6451",
                        "text": "Aliot"
                     },
                     {
                        "MMepId": "6593",
                        "text": "Annemans"
                     },
                     {
                        "MMepId": "6382",
                        "text": "Arnautu"
                     },
                     {
                        "MMepId": "6394",
                        "text": "Bay"
                     },
                     {
                        "MMepId": "6405",
                        "text": "Bilde"
                     },
                     {
                        "MMepId": "6179",
                        "text": "Bizzotto"
                     },
                     {
                        "MMepId": "5010",
                        "text": "Borghezio"
                     },
                     {
                        "MMepId": "6387",
                        "text": "Boutonnet"
                     },
                     {
                        "MMepId": "6391",
                        "text": "Briois"
                     },
                     {
                        "MMepId": "6507",
                        "text": "Buonanno"
                     },
                     {
                        "MMepId": "6384",
                        "text": "D'Ornano"
                     },
                     {
                        "MMepId": "6401",
                        "text": "Ferrand"
                     },
                     {
                        "MMepId": "6132",
                        "text": "Fontana"
                     },
                     {
                        "MMepId": "6668",
                        "text": "Fountoulis"
                     },
                     {
                        "MMepId": "6450",
                        "text": "Goddyn"
                     },
                     {
                        "MMepId": "1380",
                        "text": "Gollnisch"
                     },
                     {
                        "MMepId": "6524",
                        "text": "Iwaszkiewicz"
                     },
                     {
                        "MMepId": "6404",
                        "text": "Jalkh"
                     },
                     {
                        "MMepId": "6635",
                        "text": "Jansen"
                     },
                     {
                        "MMepId": "6629",
                        "text": "Kappel"
                     },
                     {
                        "MMepId": "6517",
                        "text": "Korwin-Mikke"
                     },
                     {
                        "MMepId": "1529",
                        "text": "Le Pen Jean-Marie"
                     },
                     {
                        "MMepId": "5332",
                        "text": "Le Pen Marine"
                     },
                     {
                        "MMepId": "6372",
                        "text": "Lebreton"
                     },
                     {
                        "MMepId": "6699",
                        "text": "Loiseau"
                     },
                     {
                        "MMepId": "6633",
                        "text": "Maeijer"
                     },
                     {
                        "MMepId": "6385",
                        "text": "Martin Dominique"
                     },
                     {
                        "MMepId": "6533",
                        "text": "Marusik"
                     },
                     {
                        "MMepId": "6627",
                        "text": "Mayer"
                     },
                     {
                        "MMepId": "6395",
                        "text": "Monot"
                     },
                     {
                        "MMepId": "6403",
                        "text": "Montel"
                     },
                     {
                        "MMepId": "6399",
                        "text": "Mélin"
                     },
                     {
                        "MMepId": "6123",
                        "text": "Obermayr"
                     },
                     {
                        "MMepId": "6676",
                        "text": "Papadakis Konstantinos"
                     },
                     {
                        "MMepId": "5522",
                        "text": "Salvini"
                     },
                     {
                        "MMepId": "6389",
                        "text": "Schaffhauser"
                     },
                     {
                        "MMepId": "6634",
                        "text": "Stuger"
                     },
                     {
                        "MMepId": "6670",
                        "text": "Synadinos"
                     },
                     {
                        "MMepId": "6392",
                        "text": "Troszczynski"
                     },
                     {
                        "MMepId": "6610",
                        "text": "Vilimsky"
                     },
                     {
                        "MMepId": "6469",
                        "text": "Voigt"
                     },
                     {
                        "MMepId": "6673",
                        "text": "Zarianopoulos"
                     },
                     {
                        "MMepId": "6630",
                        "text": "de Graaff"
                     },
                     {
                        "MMepId": "6541",
                        "text": "Żółtek"
                     }
                  ]
               },
               {
                  "MIdentifier": "S&D",
                  "PoliticalGroupMemberName": {
                     "MMepId": "6479",
                     "text": "Hoffmann"
                  }
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": {
                     "MMepId": "5602",
                     "text": "Ždanoka"
                  }
               }
            ]
         },
         "ResultAbstention": {
            "MNumber": "34",
            "ResultPoliticalGroupList": [
               {
                  "MIdentifier": "ECR",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6087",
                        "text": "McClarkin"
                     },
                     {
                        "MMepId": "6466",
                        "text": "Trebesius"
                     },
                     {
                        "MMepId": "5052",
                        "text": "Zahradil"
                     }
                  ]
               },
               {
                  "MIdentifier": "EFDD",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6468",
                        "text": "Adinolfi"
                     },
                     {
                        "MMepId": "6374",
                        "text": "Bergeron"
                     },
                     {
                        "MMepId": "6430",
                        "text": "Borrelli"
                     },
                     {
                        "MMepId": "6379",
                        "text": "Grigule"
                     }
                  ]
               },
               {
                  "MIdentifier": "GUE/NGL",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6280",
                        "text": "Anderson Martina"
                     },
                     {
                        "MMepId": "6595",
                        "text": "Boylan"
                     },
                     {
                        "MMepId": "6598",
                        "text": "Carthy"
                     },
                     {
                        "MMepId": "6660",
                        "text": "Chrysogonos"
                     },
                     {
                        "MMepId": "6496",
                        "text": "De Masi"
                     },
                     {
                        "MMepId": "6024",
                        "text": "Ernst"
                     },
                     {
                        "MMepId": "6597",
                        "text": "Flanagan"
                     },
                     {
                        "MMepId": "6693",
                        "text": "Forenza"
                     },
                     {
                        "MMepId": "6076",
                        "text": "Hadjigeorgiou"
                     },
                     {
                        "MMepId": "6640",
                        "text": "Jiménez Villarejo"
                     },
                     {
                        "MMepId": "5039",
                        "text": "Konečná"
                     },
                     {
                        "MMepId": "6675",
                        "text": "Kuneva"
                     },
                     {
                        "MMepId": "5909",
                        "text": "Le Hyaric"
                     },
                     {
                        "MMepId": "6692",
                        "text": "Maltese"
                     },
                     {
                        "MMepId": "5992",
                        "text": "Matias"
                     },
                     {
                        "MMepId": "5044",
                        "text": "Maštálka"
                     },
                     {
                        "MMepId": "6310",
                        "text": "Michels"
                     },
                     {
                        "MMepId": "6599",
                        "text": "Ní Riada"
                     },
                     {
                        "MMepId": "5596",
                        "text": "Papadimoulis"
                     },
                     {
                        "MMepId": "5046",
                        "text": "Ransdorf"
                     },
                     {
                        "MMepId": "5837",
                        "text": "Scholz"
                     },
                     {
                        "MMepId": "6321",
                        "text": "Sylikiotis"
                     },
                     {
                        "MMepId": "6030",
                        "text": "Vergiat"
                     },
                     {
                        "MMepId": "5368",
                        "text": "Zimmer"
                     }
                  ]
               },
               {
                  "MIdentifier": "NI",
                  "PoliticalGroupMemberName": [
                     {
                        "MMepId": "6106",
                        "text": "Dodds Diane"
                     },
                     {
                        "MMepId": "6669",
                        "text": "Epitideios"
                     }
                  ]
               },
               {
                  "MIdentifier": "Verts/ALE",
                  "PoliticalGroupMemberName": {
                     "MMepId": "6606",
                     "text": "Andersson"
                  }
               }
            ]
         }
      }
   ]
}

function printajPodatke() {
   	for (i = 0; i < jsonSample.RollCallVoteResult.length; i++) {
	   console.log(jsonSample.RollCallVoteResult[i].RollCallVoteDescriptionText);
		if (jsonSample.RollCallVoteResult[i].RollCallVoteDescriptionText == naslov) {
		   console.log("ja");
			//za
			var vnos = document.getElementById("vnos");
			
			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList.length; j++)  {
				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
				   console.log(jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId);
				   console.log(jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].text);
				   vnos.innerHTML = vnos.innerHTML + jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].text + " " + jsonSample.RollCallVoteResult[i].ResultFor.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId + "<br>";
				}
			}
			
			//proti
			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList.length; j++)  {
				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
				   vnos.innerHTML = vnos.innerHTML + jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].text + " " + jsonSample.RollCallVoteResult[i].ResultAgainst.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId + "<br>";
				}
			}
			
			//neodločeno
			for (j = 0; j < jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList.length; j++)  {
				for (k = 0; k < jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList[j].PoliticalGroupMemberName.length; k++) {
				   vnos.innerHTML = vnos.innerHTML + jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].text + " " + jsonSample.RollCallVoteResult[i].ResultAbstention.ResultPoliticalGroupList[j].PoliticalGroupMemberName[k].MMepId + "<br>";
				}
			}
		}
	}

}


$(document).ready(function(){
    $("#printaj").click(printajPodatke());
});